<?php

class Iletisim extends Controller {
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$model = $this->load->model('Index_Model');

		//sabitler
		$data['nav'] = $model->nav();
		//sabitler

		$data['css'] = array('page');
		$data['js'] = array('page', 'form');

		$data['subeler'] = $model->Sube_listele();

		$this->load->view("iletisim", $data);
	}

	public function Send() {

		if ($_POST) {
			$formData = "";
			foreach ($_POST as $key =>  $item ){
				$formData.= $key. ': ' .$item.'<br>';
				
			}
		}else{
			echo "0";
		}
		$mailci = $this->load->inc('class.phpmailer'); 


		$to = array(ADMIN_MAIL);
		$konu = 'Sina Clinic';
		$mesaj = '<h5>Sitenizden soru formu doldurularak bir mesaj gönderildi.</h5><br><h4>Detaylar</h4><ul>';
		$mesaj.= $formData;
		$mesaj .= '</ul><br><hr> Bu mesaj sinaclinic.com üzerinden '.date('d-m-Y h:i').' tarihinde gönderilmiştir.';

		echo mail_send($to,$konu,$mesaj);

		
	}

}
