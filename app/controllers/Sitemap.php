<?php

class Sitemap extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    
    public function index(){
        $model = $this->load->model('Sitemap_Model');

        $datasi = $model->all_data();
        $icdatasi = $model->ic_data();

        $xml = fopen("sitemap.xml", "w");

        $code = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:n="http://www.google.com/schemas/sitemap-news/0.9">';
        foreach ($datasi as   $row) {
            $lastmod = (!empty($row['pages_update_tarih']))? $row['pages_update_tarih'] : $row['pages_insert_tarih'];

            $datetime = new DateTime($lastmod);
            $result = $datetime->format('Y-m-d\TH:i:sP');

            $code .='<url>';
            $code .='<loc>'.SITE_URL.$row['Cat_Seo_Name'].'/'.$row['pages_seo_name'].'</loc>';
            $code .='<lastmod>'.$result.'</lastmod>';
            $code .='<changefreq>weekly</changefreq>';
            $code .='<priority>0.9</priority>';
            $code .='</url>';
        }

        foreach ($icdatasi as   $row) {
            $lastmod =  $row['content_insert_tarih'];

            $datetime = new DateTime($lastmod);
            $result = $datetime->format('Y-m-d\TH:i:sP');

            $code .='<url>';
            $code .='<loc>'.SITE_URL.'Page/D/'.$row['content_id'].'</loc>';
            $code .='<lastmod>'.$result.'</lastmod>';
            $code .='<changefreq>weekly</changefreq>';
            $code .='<priority>0.9</priority>';
            $code .='</url>';
        }

        $code .='<url>';
        $code .='<loc>'.SITE_URL.'Iletisim</loc>';
        $code .='<priority>0.'.rand(5,9).'</priority>';
        $code .='</url>';
        $code .= '</urlset>';


        fwrite($xml, $code);
        fclose($xml);


        $robot = fopen('robots.txt', "w");

        $txt = 'User-agent: * 
        Disallow: /app/
        Disallow: /inc/
        Disallow: /public/
        Sitemap: '.SITE_URL.'sitemap.xml';

        fwrite($robot, $txt);
        fclose($robot);


    }


    public function ara()
    {
        $ara = $_POST['search'];

        $model =   $this->load->model('Sitemap_Model');
        $sonuc = $model->ara($ara);

        
        $html= '<button type="button" class="close" onclick="searchhide()"  aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $html .= '<div class="clearfix"></div><div class="col-md-6 list-group aramablock">';
        $html .=' <a href="#" class="list-group-item active1">'.$sonuc['sayfacount'].' Adet Sayfa Sonucu Bulundu.</a>';


        foreach ($sonuc['sayfa'] as $value) {
         $html .='<a href="'.SITE_URL.$value['Cat_Seo_Name'].'/'.$value['pages_seo_name'].'" class="list-group-item">'.$this->ararenklendir($value['pages_name'],$ara).'</a>';
     }  
     $html .='</div>';

     $html .= '<div class="col-md-6 list-group aramablock">';
     $html .=' <a href="#" class="list-group-item active2">'.$sonuc['iccount'].' Adet detaylarda Sonucu Bulundu.</a>';


     foreach ($sonuc['ic'] as $value) {
         $html .='<a href="'.SITE_URL.'Page/D/'.$value['content_id'].'" class="list-group-item">'.$this->ararenklendir($value['content_name'],$ara).'</a>';
     }
     $html .='</div>';

     echo $html;

 }





 public function ararenklendir($value,$ara)
 {
    return str_replace($ara, '<b>'.$ara.'<b>', $value);
}




}
