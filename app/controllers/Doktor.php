<?php

class Doktor extends Controller {

	public function index()
	{	

		$url = $this->load->helper('Url');
		$index_model = $this->load->model('Index_Model');
		$data['nav'] = $index_model->nav();
		$model = $this->load->model('Doktor_Model');

		$katurl = $url->get(0);

		$data['diller']  = $index_model->dillist();

		$data['doktorlar']  = $model->listele();

		$this->load->view('doktorlar', $data);
		
	}

	public function detay(){


		$url = $this->load->helper('Url');
		$model = $this->load->model('Doktor_Model');
		$index_model = $this->load->model('Index_Model');
		$doktorURL = $url->get(2);
		$data['nav'] = $index_model->nav();

		$data['doktor'] = $model->doktorGetir($doktorURL);
		$data['doktorlar']  = $model->listele();

		$this->load->view("doktor", $data);

	}

	public function sorusor()
	{
		$index_model = $this->load->model('Index_Model');
		
		$url = $this->load->helper('Url');
		$model = $this->load->model('Doktor_Model');
		$index_model = $this->load->model('Index_Model');
		$data['nav'] = $index_model->nav();

		$doktorURL = $url->get(2);
		$data['doktor'] = $model->doktorGetir($doktorURL);
		$data['doktorlar']  = $model->listele();

		$this->load->view("doktorasor", $data);
	}

}

