<?php

class Page extends Controller {

	public function __construct() {

		parent::__construct();

	}



	public function index() {

		$url = $this->load->helper('Url');

		$index_model = $this->load->model('Index_Model');

		$Page_model = $this->load->model('Page_Model');

		//sabitler

		$data['nav'] = $index_model->nav();

		//sabitler

		$katurl = $url->get(0);

		$pageurl = $url->get(1);

		$data['cat'] = $this->askdb_for_category($katurl, $Page_model);

		
		
		if($data['cat'] == false){
			
			header("HTTP/1.0 404 Not Found");

			$this->load->view("404",$data);
			
			//header('Location:' . SITE_URL . 'Error');
		}





		$data['page'] = $this->askdb_for_page($pageurl, $Page_model);

		$data['sidemenu'] = $Page_model->Sidebar_Menu($data['page']['page_tur']);

		
		if($data['page'] == false){
			
			header("HTTP/1.0 404 Not Found");


			$this->load->view("404",$data);
			
			//header('Location:' . SITE_URL . 'Error');
		}

		$data['cat_contents'] = $index_model->nav_page($data['cat']['nav_id']);

		$data['gallery'] = $Page_model->page_gallery($data['page']['page_id']);

		$data['gallery_count'] = $Page_model->page_gallery_count($data['page']['page_id']);

		$data['js'] = array('fancy','page','slider');

		$data['css'] = array('page');

		$this->load->view("page", $data);

		
	}



	public function askdb_for_page($url = '', $Page_model) {

		$sayfa_info = $Page_model->bilgipage($url);

		if ($sayfa_info) {
			return $sayfa_info;
		} else {
			return false;
		}

	}



	public function askdb_for_category($url = '', $Page_model) {

		$sayfa_info = $Page_model->bilgikat($url);

		if ($sayfa_info) {
			return $sayfa_info;
		} else {
			return false;
		}

	}


	public function askdb_for_ek($url = '', $Page_model) {

		$sayfa_info = $Page_model->bilgileri($url);

		if ($sayfa_info) {

			return $sayfa_info;

		} else {

			return false;

		}



	}



}

