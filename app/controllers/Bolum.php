<?php

class Bolum extends Controller {

	public function index()
	{
		

		$url = $this->load->helper('Url');
		$index_model = $this->load->model('Index_Model');

		$model = $this->load->model('Bolum_Model');
		$doktormodel = $this->load->model('Doktor_Model');

		$katurl = $url->get(0);
		$data['nav'] = $index_model->nav();

		$data['diller']  = $index_model->dillist();
		$data['bolumler']  = $model->listele();
		$data['doktorlar']  = $doktormodel->listele();
		$data['blog'] = $model->blogListele();

		$data['bolum']  = $model->listele();

		$this->load->view("bolum", $data);


	}


	public function detay()
	{

		$url = $this->load->helper('Url');
		$model = $this->load->model('Bolum_Model');
		$index_model = $this->load->model('Index_Model');
		$bolumUrl = $url->get(2);
		$data['nav'] = $index_model->nav();

		$data['bolum'] = $model->bolumGetir($bolumUrl);
		$data['bolumler']  = $model->listele();
		$data['bolumDoktor'] = $model->bolumegoreDoktorlar($data['bolum']['bolum_id']);

		$data['blog'] = $model->blogListele();

		$this->load->view("bolumdetay", $data);

	}

}
