<?php

class Kategori extends Controller {
	public function __construct() {
		parent::__construct();
	}

	public function index( ) {
		$url = $this->load->helper('Url');
		$index_model = $this->load->model('Index_Model');
		$kat_model = $this->load->model('Kategori_Model');

		$katurl = $url->get(0);

		$data['diller']  = $index_model->dillist();

		$data['cat'] = $this->askdb_for_category($katurl, $kat_model);
		if ($data['cat']['nav_main'] > 0) {
			$data['cat_contents'] = $index_model->nav_cat($data['cat']['nav_main']);
			$data['cat_content_type'] = 'nav';
			$data['cat_main'] = 'Anasayfa';
			$data['cat_main_name'] = $kat_model->main_name($data['cat']['nav_main']);

		} else {
			$data['cat_contents'] = $index_model->nav_page($data['cat']['nav_main']);
			$data['cat_content_type'] = 'page';
			$data['cat_main'] = $index_model->one_cat($data['cat']['nav_main']);
			$data['cat_main_name'] = $kat_model->main_name($data['cat']['nav_id']);

		}


		$data['cat_page'] = $kat_model->cat_pages($data['cat']['nav_id']);


		$data['js'] = array('fancy');
		//sabitler
		$data['nav'] = $index_model->nav();

		$data['menu_bg'] = 1;

		$Page_model = $this->load->model('Page_Model');

		$data['page'] = $index_model->nav_page_one($data['cat']['nav_id']);


		$data['content'] = $kat_model->get_content($data['cat']['nav_id']);


		
		if (count($data['cat_page']) <= 1) {


			$data['page'] = $index_model->nav_page_one($data['cat']['nav_id']);
			$data['gallery'] = $kat_model->page_gallery($data['page']['page_id']);
			$data['gallery_count'] = $Page_model->page_gallery_count($data['page']['page_id']);

			header('Location:'.SITE_URL.$data['page']['nav_url'].'/'.$data['page']['page_url']);

		}else {

			$this->load->view("categories", $data);
		}



	}

	public function hizmet() {
		$url = $this->load->helper('Url');
		$index_model = $this->load->model('Index_Model');
		$kat_model = $this->load->model('Kategori_Model');

		$data['haberler'] = $index_model->main_haber(5);
		$data['kat_icerigi']=$kat_model->main_name();
		$katurl = $url->get(0);

		$data['cat'] = $this->askdb_for_category($katurl, $kat_model);

		if ($data['cat']['nav_main'] > 0) {
			$data['cat_contents'] = $index_model->nav_cat($data['cat']['nav_main']);
			$data['cat_content_type'] = 'nav';
			$data['cat_main'] = 'Anasayfa';
			$data['cat_main_name'] = $kat_model->main_name($data['cat']['nav_main']);

		} else {
			$data['cat_contents'] = $index_model->nav_page($data['cat']['nav_main']);
			$data['cat_content_type'] = 'page';
			$data['cat_main'] = $index_model->one_cat($data['cat']['nav_main']);
			$data['cat_main_name'] = $kat_model->main_name($data['cat']['nav_id']);

		}

		$data['cat_page'] = $kat_model->cat_pages($data['cat']['nav_id']);

		$data['js'] = array('fancy');

		//sabitler
		$data['nav'] = $index_model->nav();
		//sabitler

		if (count($data['cat_page']) <= 1) {

			$Page_model = $this->load->model('Page_Model');

			$data['page'] = $index_model->nav_page_one($data['cat']['nav_id']);
			$data['gallery'] = $kat_model->page_gallery($data['page']['page_id']);
			$data['gallery_count'] = $Page_model->page_gallery_count($data['page']['page_id']);

			header('Location:'.SITE_URL.$data['page']['nav_url'].'/'.$data['page']['page_url']);

		} else {

			$this->load->view("hizmet", $data);
		}

	}

	public function askdb_for_category($url = '', $Page_model) {
		$sayfa_info = $Page_model->bilgikat($url);

		if ($sayfa_info) {
			return $sayfa_info;
		} else {
			$_SESSION['hata'] = 'Hatalı Parametre...';
			header('Location:' . SITE_URL . '/Hata');
		}

	}

}
