<?php

class Dashboard extends Controller {
	public function __construct() {
		parent::__construct();

		// Oturum Kontrolü
		Session::checkSession();
		ini_set('display_errors', 'off');
		 
	}

	public function index() {
		$this->home();
	}

	public function home() {

		global $aylar;

		/** @var Dashboard_Model $model */
		$model = $this->load->admin_model('Dashboard_Model');

		try{

		if (!isset($_SESSION['gapi'])) {
			# code...

			$gapi_set = $model->gapi();
			$gapi_mail = $gapi_set['analiticsmail'];
			$gapi_pass = $gapi_set['analiticspass'];
			$gapi_pid = $gapi_set['analiticsprofilid'];
			$this->load->inc('Gapi');

			$analytics = getService($gapi_mail, $gapi_pass);
			$profile = $gapi_pid;
			$results = getResults($analytics, $profile);

			$browser = getResultsmetric($analytics, $profile, 'ga:browser');
			$city = getResultsmetric($analytics, $profile, 'ga:city');
			$os = getResultsmetric($analytics, $profile, 'ga:operatingSystem');
			$mobilci = getResultsmetric($analytics, $profile, 'ga:mobileDeviceModel,ga:mobileDeviceBranding');
			$keywe = getResultsmetric($analytics, $profile, 'ga:fullReferrer');
			$landingpage = getResultsmetric($analytics, $profile, 'ga:landingPagePath');

			for ($i = 0; $i < count($results["rows"]); $i++) {
				$tekil[] = $results["rows"][$i][2];
				$cogul[] = $results["rows"][$i][1];
				$tarih[] = $results["rows"][$i][0];

			}
			$mobcount = 0;
			foreach ($mobilci['rows'] as $val) {
				$mobcount += $val[2];
			}

			//session a al

			$_SESSION['gapi']['landingpage'] = $landingpage['rows'];
			$_SESSION['gapi']['keywe'] = $keywe['rows'];
			$_SESSION['gapi']['mobcount'] = $mobcount;
			$_SESSION['gapi']['mobilci'] = $mobilci['rows'];

			$_SESSION['gapi']['browser'] = $browser['rows'];
			$_SESSION['gapi']['city_m'] = $city['rows'];
			$_SESSION['gapi']['os'] = $os['rows'];

			$_SESSION['gapi']['teksi'] = $tekil;
			$_SESSION['gapi']['coksu'] = $cogul;
			$_SESSION['gapi']['tarih'] = implode(',', $tarih);
			$_SESSION['gapi']['tek'] = implode(',', $tekil);
			$_SESSION['gapi']['cok'] = implode(',', $cogul);

			//session alma bitti

		} else {
			# code...
		}

	}catch (Exception $e) {
   			$_SESSION['gapi']['landingpage'] = array();
			$_SESSION['gapi']['keywe'] = array();
			$_SESSION['gapi']['mobcount'] = array();
			$_SESSION['gapi']['mobilci'] = array();

			$_SESSION['gapi']['browser'] = array();
			$_SESSION['gapi']['city_m'] = array();
			$_SESSION['gapi']['os'] = array();

			$_SESSION['gapi']['teksi'] = array();
			$_SESSION['gapi']['coksu'] = array();
			$_SESSION['gapi']['tarih'] = 0;
			$_SESSION['gapi']['tek'] = 0;
			$_SESSION['gapi']['cok'] = 0;
}

		$data['landingpage'] = $_SESSION['gapi']['landingpage'];
		$data['keywe'] = $_SESSION['gapi']['keywe'];
		$data['mobcount'] = $_SESSION['gapi']['mobcount'];
		$data['mobilci'] = $_SESSION['gapi']['mobilci'];

		$data['browser'] = $_SESSION['gapi']['browser'];
		$data['city_m'] = $_SESSION['gapi']['city_m'];
		$data['os'] = $_SESSION['gapi']['os'];

		$data['teksi'] = $_SESSION['gapi']['teksi'];
		$data['coksu'] = $_SESSION['gapi']['coksu'];
		$data['tarih'] = $_SESSION['gapi']['tarih'];
		$data['tek'] = $_SESSION['gapi']['tek'];
		$data['cok'] = $_SESSION['gapi']['cok'];
		$data['aylar'] = $aylar;
		$data['js'][] = 'gapi';
		$data['pagescount'] = $model->pagescount();
		$data['catcount'] = $model->catcount();

		$this->load->admin_view("home", $data);

	}

}
