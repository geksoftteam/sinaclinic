<?php class Page extends Controller {

	public function __construct() {

		parent::__construct();



		// Oturum Kontrolü

		Session::checkSession();

	}



	public function index($tur = 0) {
		$data['page_label '] ="";
		if (Gets(4)) {
			$data['cat_id'] =Gets(4);
		}else{
			$data['cat_id'] = '';
		}

		$data["js"] = array("table", 'fa');
		$data["jsp"] = array("page");
		$data["jsp"][] = "confirm";
		$data["css"] = array("table");
		$data["tur_index"] = $tur;
		$model = $this->load->admin_model("Page_Model");
		$data["content"] = $model->Page_listele($tur);
		$data["urunkat"] = $model->urunkat();
		$this->load->admin_view("page/page", $data);
	}



	public function Insert($tur = 0) {



		$url = $this->load->helper('Url')->get(4);

		$model = $this->load->admin_model("Page_Model");


		$data["js"][] = "editor";

		$data["kategori_id"] = $url;



		$data["jsp"] = array("page",'coklushow');

		$data["css"] = array("");

		$data["alert"] = "";


		$data["tur_index"] = $tur;



		$data["selecbox"] = $model->Nav_select();



		$this->load->admin_view("page/page_insert", $data);


	}



	public function Insert_Run($tur = 0) {

		$url = $this->load->helper('Url')->get(4);

		$model = $this->load->admin_model("Page_Model");



		$data["js"][] = "editor";

		$data["kategori_id"] = $url;



		$data["jsp"] = array("page",'coklushow');

		$data["css"] = array("");

		$data["alert"] = "";


		$data["tur_index"] = $tur;








		$form = $this->load->helper("Form");

		$alert = $this->load->helper("General");



		$form->dataPost("page_name")->isEmpty("İçerik Başlığı");



		$form->dataPost("page_kat_id")->isEmpty("İçerik Kategor/Menü");


		$form->dataPost("page_desc");
		$form->dataPost("page_video");





		$form->dataPost("page_jenerik");



		$data["selecbox"] = $model->Nav_select($form->values["page_kat_id"]);



		if ($form->postresim("resim") != false) {

			///resim yukleme işlemi



			$this->load->inc("Upload");

			$yukle = new Upload($_FILES["resim"]);

			$resimsi = $alert->seola($form->values["page_name"]) . "_" . rand(0, 99);

			$resim_adi = $yukle->yukle_th($resimsi, "public/files/page", true, $GLOBALS['pageboyutlar']);



			// resim yukleme işlemi end

		} else {

			$resim_adi = "";

		}



		$form->dataPost("page_content");



		$form->dataPost("page_tur");





		//seoname

		$page_adi = $alert->seola($_POST["page_name"]['tr']);



		$seo_name_varmi = $model->seoname($page_adi.'.html');



		if ($seo_name_varmi == 0) {



			$seo_namesi = $page_adi.'.html';



		} else {



			$seo_namesi = $page_adi . "-" . rand(0, 99).'.html';



		}









		//seo end



		$data_array = array(



			"page_name" => $form->values["page_name"],



			"page_url" => $seo_namesi,



			"page_kat_id" => $form->values["page_kat_id"],

			"page_desc" => $form->values["page_desc"],

			"page_jenerik" => $form->values["page_jenerik"],
			"page_video" => $form->values["page_video"],



			"page_image" => $resim_adi,

			"page_durum" => 1,


			"page_content" => $form->values["page_content"],

			"page_tur" => $form->values["page_tur"]);



		if ($form->submit()) {



			$last_id = $model->Page_insert($data_array);

			

			if ($last_id) {



				$this->ekresim($last_id,$data_array);



				$data["alert"] = $alert->alert("İçerik Ekleme İşlemi Başarıyla  Tamamlandı.", "success");





			} else {



				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.", "danger");



			}



		} else {



			$hata = $form->errors;

			$data["alert"] = $alert->alerts($hata, "danger");







		}



		$this->load->admin_view("page/page_insert", $data);


	}



	public function Update($id) {



		$model = $this->load->admin_model("Page_Model");

		$data["content"] = $model->Page_Guncelle_Form($id);

		$data["ekresim"] = $model->Galeri_listele_one($id);



		$data["js"][] = "editor";

		$data["jsp"] = array("page" ,"galeri", "confirm",'coklushow');

		$data["css"] = array("");

		$data["alert"] = "";

		$data["id"] = $id;

		$data["page_label"] = "Page Güncelle";

		$data["selecbox"] = $model->Nav_select($data["content"]["page_kat_id"]);

		if($data["content"]["page_tur"] == 6){

			$data["js"][] = "datepicker";

		}



		$tur = $data["content"]["page_tur"];

		$this->load->admin_view("page/page_update", $data);


	}



	public function Update_Run($id) {



		$model = $this->load->admin_model("Page_Model");

		$this->load->inc("Upload");

		$data["content"] = $model->Page_Guncelle_Form($id);

		$data["selecbox"] = $model->Nav_select($data["content"]["page_kat_id"]);



		$data["js"][] = "editor";

		$data["css"] = array("");

		$data["jsp"] = array("page");

		$data["alert"] = "";

		$data["id"] = $id;

		$data["page_label"] = "İçerik Güncelle";



		if($data["content"]["page_tur"] == 6){

			$data["js"][] = "datepicker";

		}



		$form = $this->load->helper("Form");

		$alert = $this->load->helper("General");



		$form->dataPost("page_name");



		$form->dataPost("page_kat_id");


		$form->dataPost("page_desc");


		$form->dataPost("page_jenerik");
		$form->dataPost("page_video");


		if ($form->postresim("resim") != false) {

			///resim yukleme işlemi
			

			$yukle = new Upload($_FILES["resim"]);

			$this->resimsil($_POST['oldresim']);

			$resimsi = $alert->seola($form->values["page_name"]) . "_" . rand(0, 99);

			$resim_adi = $yukle->yukle_th($resimsi, "public/files/page", true, $GLOBALS['pageboyutlar']);



			// resim yukleme işlemi end

		} else {

			$resim_adi = $_POST['oldresim'];

		}

		$form->dataPost("page_content");





		$tags = explode(' ', trim($form->values["page_name"]));

		$taglar = implode(',', $tags);



		$page_adi = $alert->seola($_POST["page_name"]['tr']);



		$seo_name_varmi = $model->seoname($page_adi.'.html', $id);



		if ($seo_name_varmi == 0) {



			$seo_namesi = $page_adi.'.html';



		} else {



			$seo_namesi = $page_adi . "-" . rand(0, 99).'.html';



		}



		$data_array = array(



			"page_name" => $form->values["page_name"],

			"page_kat_id" => $form->values["page_kat_id"],

			"page_desc" => $form->values["page_desc"],

			"page_url" => $seo_namesi,

			"page_jenerik" => $form->values["page_jenerik"],
			"page_video" => $form->values["page_video"],

			"page_image" => $resim_adi,


			"page_content" => $form->values["page_content"] );




		if ($form->submit()) {



			$last_id = $model->Page_update($data_array, $id);



			if ($last_id) {



				$this->ekresim($id,$data_array);









				$alert->redirect(SITE_URL . "Admin/Page/Update/" . $id);



			} else {



				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.", "danger");

			}



		} else {



			$hata = $form->errors;

			$data["alert"] = $alert->alerts($hata, "danger");



		}







		$tur = $data["content"]["page_tur"];


		$this->load->admin_view("page/page_update", $data);

	}



	//delete

	public function Delete() {

		$url = $this->load->helper("Form")->post("id")->isEmpty();

		$model = $this->load->admin_model("Page_Model");



		$sil = $model->Page_delete($url->values["id"]);



		if ($sil) {

			echo "1";

		} else {

			echo "0";

		}



	}



	public function list_update($id) {

		$model = $this->load->admin_model("Page_Model");



		$label = $_POST['label'];

		$val = $_POST['val'];



		$data_array = array($label => $val);



		$last_id = $model->Page_update($data_array, $id);



		if ($last_id) {

			echo 'Güncelleme İşlemi Başarılı';

		} else {

			echo 'Bir hata oldu Yeniden deneyiniz';

		}



	}



	public function kopyala($id) {

		$model = $this->load->admin_model("Page_Model");

		$kopya = $model->Page_kopyala($id);



		if ($kopya == true) {

			echo "Kopyalandı";

		} else {

			echo "Hata Oldu";

		}



	}



	public function resimsil($resim_ad = '') {

		$yol = 'public/files/page/';

		if (file_exists($yol . $resim_ad)) {

			unlink($yol . $resim_ad);



		}

		foreach ($GLOBALS['pageboyutlar'] as $resimboyut) {

			if (file_exists($yol . $resimboyut . '_' . $resim_ad)) {

				unlink($yol . $resimboyut . '_' . $resim_ad);



			}

		}

	}





	public function resim_edit()

	{

		$model = $this->load->admin_model("Page_Model");

		$helper = $this->load->helper('General');

		$this->load->inc("Upload");







		$resimler = $helper->dizin_oku('public/files/page/','*');

		foreach ($resimler as $res) {

			$yukle = new Upload('public/files/page/'.$res);

			$resimsi = explode('.', $res);

			$resim_adi = $yukle->yukle_th($resimsi[0], "public/files/pages", true, $GLOBALS['pageboyutlar']);



		}

	}



	public function resim_add_from_gallery()

	{

		ini_set('display_errors','on');

		$model = $this->load->admin_model("Page_Model");

		$pro = $model->Page_listelem();



		$this->load->inc("Upload");



		foreach ($pro as $row) {

			$resim = $row['page_image'];

			echo $row['page_name']."<br>";









			$yukle = new Upload('public/files/pages/'.$resim);

			$resimsi = explode('.', $resim);

			$resim_adi = $yukle->yukle_th($resimsi[0], "public/files/page", true, $GLOBALS['pageboyutlar']);



			copy('public/files/pages/'.$resim, 'public/files/page_tek/'.$resim);



			

			$galeri = $model->Galeri_listele($row['page_id']);





			foreach ($galeri as $gl) {



				$gl_resim = $gl['galeri_resim'];

				copy('public/files/pages/'.$gl_resim, 'public/files/page_tek/'.$gl_resim);

				$yukles = new Upload('public/files/pages/'.$gl_resim);

				$resimsis = explode('.', $gl_resim);

				$resim_adi = $yukles->yukle_th($resimsis[0], "public/files/page", true, $GLOBALS['pageboyutlar']);



			}


		}


	}


	public function listeci()

	{

		ini_set('display_errors','on');

		$this->load->inc("Upload");

		$model = $this->load->admin_model("Page_Model");

		$pro = $model->Galeri_listele();



		foreach ($pro as $row) {

			$resim = $row;

			echo $row."<br>";

			

			$yukle = new Upload('public/files/pages/'.$resim);

			$resimsi = explode('.', $resim);

			$resim_adi = $yukle->yukle_th($resimsi[0], "public/files/page", true, $GLOBALS['pageboyutlar']);



			copy('public/files/pages/'.$resim, 'public/files/page_tek/'.$resim);



		}

	}









	public function ekresim($id=0,$data_array)

	{	 



		$model_galeri = $this->load->admin_model("Galeri_Model");





		if (!empty($data_array['page_image']) && !empty($_FILES['resim2']['tmp_name'][0])) { 





			$resimler = array(); 



			foreach ($_FILES['resim2'] as $k => $l) {



				foreach ($l as $i => $v) {

					if (!array_key_exists($i, $resimler))

						$resimler[$i] = array();

					$resimler[$i][$k] = $v;

				}



			}





			$pli = 1;



			foreach ($resimler as $resim){	



				$yukle = new Upload($resim);



				$resimsi2 = seola(is_js($data_array['page_name']))."_".$id."_".$pli.'_'.rand(9,9999);



				$urun_resmi2 = $yukle->yukle_th($resimsi2,"public/files/gallery",true,$GLOBALS['galeriboyutlar']);						



				$pli++;



				$datagallery = array(



					'galeri_page_id'  => $id,



					'galeri_name' => $data_array['page_name'],



					'galeri_resim'	=> $urun_resmi2,



					'galeri_content'	=> $data_array['page_name']



				);



				$model_galeri->Galeri_insert($datagallery);

			}



		}



	}









}

