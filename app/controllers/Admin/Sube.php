<?php class Sube extends Controller{
	public function __construct() {
		parent::__construct();

        // Oturum Kontrolü
		Session::checkSession();
	}



	public function index(){
		

		$data["js"]  = array("table");
		$data["js"][]  = "editor";
		$data["jsp"]  = array("sube");
		$data["jsp"][]  = "confirm";
		$data["css"]  = array("table");				
		$data["page_label"] = "Sube Yönetimi";

		$model = $this->load->admin_model("Sube_Model");

		$data["content"]= $model->Sube_listele();	
		$this->load->admin_view("sube/sube",$data);

	}
	
	
	
	
	public function Insert(){
		

		$data["js"][]  = "editor";
		$data["jsp"]  = array("sube");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Sube Ekle";

		$this->load->admin_view("sube/sube_insert",$data);

	}
	
	
	
	public function Insert_Run(){
		

		$data["js"][]  = "editor";
		$data["jsp"]  = array("sube");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Sube Ekle";
		
		$model = $this->load->admin_model("Sube_Model");
		$form = $this->load->helper("Form");
		$alert = $this->load->helper("General");



		$form ->dataPost("sube_name")->isEmpty("Şube İsmi");
		$form ->dataPost("sube_telefon")->isEmpty("Şube Faks");
		$form ->dataPost("sube_fax")->isEmpty("Şube Resim");

		if($form->postresim("resim") != false){
            ///resim yukleme işlemi


			$this->load->inc("Upload");
			$yukle = new Upload($_FILES["resim"]);
			$resimsi = $alert->seola($form->values["sube_name"])."_".rand(0,99);
			$resim_adi = $yukle->yukle($resimsi,"public/files/content");

          // resim yukleme işlemi end
		}else{
			$resim_adi = "";
		}
		$form ->dataPost("sube_adres")->isEmpty("Şube Maps");
		$form ->dataPost("sube_maps")->isEmpty("Şube İçerik");
		$form ->dataPost("sube_content")->isEmpty("");




		$sube_name = $alert->seola($_POST["sube_name"]['tr']);



		$seo_name_varmi = $model->seoname($sube_name.'.html');



		if ($seo_name_varmi == 0) {



			$seo_namesi = $sube_name.'.html';



		} else {



			$seo_namesi = $sube_name . "-" . rand(0, 99).'.html';



		}


		$data_array =  array(

			"sube_name"      => $form->values["sube_name"],
			"sube_seo_name" => $seo_namesi,

			"sube_telefon"      => $form->values["sube_telefon"],

			"sube_fax"      => $form->values["sube_fax"],

			"sube_image"      => $resim_adi,

			"sube_adres"      => $form->values["sube_adres"],

			"sube_maps"      => $form->values["sube_maps"],

			"sube_content"      => $form->values["sube_content"]);
		

		
		if($form->submit()){	

			$last_id = $model->Sube_insert($data_array);

			if($last_id){ 

				$data["alert"] = $alert->alert("Sube Ekleme İşlemi Başarıyla  Tamamlandı.","success");	  
				$this->load->admin_view("sube/sube_insert",$data); 		  

			}else{

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
				$this->load->admin_view("sube/sube_insert",$data);
			}

		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
			$this->load->admin_view("sube/sube_insert",$data); 
			
		}

	}
	
	
	
	public function Update($id){
		
		
		$model = $this->load->admin_model("Sube_Model");
		$data["content"] = $model->Sube_Guncelle_Form($id);

		
		$data["js"][]  = "editor";
		$data["jsp"]  = array("sube");
		$data["css"]  = array("");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Sube Güncelle";

		$this->load->admin_view("sube/sube_update",$data);

	}
	
	
	
	public function Update_Run($id){

		$model = $this->load->admin_model("Sube_Model");		
		$this->load->inc("Upload");

		$data["content"] = $model->Sube_Guncelle_Form($id);
		

		
		$data["js"][]  = "editor";
		$data["css"]  = array("");
		$data["jsp"]  = array("sube");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Sube Güncelle";

		$form = $this->load->helper("Form");
		$alert = $this->load->helper("General");


		$form ->dataPost("sube_name")->isEmpty("Şube İsmi");
		$form ->dataPost("sube_telefon")->isEmpty("Şube Telefonu");
		$form ->dataPost("sube_fax");

		if ($form->postresim("resim") != false) {

			///resim yukleme işlemi
			

			$yukle = new Upload($_FILES["resim"]);

			$this->resimsil($_POST['oldresim']);

			$resimsi = $alert->seola($form->values["sube_name"]) . "_" . rand(0, 99);

			$resim_adi = $yukle->yukle_th($resimsi, "public/files/page", true, $GLOBALS['pageboyutlar']);



			// resim yukleme işlemi end

		} else {

			$resim_adi = $_POST['oldresim'];

		}




		$form ->dataPost("sube_adres");
		$form ->dataPost("sube_maps");
		$form ->dataPost("sube_content");



//seoname



		$data_array =  array(

			"sube_name"      => $form->values["sube_name"],

			"sube_telefon"      => $form->values["sube_telefon"],

			"sube_fax"      => $form->values["sube_fax"],

			"sube_image"      => $resim_adi,

			"sube_adres"      => $form->values["sube_adres"],

			"sube_maps"      => $form->values["sube_maps"],

			"sube_content"      => $form->values["sube_content"]);

		

		
		if($form->submit()){	

			$last_id = $model->Sube_update($data_array,$id);

			if($last_id){ 

				$alert->redirect(SITE_URL."Admin/Sube/Update/".$id);	  

			}else{

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
				$this->load->admin_view("sube/sube_update",$data);
			}

		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
			$this->load->admin_view("sube/sube_update",$data);
			
		}

	}
	
	
	//delete
	public function Delete(){
		$url = $this->load->helper("Form")->post("id")->isEmpty();
		$model = $this->load->admin_model("Sube_Model");

		
		$sil = $model->Sube_delete($url->values["id"]);
		
		if($sil){
			echo "1";	
		}else{
			echo "0";
		}
		
		
	}	  


	public function resimsil($resim_ad = '') {

		$yol = 'public/files/page/';

		if (file_exists($yol . $resim_ad)) {

			unlink($yol . $resim_ad);



		}

		foreach ($GLOBALS['pageboyutlar'] as $resimboyut) {

			if (file_exists($yol . $resimboyut . '_' . $resim_ad)) {

				unlink($yol . $resimboyut . '_' . $resim_ad);



			}

		}

	}



	public function list_update($id)
	{
		$model = $this->load->admin_model("Sube_Model");		

		$label = $_POST['label'];
		$val = $_POST['val'];

		$data_array = array($label =>$val  );

		$last_id = $model->Sube_update($data_array,$id);

		if ($last_id) {
			echo 'Güncelleme İşlemi Başarılı';
		} else {
			echo 'Bir hata oldu Yeniden deneyiniz';
		}

	}



	public function kopyala($id)
	{
		$model = $this->load->admin_model("Sube_Model");
		$kopya = $model->Sube_kopyala($id);

		if ($kopya == true) {
			echo "Kopyalandı";
		} else {
			echo "Hata Oldu";
		}
		
	}


}
