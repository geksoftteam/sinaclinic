<?php class Nav extends Controller {

	public function __construct() {

		parent::__construct();

		// Oturum Kontrolü

		Session::checkSession();

	}

	public function index() {

		$data["js"] = array("table");

		$data["js"][] = "editor";

		$data["jsp"] = array("nav");

		$data["jsp"][] = "confirm";

		$data["css"] = array("table");

		$data["page_label"] = "Menü/Kategori Yönetimi";

		$model = $this->load->admin_model("Nav_Model");

		$data["content"] = $model->Nav_listele();

		$this->load->admin_view("nav/nav", $data);

	}

	public function Insert() {

		$data["js"][] = "editor";

		$data["jsp"] = array("nav");

		$data["css"] = array("");

		$data["alert"] = "";

		$data["page_label"] = "Menü/Kategori Ekle";

		$model = $this->load->admin_model("Nav_Model");

		$data["selecbox"] = $model->Nav_select();

		$data['menuList'] = $model->menuListele();

		$this->load->admin_view("nav/navnew", $data);
		//$this->load->admin_view("nav/nav_insert", $data);

	}

	public function Insert_Run() {

		$data["js"][] = "editor";

		$data["jsp"] = array("nav");

		$data["css"] = array("");

		$data["alert"] = "";

		$data["page_label"] = "Menü/Kategori Ekle";

		$model = $this->load->admin_model("Nav_Model");

		$form = $this->load->helper("Form");

		$alert = $this->load->helper("General");

		$data["selecbox"] = $model->Nav_select();

		$form->dataPost("nav_name")->isEmpty("Menu/Kategori Adı");
		
		$form->dataPost("nav_title");
		
		$form->dataPost("nav_desc");
		
		$form->dataPost("nav_keyw");

		$form->dataPost("nav_main");

		$form->dataPost("nav_content");

		$page_adi = $alert->seola($_POST["nav_name"]['tr']);

		$seo_name_varmi = $model->seoname($page_adi);

		if ($seo_name_varmi == 0) {

			$seo_namesi = $page_adi;

		} else {

			$seo_namesi = $page_adi . "-" . rand(0, 99);

		}

		$data_array = array(

			"nav_name" => $form->values["nav_name"],
			
			"nav_title" => $form->values["nav_title"],
			
			"nav_desc" => $form->values["nav_desc"],
			
			"nav_keyw" => $form->values["nav_keyw"],

			"nav_main" => $form->values["nav_main"],

			"nav_url" => $seo_namesi,

			"nav_content" => $form->values["nav_content"]);

		if ($form->submit()) {
			

			$last_id = $model->Nav_insert($data_array);

			if ($last_id) {

				$data["alert"] = $alert->alert("Nav Ekleme İşlemi Başarıyla  Tamamlandı.", "success");

				$this->load->admin_view("nav/nav_insert", $data);

			} else {

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.", "danger");

				$this->load->admin_view("nav/nav_insert", $data);

			}

		} else {

			$hata = $form->errors;

			$data["alert"] = $alert->alerts($hata, "danger");

			$this->load->admin_view("nav/nav_insert", $data);

		}

	}

	public function Update($id) {

		$model = $this->load->admin_model("Nav_Model");

		$data["content"] = $model->Nav_Guncelle_Form($id);

		$data["selecbox"] = $model->Nav_select($data["content"]["nav_main"]);

		$data["js"][] = "editor";

		$data["jsp"] = array("nav");

		$data["css"] = array("");

		$data["alert"] = "";

		$data["id"] = $id;

		$data["page_label"] = "Nav Güncelle";

		$this->load->admin_view("nav/nav_update", $data);

	}

	public function Update_Run($id) {

//prep($_POST);
		$model = $this->load->admin_model("Nav_Model");

		$data["content"] = $model->Nav_Guncelle_Form($id);
		$data["selecbox"] = $model->Nav_select($data["content"]["nav_main"]);

		$data["js"][] = "editor";

		$data["css"] = array("");

		$data["jsp"] = array("nav");

		$data["alert"] = "";

		$data["id"] = $id;

		$data["page_label"] = "Nav Güncelle";

		$form = $this->load->helper("Form");

		$alert = $this->load->helper("General");

		$form->dataPost("nav_name")->isEmpty("Menu/Kategori Adı");
		
		$form->dataPost("nav_title");
		
		$form->dataPost("nav_desc");
		
		$form->dataPost("nav_keyw");

		$form->dataPost("nav_main");

		$form->dataPost("nav_content");

		$data_array = array(

			"nav_name" => $form->values["nav_name"],
			
			"nav_title" => $form->values["nav_title"],
			
			"nav_desc" => $form->values["nav_desc"],
			
			"nav_keyw" => $form->values["nav_keyw"],

			"nav_main" => $form->values["nav_main"],

			"nav_content" => $form->values["nav_content"]);

			//prep($data_array);

		if ($form->submit()) {

			$last_id = $model->Nav_update($data_array, $id);

			if ($last_id) {

				$alert->redirect(SITE_URL . "Admin/Nav/Update/" . $id);

			} else {

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.", "danger");

				$this->load->admin_view("nav/nav_update", $data);

			}

		} else {

			$hata = $form->errors;

			$data["alert"] = $alert->alerts($hata, "danger");

			$this->load->admin_view("nav/nav_update", $data);

		}

	}

	//delete

	public function Delete() {

		$url = $this->load->helper("Form")->post("id")->isEmpty();

		$model = $this->load->admin_model("Nav_Model");

		$sil = $model->Nav_delete($url->values["id"]);

		if ($sil) {

			echo "1";

		} else {

			echo "0";

		}

	}

	public function list_update($id) {

		$model = $this->load->admin_model("Nav_Model");

		$label = $_POST['label'];

		$val = $_POST['val'];

		$data_array = array($label => $val);

		$last_id = $model->Nav_update($data_array, $id);

		if ($last_id) {

			echo 'Güncelleme İşlemi Başarılı';

		} else {

			echo 'Bir hata oldu Yeniden deneyiniz';

		}

	}

	public function kopyala($id) {

		$model = $this->load->admin_model("Nav_Model");

		$kopya = $model->Nav_kopyala($id);

		if ($kopya == true) {

			echo "Kopyalandı";

		} else {

			echo "Hata Oldu";

		}

	}

}

