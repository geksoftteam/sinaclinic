<?php class Galeri extends Controller {

	public function __construct() {

		parent::__construct();



		// Oturum Kontrolü

		Session::checkSession();

	}



	public function index($page_id = 0) {



		$data["js"] = array("table");

		##editor##



		$data["jsp"] = array("ajaxform", "galeri", "confirm");

		$data["css"] = array("table");

		$data["page_label"] = "Galeri Yönetimi";



		$model = $this->load->admin_model("Galeri_Model");



		$data["content"] = $model->Galeri_listele($page_id);

		if ($page_id == 0) {

			$data["page"] = $model->pages_info();

			$data["page_info_tip"] = 'coklu';

			$data["page_idsi"] = '';



		} else {

			$data["page"] = $model->page_info($page_id);

			$data["page_info_tip"] = 'tekil';

			$data["page_idsi"] = $data["page"]['page_id'];



		}

		$this->load->admin_view("galeri/galeri", $data);



	}



	public function Insert($page_id = 0) {



		$model = $this->load->admin_model("Galeri_Model");

		if ($page_id == 0) {

			$data["page"] = $model->pages_info();

			$data["page_info_tip"] = 'coklu';

			$data["page_idsi"] = '';



		} else {

			$data["page"] = $model->page_info($page_id);

			$data["page_info_tip"] = 'tekil';

			$data["page_idsi"] = $data["page"]['page_id'];



		}



		$data["jsp"] = array("ajaxform", "galeri", "confirm");



		$data["css"] = array("");

		$data["alert"] = "";

		$data["page_label"] = "Galeri Ekle";



		$this->load->admin_view("galeri/galeri_insert", $data);



	}



	public function Insert_Run($page_id = 0) {



		##editor##

		$data["jsp"] = array("ajaxform", "galeri", "confirm");



		$data["css"] = array("");

		$data["alert"] = "";

		$data["page_label"] = "Galeri Ekle";



		$model = $this->load->admin_model("Galeri_Model");

		$form = $this->load->helper("Form");

		$alert = $this->load->helper("General");



		$form->dataPost("galeri_name");

		$form->dataPost("galeri_content");

		$form->dataPost("galeri_page_id");



		if ($form->postresim("resim") != false) {

			///resim yukleme işlemi



			$this->load->inc("Upload");

			$yukle = new Upload($_FILES["resim"]);

			$resimsi = $alert->seola($form->values["galeri_name"]) . "_" . rand(0, 99);

			$resim_adi = $yukle->yukle_th($resimsi, "public/files/gallery", true, $GLOBALS['galeriboyutlar']);



			// resim yukleme işlemi end

		} else {

			$resim_adi = "";

		}



		if ($page_id == 0) {

			$data["page"] = $model->pages_info();

			$data["page_info_tip"] = 'coklu';

			$data["page_idsi"] = $form->values["galeri_page_id"];



		} else {

			$data["page"] = $model->page_info($form->values["galeri_page_id"]);

			$data["page_info_tip"] = 'tekil';

			$data["page_idsi"] = $data["page"]['page_id'];



		}



		$data_array = array(



			"galeri_name" => $form->values["galeri_name"],



			"galeri_content" => $form->values["galeri_content"],



			"galeri_page_id" => $form->values["galeri_page_id"],



			"galeri_resim" => $resim_adi);



		if ($form->submit()) {



			$last_id = $model->Galeri_insert($data_array);



			if ($last_id) {



				$data["alert"] = $alert->alert("Galeri Ekleme İşlemi Başarıyla  Tamamlandı.", "success");

				$this->load->admin_view("galeri/galeri_insert", $data);



			} else {



				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.", "danger");

				$this->load->admin_view("galeri/galeri_insert", $data);

			}



		} else {



			$hata = $form->errors;

			$data["alert"] = $alert->alerts($hata, "danger");

			$this->load->admin_view("galeri/galeri_insert", $data);



		}



	}



	public function Update($id) {



		$model = $this->load->admin_model("Galeri_Model");

		$data["content"] = $model->Galeri_Guncelle_Form($id);



		##editor##

		$data["jsp"] = array("galeri");

		$data["css"] = array("");

		$data["alert"] = "";

		$data["id"] = $id;

		$data["page_label"] = "Galeri Güncelle";



		$this->load->admin_view("galeri/galeri_update", $data);



	}



	public function Update_Run($id) {



		$model = $this->load->admin_model("Galeri_Model");



		$data["content"] = $model->Galeri_Guncelle_Form($id);



		##editor##

		$data["css"] = array("");

		$data["jsp"] = array("galeri");

		$data["alert"] = "";

		$data["id"] = $id;

		$data["page_label"] = "Galeri Güncelle";



		$form = $this->load->helper("Form");

		$alert = $this->load->helper("General");



		$form->dataPost("galeri_name");

		$form->dataPost("galeri_content");

		$form->dataPost("galeri_page_id");



		if ($form->postresim("resim") != false) {

			///resim yukleme işlemi



			$this->load->inc("Upload");

			$yukle = new Upload($_FILES["resim"]);

			$resimsi = $alert->seola($form->values["galeri_name"]) . "_" . rand(0, 99);

			$resim_adi = $yukle->yukle($resimsi, "public/files/content");



			// resim yukleme işlemi end

		} else {

			$resim_adi = "";

		}



		$data_array = array(



			"galeri_name" => $form->values["galeri_name"],



			"galeri_content" => $form->values["galeri_content"],



			"galeri_page_id" => $form->values["galeri_page_id"],



			"galeri_resim" => $resim_adi);



		if ($form->submit()) {



			$last_id = $model->Galeri_update($data_array, $id);



			if ($last_id) {



				$alert->redirect(SITE_URL . "Admin/Galeri/Update/" . $id);



			} else {



				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.", "danger");

				$this->load->admin_view("galeri/galeri_update", $data);

			}



		} else {



			$hata = $form->errors;

			$data["alert"] = $alert->alerts($hata, "danger");

			$this->load->admin_view("galeri/galeri_update", $data);



		}



	}



	//delete

	public function Delete() {

		$url = $this->load->helper("Form")->post("id")->isEmpty();

		$model = $this->load->admin_model("Galeri_Model");



		$sil = $model->Galeri_delete($url->values["id"]);



		if ($sil) {

			echo "1";

		} else {

			echo "0";

		}



	}



	public function list_update($id) {

		$model = $this->load->admin_model("Galeri_Model");



		$label = $_POST['label'];

		$val = $_POST['val'];



		$data_array = array($label => $val);



		$last_id = $model->Galeri_update($data_array, $id);



		if ($last_id) {

			echo 'Güncelleme İşlemi Başarılı';

		} else {

			echo 'Bir hata oldu Yeniden deneyiniz';

		}



	}



	public function kopyala($id) {

		$model = $this->load->admin_model("Galeri_Model");

		$kopya = $model->Galeri_kopyala($id);



		if ($kopya == true) {

			echo "Kopyalandı";

		} else {

			echo "Hata Oldu";

		}



	}



	//galeri multi upload



	public function galeriupload() {



		if (!isset($_POST['page_id'])) {



			die('Hatalı istek');



		}



		$randid = rand(0, 999);



		$id = $_POST['page_id'];



		$adi = $_POST['page_name'];



		$helper = $this->load->helper("General");



		$resimler_adi = $helper->seola($adi);



		$model = $this->load->admin_model("Galeri_Model");



		$this->load->inc("Upload");



		$resimler = array();



		//sart each



		foreach ($_FILES['resim'] as $k => $l) {



			foreach ($l as $i => $v) {



				if (!array_key_exists($i, $resimler)) {

					$resimler[$i] = array();

				}



				$resimler[$i][$k] = $v;



			}



		}



		//end each



		//start each



		$pli = 1;



		foreach ($resimler as $resim) {



			$yukle = new Upload($resim);



			$resimsi = $resimler_adi . "_" . $id . "_" . $pli . '_' . $randid;



			$page_resmi = $yukle->yukle_th($resimsi, "public/files/gallery", true, $GLOBALS['galeriboyutlar']);



			$resim_dizi[] = $page_resmi;



			$pli++;



			$data = array(



				'galeri_page_id' => $id,



				'galeri_name' => $adi,



				'galeri_resim' => $page_resmi,



				'galeri_content' => $adi,



			);



			$model->Galeri_insert($data);



		}



		//end each



		foreach ($resim_dizi as $mapf) {



			echo '<img src="' . SITE_UPLOAD_DIR . 'gallery/' . $GLOBALS['galeriboyutlar'][0] . '_' . $mapf . '" class="img-thumbnail pull-left" alt="' . $adi . '">';



		}



	}



	public function galeriedit($id = 0) {



		$model = $this->load->admin_model("Galeri_Model");

		$form = $this->load->helper("Form");



		$resimadi = explode('.', $_POST['oldresim']);



		$this->load->inc("Upload");



		$oldresim = $_POST['oldresim'];



		if (isset($_FILES['resim']['tmp_name']) && !empty($_FILES['resim']['tmp_name'])) {



			//ResimSil($oldresim);
			$this->resimsil($oldresim);



			$yukle = new Upload($_FILES['resim']);



			$page_resmi = $yukle->yukle_th($resimadi[0], "public/files/gallery", true, $GLOBALS['galeriboyutlar']);



		} else {



			$page_resmi = $oldresim;



		}



		$form->dataPost("galeri_name");

		$form->dataPost("galeri_content");



		$data = array(



			'galeri_name' => $form->values["galeri_name"],



			'galeri_resim' => $page_resmi,



			'galeri_content' => $form->values["galeri_content"],



		);



		$gun = $model->Galeri_update($data, $id);



		if ($gun) {



			echo '<div class="alert alert-info">Güncelleme İşlemi Başarılı</div>';



		} else {



			echo '<div class="alert alert-danger">Bir hata oldu İşlem Başarısız</div>';



		}



	}



	public function GalleryDelete() {



		//print_r($_POST);



		$resimi = $_POST['resim'];



		$idsi = $_POST['id'];



		$this->resimsil($resimi);



		$model = $this->load->admin_model("Galeri_Model");



		$sil = $model->Galeri_delete($idsi);



		if ($sil) {echo "1";} else {echo "0";}

	}



	public function resimsil($resim_ad = '') {

		$yol = 'public/files/gallery/';

		if (file_exists($yol . $resim_ad)) {

			unlink($yol . $resim_ad);



		}

		foreach ($GLOBALS['galeriboyutlar'] as $resimboyut) {

			if (file_exists($yol . $resimboyut . '_' . $resim_ad)) {

				unlink($yol . $resimboyut . '_' . $resim_ad);



			}

		}

	}



	// galeri multi bitti



}

