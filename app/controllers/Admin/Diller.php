<?php class Diller extends Controller {
	public function __construct() {
		parent::__construct();

		// Oturum Kontrolü
		Session::checkSession();
	}

	public function index() {

		$data["js"] = array("table");
		$data["js"][] = "editor";
		$data["jsp"] = array("diller");
		$data["jsp"][] = "confirm";
		$data["css"] = array("table");
		$data["page_label"] = "Diller Yönetimi";

		$model = $this->load->admin_model("Diller_Model");

		$data["content"] = $model->Diller_listele();
		$this->load->admin_view("diller/diller", $data);

	}

	public function Insert() {

		$data["js"][] = "editor";
		$data["jsp"] = array("diller");
		$data["css"] = array("");
		$data["alert"] = "";
		$data["page_label"] = "Diller Ekle";

		$this->load->admin_view("diller/diller_insert", $data);

	}

	public function Insert_Run() {

		$data["js"][] = "editor";
		$data["jsp"] = array("diller");
		$data["css"] = array("");
		$data["alert"] = "";
		$data["page_label"] = "Diller Ekle";

		$model = $this->load->admin_model("Diller_Model");
		$form = $this->load->helper("Form");
		$alert = $this->load->helper("General");

		$form->post("diller_name")->isEmpty("diller_name");

		$form->post("diller_kod")->isEmpty("diller_kod");
		$form->datapost("str");

		$data_array = array(

			"diller_name" => $form->values["diller_name"],

			"diller_kod" => $form->values["diller_kod"],
			"dil_string" => $form->values["str"],
		);

		if ($form->submit()) {

			$last_id = $model->Diller_insert($data_array);

			if ($last_id) {

				$data["alert"] = $alert->alert("Diller Ekleme İşlemi Başarıyla  Tamamlandı.", "success");
				$this->load->admin_view("diller/diller_insert", $data);

			} else {

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.", "danger");
				$this->load->admin_view("diller/diller_insert", $data);
			}

		} else {

			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata, "danger");
			$this->load->admin_view("diller/diller_insert", $data);

		}

	}

	public function Update($id) {

		$model = $this->load->admin_model("Diller_Model");
		$data["content"] = $model->Diller_Guncelle_Form($id);
		$data["content_tr"] = $model->Diller_Guncelle_Form(1);

		$data["js"][] = "editor";
		$data["jsp"] = array("diller");
		$data["css"] = array("");
		$data["alert"] = "";
		$data["id"] = $id;
		$data["page_label"] = "Diller Güncelle";

		$this->load->admin_view("diller/diller_update", $data);

	}

	public function Update_Run($id) {

		$model = $this->load->admin_model("Diller_Model");

		$data["content"] = $model->Diller_Guncelle_Form($id);
		$data["content_tr"] = $model->Diller_Guncelle_Form(1);

		$data["js"][] = "editor";
		$data["css"] = array("");
		$data["jsp"] = array("diller");
		$data["alert"] = "";
		$data["id"] = $id;
		$data["page_label"] = "Diller Güncelle";

		$form = $this->load->helper("Form");
		$alert = $this->load->helper("General");

		$form->post("diller_name")->isEmpty("diller_name");
		$form->post("diller_kod")->isEmpty("diller_kod");
		$form->datapost("str");

		$data_array = array(

			"diller_name" => $form->values["diller_name"],
			"diller_kod" => $form->values["diller_kod"],
			"dil_string" => $form->values["str"],
		);

		if ($form->submit()) {

			$last_id = $model->Diller_update($data_array, $id);

			if ($last_id) {

				$alert->redirect(SITE_URL . "Admin/Diller/Update/" . $id);

			} else {

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.", "danger");
				$this->load->admin_view("diller/diller_update", $data);
			}

		} else {

			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata, "danger");
			$this->load->admin_view("diller/diller_update", $data);

		}

	}

	//delete
	public function Delete() {
		$url = $this->load->helper("Form")->post("id")->isEmpty();
		$model = $this->load->admin_model("Diller_Model");

		$sil = $model->Diller_delete($url->values["id"]);

		if ($sil) {
			echo "1";
		} else {
			echo "0";
		}

	}

	public function list_update($id) {
		$model = $this->load->admin_model("Diller_Model");

		$label = $_POST['label'];
		$val = $_POST['val'];

		$data_array = array($label => $val);

		$last_id = $model->Diller_update($data_array, $id);

		if ($last_id) {
			echo 'Güncelleme İşlemi Başarılı';
		} else {
			echo 'Bir hata oldu Yeniden deneyiniz';
		}

	}

	public function kopyala($id) {
		$model = $this->load->admin_model("Diller_Model");
		$kopya = $model->Diller_kopyala($id);

		if ($kopya == true) {
			echo "Kopyalandı";
		} else {
			echo "Hata Oldu";
		}

	}

}
