<?php class Generalsettings extends Controller {
	public function __construct() {
		parent::__construct();

		// Oturum Kontrolü
		Session::checkSession();
	}

	public function index() {

		$model = $this->load->admin_model("Generalsettings_Model");
		$data["content"] = $model->Guncelle_Form();

		##editor##
		$data["jsp"] = array("generalsettings");
		$data["css"] = array("");
		$data["alert"] = "";
		$data["page_label"] = "Genel Ayarlar";

		$data["sociallink"] = $GLOBALS['sociallink'];

		$this->load->admin_view("generalsettings/generalsettings_update", $data);

	}

	public function Update_Run() {

		$model = $this->load->admin_model("Generalsettings_Model");
		$data["content"] = $model->Guncelle_Form();

		##editor##
		$data["css"] = array("");
		$data["jsp"] = array("generalsettings");
		$data["alert"] = "";
		$data["id"] = 1;
		$data["page_label"] = "Genel Ayarlar";
		global $sociallink;
		$data["sociallink"] = $sociallink;

		$form = $this->load->helper("Form");
		$alert = $this->load->helper("General");

		$form->post("General_Site_Name")->isEmpty("Site Adı");

 		$form->post("General_desc");
		$form->post("General_title");

		$form->post("googleanalitics");
		$form->post("analiticsmail");
		if ($form->postresim("dosya") != false) {
			$name = $_FILES['dosya']['name'];
			$dosya_adi = basename($name);
			$uzanti = substr($name, -3);
			$konum = 'app/helper/';
			$dosya = $konum . $dosya_adi;
			if ($uzanti == 'p12') {
				@unlink($konum . $_POST['olddosya']);
				move_uploaded_file($_FILES['dosya']['tmp_name'], $dosya);
			} else {
				$_POST['hatadosya'] = "";
				$form->post("hatadosya")->isEmpty("Sadece p12 uzantılı dosya yüklenmelidir. Dosya");

			}

		} else {
			$dosya_adi = $_POST['olddosya'];
		}
		$form->post("analiticsprofilid");

		$form->post("adres");
		$form->post("tel");
		$form->post("gsm");
		$form->post("fax");
		$form->post("maps");
		$form->post("mail");
        $this->load->inc("Upload");

        if ($form->postresim("resim") != false) {

            ///resim yukleme işlemi


            $yukle = new Upload($_FILES["resim"]);

            $this->resimsil($_POST['oldresim']);

            $resimsi = $alert->seola("logo_". rand(0, 99));

            $resim_adi = $yukle->yukle_th($resimsi, "public/files/logo", true);



            // resim yukleme işlemi end

        } else {

            $resim_adi = $_POST['oldresim'];

        }

		$socialci = json_encode($_POST['social']);


		$data_array = array(

			"General_Site_Name" => $form->values["General_Site_Name"],

 			"General_desc" => $form->values["General_desc"],
			"General_title" => $form->values["General_title"],


			"googleanalitics" => $form->values["googleanalitics"],
			"analiticsmail" => $form->values["analiticsmail"],
			"analiticspass" => $dosya_adi,
			"analiticsprofilid" => $form->values["analiticsprofilid"],

			"adres" => $form->values["adres"],
			"tel" => $form->values["tel"],
			"gsm" => $form->values["gsm"],
			"fax" => $form->values["fax"],
			"maps" => $form->values["maps"],
			'social' => $socialci,
			'mail' => $form->values["mail"],
            'site_logo' => $resim_adi
		);




		if ($form->submit()) {

			$last_id = $model->update($data_array, $id);

			if ($last_id) {

				$alert->redirect(SITE_URL . "Admin/Generalsettings/index");

			} else {

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.", "danger");
				$this->load->admin_view("generalsettings/generalsettings_update", $data);
			}

		} else {

			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata, "danger");
			$this->load->admin_view("generalsettings/generalsettings_update", $data);

		}

	}


    public function resimsil($resim_ad = '') {

        $yol = 'public/files/page/';

        if (file_exists($yol . $resim_ad)) {

            unlink($yol . $resim_ad);

        }

        foreach ($GLOBALS['pageboyutlar'] as $resimboyut) {

            if (file_exists($yol . $resimboyut . '_' . $resim_ad)) {

                unlink($yol . $resimboyut . '_' . $resim_ad);



            }

        }

    }
}
