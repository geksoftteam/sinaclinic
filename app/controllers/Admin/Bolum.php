<?php class Bolum extends Controller{
	public function __construct() {
		parent::__construct();

        // Oturum Kontrolü
		Session::checkSession();
	}



	public function index(){
		

		$data["js"]  = array("table");
		##editor##
		$data["jsp"]  = array("bolum");
		$data["jsp"][]  = "confirm";
		$data["css"]  = array("table");				
		$data["page_label"] = "Bolum Yönetimi";

		$model = $this->load->admin_model("Bolum_Model");

		$data["content"]= $model->Bolum_listele();	
		$data["subeListele"]= $model->sube_listele();	


		$this->load->admin_view("bolum/bolum",$data);

	}
	

	
	public function Insert(){
		
		$model = $this->load->admin_model("Bolum_Model");
		$subeModel = $this->load->admin_model("Sube_Model");

		##editor##
		$data["jsp"]  = array("bolum");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Bolum Ekle";
		$data['subeler'] = $subeModel->Sube_listele();
		$this->load->admin_view("bolum/bolum_insert",$data);

	}
	
	
	
	public function Insert_Run(){
		

		##editor##
		$data["jsp"]  = array("bolum");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Bolum Ekle";
		
		$model = $this->load->admin_model("Bolum_Model");
		$form = $this->load->helper("Form");
		$alert = $this->load->helper("General");



		$form ->dataPost("bolum_name")->isEmpty("bolum_name");

		$data_array =  array(

			"bolum_name"      => $form->values["bolum_name"] );
		


		
		if($form->submit()){	

			$last_id = $model->Bolum_insert($data_array);

			if($last_id){ 

				$data["alert"] = $alert->alert("Bolum Ekleme İşlemi Başarıyla  Tamamlandı.","success");	  
				$this->load->admin_view("bolum/bolum_insert",$data); 		  

			}else{

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
				$this->load->admin_view("bolum/bolum_insert",$data);
			}

		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
			$this->load->admin_view("bolum/bolum_insert",$data); 
			
		}

	}
	
	
	
	public function Update($id){
		
		
		$model = $this->load->admin_model("Bolum_Model");
		$data["content"] = $model->Bolum_Guncelle_Form($id);

		
		##editor##
		$data["jsp"]  = array("bolum");
		$data["css"]  = array("");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Bolum Güncelle";

		$this->load->admin_view("bolum/bolum_update",$data);

	}
	
	
	
	public function Update_Run($id){

		$model = $this->load->admin_model("Bolum_Model");		

		$data["content"] = $model->Bolum_Guncelle_Form($id);
		

		
		##editor##
		$data["css"]  = array("");
		$data["jsp"]  = array("bolum");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Bolum Güncelle";

		$form = $this->load->helper("Form");
		$alert = $this->load->helper("General");


		$form ->dataPost("bolum_name")->isEmpty("bolum_name");

		$data_array =  array(

			"bolum_name"      => $form->values["bolum_name"]);

		

		
		if($form->submit()){	

			$last_id = $model->Bolum_update($data_array,$id);

			if($last_id){ 

				$alert->redirect(SITE_URL."Admin/Bolum/Update/".$id);	  

			}else{

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
				$this->load->admin_view("bolum/bolum_update",$data);
			}

		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
			$this->load->admin_view("bolum/bolum_update",$data);
			
		}

	}
	
	
	//delete
	public function Delete(){
		$url = $this->load->helper("Form")->post("id")->isEmpty();
		$model = $this->load->admin_model("Bolum_Model");

		
		$sil = $model->Bolum_delete($url->values["id"]);
		
		if($sil){
			echo "1";	
		}else{
			echo "0";
		}
		
		
	}	  




	public function list_update($id)
	{
		$model = $this->load->admin_model("Bolum_Model");		

		$label = $_POST['label'];
		$val = $_POST['val'];

		$data_array = array($label =>$val  );

		$last_id = $model->Bolum_update($data_array,$id);

		if ($last_id) {
			echo 'Güncelleme İşlemi Başarılı';
		} else {
			echo 'Bir hata oldu Yeniden deneyiniz';
		}

	}



	public function kopyala($id)
	{
		$model = $this->load->admin_model("Bolum_Model");
		$kopya = $model->Bolum_kopyala($id);

		if ($kopya == true) {
			echo "Kopyalandı";
		} else {
			echo "Hata Oldu";
		}
		
	}

	public function subeUpdate()
	{
		$model = $this->load->admin_model("Bolum_Model");		
		$subeId = isset($_POST['subeId']) ? $_POST['subeId'] : "";
		$bolumId = isset($_POST['bolumId']) ? $_POST['bolumId'] : "";

		$dataArray = array(
			'sube_id' => $subeId,
			'bolum_id' => $bolumId
		);
		

		if ($_POST['durum']=='true') {
			$insert = $model->subeBolumInsert($dataArray);
			echo "Güncelleme işlemi başarılı";
		} else{
			$delete = $model->subeBolumDelete($subeId,$bolumId);
			echo "Silme işlemi başarılı";
		}

	}
	


}
