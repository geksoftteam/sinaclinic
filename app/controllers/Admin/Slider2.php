<?php class Slider2 extends Controller{
    public function __construct() {
        parent::__construct();
        
        // Oturum Kontrolü
        Session::checkSession();
    }
    
    
		
    public function index(){
		
		   
		$data["js"]  = array("table");
		##editor##
		$data["jsp"]  = array("slider2");
		$data["jsp"][]  = "confirm";
		$data["css"]  = array("table");				
		$data["page_label"] = "Slider2 Yönetimi";
				
	  $model = $this->load->admin_model("Slider2_Model");
	
		$data["content"]= $model->Slider2_listele();	
        $this->load->admin_view("slider2/slider2",$data);
    	 
    }
	
	
	
	
	    public function Insert(){
		
		   
		##editor##
		$data["jsp"]  = array("slider2");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Slider Ekle";
	
        $this->load->admin_view("slider2/slider2_insert",$data);
    	 
    }
	
	
	
	    public function Insert_Run(){
		
		   
		##editor##
		$data["jsp"]  = array("slider2");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Slider Ekle";
		
		$model = $this->load->admin_model("Slider2_Model");
		 $form = $this->load->helper("Form");
		  $alert = $this->load->helper("General");
		  
		  
         
		 $form ->dataPost("slider2_name")->isEmpty("Yazı 1");
$form ->dataPost("slider2_t1")->isEmpty("Yazı 2");
$form ->dataPost("slider2_t2")->isEmpty("Yazı 3");
$form ->dataPost("slider2_link")->isEmpty("Link");

if($form->postresim("resim") != false){
            ///resim yukleme işlemi


                        $this->load->inc("Upload");
                        $yukle = new Upload($_FILES["resim"]);
                        $resimsi = $alert->seola($form->values["slider2_name"])."_".rand(0,99);
                        $resim_adi = $yukle->yukle_th($resimsi, "public/files/slider", true, array('192x40'));

          // resim yukleme işlemi end
                      }else{
                        $resim_adi = "";
                      }

$data_array =  array(

"slider2_name"      => $form->values["slider2_name"],

"slider2_t1"      => $form->values["slider2_t1"],

"slider2_t2"      => $form->values["slider2_t2"],

"slider2_link"      => $form->values["slider2_link"],

"slider2_resim"      => $resim_adi);
		
		 
		
		if($form->submit()){	

	  $last_id = $model->Slider2_insert($data_array);
	  
	      if($last_id){ 
		  
			$data["alert"] = $alert->alert("Slider Ekleme İşlemi Başarıyla  Tamamlandı.","success");	  
			$this->load->admin_view("slider2/slider2_insert",$data); 		  
			  
            }else{
              
	      $data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
		 $this->load->admin_view("slider2/slider2_insert",$data);
            }
	  
		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
			$this->load->admin_view("slider2/slider2_insert",$data); 
			
			}
    	 
    }
	
	
	
	    public function Update($id){
		
		
		$model = $this->load->admin_model("Slider2_Model");
		$data["content"] = $model->Slider2_Guncelle_Form($id);
		 
		
		##editor##
		$data["jsp"]  = array("slider2");
		$data["css"]  = array("");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Slider Güncelle";
	
        $this->load->admin_view("slider2/slider2_update",$data);
    	 
    }
	
	
	
		    public function Update_Run($id){
				
			$model = $this->load->admin_model("Slider2_Model");		
		 
		$data["content"] = $model->Slider2_Guncelle_Form($id);
		
		 
		
		##editor##
		$data["css"]  = array("");
		$data["jsp"]  = array("slider2");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Slider Güncelle";
	 
		 $form = $this->load->helper("Form");
		  $alert = $this->load->helper("General");
		  
		  
        $form ->dataPost("slider2_name")->isEmpty("Yazı 1");
$form ->dataPost("slider2_t1")->isEmpty("Yazı 2");
$form ->dataPost("slider2_t2")->isEmpty("Yazı 3");
$form ->dataPost("slider2_link")->isEmpty("Link");

if($form->postresim("resim") != false){
            ///resim yukleme işlemi

					$oldresim = $_POST['oldresim'];
					@unlink('public/files/slider/'.$oldresim);
                        $this->load->inc("Upload");
                        $yukle = new Upload($_FILES["resim"]);
                        $resimsi = $alert->seola($form->values["slider2_name"])."_".rand(0,99);
                        $resim_adi = $yukle->yukle_th($resimsi, "public/files/slider", true, array('192x40'));

          // resim yukleme işlemi end
                      }else{
                        $resim_adi = $_POST['oldresim'];
                      }

$data_array =  array(

"slider2_name"      => $form->values["slider2_name"],

"slider2_t1"      => $form->values["slider2_t1"],

"slider2_t2"      => $form->values["slider2_t2"],

"slider2_link"      => $form->values["slider2_link"],

"slider2_resim"      => $resim_adi);
			 
		
		 
		
		if($form->submit()){	

	  $last_id = $model->Slider2_update($data_array,$id);
	  
	      if($last_id){ 
		  
		    $alert->redirect(SITE_URL."Admin/Slider2/Update/".$id);	  
			  
            }else{
              
	      $data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
		$this->load->admin_view("slider2/slider2_update",$data);
            }
	  
		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
		$this->load->admin_view("slider2/slider2_update",$data);
			
			}
    	 
    }
	
	
	//delete
		public function Delete(){
		$url = $this->load->helper("Form")->post("id")->isEmpty();
		$model = $this->load->admin_model("Slider2_Model");
	 
		
		$sil = $model->Slider2_delete($url->values["id"]);
		
		if($sil){
		echo "1";	
			}else{
				echo "0";
				}
		
		
		}	  




  public function list_update($id)
		{
			$model = $this->load->admin_model("Slider2_Model");		
			  
			$label = $_POST['label'];
			$val = $_POST['val'];

			$data_array = array($label =>$val  );

			$last_id = $model->Slider2_update($data_array,$id);

			if ($last_id) {
				echo 'Güncelleme İşlemi Başarılı';
			} else {
				echo 'Bir hata oldu Yeniden deneyiniz';
			}
			
		}



		public function kopyala($id)
	{
		$model = $this->load->admin_model("Slider2_Model");
		$kopya = $model->Slider2_kopyala($id);

		if ($kopya == true) {
			echo "Kopyalandı";
		} else {
			echo "Hata Oldu";
		}
		
	}
  
    
}
