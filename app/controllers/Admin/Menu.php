<?php


class Menu extends Controller {

    /** @var string  */
    protected $menuName = "Main_Menu";

    /** @var string  */
    protected $menuBasePath = "";

    /** @var string  */
    protected $mainMenuPath = "";

    /** @var \App\Libs\MenuManager  */
    protected $menuManager;

    public function __construct() {
        parent::__construct();

        // Oturum Kontrolü

        Session::checkSession();
        $ds = DIRECTORY_SEPARATOR;
        $this->menuBasePath = ROOT . $ds . 'app' . $ds . 'views' .$ds;
        $this->mainMenuPath = $this->menuBasePath . $this->menuName . ".config.xml";
        $this->menuManager = new \App\Libs\MenuManager();
    }

    public function index() {

        $data["js"] = array("table");

       // $data["js"][] = "editor";
        $data["js"][] = "nestable_menu";

       // $data["jsp"] = array("nav");

        $data["jsp"][] = "confirm";

        $data["css"] = array("table");

        $data["page_label"] = "Menü/Kategori Yönetimi";



        $fs = new App\Libs\Filesystem\Filesystem();

        if(!$fs->exists($this->mainMenuPath)){
            $menu = new \App\Libs\Menu();
            $this->menuManager->saveMenu($menu,$this->mainMenuPath);
        }else{
            $menu = $this->menuManager->loadMenu($this->mainMenuPath);
        }


        $data["content"] = $menu;

        $this->load->admin_view("nav/menu", $data);

    }

    public function main_menu_edit_post(){

        try{
            $data = $_POST['main_menu_json'];

            $data = $this->menuManager->parseJson($data);

            $menu = new \App\Libs\Menu($data);


            $this->menuManager->saveMenu($menu, $this->mainMenuPath);

        }catch (\Exception $exp){

        }

        header("Location: ". SITE_URL . 'Admin/Menu');
    }


    public function get_kategoriler(){
        $Page =  (isset($_POST['Page'])) ? $_POST['Page'] : 1;
        $PageSize = (isset($_POST['PageSize'])) ? $_POST['PageSize'] : 10;
        $searchTerm = (isset($_POST['searchTerm'])) ? $_POST['searchTerm'] : null;

        /** @var Admin_Model $model */
        $model = $this->load->admin_model('Admin_Model');

        $res =  $model->getKategorilerForMenu($searchTerm, $Page - 1, $PageSize );

        foreach ($res->Data as &$dt){
            foreach ($dt as &$val){
                $val = is_js($val);
            }
        }


        header('Content-Type: application/json');
        echo json_encode($res, JSON_PRETTY_PRINT);
    }

    public function get_pages(){
        $Page =  (isset($_POST['Page'])) ? $_POST['Page'] : 1;
        $PageSize = (isset($_POST['PageSize'])) ? $_POST['PageSize'] : 10;
        $searchTerm = (isset($_POST['searchTerm'])) ? $_POST['searchTerm'] : null;

        /** @var Admin_Model $model */
        $model = $this->load->admin_model('Admin_Model');

        $res =  $model->getPagesForMenu($searchTerm, $Page - 1, $PageSize );

        foreach ($res->Data as &$dt){
            foreach ($dt as &$val){
                $val = is_js($val);
            }
        }

        header('Content-Type: application/json');
        echo json_encode($res, JSON_PRETTY_PRINT);
    }

    public function get_subeler(){
        $Page =  (isset($_POST['Page'])) ? $_POST['Page'] : 1;
        $PageSize = (isset($_POST['PageSize'])) ? $_POST['PageSize'] : 10;
        $searchTerm = (isset($_POST['searchTerm'])) ? $_POST['searchTerm'] : null;

        /** @var Admin_Model $model */
        $model = $this->load->admin_model('Admin_Model');

        $res =  $model->getSubelerForMenu($searchTerm, $Page - 1, $PageSize );
        foreach ($res->Data as &$dt){
            foreach ($dt as &$val){
                $val = is_js($val);
            }
        }
        header('Content-Type: application/json');
        echo json_encode($res, JSON_PRETTY_PRINT);
    }


    public function get_bolumler(){
        $Page =  (isset($_POST['Page'])) ? $_POST['Page'] : 1;
        $PageSize = (isset($_POST['PageSize'])) ? $_POST['PageSize'] : 10;
        $searchTerm = (isset($_POST['searchTerm'])) ? $_POST['searchTerm'] : null;

        /** @var Admin_Model $model */
        $model = $this->load->admin_model('Admin_Model');

        $res =  $model->getBolumlerForMenu($searchTerm, $Page - 1, $PageSize );
        foreach ($res->Data as &$dt){
            foreach ($dt as &$val){
                $val = is_js($val);
            }
        }
        header('Content-Type: application/json');
        echo json_encode($res, JSON_PRETTY_PRINT);
    }


    public function get_doktorlar(){
        $Page =  (isset($_POST['Page'])) ? $_POST['Page'] : 1;
        $PageSize = (isset($_POST['PageSize'])) ? $_POST['PageSize'] : 10;
        $searchTerm = (isset($_POST['searchTerm'])) ? $_POST['searchTerm'] : null;

        /** @var Admin_Model $model */
        $model = $this->load->admin_model('Admin_Model');

        $res =  $model->getDoktorForMenu($searchTerm, $Page - 1, $PageSize );
        foreach ($res->Data as &$dt){
            foreach ($dt as &$val){
                $val = is_js($val);
            }
        }
        header('Content-Type: application/json');
        echo json_encode($res, JSON_PRETTY_PRINT);
    }


}
