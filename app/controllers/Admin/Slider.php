<?php class Slider extends Controller{
    public function __construct() {
        parent::__construct();
        
        // Oturum Kontrolü
        Session::checkSession();
    }
    
    
		
    public function index(){
		
		   
		$data["js"]  = array("table");
		##editor##
		$data["jsp"]  = array("slider");
		$data["jsp"][]  = "confirm";
		$data["css"]  = array("table");				
		$data["page_label"] = "Slider Yönetimi";
				
	  $model = $this->load->admin_model("Slider_Model");
	
		$data["content"]= $model->Slider_listele();	
        $this->load->admin_view("slider/slider",$data);
    	 
    }
	
	
	
	
	    public function Insert($id=0){
		
		   
		##editor##
		$data["jsp"]  = array("slider");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Slider Ekle";

		  $model = $this->load->admin_model("Slider_Model");
		 
	
		$data["content"]= $model->Slider_listele();	
		$data["materail"]= $model->Sliderx_listele($id);
		$data['material_id'] = $id;	
		 
		 $claslars = file_get_contents(SITE_PUBLIC.'front/slider.css');
		 $claslar = explode('*+*',$claslars);
		 $classs = explode('*-*',$claslar[1]);
		 $data['clasm'] = explode(',',$classs[0]);



	
        $this->load->admin_view("slider/slider_insert",$data);
    	 
    }
	
	
	
	    public function Insert_Run(){
		
		   
		##editor##
		$data["jsp"]  = array("slider");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Slider Ekle";
		
		$model = $this->load->admin_model("Slider_Model");
		 $form = $this->load->helper("Form");
		  $alert = $this->load->helper("General");
		  
		  $data["content"]= $model->Slider_listele();	
         
		 $form ->dataPost("slider_name")->isEmpty("Ekran Adı");
 

$data_array =  array(

"slider_name"      => $form->values["slider_name"],

"slider_sira"      => 0,

"slider_durum"      => 1);
		
		 
		
		if($form->submit()){	

	  $last_id = $model->Slider_insert($data_array);
	  
	      if($last_id){ 
		  
		 header('Location:'.SITE_URL.'Admin/Slider/Insert/'.$last_id);	  
			  
            }else{
              
	      $data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
		 $this->load->admin_view("slider/slider_insert",$data);
            }
	  
		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
			$this->load->admin_view("slider/slider_insert",$data); 
			
			}
    	 
    }
	
	
	
	    public function Update($id){
		
		
		$model = $this->load->admin_model("Slider_Model");
		$data["content"] = $model->Slider_Guncelle_Form($id);
		 
		
		##editor##
		$data["jsp"]  = array("slider");
		$data["css"]  = array("");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Slider Güncelle";
	
        $this->load->admin_view("slider/slider_update",$data);
    	 
    }
	
	
	
		    public function Update_Run($id){
				
			$model = $this->load->admin_model("Slider_Model");		
		 
		$data["content"] = $model->Slider_Guncelle_Form($id);
		
		 
		
		##editor##
		$data["css"]  = array("");
		$data["jsp"]  = array("slider");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Slider Güncelle";
	 
		 $form = $this->load->helper("Form");
		  $alert = $this->load->helper("General");
		  
		  
        $form ->dataPost("slider_name")->isEmpty("Ekran Adı");
$form ->dataPost("slider_sira")->isEmpty("Sıra");
$form ->dataPost("slider_durum")->isEmpty("Durum");

$data_array =  array(

"slider_name"      => $form->values["slider_name"],

"slider_sira"      => $form->values["slider_sira"],

"slider_durum"      => $form->values["slider_durum"]);
			 
		
		 
		
		if($form->submit()){	

	  $last_id = $model->Slider_update($data_array,$id);
	  
	      if($last_id){ 
		  
		    $alert->redirect(SITE_URL."Admin/Slider/Update/".$id);	  
			  
            }else{
              
	      $data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
		$this->load->admin_view("slider/slider_update",$data);
            }
	  
		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
		$this->load->admin_view("slider/slider_update",$data);
			
			}
    	 
    }
	
	
	//delete
		public function Delete(){
		$url = $this->load->helper("Form")->post("id")->isEmpty();
		$model = $this->load->admin_model("Slider_Model");
	 
		
		$sil = $model->Slider_delete($url->values["id"]);
		
		if($sil){
		echo "1";	
			}else{
				echo "0";
				}
		
		
		}	  




  public function list_update($id)
		{
			$model = $this->load->admin_model("Slider_Model");		
			  
			$label = $_POST['label'];
			$val = $_POST['val'];

			$data_array = array($label =>$val  );

			$last_id = $model->Slider_update($data_array,$id);

			if ($last_id) {
				echo 'Güncelleme İşlemi Başarılı';
			} else {
				echo 'Bir hata oldu Yeniden deneyiniz';
			}
			
		}



		public function kopyala($id)
	{
		$model = $this->load->admin_model("Slider_Model");
		$kopya = $model->Slider_kopyala($id);

		if ($kopya == true) {
			echo "Kopyalandı";
		} else {
			echo "Hata Oldu";
		}
		
	}
  
    
}
