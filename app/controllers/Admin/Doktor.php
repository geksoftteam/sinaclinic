<?php class Doktor extends Controller{
	public function __construct() {
		parent::__construct();

        // Oturum Kontrolü
		Session::checkSession();
	}



	public function index(){
		

		$data["js"]  = array("table");
		$data["js"][]  = "editor";
		$data["jsp"]  = array("doktor");
		$data["jsp"][]  = "confirm";
		$data["css"]  = array("table");				
		$data["page_label"] = "Doktor Yönetimi";


		$model = $this->load->admin_model("Doktor_Model");
		$data["content"]= $model->Doktor_listele();	
		$this->load->admin_view("doktor/doktor",$data);

	}
	
	
	
	
	public function Insert(){
		

		$data["js"][]  = "editor";
		$data["jsp"]  = array("doktor");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Doktor Ekle";
		
		$doktormodal = $this->load->admin_model("Doktor_Model");

		$data["subeListele"]= $doktormodal->subeBolumGetir();	

		$this->load->admin_view("doktor/doktor_insert",$data);

	}
	
	
	
	public function Insert_Run(){
		

		$data["js"][]  = "editor";
		$data["jsp"]  = array("doktor");
		$data["css"]  = array("");
		$data["alert"]  = "";				
		$data["page_label"] = "Doktor Ekle";
		
		$model = $this->load->admin_model("Doktor_Model");
		$form = $this->load->helper("Form");
		$alert = $this->load->helper("General");



		$form ->dataPost("doktor_name")->isEmpty("Doktor İsmi");
		$form ->dataPost("doktor_desc")->isEmpty("Descraption");
		$form ->dataPost("doktor_content");

		if($form->postresim("resim") != false){
            ///resim yukleme işlemi


			$this->load->inc("Upload");
			$yukle = new Upload($_FILES["resim"]);
			$resimsi = $alert->seola($form->values["doktor_name"])."_".rand(0,99);
			$resim_adi = $yukle->yukle($resimsi,"public/files/page");

          // resim yukleme işlemi end
		}else{
			$resim_adi = "";
		}


		$doktorname = $alert->seola($_POST["doktor_name"]['tr']);
		$seo_name_varmi = $model->seoname($doktorname.'.html');
		if ($seo_name_varmi == 0) {
			$seo_namesi = $doktorname.'.html';
		} else {
			$seo_namesi = $doktorname . "-" . rand(0, 99).'.html';
		}





		$data_array =  array(

			"doktor_name"      => $form->values["doktor_name"],

			"doktor_seo_name" => $seo_namesi,

			"doktor_desc"      => $form->values["doktor_desc"],

			"doktor_content"      => $form->values["doktor_content"],

			"doktor_image"      => $resim_adi,

			"doktor_durum"      => 1);
		

		
		if($form->submit()){	

			$last_id = $model->Doktor_insert($data_array);

			foreach ($_POST['subebolum'] as $item) {

				$exp = explode('-', $item);
				$subeId = $exp[0];
				$bolumId = $exp[1];

				$insertData = array(
					'doktor_id' => $last_id,
					'doktor_bolum_id' =>$bolumId,
					'doktor_sube_id' => $subeId,
				);

				$subebolumInsert = $model->subebolum_insert($insertData);


			}

			if($last_id){ 

				$data["alert"] = $alert->alert("Doktor Ekleme İşlemi Başarıyla  Tamamlandı.","success");	  
				$this->load->admin_view("doktor/doktor_insert",$data); 		  

			}else{

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
				$this->load->admin_view("doktor/doktor_insert",$data);
			}

		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
			$this->load->admin_view("doktor/doktor_insert",$data); 
			
		}

	}
	
	
	
	public function Update($id){
		
		
		$model = $this->load->admin_model("Doktor_Model");
		$data["content"] = $model->Doktor_Guncelle_Form($id);

		
		$data["js"][]  = "editor";
		$data["jsp"]  = array("doktor");
		$data["css"]  = array("");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Doktor Güncelle";

		$data["subeListele"]= $model->subeBolumGetir();	
		$data["doktorBolumGetir"]= $model->doktorBolumGetir($data['content']['doktor_id']);	

		$array = array();
		foreach ($data["doktorBolumGetir"] as $value) {
			$array[] = $value['sube_id'].'-'.$value['bolum_id'];
		}

		$data["doktorBolumGetir"] = $array;

		$this->load->admin_view("doktor/doktor_update",$data);

	}
	

	
	
	public function Update_Run($id){

		$model = $this->load->admin_model("Doktor_Model");		

		$data["content"] = $model->Doktor_Guncelle_Form($id);
		

		
		$data["js"][]  = "editor";
		$data["css"]  = array("");
		$data["jsp"]  = array("doktor");
		$data["alert"]  = "";
		$data["id"]  = $id;				
		$data["page_label"] = "Doktor Güncelle";

		$form = $this->load->helper("Form");
		$alert = $this->load->helper("General");


		$form ->dataPost("doktor_name")->isEmpty("Doktor İsmi");
		$form ->dataPost("doktor_desc")->isEmpty("Descraption");
		$form ->dataPost("doktor_content");

		if($form->postresim("resim") != false){
            ///resim yukleme işlemi


			$this->load->inc("Upload");
			$yukle = new Upload($_FILES["resim"]);
			$resimsi = $alert->seola($form->values["doktor_name"])."_".rand(0,99);
			$resim_adi = $yukle->yukle($resimsi,"public/files/page");

          // resim yukleme işlemi end
		}else{
			$resim_adi = "";
		}


		$data_array =  array(

			"doktor_name"      => $form->values["doktor_name"],

			"doktor_desc"      => $form->values["doktor_desc"],

			"doktor_content"      => $form->values["doktor_content"],

			"doktor_image"      => $resim_adi );

		
		if($form->submit()){	

			$last_id = $model->Doktor_update($data_array,$id);

			if($last_id){ 

				$doktormapSil = $model->doktorMapSil($id);

				foreach ($_POST['subebolum'] as $item) {

					$exp = explode('-', $item);
					$subeId = $exp[0];
					$bolumId = $exp[1];

					$insertData = array(
						'doktor_id' => $id,
						'doktor_bolum_id' =>$bolumId,
						'doktor_sube_id' => $subeId,
					);

					$subebolumInsert = $model->subebolum_insert($insertData);


				}
				

				$alert->redirect(SITE_URL."Admin/Doktor/Update/".$id);	  

			}else{

				$data["alert"] = $alert->alert("Bir hata Oluştu. Yeniden Deneyiniz.","danger");	  
				$this->load->admin_view("doktor/doktor_update",$data);
			}

		}else{
			
			$hata = $form->errors;
			$data["alert"] = $alert->alerts($hata,"danger");	  
			$this->load->admin_view("doktor/doktor_update",$data);
			
		}

	}
	
	
	//delete
	public function Delete(){
		$url = $this->load->helper("Form")->post("id")->isEmpty();
		$model = $this->load->admin_model("Doktor_Model");

		
		$sil = $model->Doktor_delete($url->values["id"]);

		$doktormapSil = $model->doktorMapSil($url->values["id"]);


		if($sil){
			echo "1";	
		}else{
			echo "0";
		}
		
		
	}	  




	public function list_update($id)
	{
		$model = $this->load->admin_model("Doktor_Model");		

		$label = $_POST['label'];
		$val = $_POST['val'];

		$data_array = array($label =>$val  );

		$last_id = $model->Doktor_update($data_array,$id);

		if ($last_id) {
			echo 'Güncelleme İşlemi Başarılı';
		} else {
			echo 'Bir hata oldu Yeniden deneyiniz';
		}

	}



	public function kopyala($id)
	{
		$model = $this->load->admin_model("Doktor_Model");
		$kopya = $model->Doktor_kopyala($id);

		if ($kopya == true) {
			echo "Kopyalandı";
		} else {
			echo "Hata Oldu";
		}
		
	}


	public function bolumGetir()
	{
		$model = $this->load->admin_model("Doktor_Model");
		

		if ($_POST['subeId']>0) {

			$bolum = $model->subeBolumGetir($_POST['subeId']);
			

			$html = ''; 
			foreach ($bolum as $key => $value) {
				$html.= '<input type="checkbox" value="'.$value['bolum_id'].'">';
				$html.= '<label>'.is_js($value['bolum_name']).'-'.is_js($value['sube_name']).'</label>';
			}

			echo $html;

		}else{

			echo "Lütfen Şube Seçiniz";

		}


	}

}
