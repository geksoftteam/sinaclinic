<?php

class Index extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {



        $model = $this->load->model('Index_Model');
        $bolummodel = $this->load->model('Bolum_Model');
        $subemodel = $this->load->model('Sube_Model');
        $doktormodel = $this->load->model('Doktor_Model');

        $data['nav'] = $model->nav();
        // $data['slider'] = $model->slider();
        $data['css'] = array('index');
        $data['slider'] = $model->slider();
        $data['blog'] = $bolummodel->blogListele();
        $data['subeler']  = $subemodel->listele();
        $data['bolumler']  = $bolummodel->listele();
        $data['doktorlar']  = $doktormodel->listele();

        $this->load->view("index", $data);

    }

    public function set_language()
    {

        if (!isset($_SESSION)) {
            session_start();
        }
        $kod = $_POST['kod'];

        $_SESSION['dil'] = $kod;

        echo '1';
    }

}
