<?php

class Sube extends Controller {
	
	public function index()
	{
		$url = $this->load->helper('Url');
		$index_model = $this->load->model('Index_Model');


		$model = $this->load->model('Sube_Model');

		$katurl = $url->get(0);
		$data['nav'] = $index_model->nav();

		$data['diller']  = $index_model->dillist();
		$data['subeler']  = $model->listele();

		$this->load->view("sube", $data);


	}

	public function detay()
	{

		$url = $this->load->helper('Url');
		$model = $this->load->model('Sube_Model');
		$index_model = $this->load->model('Index_Model');
		$subeUrl = $url->get(2);
		$data['nav'] = $index_model->nav();


		$data['sube'] = $model->subeGetir($subeUrl);
		$data['subeler']  = $model->listele();

		$this->load->view("subedetay", $data);

	}

}
