		// JavaScript Document

		$(document).ready(function() {
			$site_url = $("#site_url").val();
			$("#historym").load($site_url+'Admin/Manset/logbilgi');

		}); 

		$('.tools-div').hover(function() {
			img = $(this).html();
			$('.tool-onizle').html(img+'<h4> eklemek için çift tıklayınız</h4>');
			$('.tool-onizle').children('img').css({'width':'400px','background':'#ccc'});
			$('.tool-onizle').show();
			$('.actbtnler,.dragli').hide();
			$('.imgbtn,.uygulabtn,#img_onizle').show();
			
		}, function() {
			$('.tool-onizle').html("");
			$('.tool-onizle').hide();
		});

		$('.tools-div').dblclick(function() {
			img = $(this).html();
			$('.imgbtn').fadeIn();
			$('.uygulabtn').fadeIn();
			$('.textbtn').fadeOut();

			$('#img_onizle').html(img);
			imghitap = $('#img_onizle').children('img');
			imghitap.removeAttr('width');
			imghitap.removeAttr('class');
			imgadi = imghitap.data('id');
			$('#toolsresmi').val(imgadi);
			$('#img_onizle').css({
				'left': '0px',
				'top': '0px'
			});
			$('#dragtop,#dragleft').val(0);

			//$('#pancere').append(imgadi +' Eklendi<br>');
		});



		//resim ekleye tıklayınca
		$('#addimagebtn').click(function() {
			
			$('.actbtnler,.dragli').hide();
			$('.imgbtn,.uygulabtn,#img_onizle').show();


		});

		//şekil ekleye tıklayınca

		$('#addshapebtn').click(function() {
			
			$('.actbtnler,.dragli').hide();
			$('.shapebtn,.uygulabtnshape,#shape_onizle').show();

		});

			$('.shapebtn').click(function() {
			 
			$('#shape_onizle').css('background','none').hide();

		});

		//yazı ekleye tıklayınca

		$('#addtextbtn').click(function() {	
			$('.actbtnler,.dragli').hide();
			$('.textbtn,.uygulabtntext,#text_onizle').show();	 
			
		});

		// tab btnler bitti

		$(function() {
			$( ".dragli" ).draggable({
				drag: function() {
					var xPos = $(this).css('left').split('p');
					var yPos = $(this).css('top').split('p');


					$('#dragtop').val(yPos[0]);
					$('#dragleft').val(xPos[0]);
				},


				stop: function() {

				}

			});
		});

		$('.kbtn').click(function(event) {
			actdizi = $(this).data('id').split('+');;
			act = actdizi[0];
			kim = actdizi[1];

			if (act == 'islemcenter') {

				islemcenter(kim,'yatay');
			} 
			else if (act == 'dikislemcenter') {

				islemcenter(kim,'dikey');
			}
			else if (act == 'islemleft') {

				sagsol(kim,'sol');
			}

			else if (act == 'islemright') {

				sagsol(kim,'sag');
			}

			else if (act == 'dikislemtop') {

				ustalt(kim,'ust');
			}

			else if (act == 'dikislembottom') {

				ustalt(kim,'alt');
			}


		});



		function islemcenter(id,tur){

			re_gen =   $('#'+id).width();
			re_yuk = $('#'+id).height();

			panel_gen = 690;
			panel_yuk = 334;
			left_deger = panel_gen-re_gen;

			

			left_degeer = Math.round(left_deger/2);

			top_deger = panel_yuk-re_yuk;
			top_degeer = Math.round(top_deger/2);
			if(tur == 'yatay'){
				$('#'+id).css("left",left_degeer+"px");


			}else if(tur == 'dikey'){
				$('#'+id).css("top",top_degeer+"px");
			}


			var xPos = $('#'+id).css('left').split('p');
			var yPos = $('#'+id).css('top').split('p');


			$('#dragtop').val(yPos[0]);
			$('#dragleft').val(xPos[0]);
		}


		function sagsol(id,tur){

			re_gen =   $('#'+id).width();	 
			panel_gen = 690;	  
			left_deger = panel_gen-re_gen;



			if(tur == 'sol'){
				$('#'+id).css("left","0px");



			}else if(tur == 'sag'){

				$('#'+id).css("left",left_deger+"px");
			}

			var xPos = $('#'+id).css('left').split('p');
			var yPos = $('#'+id).css('top').split('p');


			$('#dragtop').val(yPos[0]);
			$('#dragleft').val(xPos[0]);
		}


		function ustalt(id,tur){


			re_yuk = $('#'+id).height(); 	 
			panel_yuk = 334;	  
			top_deger = panel_yuk-re_yuk;	  

			if(tur == 'ust'){
				$('#'+id).css("top","0px");


			}else if(tur == 'alt'){
				$('#'+id).css("top",top_deger+"px");

			}

			var xPos = $('#'+id).css('left').split('p');
			var yPos = $('#'+id).css('top').split('p');


			$('#dragtop').val(yPos[0]);
			$('#dragleft').val(xPos[0]);
		}


		function uygula(){

			ypoz = $('#dragtop').val();
			xpoz = $('#dragleft').val();
			mresim = $('#mansetresmi').val();
			tools = $('#toolsresmi').val();
			updir = $('#updir').val();


			$site_url = $("#site_url").val();

			$.post( $site_url+'Admin/Manset/resimkoy', { resim: mresim,x:xpoz,y:ypoz,tool:tools })
			.done(function(qelen) {
				$('#mansetresmi').val("public/files/temp/"+qelen);
				$('#banner').attr('src',updir+'temp/'+qelen+"?v="+Math.floor((Math.random() * 100) + 1));
				$("#historym").load($site_url+'Admin/Manset/logbilgi');
				sole('Resim Eklendi');
				$('#img_onizle').html("");


				//$('#pancere').append(qelen +' Üretildi<br>');
				//$('#pancere').append("public/files/temp/"+qelen+' yazıldı<br>');
			}); 
		}


		function tempgoster (q) {
			$site_url = $("#site_url").val();
			modalyapbig('İşlem Final Resmi','<img src="'+$site_url+'public/files/temp/'+q+'">','block');
		}

		function tumsil() {
			p  = '<p>Temp Temizlenecek Geçmiş Logları Silinecek Emin misiniz ?</p><hr>';
			p  = p+'<button type="button" class="btn btn-warning pull-right" onclick="tumsilonay();">Tümünü Sil</button>';
			p  = p+'<button type="button" class="btn btn-info pull-left" onclick="tumsiliptal();">Silme</button>';
			p  = p+'<div class="clearfix"></div><hr>';
			modalyap('Emin misiniz ?',p,'block');

		}

		function tumsiliptal() {

			$('#myModal').modal('hide');
		}

		function tumsilonay() {
			tumsiliptal();
			$site_url = $("#site_url").val();
			$ilkhai = $('#ilkhali').val();
			updir = $('#updir').val();
			$.post( $site_url+'Admin/Manset/temptemizle', { ilk:$ilkhai })
			.done(function() {

				$('#mansetresmi').val("public/files/news/"+$ilkhai);
				$('#banner').attr('src',updir+'news/'+$ilkhai+"?v="+Math.floor((Math.random() * 100) + 1));
				$("#historym").html('<h3>Geçmiş Silindi</h3>');
				sole('Tümü Silinip En başa Alındı');
			}); 
		}

		function geridon (q) {
			updir = $('#updir').val();
			$('#mansetresmi').val("public/files/temp/"+q);
			$('#banner').attr('src',updir+'temp/'+q+"?v="+Math.floor((Math.random() * 100) + 1));
			$("#historym").load($site_url+'Admin/Manset/logbilgi');
			sole('Geri Dönüldü');
			$('#img_onizle').html("");

			//$('#pancere').append(updir+'temp/'+q +' resim olarak gosterildi<br>');
			//$('#pancere').append("public/files/temp/"+q+' geri gelindi<br>');
		}

		$('#kare').click(function(event) {
			$('#shape_onizle').css({
				width : '100px',
				height: '100px',
				background:'#fff'
			}).show();

			$('.shapebtn').fadeIn();
			$('.uygulabtnshape').fadeIn();


		});


		$( "#shape_onizle" ).resizable({
			containment: "#mansetpencere",
			resize: function() {
				$('#geni').val($(this).width());
				$('#yuku').val($(this).height());
			},


			stop: function() {

			}
		});

		$('.renkuygula').click(function() {

			var renkci = $('#rengi').val();
			$( "#shape_onizle" ).css('background', renkci);

		});




		//shape uygulama işlemleri start
		function uygulashape (){
			
			ypoz = $('#dragtop').val();
			xpoz = $('#dragleft').val();
			renk = $('#rengi').val();
			gen = $('#geni').val();
			yuk = $('#yuku').val();
			mresim = $('#mansetresmi').val();
			updir = $('#updir').val();


			$site_url = $("#site_url").val();

			$.post( $site_url+'Admin/Manset/shapekoy', { resim:mresim,prenk: renk,px:xpoz,py:ypoz,pgen:gen,pyuk:yuk })
			.done(function(qelen) {
				$('#mansetresmi').val("public/files/temp/"+qelen);
				$('#banner').attr('src',updir+'temp/'+qelen+"?v="+Math.floor((Math.random() * 100) + 1));
				$("#historym").load($site_url+'Admin/Manset/logbilgi');
				sole('Şekil Uygulandı');
				$('#shape_onizle').css("background",'none');

				//	$('#pancere').append(updir+'temp/'+qelen+' resim olarak gosterildi<br>');
				//  $('#pancere').append("public/files/temp/"+qelen+' Yazıldı<br>');
			}); 
		}

		//shape uygulama işlemleri end

		//text basladı
		$('.fontsec').click(function() {
			var fontname = $(this).data('id');
			var fontad = $(this).data('bilgi');
			$('#text_onizle').css({
				'font-family': fontname
			});
			$('#fontfamili').val(fontad);
			var fontyazacak = fontad.substring(0,20);
			$('#fontsecbtnsi').html(fontyazacak+'<span class="caret"></span>');
		});

		$('.renkuygula').click(function() {
			var rengisi = $('#yazirengci').val();
			$('#text_onizle').css({		 
				'color':rengisi
			});
		});


		$('.boyutuygula').click(function() {
			var boyutcu = $('#fontsizeci').val();
			$('#text_onizle').css({		 
				'font-size': boyutcu+'px',
				'line-height':boyutcu+'px'
			});
		});

		$('#yazi_ekle').keyup(function() {
			$('#text_onizle').html($(this).val());

		});


		function uygulatext (){

			ypoz = $('#dragtop').val();
			xpoz = $('#dragleft').val();
			renk = $('#yazirengci').val();
			yazi = $('#yazi_ekle').val();
			mresim = $('#mansetresmi').val();
			updir = $('#updir').val();
			fontsu = $('#fontfamili').val();
			fondsizesi = $('#fontsizeci').val();


			$site_url = $("#site_url").val();

			$.post( $site_url+'Admin/Manset/yaziekle', { resim:mresim ,prenk:renk,px:xpoz,py:ypoz,pyazi:yazi,fond:fontsu,fondsize:fondsizesi})
			.done(function(qelen) {
				$('#mansetresmi').val("public/files/temp/"+$.trim(qelen));
				$('#banner').attr('src',$.trim(updir)+'temp/'+$.trim(qelen)+"?v="+Math.floor((Math.random() * 100) + 1));
				$("#historym").load($site_url+'Admin/Manset/logbilgi');
				sole('Yazı Eklendi');
				$('#text_onizle').html('');

				//$('#pancere').append(updir+'temp/'+qelen+' resim olarak gosterildi<br>');
				//$('#pancere').append("public/files/temp/"+qelen+' Yazıldı<br>');
			}); 
		}

		$('.mansetsec').click(function() {
			$('.minimansetler').removeClass('curimg');
			$(this).children('.minimansetler').addClass('curimg');
			
			updir = $('#updir').val();
			var resimarray = $(this).data('id').split('+');
			resim   = resimarray[0];
			haberid = resimarray[1];
			$('#haberid').val(haberid);
			$('#oldresim').val(resim);
			$('#mansetresmi').val('public/files/news/690x334_'+resim);
			$('#ilkhali').val('690x334_'+resim);
			$('#banner').attr('src',updir+'news/690x334_'+resim+"?v="+Math.floor((Math.random() * 100) + 1));

		});

		$('.reuploadsec').click(function() {	
			$('#resiminp').click()

		});

		function reupload () {
			updir = $('#updir').val();
			$('#reuploadmanset').ajaxForm({
				//target:"#sonucfm"
				success:    function(data) { 

					$('#mansetresmi').val('public/files/news/690x334_'+data);
					$('#ilkhali').val(data);
					$('#banner').attr('src',updir+'news/690x334_'+data+"?v="+Math.floor((Math.random() * 100) + 1)); 
					$('.curimg').attr('src',updir+'news/36x36_'+data+"?v="+Math.floor((Math.random() * 100) + 1));
				} 
			}).submit();
		}


	$('.sonkaydet').click(function() {
		
		 	sonresim = $('#mansetresmi').val();
			$site_url = $("#site_url").val();
			haberid = $('#haberid').val();
			resim= $('#oldresim').val();

			$.post( $site_url+'Admin/Manset/sonkaydet', { resim: sonresim,id:haberid,oldresim:resim})
			.done(function(data) {
				 
				sole('İşlem Tamamlandı');
				 
			}); 

	});

