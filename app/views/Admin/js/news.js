// JavaScript Document


/*----------------Delete --------------------*/	




$(".delete_link").click(function() {
	
	data_id  = $(this).data('id');	
	$site_url = $("#site_url").val();

	$.confirm({
		title:"İçerik Silme Onayı",
		text:"<b>Bu İçerik silinirse geri alınamaz devam etmek istiyor musunuz?</b>",
		confirm: function() {
			deleteconfirm(data_id,$site_url)
		},
		cancel: function() {
			sole("İptal Edildi   ");
		}
	});
});





function deleteconfirm(data_id,$site_url){

	$.post( $site_url+"Admin/News/Delete", { id: data_id })
	.done(function(data) {

		if(data==1){
			$("#tr_"+data_id).fadeOut();
			sole("Silme işlemi Başarılı"); 
		}else{
			sole("Bir Hata Oluştu Tekrar Deneyiniz");  
		}


	});

}


function ara(key,value){



	$site_url = $("#site_url").val();

	$.post( $site_url+'Admin/News/Ara', { label: key,val:value })
	.done(function(data) {
		location.href=data;
     //alert(data);
 });   





}



$('.degis').change(function() {
	key = $(this).attr('name');
	val = $(this).val();

	if (key == 'news_konum') {

		if (val == "-1") {key = 'news_konum_ana';val = "-1";}else{key = $(this).val();
			val = 1;
		}

	};


	ara(key,val);		
});


$(".ent").keyup(function(e){


	if (e.keyCode === 13) {
		key = $(this).attr('name');
		val = $(this).val();
		ara(key,val);		

	}

});

$(".quick").click(function() {
	
	data_id  = $(this).data('id').split('+');	
	idsi = data_id[0];
	baslik = data_id[1];
	$site_url = $("#site_url").val();

	$.post( $site_url+'Admin/News/q_update_form', { id: idsi })
	.done(function(data) {
		modalyapbig(baslik,data,'block');
	});   

}); 


function guncelle(){

	$('#hizliguncelleform').ajaxForm({
		target:"#sonucfm"
	}).submit();

}

$('.durumsec').click(function() {

	$site_url = $("#site_url").val();
	data_id = $(this).data('id').split('+');
	idsi = data_id[0];
	label = data_id[1];
	val	= $(this).val();

	$.post( $site_url+'Admin/News/list_update/'+idsi, { label: label,val:val })
	.done(function(data) {
		sole(data);
	});   


});


$('.konumsec').click(function() {

	$site_url = $("#site_url").val();
	data_id = $(this).data('id').split('+');
	idsi = data_id[0];
	label = data_id[1];
	if ($( this ).prop( "checked" )) {

		val =1;

	}else{	val =0;}



	$.post( $site_url+'Admin/News/list_update/'+idsi, { label: label,val:val })
	.done(function(data) {
		sole(data);
	});   


});

$('.catchange').change(function() {
	$site_url = $("#site_url").val();
	gecerli_id = $(this).val();
	$.post( $site_url+'Admin/News/ekkategori', { id: gecerli_id})
	.done(function(data) {
		$('#ekkategoriler').html(data);
	}); 

});


$('.catchangeup').change(function() {
	$site_url = $("#site_url").val();
	gecerli_id = $(this).val();
	newsid = $('#newsid').val();
	$.post( $site_url+'Admin/News/ekkategoriup', { id: gecerli_id,curid:newsid})
	.done(function(data) {
		$('#ekkategoriler').html(data);
	}); 

});



$(".galeri").click(function() {
	$site_url = $("#site_url").val();
	data_id  = $(this).data('id').split('+');	
	idsi = data_id[0];
	baslik = data_id[1];
	  $('#haberidsi').val(idsi);
	  $('#haberadi').val(baslik);
	  $('#onbilgiupload').css('text-align', 'center');
	  $('#onbilgiupload').html('<b>'+baslik+ '</b> Haberi için Galeri oluşturuyorsunuz. <a href="'+$site_url+'Admin/News/Gallery/'+idsi+'" class="btn btn-info btn-small"><i class="fa fa-share"></i> Haberin Galerisine Git</a>'); 
 
	 $('#resimmodal').modal({
  backdrop: 'static'
});
$('#modalkapatbig').css('display','block');
$('.mymodalinh4ubigresim').html(baslik); 
	 

}); 
 
$(document).ready(function() {
	

	var bar = $('#bar');
	var percent = $('.percent');
	var status = $('#sonucfmupload');

	$('#galeriform').ajaxForm({
		beforeSend: function() {
			status.empty();
			var percentVal = '0%';
			bar.width(percentVal)
			percent.html(percentVal);
		},
		uploadProgress: function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			bar.width(percentVal)
			percent.html(percentVal);
					//console.log(percentVal, position, total);
				},
				success: function() {
					var percentVal = '100%';
					bar.width(percentVal)
					percent.html(percentVal);
				},
				complete: function(xhr) {
					status.html(xhr.responseText);
				}
			}); 



	//ek kategorler durumu

	 katdurumsu = $('#katfiltre').val();
	if (katdurumsu > 0) {
	$site_url = $("#site_url").val();
	gecerli_id = $('#katfiltre').val();
	newsid = $('#newsid').val();
	$.post( $site_url+'Admin/News/ekkategoriup', { id: gecerli_id,curid:newsid})
	.done(function(data) {
		$('#ekkategoriler').html(data);
	}); 
}
	//ek kategoriler durumu end

 

});
 

 $('.pointer').click(function(event) {
 	var divid = $(this).data('id').split('+');
 	$('.panel').slideUp('slow');
 	$('#pn'+divid[0]).slideDown('slow');
 });

 function formkaydet (formid) {
 	$('#galeriedit'+formid).ajaxForm({
		target:"#sonucu"+formid
	}).submit();
 }


$(".delete_gallery").click(function() {
	
	data_id  = $(this).data('id');	
	$site_url = $("#site_url").val();

	$.confirm({
		title:"İçerik Silme Onayı",
		text:"<b>Bu İçerik silinirse geri alınamaz devam etmek istiyor musunuz?</b>",
		confirm: function() {
			deleteconfirmgallery(data_id,$site_url)
		},
		cancel: function() {
			sole("İptal Edildi   ");
		}
	});
});


function deleteconfirmgallery(data_id,$site_url){

	datam = data_id.split('+');

	$.post( $site_url+"Admin/News/GalleryDelete", { id: datam[0] ,resim:datam[1]})
	.done(function(data) {

		if(data==1){
			$("#resimci"+datam[0]).hide();
			$("#pn"+datam[0]).hide();
			sole("Silme işlemi Başarılı"); 
		}else{
			sole("Bir Hata Oluştu Tekrar Deneyiniz");  
		}


	});

}









