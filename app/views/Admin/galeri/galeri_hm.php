<div class="row">
  <div class="col-md-12">
    <div class="grid simple " style="padding-top:10px;">
      <!-- header -->
      <div class="grid-title no-border">
        <h3 class="content-header pull-left"><?php echo $page_label; ?> </h3>
        <div class="col-sm-6 pull-right ">

          <a href="<?php echo ADMIN_URL . 'Galeri/Insert/' . $page_idsi ?>" class="btn btn-warning pull-right col-sm-3">Tekil İşlem</a>

          <button type="button" class="btn btn-success pull-right col-sm-3" onclick=" $('#resimmodal2').modal({backdrop: 'static'});"><i class="fa fa-plus"></i> Çoklu İşlem</button>
          <div class="col-sm-6">

          <?php if ($page_info_tip == 'tekil') {?>

            <select name="page_nameci" class="form-controlpull-right" id="page_nameci">

            <option value="<?php echo $page['page_id'] ?>"><?php echo is_js($page['page_name']) ?></option>
            </select>
          <?php } else {?>
          <select name="page_nameci" class="form-controlpull-right" id="page_nameci"
onchange="modalset($('#page_nameci').val(),$('#page_nameci option:selected').text());"
          >
          <option value="0">İçerik Seçiniz</option>
          <?php foreach ($page as $pg): ?>


            <option value="<?php echo $pg['page_id'] ?>"><?php echo is_js($pg['page_name']) ?></option>

          <?php endforeach?>

            </select>
          <?php }?>

          </div>
        </div>
      </div>

      <!-- header end -->
      <div class="grid-body no-border">
        <div class="col-md-12">
          <div class="row">
            <?php foreach ($content as $key => $resim) {
	$fi = $resim['galeri_id'];
	?>

             <div class="col-md-2 pointer " data-id="<?php echo $fi . '+' . $resim['galeri_resim'] ?>" id="resimci<?php echo $fi ?>" style="margin-bottom:5px; cursor: pointer;"><img src="<?php echo SITE_UPLOAD_DIR ?>gallery/<?php echo $GLOBALS['galeriboyutlar'][0] . '_' . $resim['galeri_resim'] ?>" alt="<?php echo $resim['galeri_name'] ?>" class="img-thumbnail"></div>
             <div class="col-md-12 panel dnone" id="pn<?php echo $fi ?>" style="position:releative; clear:both; background:#f4f4f4; margin:10px auto; padding:15px;">
              <form action="<?php echo SITE_URL ?>Admin/Galeri/galeriedit/<?php echo $resim['galeri_id'] ?>" method="post" class="form-horizontal row-border col-md-7 pull-left" id="galeriedit<?php echo $fi ?>" enctype="multipart/form-data">
                <div id="sonucu<?php echo $fi ?>"></div>
                <div class="form-group"><label   class="control-label col-sm-3"></label>
                  <div class="col-sm-9 dnone">
                    <input type="file" name="resim" id="re<?php echo $fi ?>" onchange="resim_on_izle(this,'<?php echo $fi ?>');">
                    <input type="hidden" name="oldresim" value="<?php echo $resim['galeri_resim'] ?>"  >
                  </div>
                  <div class="col-sm-9">
                    <button type="button" class="btn btn-success" onclick="$('#re<?php echo $fi ?>').click();">Yeni Resim Seç</button>
                    <button type="button" class="btn btn-danger delete_gallery" data-id="<?php echo $fi . '+' . $resim['galeri_resim'] ?>" >Bu resmi Sil</button>
                  </div>
                </div>

             <?php foreach ($maindiller as $dilc) {
		$rtl = "";
		$dilkod = $dilc["diller_kod"];if ($dilkod == "ar") {$rtl = "rtlc ";}?>

<div class="form-group">

     <label class="col-sm-3 control-label">Başlık  :</label>

     <div class="col-sm-9">

     <input type="text" name="galeri_name[<?php echo $dilkod; ?>]"
value="<?php echo is_js($resim['galeri_name'], $dilkod); ?>"
 class="form-control <?php echo $rtl; ?>">

     </div>

     </div>

<?php }?>


               <?php foreach ($maindiller as $dilc) {
		$rtl = "";
		$dilkod = $dilc["diller_kod"];if ($dilkod == "ar") {$rtl = "rtlc ";}?>

<div class="form-group">

                        <label class="col-sm-3 control-label">Kısa Metin

                        <small class="tip_top" title="Anahtar kelime meta etiketi 260 karakterden az olmalıdır. 260 karakterden uzun olması arama motorları tarafından spam olarak yorumlanmaktadır.">(Keywords max 260 Karakter)</small>:</label>

                        <div class="col-sm-9">

                        <textarea  data-say="260" data-help="galeri_content_say<?php echo $dilkod; ?>" rows="4" class="form-control say <?php echo $rtl; ?> " name="galeri_content[<?php echo $dilkod; ?>]">
<?php echo is_js($resim['galeri_content'], $dilkod); ?>
</textarea>

                        <p class="help-block" id="galeri_content_say<?php echo $dilkod; ?>"></p>
                        </div>
                        </div>

<?php }?>
                <div class="form-group">
                 <button type="button" class="btn btn-info pull-right" onclick="formkaydet('<?php echo $fi ?>');">Kaydet</button>
               </div>
             </form>
             <div class="col-md-4 pull-right" style="position:releative">
              <button type="button" onclick="$('#pn<?php echo $fi ?>').slideUp('slow');" class="btn btn-link btn-mini" style="position:absolute; right:-10px; top:-10px;"><i class="fa fa-times"></i></button>
              <img src="<?php echo SITE_UPLOAD_DIR ?>gallery/<?php echo $GLOBALS['galeriboyutlar'][0] . '_' . $resim['galeri_resim'] ?>" id="onizle<?php echo $fi ?>" alt="<?php echo $resim['galeri_name'] ?>" class="img-thumbnail"></div>
            </div>
            <?php }?>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>


    </div>
  </div>
</div>

<?php if ($page_info_tip == 'tekil') {
	$modalpage = $page;

} else {
	$modalpage = $page[0];

}?>

<!-- modal -->

<div id="resimmodal2" class="modal fade bs-example-modal-lg in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" id="modalkapatbig" onclick="location.reload();"  >×</button>
        <h4 class="modal-title mymodalinh4ubig" id="mySmallModalLabel"><?php echo is_js($modalpage['page_name']) ?></h4>
      </div>
      <div class="clearfix"></div>
      <div class="modal-body mymodalinbodisibig" >
        <div class="col-md-12">
          <div id="onbilgiupload" class="col-md-12"><b><?php echo is_js($modalpage['page_name']) ?></b> İçeriği için galeri oluşturuyorsunuz.</div>

          <form action="<?php echo SITE_URL ?>Admin/Galeri/galeriupload/" method="post" class="form-horizontal row-border col-md-12" id="galeriform" enctype="multipart/form-data">

            <hr class="bir">

            <div class="form-group col-md-12 pull-left">



              <div class="col-sm-8 dnone">
                <input type="hidden" id="page_id" name="page_id" value="<?php echo $modalpage['page_id'] ?>" >
                <input type="hidden" id="page_name" name="page_name" value="<?php echo is_js($modalpage['page_name']) ?>" >
                <input type="file" id="resimler" name="resim[]" multiple >
              </div>
              <div class="col-md-6">
                <button type="button" class="btn btn-success btn-large" onclick="$('#resimler').click();"><i class="fa fa-picture-o" style="margin:5px"></i>Galeri Resimleri Seçiniz</button>
              </div>
              <div class="col-md-6">
                <button type="submit" class="btn btn-primary btn-large col-md-12 pull-left"  >Yüklemeye Başla</button>
              </div>
            </div>


            <div class="col-md-12">

              <div class="progress progress-striped active progress-large" style="background:#fff">
                <div data-percentage="0%" style="width: 0%;" id="bar" class="progress-bar percent progress-bar-success"></div>
              </div>

            </div>


            <div id="sonucfmupload"></div>

            <div class="bottom text-right">

            </div>

          </form>

        </div>
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
      <hr>
      <div class="clearfix"></div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
