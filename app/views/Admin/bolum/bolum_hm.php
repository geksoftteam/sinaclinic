<div class="row">
  <div class="col-md-12">
    <div class="grid simple " style="padding-top:10px;">

     <div class="grid-title no-border"> 
       <h3 class="content-header pull-left"><?php echo $page_label; ?></h3>
       <div class="col-lg-5 pull-right text-right">


        <button id="editable-sample_new" class="btn btn-info tipsi"
        onclick="location.href='<?php echo SITE_URL ?>Admin/Bolum/Insert'"
        title="Yeni Ekleme">
        Bolum Ekle <i class="fa fa-plus"></i>
      </button>

    </div>
    <div class="clearfix"></div>
  </div>

  <div class="grid-body no-border">
    <table class="table no-more-tables" id="example">
      <thead>
        <tr>
          <th class="col-md-1">NO</th>
          <th>Bölüm İsmi</th> 
          <th>Şubeler</th> 
          <th>İşlem</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=1;

        foreach($content as $content_row){?>
          <tr class=""
          id="tr_<?php echo $content_row["bolum_id"]; ?>" >
          <td>
            <?php echo $i; ?>
          </td>

          <td><?php echo is_js($content_row["bolum_name"]);   ?></td>


          <td>

           <?php foreach ($subeListele as $item): ?>



            <label class="checkbox-inline">
              <input type="checkbox" class="bolumsube" data-subeId="<?php echo $item['sube_id'] ?>" data-bolumId="<?php echo $content_row["bolum_id"]; ?>" <?php echo (in_array($item['sube_id'], $content_row['sube_ids'])) ? "checked" : "" ?>><?= is_js($item['sube_name']) ?>
            </label>

          <?php endforeach ?>


        </td>


        <td>
          <?php if (COPY_STATUS == 1): ?>

            <a href="javascript:;" data-id="<?php echo $content_row["bolum_id"]; ?>" class="btn btn-success btn-xs  tipsi copy " title="Kopyala" data-original-title="Kopyala"><i class="fa fa-copy"></i></a>
          <?php endif ?>

          <a href="<?php echo SITE_URL ?>Admin/Bolum/Update/<?php echo $content_row["bolum_id"]; ?>" class="btn btn-info btn-xs tipsi" title="" data-original-title="Düzenle"><i class="fa fa-edit"></i></a>

          <a href="javascript:void(0);" class="btn btn-danger btn-xs tipsi delete_link" title="" data-original-title="Sil"  data-id="<?php echo $content_row["bolum_id"]; ?>"><i class="fa fa-times"></i></a>

        </td>
      </tr>

      <?php $i++;} // end each for main level?>
    </tbody>
  </table>
</div>
</div>
</div>
</div>