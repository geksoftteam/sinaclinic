<div class="col-sm-12">
 <div class="grid simple">
    <div class="grid-title no-border">
      <h4><?php echo $page_label; ?></h4>

  </div>
  <div class="grid-body no-border">

      <form action="<?php echo SITE_URL ?>Admin/Sube/Update_Run/<?php echo $content["sube_id"]; ?>" method="post" class="form-horizontal row-border col-sm-12" id="sube" enctype="multipart/form-data">
          <?php echo $alert; ?>



          <?php  foreach ($maindiller as   $dilc) {$rtl = "";  $dilkod = $dilc["diller_kod"];  if($dilkod == "ar"){ $rtl = "rtlc ";} ?>

          <div class="form-group">

           <label class="col-sm-3 control-label">Şube İsmi (<?php echo $dilkod; ?>) :</label>

           <div class="col-sm-9">

               <input type="text" name="sube_name[<?php echo $dilkod; ?>]"  
               value="<?php echo is_js($content['sube_name'],$dilkod); ?>"
               class="form-control <?php echo $rtl; ?>zor">

           </div>

       </div>

   <?php } ?>



   <div class="form-group">

       <label class="col-sm-3 control-label">Şube Telefonu:</label>

       <div class="col-sm-9">

           <input type="text" name="sube_telefon"  
           value="<?php echo is_js($content['sube_telefon'],$dilkod); ?>"
           class="form-control zor">

       </div>

   </div>

   <div class="form-group">

    <label class="col-sm-3 control-label">Şube Faks:</label>

    <div class="col-sm-9">

       <input type="text" name="sube_fax"  
       value="<?php echo is_js($content['sube_fax'],$dilkod); ?>"
       class="form-control zor">

   </div>

</div>

<hr class="bir">

<div class="form-group">

    <label class="col-sm-3 control-label">Şube Resmi:</label>

    

    <div class="col-sm-9">



     <input type="file" name="resim" onchange="resim_on_izle(this,1)" >

     <input type="hidden" name="oldresim" value="<?php echo $content['sube_image'] ?>">

     <img id="onizle1" src="<?php echo SITE_UPLOAD_DIR . 'page/' . $GLOBALS['pageboyutlar'][0] . '_' . $content['sube_image'] ?>" width="100" <?php if (empty($content['sube_image'])): ?>

     style="display:none;"

     <?php endif ?>  />



 </div>




</div>


<hr class="bir">

<?php  foreach ($maindiller as   $dilc) {$rtl = "";  $dilkod = $dilc["diller_kod"];  if($dilkod == "ar"){ $rtl = "rtlc ";} ?>

<div class="form-group">

    <label class="col-sm-3 control-label">Şube Adresi (<?php echo $dilkod; ?>) :</label>

    <div class="col-sm-9">

       <input type="text" name="sube_adres[<?php echo $dilkod; ?>]"  
       value="<?php echo is_js($content['sube_adres'],$dilkod); ?>"
       class="form-control <?php echo $rtl; ?>zor">

   </div>

</div>

<?php } ?>

<?php  foreach ($maindiller as   $dilc) {$rtl = "";  $dilkod = $dilc["diller_kod"];  if($dilkod == "ar"){ $rtl = "rtlc ";} ?>

<div class="form-group">

    <label class="col-sm-3 control-label">Şube Maps (<?php echo $dilkod; ?>) :</label>

    <div class="col-sm-9">

     
       <textarea name="sube_maps[<?php echo $dilkod; ?>]" rows="4" class="form-control col-md-12"  >
        <?php echo is_js($content['sube_maps'],$dilkod); ?>
    </textarea>

</div>

</div>

<?php } ?>

<?php foreach ($maindiller as   $dilc) {$rtl = "";  $dilkod = $dilc["diller_kod"]; if($dilkod == "ar"){ $rtl = "rtlc ";} ?>

<div class="form-group col-sm-12">

    <label class="col-sm-5 control-label pull-left text-left"><h2>Şube İçerik (<?php echo $dilkod; ?>)  </h2> </label>

    <div class="clearfix"></div>

    <div class="col-sm-10 text-right">

        <textarea name="sube_content[<?php echo $dilkod; ?>]" rows="4" class="form-control editor col-md-12"  >
            <?php echo is_js($content['sube_content'],$dilkod); ?>
        </textarea>


    </div>

</div>

<?php } ?>



<div class="bottom text-right">
  <button type="button" data-id="sube" class="btn btn-primary submit_btn">Güncelle</button>

</div>

</form>
</div><!--/porlets-content-->
</div><!--/block-web--> 
</div><!--/col-md-6-->
