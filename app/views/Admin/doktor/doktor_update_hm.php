<div class="col-sm-12">
 <div class="grid simple">
    <div class="grid-title no-border">
      <h4><?php echo $page_label; ?></h4>

  </div>
  <div class="grid-body no-border">

      <form action="<?php echo SITE_URL ?>Admin/Doktor/Update_Run/<?php echo $content["doktor_id"]; ?>" method="post" class="form-horizontal row-border col-sm-12" id="doktor" enctype="multipart/form-data">
          <?php echo $alert; ?>



          <?php  foreach ($maindiller as   $dilc) {$rtl = "";  $dilkod = $dilc["diller_kod"];  if($dilkod == "ar"){ $rtl = "rtlc ";} ?>

          <div class="form-group">

           <label class="col-sm-3 control-label">Doktor İsmi (<?php echo $dilkod; ?>) :</label>

           <div class="col-sm-9">

               <input type="text" name="doktor_name[<?php echo $dilkod; ?>]"  
               value="<?php echo is_js($content['doktor_name'],$dilkod); ?>"
               class="form-control <?php echo $rtl; ?>zor">

           </div>

       </div>

   <?php } ?>

   <?php foreach ($maindiller as   $dilc) {$rtl = "";  $dilkod = $dilc["diller_kod"]; if($dilkod == "ar"){ $rtl = "rtlc ";} ?>

   <div class="form-group">

    <label class="col-sm-3 control-label">Descraption (<?php echo $dilkod; ?>) 

        <small class="tip_top" title="Açıklama meta etiketi 160 karakterden az olmalıdır. 160 karakterden uzun olması arama motorları tarafından spam olarak yorumlanmaktadır.">(Page Title max 160 Karakter)</small>:</label>

        <div class="col-sm-9">

            <textarea  data-say="160" rows="4" data-help="doktor_desc_say<?php echo $dilkod; ?>" class="form-control say  <?php echo $rtl; ?>  zor" name="doktor_desc[<?php echo $dilkod; ?>]"><?php echo is_js($content['doktor_desc'],$dilkod); ?></textarea>

            <p class="help-block" id="doktor_desc_say<?php echo $dilkod; ?>"></p>
        </div>
    </div>

<?php } ?>

<hr class="bir">

<div class="form-group">

 <label class="col-sm-3 control-label">Doktor Resmi:</label>

 <div class="col-sm-9">

     <input type="file" name="resim" onchange="resim_on_izle(this,1)" >

     <img id="onizle1" src="" width="100" style="display:none;" />

 </div>

</div>

<hr class="bir">

<div class="form-group">

  <label class="col-sm-3 control-label">Doktor Şube ve Bölümü:</label>

  <div class="col-sm-9">



    <?php foreach ($subeListele as $item): ?>
      <div class="form-check form-check-inline">

        <input class="form-check-input" type="checkbox" name="subebolum[]" value="<?= $item['sube_id'].'-'.$item['bolum_id'] ?>" <?php echo in_array($item['sube_id'].'-'.$item['bolum_id'], $doktorBolumGetir) ? 'checked' : ""; ?>  >
        <label class="form-check-label" style="display: inline-block;"><?= is_js($item['sube_name']).'-'.is_js($item['bolum_name']) ?></label>
    </div>

<?php endforeach ?>

</div>
<div class="col-md-3">
    <div class="bolumSec"></div>

</div>
</div>

<hr class="bir">

<?php foreach ($maindiller as   $dilc) {$rtl = "";  $dilkod = $dilc["diller_kod"]; if($dilkod == "ar"){ $rtl = "rtlc ";} ?>

<div class="form-group col-sm-12">

    <label class="col-sm-5 control-label pull-left text-left"><h2>Doktor İçerik (<?php echo $dilkod; ?>)  </h2> </label>

    <div class="clearfix"></div>

    <div class="col-sm-10 text-right">

        <textarea name="doktor_content[<?php echo $dilkod; ?>]" rows="4" class="form-control editor col-md-12"  >
            <?php echo is_js($content['doktor_content'],$dilkod); ?>
        </textarea>


    </div>

</div>

<?php } ?>





<div class="bottom text-right">
  <button type="button" data-id="doktor" class="btn btn-primary submit_btn">Güncelle</button>

</div>

</form>
</div><!--/porlets-content-->
</div><!--/block-web--> 
</div><!--/col-md-6-->
