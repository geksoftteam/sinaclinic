<div class="row">
    <div class="col-md-12">
        <div class="grid simple " style="padding-top:10px;">

           <div class="grid-title no-border"> 
               <h3 class="content-header pull-left"><?php echo $page_label; ?></h3>
               <div class="col-lg-5 pull-right text-right">


                <button id="editable-sample_new" class="btn btn-info tipsi"
                onclick="location.href='<?php echo SITE_URL ?>Admin/Doktor/Insert'"
                title="Yeni Ekleme">
                Doktor Ekle <i class="fa fa-plus"></i>
            </button>

        </div>
        <div class="clearfix"></div>
    </div>

    <div class="grid-body no-border">
        <table class="table no-more-tables" id="example">
          <thead>
              <tr>
                  <th class="col-md-1">NO</th>
                  <th>Doktor İsmi</th> <th>Durum</th>
                  <th>İşlem</th>
              </tr>
          </thead>
          <tbody>
              <?php $i=1;

              foreach($content as $content_row){?>
                  <tr class=""
                  id="tr_<?php echo $content_row["doktor_id"]; ?>" >
                  <td>
                      <?php echo $i; ?>
                  </td>


                  <td><?php echo is_js($content_row["doktor_name"]);   ?></td>


                  <td><div class="radio radio-success">

                    <input id="yes<?php echo $content_row["doktor_id"] ?>doktor_durum"

                    type="radio" data-id="<?php echo $content_row["doktor_id"].'+doktor_durum'; ?>"

                    class="durumsecdoktor" name="doktor_durum<?php echo $content_row["doktor_id"] ?>" value="1"

                    <?php if ($content_row["doktor_durum"] == "1") { echo 'checked="checked"';} ?>>

                    <label for="yes<?php echo $content_row["doktor_id"] ?>doktor_durum">Aktif</label>

                </div> <div class="radio">

                    <input id="no<?php echo $content_row["doktor_id"] ?>doktor_durum"

                    type="radio" name="doktor_durum<?php echo $content_row["doktor_id"] ?>"

                    data-id="<?php echo $content_row["doktor_id"].'+doktor_durum'; ?>"

                    class="durumsecdoktor" value="2"

                    <?php if ($content_row["doktor_durum"] == "2") { echo 'checked="checked"';} ?>>

                    <label for="no<?php echo $content_row["doktor_id"] ?>doktor_durum">Pasif</label>

                </div></td>

                <td>
                  <?php if (COPY_STATUS == 1): ?>
                      <a href="javascript:;" data-id="<?php echo $content_row["doktor_id"]; ?>" class="btn btn-success btn-xs  tipsi copy " title="Kopyala" data-original-title="Kopyala"><i class="fa fa-copy"></i></a>
                  <?php endif ?>
                  <a href="<?php echo SITE_URL ?>Admin/Doktor/Update/<?php echo $content_row["doktor_id"]; ?>" class="btn btn-info btn-xs tipsi" title="" data-original-title="Düzenle"><i class="fa fa-edit"></i></a>

                  <a href="javascript:void(0);" class="btn btn-danger btn-xs tipsi delete_link" title="" data-original-title="Sil"  data-id="<?php echo $content_row["doktor_id"]; ?>"><i class="fa fa-times"></i></a>
              </td>
          </tr>

          <?php $i++;} // end each for main level?>
      </tbody>
  </table>
</div>
</div>
</div>
</div>