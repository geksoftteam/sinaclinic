<?php

/**
 * @param \App\Libs\MenuItem $menuItem
 * @return \App\Libs\Html\HtmlElement
 */
function renderItem($menuItem)
{
    $li = new \App\Libs\Html\HtmlElement('li');
    $li->addClass('dd-item dd3-item');
    $attr = $menuItem->toArray();

    foreach ($attr as $ky => $vl) {
        $li->setAttr('data-' . $ky, $vl);
    }
    $divHandle = new \App\Libs\Html\HtmlElement('div');
    $divHandle->addClass('dd-handle dd3-handle');
    $li->addChild($divHandle);

    $divContent = new \App\Libs\Html\HtmlElement('div');
    $divContent->addClass('dd3-content');
    $divContent->addChild(new \App\Libs\Html\TextNode($menuItem->getText()));

    $btnDel = new \App\Libs\Html\HtmlElement('button');
    $btnDel->setAttr('title', 'sil');
    $btnDel->addClass('remove-item pull-right btn-danger btn btn-danger btn-mini tipsi ml10');
    $delI = new \App\Libs\Html\HtmlElement('i'); 
    $delI->addClass('fa fa-times');
    $btnDel->addChild($delI);
    $divContent->addChild($btnDel);

    $btnAdd = new \App\Libs\Html\HtmlElement('button');
    $btnAdd->setAttr('title', 'ekle');
    $btnAdd->addClass('add-child-item edit-item pull-right btn-success btn btn-info btn-mini tipsi ml10');
    $addI = new \App\Libs\Html\HtmlElement('i');
    $addI->addClass('fa fa-plus');
    $btnAdd->addChild($addI);
    $divContent->addChild($btnAdd);

    $btnEdit = new \App\Libs\Html\HtmlElement('button');
    $btnEdit->setAttr('title', 'Düzenle');
    $btnEdit->addClass('edit-item pull-right btn-success btn btn-info btn-mini tipsi ml10');
    $editI = new \App\Libs\Html\HtmlElement('i');
    $editI->addClass('fa fa-edit');
    $btnEdit->addChild($editI);
    $divContent->addChild($btnEdit);

    $li->addChild($divContent);

    if ($menuItem->hasChilds()) {
        $ol = new \App\Libs\Html\HtmlElement('ol');
        $ol->addClass('dd-list');

        foreach ($menuItem->getChilds() as $mItem) {
            $ol->addChild(renderItem($mItem));
        }

        $li->addChild($ol);
    }
    return $li;
}

?>

<div class="row">
    <div class="col-md-12">
        <div class="grid simple " style="padding-top:10px;">

            <div class="grid-title no-border">
                <h3 class="content-header pull-left"><?php echo $page_label; ?></h3>
                <div class="col-lg-5 pull-right text-right">


                    <button id="editable-sample_new" class="btnAddNew btn btn-info tipsi"
                    type="button"
                    title="Yeni Ekleme">
                    Menü Ekle <i class="fa fa-plus"></i>
                </button>
                <button type="button" id="btnSubmit" class="btn btn-raised btn-primary btn-round waves-effect m-l-20">
                    Kaydet
                </button>

            </div>
            <form method="post" action="<?= SITE_URL . 'Admin/Menu/main_menu_edit_post' ?>" id="frmMainMenu">
                <input type="hidden" id="main_menu_json" name="main_menu_json">
            </form>


            <div class="clearfix"></div>
        </div>

        <div class="body">
            <div class="dd nestable-with-handle">
                <ol class="dd-list">
                    <?php
                    /** @var \App\Libs\Menu $content */
                    foreach ($content->getMenuItems() as $menuItem) {
                        $nd = renderItem($menuItem);
                        echo $nd->toHtmlString();
                    }
                    ?>
                </ol>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">

    /*=========== EventObserver =================*/
    (function (glb) {
        glb['GekEventObserver'] = function () {
            var events = {};
            this.on = function (name, func) {
                if (!events[name]) events[name] = [];
                events[name].push(func);
            };
            this.trigger = function (name, data) {
                if (events[name] && events[name].length > 0) {
                    for (var i = 0; i < events[name].length; i++) {
                        events[name][i].call(this, data);
                    }
                }
            };
            this.off = function (name, func) {
                var l = arguments.length;
                if (l == 0) {
                    events = {};
                } else if (l == 1 && events[name]) {
                    delete events[name];
                } else if (l == 2 && events[name] && events[name].length > 0) {
                    for (var i = 0, _l = events[name].length; i < _l; i++) {
                        if (events[name][i] == func) {
                            delete events[name][i];
                        }
                    }
                }
            };
        };
    })(window);

    /*=================== Object Helper ================*/

    (function (win) {
        var ObjHelper = function () {
            var extend = function () {

                var extended = {};
                var deep = false;
                var i = 0;
                var length = arguments.length;

                if (Object.prototype.toString.call(arguments[0]) === "[object Boolean]") {
                    deep = arguments[0];
                    i++;
                }

                var merge = function (obj) {
                    for (var prop in obj) {
                        if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                            if (deep && Object.prototype.toString.call(obj[prop]) === "[object Object]") {
                                extended[prop] = extend(true, extended[prop], obj[prop]);
                            } else {
                                extended[prop] = obj[prop];
                            }
                        }
                    }
                };

                for (; i < length; i++) {
                    var obj = arguments[i];
                    merge(obj);
                }

                return extended;
            };
            return {
                extend: extend
            }
        };
        win['ObjHelper'] = ObjHelper();
    })(window);

    document.addEventListener("DOMContentLoaded", function (event) {

        (function (glb) {
            var kategorilerList = function () {
                var defaults = {
                    gridSelector: "#kategoriler-table",
                    readUrl: '<?= SITE_URL . "Admin/Menu/get_kategoriler" ?>',
                    searchButtonSelector: '#kategoriler-search-list',
                    searchFieldsSelector: "#kategorilerSearchTerm",

                },
                options = null,
                grid = null,
                gridApi = null,
                btnSearch = null,
                searchFields = null,
                columns = [
                {"data": "nav_id"},
                {"data": "nav_name"},
                {"data": "nav_url"},
                {"data": null, "render": renderSelectButton},
                ];


                function renderSelectButton(data, type, row) {
                    return '<button type="button" class="md-btn md-btn-small md-btn-primary md-btn-wave selectRow">Seç</button>';
                }

                function additionalData(data) {
                    if (!data) {
                        data = {};
                    }
                    if (searchFields != null && searchFields.length) {
                        $(searchFields).each(function () {
                            var isCheckbox = ($(this).attr('name') === 'checkbox');
                            var name = $(this).attr('name');
                            data[name] = isCheckbox ? ($(this).val() && $(this).prop('checked')) : $(this).val();
                        });
                    }
                    // data = addAntiForgeryToken(data);
                    return data;
                }

                function requestDataAdapter(data) {
                    data = data || {};
                    if (gridApi) {
                        var info = gridApi.page.info();
                        data.Page = (info.page + 1);
                        data.PageSize = info.length;
                    } else {
                        data.Page = data.start > 0 ? (Math.round((data.start / data.length))) : 1;
                        data.PageSize = data.length;
                    }
                    data = additionalData(data);
                    return data;
                }

                function responseDataAdapter(data) {
                    var orgData = jQuery.parseJSON(data);
                    var adaptedData = {
                        data: orgData.Data,
                        recordsTotal: orgData.Total,
                        recordsFiltered: orgData.Total
                    };
                    return JSON.stringify(adaptedData);
                }

                function init(opts) {
                    GekEventObserver.call(this);
                    options = ObjHelper.extend(defaults, opts);
                    grid = $(options.gridSelector);

                    btnSearch = $(options.searchButtonSelector);
                    searchFields = $(options.searchFieldsSelector);

                    grid.dataTable({
                        'processing': true,
                        'serverSide': true,
                        "searching": false,
                        "ordering": false,
                        "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                        'ajax': {
                            "type": "POST",
                            "url": options.readUrl,
                            "data": requestDataAdapter,
                            "dataFilter": responseDataAdapter
                        },
                        'columns': columns,
                        'columnDefs': []
                    });

                    gridApi = grid.api();
                    attachEvents();
                }

                function attachEvents() {

                    btnSearch.on('click', function () {
                        gridApi.ajax.reload();
                    });

                    if (searchFields != null && searchFields.length) {
                        searchFields.keydown(function (event) {
                            if (event.keyCode === 13) {
                                btnSearch.click();
                                return false;
                            }
                        });
                    }

                    grid.on('click', 'tbody .selectRow', function (e) {
                        var trElem = $(this).parents('tr');
                        var row = gridApi.row(trElem);
                        var rowData = row.data();
                        me.trigger("select", rowData);
                    });

                }

                var me = {
                    init: init,
                };

                return me;
            };

            glb['kategorilerList'] = kategorilerList();
        })(window);

        (function (glb) {
            var pagesList = function () {
                var defaults = {
                    gridSelector: "#pages-table",
                    readUrl: '<?= SITE_URL . "Admin/Menu/get_pages" ?>',
                    searchButtonSelector: '#pages-search-list',
                    searchFieldsSelector: "#pagesSearchTerm",

                },
                options = null,
                grid = null,
                gridApi = null,
                btnSearch = null,
                searchFields = null,
                columns = [
                {"data": "page_id"},
                {"data": "page_name"},
                {"data": "page_url"},
                {"data": null, "render": renderSelectButton},
                ];


                function renderSelectButton(data, type, row) {
                    return '<button type="button" class="md-btn md-btn-small md-btn-primary md-btn-wave selectRow">Seç</button>';
                }

                function additionalData(data) {
                    if (!data) {
                        data = {};
                    }
                    if (searchFields != null && searchFields.length) {
                        $(searchFields).each(function () {
                            var isCheckbox = ($(this).attr('name') === 'checkbox');
                            var name = $(this).attr('name');
                            data[name] = isCheckbox ? ($(this).val() && $(this).prop('checked')) : $(this).val();
                        });
                    }
                    // data = addAntiForgeryToken(data);
                    return data;
                }

                function requestDataAdapter(data) {
                    data = data || {};
                    if (gridApi) {
                        var info = gridApi.page.info();
                        data.Page = (info.page + 1);
                        data.PageSize = info.length;
                    } else {
                        data.Page = data.start > 0 ? (Math.round((data.start / data.length))) : 1;
                        data.PageSize = data.length;
                    }
                    data = additionalData(data);
                    return data;
                }

                function responseDataAdapter(data) {
                    var orgData = jQuery.parseJSON(data);
                    var adaptedData = {
                        data: orgData.Data,
                        recordsTotal: orgData.Total,
                        recordsFiltered: orgData.Total
                    };
                    return JSON.stringify(adaptedData);
                }

                function init(opts) {
                    GekEventObserver.call(this);
                    options = ObjHelper.extend(defaults, opts);
                    grid = $(options.gridSelector);

                    btnSearch = $(options.searchButtonSelector);
                    searchFields = $(options.searchFieldsSelector);

                    grid.dataTable({
                        'processing': true,
                        'serverSide': true,
                        "searching": false,
                        "ordering": false,
                        "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                        'ajax': {
                            "type": "POST",
                            "url": options.readUrl,
                            "data": requestDataAdapter,
                            "dataFilter": responseDataAdapter
                        },
                        'columns': columns,
                        'columnDefs': []
                    });

                    gridApi = grid.api();
                    attachEvents();
                }

                function attachEvents() {

                    btnSearch.on('click', function () {
                        gridApi.ajax.reload();
                    });

                    if (searchFields != null && searchFields.length) {
                        searchFields.keydown(function (event) {
                            if (event.keyCode === 13) {
                                btnSearch.click();
                                return false;
                            }
                        });
                    }

                    grid.on('click', 'tbody .selectRow', function (e) {
                        var trElem = $(this).parents('tr');
                        var row = gridApi.row(trElem);
                        var rowData = row.data();
                        me.trigger("select", rowData);
                    });

                }

                var me = {
                    init: init,
                };

                return me;
            };

            glb['pagesList'] = pagesList();
        })(window);

        (function (glb) {
            var subelerList = function () {
                var defaults = {
                    gridSelector: "#subeler-table",
                    readUrl: '<?= SITE_URL . "Admin/Menu/get_subeler" ?>',
                    searchButtonSelector: '#subeler-search-list',
                    searchFieldsSelector: "#subelerSearchTerm",

                },
                options = null,
                grid = null,
                gridApi = null,
                btnSearch = null,
                searchFields = null,
                columns = [
                {"data": "sube_id"},
                {"data": "sube_name"},
                {"data": "sube_seo_name"},
                {"data": null, "render": renderSelectButton},
                ];


                function renderSelectButton(data, type, row) {
                    return '<button type="button" class="md-btn md-btn-small md-btn-primary md-btn-wave selectRow">Seç</button>';
                }

                function additionalData(data) {
                    if (!data) {
                        data = {};
                    }
                    if (searchFields != null && searchFields.length) {
                        $(searchFields).each(function () {
                            var isCheckbox = ($(this).attr('name') === 'checkbox');
                            var name = $(this).attr('name');
                            data[name] = isCheckbox ? ($(this).val() && $(this).prop('checked')) : $(this).val();
                        });
                    }
                    // data = addAntiForgeryToken(data);
                    return data;
                }

                function requestDataAdapter(data) {
                    data = data || {};
                    if (gridApi) {
                        var info = gridApi.page.info();
                        data.Page = (info.page + 1);
                        data.PageSize = info.length;
                    } else {
                        data.Page = data.start > 0 ? (Math.round((data.start / data.length))) : 1;
                        data.PageSize = data.length;
                    }
                    data = additionalData(data);
                    return data;
                }

                function responseDataAdapter(data) {
                    var orgData = jQuery.parseJSON(data);
                    var adaptedData = {
                        data: orgData.Data,
                        recordsTotal: orgData.Total,
                        recordsFiltered: orgData.Total
                    };
                    return JSON.stringify(adaptedData);
                }

                function init(opts) {
                    GekEventObserver.call(this);
                    options = ObjHelper.extend(defaults, opts);
                    grid = $(options.gridSelector);

                    btnSearch = $(options.searchButtonSelector);
                    searchFields = $(options.searchFieldsSelector);

                    grid.dataTable({
                        'processing': true,
                        'serverSide': true,
                        "searching": false,
                        "ordering": false,
                        "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                        'ajax': {
                            "type": "POST",
                            "url": options.readUrl,
                            "data": requestDataAdapter,
                            "dataFilter": responseDataAdapter
                        },
                        'columns': columns,
                        'columnDefs': []
                    });

                    gridApi = grid.api();
                    attachEvents();
                }

                function attachEvents() {

                    btnSearch.on('click', function () {
                        gridApi.ajax.reload();
                    });

                    if (searchFields != null && searchFields.length) {
                        searchFields.keydown(function (event) {
                            if (event.keyCode === 13) {
                                btnSearch.click();
                                return false;
                            }
                        });
                    }

                    grid.on('click', 'tbody .selectRow', function (e) {
                        var trElem = $(this).parents('tr');
                        var row = gridApi.row(trElem);
                        var rowData = row.data();
                        me.trigger("select", rowData);
                    });

                }

                var me = {
                    init: init,
                };

                return me;
            };

            glb['subelerList'] = subelerList();
        })(window);

        (function (glb) {
            var bolumlerList = function () {
                var defaults = {
                    gridSelector: "#bolumler-table",
                    readUrl: '<?= SITE_URL . "Admin/Menu/get_bolumler" ?>',
                    searchButtonSelector: '#bolumler-search-list',
                    searchFieldsSelector: "#bolumlerSearchTerm",

                },
                options = null,
                grid = null,
                gridApi = null,
                btnSearch = null,
                searchFields = null,
                columns = [
                {"data": "bolum_id"},
                {"data": "bolum_name"},
                {"data": "bolum_seo_name"},
                {"data": null, "render": renderSelectButton},
                ];


                function renderSelectButton(data, type, row) {
                    return '<button type="button" class="md-btn md-btn-small md-btn-primary md-btn-wave selectRow">Seç</button>';
                }

                function additionalData(data) {
                    if (!data) {
                        data = {};
                    }
                    if (searchFields != null && searchFields.length) {
                        $(searchFields).each(function () {
                            var isCheckbox = ($(this).attr('name') === 'checkbox');
                            var name = $(this).attr('name');
                            data[name] = isCheckbox ? ($(this).val() && $(this).prop('checked')) : $(this).val();
                        });
                    }
                    // data = addAntiForgeryToken(data);
                    return data;
                }

                function requestDataAdapter(data) {
                    data = data || {};
                    if (gridApi) {
                        var info = gridApi.page.info();
                        data.Page = (info.page + 1);
                        data.PageSize = info.length;
                    } else {
                        data.Page = data.start > 0 ? (Math.round((data.start / data.length))) : 1;
                        data.PageSize = data.length;
                    }
                    data = additionalData(data);
                    return data;
                }

                function responseDataAdapter(data) {
                    var orgData = jQuery.parseJSON(data);
                    var adaptedData = {
                        data: orgData.Data,
                        recordsTotal: orgData.Total,
                        recordsFiltered: orgData.Total
                    };
                    return JSON.stringify(adaptedData);
                }

                function init(opts) {
                    GekEventObserver.call(this);
                    options = ObjHelper.extend(defaults, opts);
                    grid = $(options.gridSelector);

                    btnSearch = $(options.searchButtonSelector);
                    searchFields = $(options.searchFieldsSelector);

                    grid.dataTable({
                        'processing': true,
                        'serverSide': true,
                        "searching": false,
                        "ordering": false,
                        "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                        'ajax': {
                            "type": "POST",
                            "url": options.readUrl,
                            "data": requestDataAdapter,
                            "dataFilter": responseDataAdapter
                        },
                        'columns': columns,
                        'columnDefs': []
                    });

                    gridApi = grid.api();
                    attachEvents();
                }

                function attachEvents() {

                    btnSearch.on('click', function () {
                        gridApi.ajax.reload();
                    });

                    if (searchFields != null && searchFields.length) {
                        searchFields.keydown(function (event) {
                            if (event.keyCode === 13) {
                                btnSearch.click();
                                return false;
                            }
                        });
                    }

                    grid.on('click', 'tbody .selectRow', function (e) {
                        var trElem = $(this).parents('tr');
                        var row = gridApi.row(trElem);
                        var rowData = row.data();
                        me.trigger("select", rowData);
                    });

                }

                var me = {
                    init: init,
                };

                return me;
            };

            glb['bolumlerList'] = bolumlerList();
        })(window);

        (function (glb) {
            var doktorlarList = function () {
                var defaults = {
                    gridSelector: "#doktorlar-table",
                    readUrl: '<?= SITE_URL . "Admin/Menu/get_doktorlar" ?>',
                    searchButtonSelector: '#doktorlar-search-list',
                    searchFieldsSelector: "#doktorlarSearchTerm",

                },
                options = null,
                grid = null,
                gridApi = null,
                btnSearch = null,
                searchFields = null,
                columns = [
                {"data": "doktor_id"},
                {"data": "doktor_name"},
                {"data": "doktor_seo_name"},
                {"data": null, "render": renderSelectButton},
                ];


                function renderSelectButton(data, type, row) {
                    return '<button type="button" class="md-btn md-btn-small md-btn-primary md-btn-wave selectRow">Seç</button>';
                }

                function additionalData(data) {
                    if (!data) {
                        data = {};
                    }
                    if (searchFields != null && searchFields.length) {
                        $(searchFields).each(function () {
                            var isCheckbox = ($(this).attr('name') === 'checkbox');
                            var name = $(this).attr('name');
                            data[name] = isCheckbox ? ($(this).val() && $(this).prop('checked')) : $(this).val();
                        });
                    }
                    // data = addAntiForgeryToken(data);
                    return data;
                }

                function requestDataAdapter(data) {
                    data = data || {};
                    if (gridApi) {
                        var info = gridApi.page.info();
                        data.Page = (info.page + 1);
                        data.PageSize = info.length;
                    } else {
                        data.Page = data.start > 0 ? (Math.round((data.start / data.length))) : 1;
                        data.PageSize = data.length;
                    }
                    data = additionalData(data);
                    return data;
                }

                function responseDataAdapter(data) {
                    var orgData = jQuery.parseJSON(data);
                    var adaptedData = {
                        data: orgData.Data,
                        recordsTotal: orgData.Total,
                        recordsFiltered: orgData.Total
                    };
                    return JSON.stringify(adaptedData);
                }

                function init(opts) {
                    GekEventObserver.call(this);
                    options = ObjHelper.extend(defaults, opts);
                    grid = $(options.gridSelector);

                    btnSearch = $(options.searchButtonSelector);
                    searchFields = $(options.searchFieldsSelector);

                    grid.dataTable({
                        'processing': true,
                        'serverSide': true,
                        "searching": false,
                        "ordering": false,
                        "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                        'ajax': {
                            "type": "POST",
                            "url": options.readUrl,
                            "data": requestDataAdapter,
                            "dataFilter": responseDataAdapter
                        },
                        'columns': columns,
                        'columnDefs': []
                    });

                    gridApi = grid.api();
                    attachEvents();
                }

                function attachEvents() {

                    btnSearch.on('click', function () {
                        gridApi.ajax.reload();
                    });

                    if (searchFields != null && searchFields.length) {
                        searchFields.keydown(function (event) {
                            if (event.keyCode === 13) {
                                btnSearch.click();
                                return false;
                            }
                        });
                    }

                    grid.on('click', 'tbody .selectRow', function (e) {
                        var trElem = $(this).parents('tr');
                        var row = gridApi.row(trElem);
                        var rowData = row.data();
                        me.trigger("select", rowData);
                    });

                }

                var me = {
                    init: init,
                };

                return me;
            };

            glb['doktorlarList'] = doktorlarList();
        })(window);

        (function (win) {
            var menuManager = function () {
                var selector = '.dd',
                btnAddNewSel = '.btnAddNew',
                btnAddChildSel = '.add-child-item',
                btnEditSel = '.edit-item',
                btnRemoveSel = '.remove-item',
                editModalSel = '#editFormModal',
                editModalTitleSel = '#editFormModalTitle',
                editFormSel = '#frmMenu',
                btnSaveItemSel = '#btnSave',
                btnChancelItemSel = '#btnCancel',
                btnSubmitSel = '#btnSubmit',
                frmMainSel = '#frmMainMenu',
                hdnDataSel = '#main_menu_json',
                kategoriReadUrl = '<?= SITE_URL . "Admin/Menu/get_kategoriler" ?>',
                pagesReadUrl = '<?= SITE_URL . "Admin/Menu/get_pages" ?>',
                subelerReadUrl = '<?= SITE_URL . "Admin/Menu/get_subeler" ?>',
                bolumlerReadUrl = '<?= SITE_URL . "Admin/Menu/get_bolumler" ?>',
                doktorlarReadUrl = '<?= SITE_URL . "Admin/Menu/get_doktorlar" ?>',
                states = {
                    'none': 0,
                    'child': 1,
                    'edit': 2,
                    'new': 3
                },
                state = states.none,
                fields = [
                'text',
                'url',
                'menuType',
                'iconClass',
                'loginRequired',
                'tableName',
                'tableId',
                ],
                curItem = null;


                function createItem(dataAttrs) {
                    //console.log(dataAttrs);
                    var liElem = createElem('li', 'dd-item dd3-item');
                    if (dataAttrs) {
                        for (var key in dataAttrs) {
                            if (dataAttrs.hasOwnProperty(key)) {
                                liElem.setAttribute('data-' + key, dataAttrs[key]);
                            }

                        }
                    }
                    var hndDiv = createElem('div', 'dd-handle dd3-handle');
                    liElem.appendChild(hndDiv);
                    var cntDiv = createElem('div', 'dd3-content');

                    cntDiv.appendChild(document.createTextNode(dataAttrs.text));
                    var btnRemove = createElem('button', btnRemoveSel.replace('.', '') + ' pull-right btn-danger');
                    btnRemove.setAttribute('title', 'sil');
                    var rmvI = createElem('i', 'fa fa-times');
                    btnRemove.appendChild(rmvI);
                    cntDiv.appendChild(btnRemove);
                    var btnAdd = createElem('button', btnAddChildSel.replace('.', '') + ' pull-right btn-info');
                    btnAdd.setAttribute('title', 'ekle');
                    var addI = createElem('i', 'fa fa-plus');
                    btnAdd.appendChild(addI);
                    cntDiv.appendChild(btnAdd);

                    var btnEdit = createElem('button', btnEditSel.replace('.', '') + ' pull-right btn-success');
                    btnEdit.setAttribute('title', 'düzenle');
                    var editI = createElem('i', 'fa fa-edit');
                    btnEdit.appendChild(editI);
                    cntDiv.appendChild(btnEdit);

                    liElem.appendChild(cntDiv);
                    return liElem;
                }

                function createElem(tagName, cls) {
                    var elem = document.createElement(tagName);
                    if (cls) {
                        elem.setAttribute('class', cls);
                    }
                    return elem;

                }

                function handleSubmit(form) {
                    var dt = {
                    };
                    for (var i = 0; i < fields.length; i++) {
                        var c = fields[i];
                        if (c !== 'permissions') {
                            dt[c.toLowerCase()] = $(form).find('#' + c).val();
                        }
                    }
                    switch (state) {
                        case states.child:
                        var nol = curItem.find('> ol.dd-list');
                        if (nol == null || nol.length <= 0) {
                            curItem.append('<ol class="dd-list"></ol>');
                            nol = curItem.find('ol.dd-list');
                        }
                        var nli = createItem(dt);
                        nol.append(nli);
                        break;
                        case states.new:
                        var col = $(selector).find('> ol.dd-list');
                        var cli = createItem(dt);
                        col.append(cli);
                        break;
                        case states.edit:
                        var eli = createItem(dt);
                        var eOl = curItem.find('> ol.dd-list');
                        if (eOl != null && eOl.length > 0) {
                            eli.append(eOl[0]);
                        }
                        curItem.after(eli);
                        curItem.remove();

                        break;
                        case states.none:
                        default:
                        alert('Bir hata oluştu!');
                        break;
                    }

                    curItem = null;
                    state = states.none;
                    $(editModalSel).modal('hide');
                   // $(selector).data("nestable", null);
                   // $(selector).find('li.dd-item > button').remove();
                   $(selector).nestable().nestable('collapseAll');


               }

               function showForm(data) {
                if (data) {
                    $(editModalTitleSel).text('Öğe düzenle');
                    for (var ky in data) {

                        if (data.hasOwnProperty(ky)) {
                            if (ky === 'loginRequired') {
                                $('#' + ky).prop('checked', (data[ky] == 1));
                            } else {
                                $('#' + ky).val(data[ky]);
                            }

                        }

                    }

                } else {
                    $(editModalTitleSel).text('Yeni Öğe');
                    $(editFormSel + ' input[type=text]').val('');
                    $(editFormSel + ' select').val('');
                    $(editFormSel + ' input[type=checkbox]').prop('checked', false);
                }

                $(editModalSel).modal();

            }

            function fillForm(type, data) {
                var res = {
                    text: '',
                    url: '',
                    menuType : 'normal',
                    iconClass : '',
                    loginRequired : false,
                    tableName : '',
                    tableId : ''
                };
                var msg = '';
                switch (type) {
                    case 'kategori':
                    res.menuType = 'kategori';
                    res.text = data.nav_name;
                    res.url = '/' + data.nav_url;
                    res.tableId = data.nav_id;
                    res.tableName = 'nav';
                    msg = res.tableId + 'id li kategori seçildi.';
                    break;
                    case 'page':
                    res.menuType = 'page';
                    res.text = data.page_name;
                    res.url = '/' + data.nav_url + '/' + data.page_url;
                    res.tableId = data.page_id;
                    res.tableName = 'page';
                    msg = res.tableId + 'id li sayfa seçildi.';
                    break;
                    case 'sube':
                    res.menuType = 'sube';
                    res.text = data.sube_name;
                    res.url = '/Sube/Detay/' + data.sube_seo_name;
                    res.tableId = data.sube_id;
                    res.tableName = 'sube';
                    msg = res.tableId + 'id li şube seçildi.';
                    break;
                    case 'bolum':
                    res.menuType = 'bolum';
                    res.text = data.bolum_name;
                    res.url = '/Bolum/Detay/' + data.bolum_seo_name;
                    res.tableId = data.bolum_id;
                    res.tableName = 'bolum';
                    msg = res.tableId + 'id li bölüm seçildi.';
                    break;
                    case 'doktor':
                    res.menuType = 'doktor';
                    res.text = data.doktor_name;
                    res.url = '/Doktor/Detay/' + data.doktor_seo_name;
                    res.tableId = data.doktor_id;
                    res.tableName = 'doktor';
                    msg = res.tableId + 'id li doktor seçildi.';
                    break;
                    default:
                    msg = 'Bilinmeyen menu tipi: ' + type;
                    break;
                }
                $(editFormSel + ' select#menuType').val(res.menuType);
                $(editFormSel + ' input[name=text]').val(res.text);
                $(editFormSel + ' input[name=url]').val(res.url);
                $(editFormSel + ' input[name=iconClass]').val(res.iconClass);
                $(editFormSel + ' input[name=tableName]').val(res.tableName);
                $(editFormSel + ' input[name=tableId]').val(res.tableId);
                $(editFormSel + ' #loginRequired').prop('checked',res.loginRequired);
                alert(msg);


            }

            function init() {
                $(selector).nestable().nestable('collapse');
                win['kategorilerList'].init();
                win['pagesList'].init();
                win['subelerList'].init();
                win['bolumlerList'].init();
                win['doktorlarList'].init();

                attachEvents();
                $(editFormSel).validate({
                    submitHandler: handleSubmit
                });

            }


            function attachEvents() {

                win['kategorilerList'].on('select', function (dt) {
                    fillForm('kategori', dt);
                });

                win['pagesList'].on('select', function (dt) {
                    fillForm('page', dt);
                });

                win['subelerList'].on('select', function (dt) {
                    fillForm('sube', dt);
                });

                win['bolumlerList'].on('select', function (dt) {
                    fillForm('bolum', dt);
                });

                win['doktorlarList'].on('select', function (dt) {
                    fillForm('doktor', dt);
                });

                $(selector).on('change', function () {
                    var $this = $(this);
                    var serializedData = window.JSON.stringify($($this).nestable('serialize'));
                    $(hdnDataSel).val(serializedData);
                });

                $(selector).on('click', btnRemoveSel, function () {
                    $(this).parent().parent().remove();
                });

                $(selector).on('click', btnEditSel, function () {
                    curItem = $(this).parent().parent();
                    state = states.edit;
                    var data = {};
                    for (var i = 0; i < fields.length; i++) {
                        data[fields[i]] = curItem.attr('data-' + fields[i]);
                    }
                    showForm(data);
                });


                $(selector).on('click', btnAddChildSel, function () {
                    curItem = $(this).parent().parent();
                    state = states.child;
                    showForm();
                });

                $(btnSaveItemSel).on('click', function () {

                    $(editFormSel).submit();

                });

                $(btnChancelItemSel).on('click', function () {
                    btnSubmitSel = states.none;
                    curItem = null;
                });

                $(btnAddNewSel).on('click', function () {
                    state = states.new;
                    showForm();
                });
                $(btnSubmitSel).on('click', function () {
                    var serializedData = window.JSON.stringify($(selector).nestable('serialize'));
                    $(hdnDataSel).val(serializedData);
                    $(frmMainSel).submit();
                });

            }

            return {
                init: init,
            }
        };

        win['menuManager'] = menuManager();
    })(window);

        //console.log(window.menuManager);

        window.menuManager.init();


    });
</script>