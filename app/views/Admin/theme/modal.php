<div id="myModal" class="modal fade bs-example-modal-sm in" tabindex="-1" role="dialog"
aria-labelledby="mySmallModalLabel" aria-hidden="false">
<div class="modal-dialog modal-sm">
    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" id="modalkapat" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title mymodalinh4u" id="mySmallModalLabel">Small modal</h4>
        </div>
        <div class="modal-body mymodalinbodisi">
            ...
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>


<div id="bigModal" class="modal fade bs-example-modal-lg in" tabindex="-1" role="dialog"
aria-labelledby="mySmallModalLabel" aria-hidden="false">
<div class="modal-dialog modal-lg">
    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" id="modalkapatbig" onclick="modalkapat('bigModal')">×</button>
            <h4 class="modal-title mymodalinh4ubig" id="mySmallModalLabel">Small modal</h4>
        </div>
        <div class="clearfix"></div>
        <div class="modal-body mymodalinbodisibig" id="modalAdmin">

        </div>
        <div class="clearfix"></div>
        <hr>
        <div class="clearfix"></div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>


<!-- Edit form modal -->
<div class="modal fade" id="editFormModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="editFormModalTitle">Modal title</h4>
            </div>
            <div class="modal-body">
                <form id="frmMenu">

                    <div class="row">
                        <div class="col-md-6">
                            <label for="text">Menu Adı</label>
                            <div class="form-group">
                                <input type="text" name="text" id="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="url">Url</label>
                            <div class="form-group">
                                <input type="text" name="url" id="url" class="form-control" required>
                            </div>
                        </div>



                    </div>

                    <div class="row" style="display: none;">
                        <div class="col-md-6">
                            <label for="iconClass">Icon Css</label>
                            <div class="form-group">
                                <input type="text" name="iconClass" id="iconClass" class="form-control" >
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="checkbox">
                                <input type="hidden" name="loginRequired" value="0" class="form-control">
                                <input type="checkbox" name="loginRequired" id="loginRequired" value="1" class="form-control">
                                <label for="loginRequired">Login Gerektirir</label>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="display: none;">
                        <input type="hidden" name="tableName" id="tableName" value="">
                        <input type="hidden" name="tableId" id="tableId" value="">
                        <div class="col-md-6">
                            <label for="menuType">Menu Tipi</label>
                            <div class="form-group">
                                <select name="menuType" id="menuType" disabled class="form-control" >
                                    <option value="normal">normal</option>
                                    <option value="kategori">Kategori</option>
                                    <option value="page">Sayfa</option>
                                    <option value="sube">Şube</option>
                                    <option value="bolum">Bölüm</option>
                                    <option value="doktor">Doktor</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active">
                            <a href="#tab1hellowWorld" role="tab" data-toggle="tab" aria-expanded="false">Normal Url</a>
                        </li>
                        <li class="">
                            <a href="#tab1FollowUs" role="tab" data-toggle="tab" aria-expanded="true">Kategori</a>
                        </li>
                        <li class="">
                            <a href="#tab1Inspire" role="tab" data-toggle="tab" aria-expanded="false">Sayfa</a>
                        </li>
                        <li class="">
                            <a href="#tabSubeler" role="tab" data-toggle="tab" aria-expanded="false">Şube</a>
                        </li>
                        <li class="">
                            <a href="#tabBolumler" role="tab" data-toggle="tab" aria-expanded="false">Bolum</a>
                        </li>
                        <li class="">
                            <a href="#tabDoktorlar" role="tab" data-toggle="tab" aria-expanded="false">Doktor</a>
                        </li>


                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1hellowWorld">

                        </div>
                        <div class="tab-pane " id="tab1FollowUs">
                            <div class="row">
                                <div class="col-md-12 row">
                                    <div class="col-md-4">
                                        <label>Arama</label>
                                        <input type="text" name="searchTerm" id="kategorilerSearchTerm">
                                    </div>
                                    <div class="col-md-4">
                                        <button id="kategoriler-search-list" type="button">Ara</button>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="grid-body no-border">
                                        <table class="table no-more-tables" id="kategoriler-table">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1">Id</th>
                                                    <th>Adı</th>
                                                    <th>Seo Name</th>
                                                    <th>İşlem</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab1Inspire">
                            <div class="row">
                                <div class="col-md-12 row">
                                    <div class="col-md-4">
                                        <label>Arama</label>
                                        <input type="text" name="searchTerm" id="pagesSearchTerm">
                                    </div>
                                    <div class="col-md-4">
                                        <button id="pages-search-list" type="button">Ara</button>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="grid-body no-border">
                                        <table class="table no-more-tables" id="pages-table">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1">Id</th>
                                                    <th>Adı</th>
                                                    <th>Seo Name</th>
                                                    <th>İşlem</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabSubeler">
                            <div class="row">
                                <div class="col-md-12 row">
                                    <div class="col-md-4">
                                        <label>Arama</label>
                                        <input type="text" name="searchTerm" id="subelerSearchTerm">
                                    </div>
                                    <div class="col-md-4">
                                        <button id="subeler-search-list" type="button">Ara</button>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="grid-body no-border">
                                        <table class="table no-more-tables" id="subeler-table">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1">Id</th>
                                                    <th>Adı</th>
                                                    <th>Seo Name</th>
                                                    <th>İşlem</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabBolumler">
                            <div class="row">
                                <div class="col-md-12 row">
                                    <div class="col-md-4">
                                        <label>Arama</label>
                                        <input type="text" name="searchTerm" id="bolumlerSearchTerm">
                                    </div>
                                    <div class="col-md-4">
                                        <button id="bolumler-search-list" type="button">Ara</button>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="grid-body no-border">
                                        <table class="table no-more-tables" id="bolumler-table">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1">Id</th>
                                                    <th>Adı</th>
                                                    <th>Seo Name</th>
                                                    <th>İşlem</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabDoktorlar">
                            <div class="row">
                                <div class="col-md-12 row">
                                    <div class="col-md-4">
                                        <label>Arama</label>
                                        <input type="text" name="searchTerm" id="doktorlarSearchTerm">
                                    </div>
                                    <div class="col-md-4">
                                        <button id="doktorlar-search-list" type="button">Ara</button>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="grid-body no-border">
                                        <table class="table no-more-tables" id="doktorlar-table">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1">Id</th>
                                                    <th>Adı</th>
                                                    <th>Seo Name</th>
                                                    <th>İşlem</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave"
                class="btn btn-default btn-round waves-effect">Kaydet
            </button>
            <button type="button" id="btnCancel" class="btn btn-danger waves-effect"
            data-dismiss="modal">Kapat
        </button>
    </div>
</div>
</div>
</div>
