		<?php
		$Page_Name = "";
		$Page_Sub_Name = "";

		$navigation = array(
	////---------------------------------------------------////////////
			'Anasayfa' => array(
				"icon" => '<i class="fa fa-home"></i>',
				"sub" => "0",
				"link" => SITE_URL . "Admin/Dashboard/home",
				"cur" => "Dashboard",

			),
			'Menü Yönetimi' => array(
				"icon" => '<i class="fa fa-th-large"></i>',
				"sub" => "0",
				"link" => SITE_URL . "Admin/Menu",
				"cur" => "Menü/Kategori",

			),

			'Kategori Yönetimi' => array(
				"icon" => '<i class="fa fa-th-large"></i>',
				"sub" => "0",
				"link" => SITE_URL . "Admin/Nav",
				"cur" => "Menü/Kategori",

			),

	///-------------------------------------------------------------------

			'İçerikler' => array(
				"icon" => '<i class="fa fa-th"></i>',
				"sub" => array(
					"Admin/Page/index/2" => "Bizden Haberler",
					"Admin/Page/index/0" => "Kurumsal Sayfalar",
					"Admin/Doktor" => "Doktorlarımız",
					"Admin/Bolum" => "Bölümlerimiz",
					"Admin/Sube" => "Şubelerimiz",

				),
				"link" => "javascript:void(0);",
			),
	///-------------------------------------------------------------------
            ///-------------------------------------------------------------------


            ///-------------------------------------------------------------------

			'Bileşenler' => array(
				"icon" => '<i class="fa fa-cubes"></i>',
				"sub" => array(
					"Admin/Slider2" => "Slider Yönetimi",
				),
				"link" => "javascript:void(0);",
			),

	////---------------------------------------------------////////////
			'Genel Ayarlar' => array(
				"icon" => '<i class="fa fa-flag"></i>',
				"sub" => array(
					"Admin/Generalsettings" => "Sistem Ayarları",
				//	"Admin/Theme" => "Tema Ayarları",
					"Admin/Admin" => "Yönetici İşlemleri",
					"Admin/Diller" => "Dil Yönetimi",
				),
				"link" => "javascript:void(0);",

			),
	////---------------------------------------------------////////////

		);

		?>

		<div class="page-sidebar" id="main-menu">
			<!-- BEGIN MINI-PROFILE -->
			<div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper" style="margin-top: 20px">
				<!-- BEGIN SIDEBAR MENU -->
				<ul>

					<?php $i = 1;foreach ($navigation as $label => $main_link) {?>

						<li class="<?php if ($main_link["sub"] == "0") {?> <?php }?> " id="li<?php echo $i; ?>"  >

							<a href="<?php echo $main_link["link"]; ?>">

								<?php echo $main_link["icon"]; ?>
								<span class="title"> <?php echo $label ?> </span>
								<?php if ($main_link["sub"] != "0") {?> <span class="arrow "></span><?php }?>

							</a>

							<?php if ($main_link["sub"] != "0") {?>

								<ul class="sub-menu">
									<?php foreach ($main_link["sub"] as $sub_link => $sub_label) {?>

										<li class="search-list ">
											<a href="<?php echo SITE_URL . $sub_link ?>" data-id="li<?php echo $i; ?>">
												<?php echo $sub_label ?>
											</a>
										</li>

									<?php }?>

								</ul>
							<?php }?>
						</li>

						<?php $i++;}?>
					</ul>
					<div class="clearfix"></div>
					<!-- END SIDEBAR MENU -->
				</div>
			</div>

			<div class="footer-widget">
				<div class="progress transparent progress-small no-radius no-margin">
					<div data-percentage="0%" class="progress-bar progress-bar-success animate-progress-bar" ></div>
				</div>
				<div class="pull-right">

					<a href="<?php echo SITE_URL . 'Admin/Login/Logout' ?>"><i class="fa fa-power-off"></i></a></div>
				</div>
