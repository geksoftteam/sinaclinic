 
<div class="row">
  <div class="col-md-12">
    <div class="grid simple " style="padding-top:10px;">

     <div class="grid-title no-border">
      <h3 class="content-header pull-left"> </h3>
      <div class="col-lg-5 pull-right text-right">


        <button id="editable-sample_new" class="btn btn-info tipsi"
        onclick="location.href='<?php echo SITE_URL ?>Admin/Page/Insert/<?php echo $tur_index.'/'.$cat_id ?>'"
        title="Yeni Ekleme">
        Ekle <i class="fa fa-plus"></i>
      </button>

    </div>
    <div class="clearfix"></div>
  </div>

  <div class="grid-body no-border">
    <table class="table no-more-tables" id="example">
      <thead>
        <tr>
          <th class="col-md-1">NO</th>
          <th>Başlık/Tanım</th>
          <th>Menü/Kategori</th>
          <th>Durum</th>
          <th>Konum</th>
          <th>Sıra</th>

          <th>İşlem</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1;

        foreach ($content as $content_row) {
         ?>
         <tr class=""
         id="tr_<?php echo $content_row["page_id"]; ?>" >
         <td>
          <?php echo $i; ?>
        </td>



        <td><?php echo is_js($content_row["page_name"]); ?></td>
        <td><?php echo is_js($content_row["nav_name"]); ?>


        <select class="cast dnone" data-id="<?php echo $content_row["page_id"];?>"  >

         <option value="<?php echo $content_row["nav_id"];?>"><?php echo is_js($content_row["nav_name"]); ?></option>
         <?php
         foreach ($urunkat as   $vr) { ?>

           <option value="<?php echo $vr["nav_id"];?>"><?php echo is_js($vr["nav_name"]); ?></option>
         <?php } ?>
       </select>


     </td>


     <td>
      <div class="radio radio-success">

        <input id="yes<?php echo $content_row["page_id"] ?>page_durum"

        type="radio" data-id="<?php echo $content_row["page_id"] . '+page_durum'; ?>"

        class="durumsecpage" name="page_durum<?php echo $content_row["page_id"] ?>" value="1"

        <?php if ($content_row["page_durum"] == "1") {echo 'checked="checked"';}?>>

        <label for="yes<?php echo $content_row["page_id"] ?>page_durum">Aktif</label>

      </div>
      <div class="radio">

        <input id="no<?php echo $content_row["page_id"] ?>page_durum"

        type="radio" name="page_durum<?php echo $content_row["page_id"] ?>"

        data-id="<?php echo $content_row["page_id"] . '+page_durum'; ?>"

        class="durumsecpage" value="2"

        <?php if ($content_row["page_durum"] == "2") {echo 'checked="checked"';}?>>

        <label for="no<?php echo $content_row["page_id"] ?>page_durum">Pasif</label>

      </div></td>


      <td>
        <?php $imp = 0;foreach ($GLOBALS['konum'] as $key => $knm): ?>

        <div class="row-fluid col-md-1">

          <div class="checkbox check-primary checkbox-circle pull-left tipsi"

          title="<?php echo $knm ?>" >

          <input class="konumsec" id="checkbox<?php echo $imp . $content_row["page_id"] ?>"

          name="<?php echo $key . $content_row["page_id"] ?>"
          <?php echo ($content_row[$key] == 1) ? 'checked' : ''; ?>

          type="checkbox" data-id="<?php echo $content_row["page_id"] . '+' . $key; ?>" value="1">

          <label for="checkbox<?php echo $imp . $content_row["page_id"] ?>">  </label>

        </div></div>
        <?php $imp++;endforeach?>

      </td>


      <td>  <input type="text" name="sira" data-id="<?php echo $content_row["page_id"]; ?>+page_sira" style="width:50px;" class="form-control   sirabelirle" value="<?php echo $content_row["page_sira"]; ?>"></td>


      <td>
        <?php if (COPY_STATUS == 1): ?>
          <a href="javascript:;" data-id="<?php echo $content_row["page_id"]; ?>" class="btn btn-success btn-mini  tipsi copy " title="Kopyala" data-original-title="Kopyala"><i class="fa fa-copy"></i></a>
        <?php endif?>


        <a href="<?php echo ADMIN_URL . 'Galeri/index/' . $content_row["page_id"] ?>" class="btn btn-primary btn-mini tipsi" title="" data-original-title="Galeri"><i class="fa fa-picture-o"></i></a>
        

        <a href="<?php echo SITE_URL ?>Admin/Page/Update/<?php echo $content_row["page_id"]; ?>" class="btn btn-info btn-mini tipsi" title="" data-original-title="Düzenle"><i class="fa fa-edit"></i></a>

        <a href="javascript:void(0);" class="btn btn-danger btn-mini tipsi delete_link" title="" data-original-title="Sil"  data-id="<?php echo $content_row["page_id"]; ?>"><i class="fa fa-times"></i></a> 

        <?php if ($content_row['page_kat_id'] == 25): ?>

          <a href="javascript:void(0);" class="btn btn-warning btn-mini tipsi delete_link" title="" data-original-title="Sil"  data-id="<?php echo $content_row["page_id"]; ?>"><i class="fa fa-plus"></i></a>
        <?php endif ?>


      </td>
    </tr>

    <?php $i++;} // end each for main level?>
  </tbody>
</table>
</div>
</div>
</div>
</div>