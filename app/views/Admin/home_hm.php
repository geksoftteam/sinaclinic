<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12"  >
	<!-- aylık analitik gosterimleri start -->
	<div class="col-md-4 col-vlg-3 col-sm-6">
		<div class="tiles green m-b-10">
			<div class="tiles-body">
				<div class="controller"> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
				<div class="tiles-title text-black"><?php $indis = date('m') - 2 + 1;
				echo $aylar[$indis];?> Ayı Ziyaret </div>
				<div class="widget-stats">
					<div class="wrapper transparent">
						<span class="item-title">Tekil Ziyaret</span> <span class="item-count animate-number semi-bold" data-value="<?php echo array_sum($teksi) ?>" data-animation-duration="700">0</span>
					</div>
				</div>
				<div class="widget-stats">
					<div class="wrapper transparent">
						<span class="item-title">Sayfa Gösterimi</span> <span class="item-count animate-number semi-bold" data-value="<?php echo array_sum($coksu) ?>" data-animation-duration="700">0</span>
					</div>
				</div>
				<div class="description"> <span class="text-white mini-description ">Google Analitics <span class="blend">Verileridir</span></span></div>
			</div>
		</div>
	</div>
	<!-- aylık kullanıcı trafiği -->
	<div class="col-md-4 col-vlg-3 col-sm-6">
		<div class="tiles blue m-b-10">
			<div class="tiles-body">
				<div class="controller"> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
				<div class="tiles-title text-black">İçerik Bilgilendirme</div>
				<div class="widget-stats">
					<div class="wrapper transparent">
						<span class="item-title">İçerik Adedi</span> <span class="item-count animate-number semi-bold" data-value="<?php echo $pagescount ?>" data-animation-duration="700">0</span>
					</div>
				</div>
				<div class="widget-stats">
					<div class="wrapper transparent">
						<span class="item-title">Kategori/Menü Adedi</span> <span class="item-count animate-number semi-bold" data-value="<?php echo $catcount ?>" data-animation-duration="700">0</span>
					</div>
				</div>
				<div class="description"> <span class="text-white mini-description ">Pasif olan içerikler <span class="blend">dahildir</span></span></div>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-vlg-3 col-sm-6">
		<div class="tiles purple m-b-10">
			<div class="tiles-body">
				<div class="controller"> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
				<div class="tiles-title text-black">Mobil/Web Analitiği</div>
				<div class="widget-stats">
					<div class="wrapper transparent">
						<span class="item-title">Mobil Ziyaret</span> <span class="item-count animate-number semi-bold" data-value="<?php echo $mobcount ?>" data-animation-duration="700">0</span>
					</div>
				</div>
				<div class="widget-stats">
					<div class="wrapper transparent">
						<span class="item-title">Desktop Ziyaret</span> <span class="item-count animate-number semi-bold" data-value="<?php echo array_sum($teksi) - intval($mobcount); ?>" data-animation-duration="700">0</span>
					</div>
				</div>

				<div class="description"> <span class="text-white mini-description ">Google Analitics <span class="blend">Verileridir</span></span></div>
			</div>
		</div>
	</div>
</div>
</div>

<div id="container" class="col-md-12 col-sm-12 col-xs-12"  ></div>

