<div class="col-md-12">
 <div class="grid simple">
  <div class="grid-title no-border">


  </div>
  <div class="grid-body no-border col-md-12">

    <form action="<?php echo SITE_URL ?>Admin/Generalsettings/Update_Run" method="post" class="form-horizontal row-border col-md-12" id="generalsettings" enctype="multipart/form-data">
      <?php echo $alert; ?>



             <ul class="nav nav-tabs" role="tablist">
                <li class="active">
                    <a href="#tab1hellowWorld" role="tab" data-toggle="tab" aria-expanded="false">Site Ayarları</a>
                </li>
                <li class="">
                    <a href="#tab1FollowUs" role="tab" data-toggle="tab" aria-expanded="true">Kurumsal Ayarlar</a>
                </li>
                <li class="">
                    <a href="#tab1Inspire" role="tab" data-toggle="tab" aria-expanded="false">Sosyal Medya Hesapları</a>
                </li>

            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab1hellowWorld">
                    <div class="row column-seperation">

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Site Adı:</label>

                            <div class="col-sm-9">

                                <input type="text" name="General_Site_Name"  value="<?php echo $content['General_Site_Name']; ?>" class="form-control zor">

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Site Logosu:</label>

                            <div class="col-sm-9">

                                <input type="file" name="resim" onchange="resim_on_izle(this,1)" >

                                <input type="hidden" name="oldresim" value="<?php echo $content['site_logo'] ?>">


                            </div>


                        </div>
                        <div class="form-group">

                            <label class="col-sm-3 control-label">Genel Title

                                <small class="tip_top" title="Seo uyumluluğu için uygun görülen max karakter sayısı 70 dir. fazla girdiğinizde seo puanınızın düşmesi haricinde her hangi bir zarar vermeyektir.">(Page Title max 70 Karakter)</small>:</label>

                            <div class="col-sm-9">

                                <textarea data-say="70" class="form-control say " name="General_title"><?php echo $content['General_title']; ?></textarea>

                                <p class="help-block" id="General_title_say"></p>
                            </div>
                        </div>



                        <div class="form-group">

                            <label class="col-sm-3 control-label">Genel Açıklama

                                <small class="tip_top" title="Anahtar kelime meta etiketi 260 karakterden az olmalıdır. 260 karakterden uzun olması arama motorları tarafından spam olarak yorumlanmaktadır.">(General Desc max 160 Karakter)</small>:</label>

                            <div class="col-sm-9">

                                <textarea name="General_desc" data-say="260" rows="4" class="form-control say " name="General_desc"><?php echo $content['General_desc']; ?></textarea>

                                <p class="help-block" id="General_desc_say"></p>
                            </div>
                        </div>



                        <hr>

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Google Analytics Kodu:</label>

                            <div class="col-sm-9">

                                <textarea  class="form-control " name="googleanalitics"><?php echo $content['googleanalitics']; ?></textarea>

                            </div>

                        </div>


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Analytics Hesabı Maili :</label>

                            <div class="col-sm-9">

                                <input type="text" name="analiticsmail"  value="<?php echo $content['analiticsmail']; ?>" class="form-control">

                            </div>

                        </div>


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Analytics Profil ID :</label>

                            <div class="col-sm-9">

                                <input type="text" name="analiticsprofilid"  value="<?php echo $content['analiticsprofilid']; ?>" class="form-control">

                            </div>

                        </div>



                        <div class="form-group">

                            <label class="col-sm-3 control-label">Analytics Hesabı Şifre Dosyası (.p12 file) :</label>

                            <div class="col-sm-9">

                                <input type="file" name="dosya"  class="form-control">
                                <input type="hidden" name="olddosya"  value="<?php echo $content['analiticspass']; ?>">

                            </div>

                        </div>

                        <hr>
                    </div>
                </div>
                <div class="tab-pane " id="tab1FollowUs">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">

                                <label class="col-sm-3 control-label">Telefon :</label>

                                <div class="col-sm-9">

                                    <input type="text" name="tel"  value="<?php echo $content['tel']; ?>" class="form-control">

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="col-sm-3 control-label">GSM :</label>

                                <div class="col-sm-9">

                                    <input type="text" name="gsm"  value="<?php echo $content['gsm']; ?>" class="form-control">

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="col-sm-3 control-label">Fax :</label>

                                <div class="col-sm-9">

                                    <input type="text" name="fax"  value="<?php echo $content['fax']; ?>" class="form-control">

                                </div>

                            </div>
                            <div class="form-group">

                                <label class="col-sm-3 control-label">Adres:</label>

                                <div class="col-sm-9">

                                    <textarea  class="form-control " rows="5" name="adres"><?php echo $content['adres']; ?></textarea>

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="col-sm-3 control-label">Mail Adresi:</label>

                                <div class="col-sm-9">

                                    <input type="text" name="mail"  value="<?php echo $content['mail']; ?>" class="form-control">


                                </div>

                            </div>


                            <div class="form-group">

                                <label class="col-sm-3 control-label">Maps Kodu:</label>

                                <div class="col-sm-9">

                                    <textarea  class="form-control" rows="5" name="maps"><?php echo $content['maps']; ?></textarea>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab1Inspire">
                    <div class="row">
                        <div class="col-md-12">



                            <?php




                            $soc = json_decode($content['social']);

                            foreach ($sociallink as  $var): ?>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $var ?></label>
                                    <div class="col-sm-9">

                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-<?php echo $var ?>"></i></div>
                                            <input type="text" class="form-control" name="social[<?php echo $var ?>]" value="<?php echo $soc->$var ?>" >

                                        </div>
                                    </div>

                                </div>

                            <?php endforeach ?>

                        </div>
                    </div>
                </div>
            </div>


          <div class="bottom text-right">
            <button type="button" data-id="generalsettings" class="btn btn-primary submit_btn">Güncelle</button>

          </div>

        </form>
      </div><!--/porlets-content-->
    </div><!--/block-web--> 
  </div><!--/col-md-6-->

