<!DOCTYPE html>
<html lang="tr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo is_js($cat['nav_name']) ?></title>
	<meta name="keywords" content="<?php echo is_js($cat['nav_keyw']) ?>" />
	<meta name="description" content="<?php echo is_js($cat['nav_desc']) ?>" />

	<?php include 'theme/src.php'; ?>

</head>

<body>

	<!--page start-->
	<div class="page">

		<!-- preloader start -->

		<!-- preloader end -->

		<!--header start-->
		<?php include 'theme/header.php'; ?>


		<div class="ttm-page-title-row">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title-box text-center">
							<div class="page-title-heading">
								<h1 class="title"><?php echo is_js($cat['nav_name']) ?></h1>
							</div><!-- /.page-title-captions -->
							<div class="breadcrumb-wrapper">
								<span>
									<a title="Ana Sayfa" href="<?php echo SITE_URL ?>">
										<i class="ti ti-home"></i>
									</a>
								</span>
								<span class="ttm-bread-sep">&nbsp; : : &nbsp;</span>
								<span><a href="<?php echo SITE_URL.$cat['nav_url'] ?>"><?php echo is_js($cat['nav_name']) ?></a></span>
							</div>
						</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div>

		<div class="site-main">
			<!-- sidebar -->
			<section class="ttm-row grid-section clearfix">
				<div class="container">

					<div class="row">
						<?php  foreach ($cat_page as $g):  ?>

							<div class="col-md-6 col-lg-4">
								<!-- featured-imagebox-services -->
								<div class="featured-imagebox featured-imagebox-services style1 mb-30">

									<?php if ($g['page_image']!=""): ?>
										<div class="featured-thumbnail"><!-- featured-thumbnail -->
											<img class="img-fluid" src="<?php echo SITE_UPLOAD_DIR.'page/'.$g['page_image'] ?>" alt="<?php echo is_js($g['page_name']) ?>" style="min-height: 300px; max-height: 300px;">
										</div>

										<?php else: ?>
											<div class="featured-thumbnail"><!-- featured-thumbnail -->
												<img class="img-fluid" src="<?php echo SITE_URL.'noimage.png' ?>" alt="<?php echo is_js($g['page_name']) ?>" style="min-height: 300px; max-height: 300px;">
											</div>
										<?php endif ?>

										<div class="featured-content box-shadow katminmax">
											<div class="featured-title"><!-- featured-title -->
												<h5><a href="<?php echo SITE_URL.$g['nav_url'].'/'.$g['page_url'] ?>">
													<?php echo is_js($g['page_name']) ?></a>
												</h5>
											</div>
											<div class="featured-desc"><!-- featured-title -->
												<p><?php echo kisalt(is_js($g['page_jenerik']),70) ?></p>
											</div>
											<a class="ttm-btn ttm-btn-size-sm ttm-btn-color-skincolor btn-inline ttm-icon-btn-right mt-10" href="<?php echo SITE_URL.$g['nav_url'].'/'.$g['page_url'] ?>">Detay<i class="ti ti-angle-double-right"></i></a>
										</div>
									</div>
								</div>

							<?php endforeach ?>
						</div>
						<!-- row end-->
					</div>
				</section>
			</div>

			<?php include 'theme/footer.php'; ?>

			<a id="totop" href="#top">
				<i class="fa fa-angle-up"></i>
			</a>

		</div><!-- page end -->


		<?php include 'theme/js.php'; ?>


	</body>

	</html>
