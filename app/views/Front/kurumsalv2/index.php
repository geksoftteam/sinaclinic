<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $set['General_title'] ?></title>
    <meta name="keywords" content="<?php echo $set['General_keyw'] ?>" />
    <meta name="description" content="<?php echo $set['General_desc'] ?>" />

    <?php include 'theme/src.php'; ?>

</head>

<body>

    <!--page start-->
    <div class="page">

        <!-- preloader start -->

        <!-- preloader end -->

        <!--header start-->
        <?php include 'theme/header.php'; ?>


        <?php include 'theme/slider.php'; ?>


        <!--site-main start-->
        <div class="site-main">

            <!-- aboutus-section -->

            <!-- aboutus-section end -->


            <!-- services-section -->
            <section class="ttm-row services-section ttm-bgcolor-darkgrey ttm-bg ttm-bgimage-yes bg-img7 clearfix">
                <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
                <div class="container">
                    <div class="row"><!-- row -->
                        <div class="col-lg-6 offset-lg-3">
                            <!-- section title -->

                        </div>
                    </div>
                    <!-- row end -->
                    <!-- row -->

                </div>
            </section>
            <!-- services-section end -->

            <!-- topzero-padding-section -->
            <section class="ttm-row zero-padding-section mt_95 res-991-mt-0 clearfix">
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-lg-3">
                            <!-- col-bg-img-three -->
                            <div class="col-bg-img-three ttm-bg ttm-col-bgimage-yes res-991-h-auto">
                             <div class="ttm-col-wrapper-bg-layer ttm-bg-layer">
                                <div class="ttm-bg-layer-inner"></div>
                            </div>
                        </div>
                        <!-- Responsive View image -->
                        <img src="<?php echo $theme_patch ?>/images/bg-image/col-bgimage-3.jpg" class="ttm-equal-height-image" alt="col-bgimage-3">
                    </div>
                    <div class="col-lg-9">
                        <div class="ttm-bgcolor-skincolor ttm-bg ttm-col-bgcolor-yes ttm-right-span">
                            <div class="ttm-col-wrapper-bg-layer ttm-bg-layer">
                                <div class="ttm-bg-layer-inner"></div>
                            </div>
                            <div class="layer-content">
                                <div class="spacing-6 ttm-textcolor-white">
                                    <h3 class="mb-5"><?= $dil_set[31] ?></h3>
                                    <p class="mb-0"><?= $dil_set[32] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- topzero-padding-section -->

        <!-- tab-section -->
        <section class="ttm-row tab-section clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- section title -->
                        <div class="section-title text-center with-desc clearfix">
                            <div class="title-header">
                                <h2>Eğitimlerimiz</h2>

                            </div>
                        </div><!-- section title end -->
                    </div>
                </div>
                <!-- row end -->
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ttm-tabs text-center ttm-tab-style-classic style1">
                            <ul class="tabs mb-20">

                                <?php $i =0; foreach ($hizmetler as $var): ?>


                                <li class="tab <?= $i == 0 ? 'active':'' ?>" data-id="<?= $var['page_id'] ?>">
                                    <a href="javascript:;" data-id="<?= $var['page_id'] ?>"> <i class="flaticon flaticon-report"></i> <?= is_js($var['page_name']) ?> </a>
                                </li>

                                <?php $i++; endforeach ?>
                            </ul>

                            <div class="content-tab width-100 box-shadow">
                                <!-- content-inner -->
                                <?php $i = 0; foreach ($hizmetler as $var): ?>

                                <div class="content-inner services_cont_boxes <?= $i == 0 ?'':'dnone' ?>" id="services_<?= $var['page_id'] ?>">
                                    <div class="row">


                                        <div class="col-lg-<?= $var['page_image']!="" ? '6':'12' ?>">
                                            <div class="text-left">
                                                <h3 class="title fs-30"><?= is_js($var['page_name']) ?></h3>
                                                <p><?= is_js($var['page_content']) ?></p>


                                            </div>
                                        </div>

                                        <?php if ($var['page_image']!=""): ?>
                                           <div class="col-lg-6">
                                            <div class="grey-border res-991-mt-30 text-left">
                                                <!-- ttm_single_image-wrapper -->
                                                <div class="ttm_single_image-wrapper with-border15">
                                                    <img class="img-fluid grey-boder" src="<?php echo SITE_UPLOAD_DIR.'page/'.$var['page_image'] ?>" title="single-img-three" alt="single-img-three">
                                                </div><!-- ttm_single_image-wrapper end -->
                                            </div>
                                        </div>
                                    <?php endif ?>

                                </div><!-- row end -->
                            </div><!-- content-inner --><!-- row end-->
                            <?php $i++; endforeach ?>


                        </div><!-- content-inner --><!-- row end-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ttm-row  ttm-bgcolor-grey ttm-bg ttm-bgimage-yes bg-img14 clearfix">
    <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- section title -->
                <div class="section-title text-center with-desc clearfix">
                    <div class="title-header">
                        <h2>Bizen Haberler</h2>

                    </div>
                </div><!-- section title end -->
            </div>
        </div>
        <!-- row end -->
        <!-- row -->
        <div class="row">
            <div  class="services-slide owl-carousel" data-item="3" data-nav="false" data-dots="false" data-auto="false">
                <!-- featured-imagebox-services -->
                <?php foreach ($blog as $etkinlik): ?>

                    <div class="featured-imagebox featured-imagebox-services style1">
                        <div class="featured-thumbnail"><!-- featured-thumbnail -->
                            <img class="img-fluid" src="<?php echo SITE_UPLOAD_DIR.'page/360x200_'.$etkinlik['page_image'] ?>" alt="image">
                        </div>
                        <div class="featured-content box-shadow">
                            <div class="featured-title"><!-- featured-title -->
                                <h5><a href="<?php echo SITE_URL.$etkinlik['nav_url'].'/'.$etkinlik['page_url'] ?>"><?= is_js($etkinlik['page_name']) ?></a></h5>
                            </div>
                            <div class="featured-desc"><!-- featured-title -->
                                <p><?= kisalt(is_js($etkinlik['page_jenerik']),150) ?></p>
                            </div>
                            <a class="ttm-btn ttm-btn-size-sm ttm-btn-color-skincolor btn-inline ttm-icon-btn-right mt-10" href="<?php echo SITE_URL.$etkinlik['nav_url'].'/'.$etkinlik['page_url'] ?>">Detaylar <i class="ti ti-angle-double-right"></i></a>
                        </div>
                    </div>

                <?php endforeach ?>
                <!-- featured-imagebox-services -->
                <!-- featured-imagebox-services -->

            </div>
        </div>
        <!-- row end-->
    </div>
</section>



</div><!--site-main end-->

<!--footer start-->
<?php include 'theme/footer.php'; ?>
<!--footer end-->

<!--back-to-top start-->
<a id="totop" href="#top">
    <i class="fa fa-angle-up"></i>
</a>
<!--back-to-top end-->

</div><!-- page end -->

<!-- Javascript -->

<?php include 'theme/js.php'; ?>

<!-- Javascript end-->

</body>

</html>
