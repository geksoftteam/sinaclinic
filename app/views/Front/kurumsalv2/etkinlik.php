<!DOCTYPE html>
<html lang="tr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo is_js($page['page_title']) ?></title>

	<meta name="description" content="<?php echo is_js($page['page_desc']) ?>" />

	<?php include 'theme/src.php'; ?>

</head>

<body>

	<!--page start-->
	<div class="page">

		<!-- preloader start -->

		<!-- preloader end -->

		<!--header start-->
		<?php include 'theme/header.php'; ?>


		<div class="ttm-page-title-row">
			<div class="container">
				<div class="row">
					<div class="col-md-12"> 
						<div class="title-box text-center">
							<div class="page-title-heading">
								<h1 class="title"><?php echo is_js($page['page_title']) ?></h1>
							</div><!-- /.page-title-captions -->
							<div class="breadcrumb-wrapper">
								<span>
									<a title="Ana Sayfa" href="<?php echo SITE_URL ?>">
										<i class="ti ti-home"></i>
									</a>
								</span>
								<span class="ttm-bread-sep">&nbsp; : : &nbsp;</span>
								<span><a href="<?php echo SITE_URL.$cat['nav_url'] ?>"><?php echo is_js($cat['nav_name']) ?></a></span>
							</div>  
						</div>
					</div><!-- /.col-md-12 -->  
				</div><!-- /.row -->  
			</div><!-- /.container -->                      
		</div>

		<div class="site-main">
			<section class="ttm-row project-single-section clearfix">
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-lg-6 col-xl-8">

							<div class="ttm_single_image-wrapper mb-35">
								
								<img class="img-fluid" src="<?php echo SITE_UPLOAD_DIR.'page/'.$page['page_image'] ?>" title="single-img-twenty" alt="single-img-twenty">

							</div> 

						</div>
						<div class="col-lg-6 col-xl-4">
							<div class="ttm-pf-single-detail-box mb-35">
								<div class="ttm-pf-single-title">
									<h5>Etkinlik Bilgileri</h5>
								</div>
								<p><?php echo is_js($page['page_jenerik']) ?></p>
								<ul class="ttm-pf-detailbox-list">

									<?php if (!empty($page['etkinlik_kisi'])): ?>

										<li><i class="fa fa-user"></i>
											<span> Etkinlik Sahibi : </span><?php echo is_js($page['etkinlik_kisi']) ?>
										</li>

									<?php endif ?>
									<?php if (!empty($page['etkinlik_tarihi'])): ?>

										<li><i class="fa fa-calendar"></i><span> Etkinlik Tarihi :  </span><?php echo is_js($page['etkinlik_tarihi']) ?></li>

									<?php endif ?>


								</ul>
							</div>
						</div>
					</div>
					<!-- row end-->
					<!-- row -->
					<div class="row">
						<div class="col-12">
							<h4><?php echo is_js($page['page_name']) ?></h4>
							<?php echo is_js($page['page_content']) ?>
							<hr>

							<?php if (!empty($gallery)): ?>
								<div class="row multi-columns-row ttm-boxes-spacing-5px">
									<?php foreach ($gallery as $g): ?>
										<div class="col-lg-4 col-md-6 ttm-box-col-wrapper">

											<div class="featured-imagebox featured-imagebox-portfolio style1">

												<div class="featured-thumbnail">
													<img class="img-fluid" src="<?php echo SITE_UPLOAD_DIR.'gallery/'.$g['galeri_resim'] ?>" alt="image">
												</div> 
												<div class="ttm-box-view-overlay">
													<div class="featured-iconbox ttm-media-link"> 
														<a class="ttm_prettyphoto ttm_image" data-gal="prettyPhoto[gallery1]" title="" data-rel="prettyPhoto" href="<?php echo SITE_UPLOAD_DIR.'gallery/'.$g['galeri_resim'] ?>"><i class="ti ti-search"></i></a>
													</div>
												</div> 

											</div> 
										</div>
									<?php endforeach ?>
								</div>
							<?php endif ?>

						</div>





					</div><!-- row end-->
					<!-- row -->
					<hr>
					<div class="row">
						<!-- blog-slide -->
						<div class="col-md-12 mt40">
							<!-- section title -->
							<div class="section-title text-center with-desc clearfix">
								<div class="title-header">
									<h2 class="title">Yaklaşan Etkinlikler </h2>
								</div>
							</div><!-- section title end -->
						</div>
						<div class="blog-slide owl-carousel owl-theme owl-loaded " data-item="3" data-nav="false" data-dots="false" data-auto="false">
							
							<!-- featured-imagebox-blog -->
							<?php foreach ($etkinliklist as $key => $value): ?>
								

								<div class="featured-imagebox featured-imagebox-blog">
									<div class="featured-thumbnail"><!-- featured-thumbnail -->
										<img class="img-fluid" src="<?php echo SITE_UPLOAD_DIR.'page/'.$value['page_image'] ?>" alt="">
										<div class="ttm-blog-overlay-iconbox">
											<a href="<?php echo SITE_URL.$value['nav_url'].'/'.$value['page_url'] ?>"><i class="ti ti-plus"></i></a>
										</div>
										<div class="ttm-box-view-overlay"></div>
									</div>
									<div class="featured-content"><!-- featured-content -->
										<div class="ttm-box-post-date"><!-- ttm-box-post-date -->
											<span class="ttm-entry-date">
												<time class="entry-date" datetime="<?php echo is_js($value['etkinlik_tarihi']) ?>">
													<span class="entry-month entry-year">
														<?php echo is_js($value['etkinlik_tarihi']) ?>

													</span>
												</time>
											</span>
										</div>
										<div class="featured-title"><!-- featured-title -->
											<h5><a href="<?php echo SITE_URL.$value['nav_url'].'/'.$value['page_url'] ?>"><?php echo is_js($value['page_name']) ?></a></h5>
										</div>

									</div>
								</div>

							<?php endforeach ?>
						</div>
					</div><!-- row end --> 
				</div>
			</section>
		</div>

		<?php include 'theme/footer.php'; ?>

		<a id="totop" href="#top">
			<i class="fa fa-angle-up"></i>
		</a>

	</div><!-- page end -->


	<?php include 'theme/js.php'; ?>


</body>

</html>