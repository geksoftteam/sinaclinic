<!DOCTYPE html>
<html lang="tr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo is_js($page['page_title']) ?></title>

	<meta name="description" content="<?php echo is_js($page['page_desc']) ?>" />

	<?php include 'theme/src.php'; ?>

</head>

<body>

	<!--page start-->
	<div class="page">

		<!-- preloader start -->

		<!-- preloader end -->

		<!--header start-->
		<?php include 'theme/header.php'; ?>


		<div class="ttm-page-title-row">
			<div class="container">
				<div class="row">
					<div class="col-md-12"> 
						<div class="title-box text-center">
							<div class="page-title-heading">
								<h1 class="title"><?php echo is_js($page['page_title']) ?></h1>
							</div><!-- /.page-title-captions -->
							<div class="breadcrumb-wrapper">
								<span>
									<a title="Ana Sayfa" href="<?php echo SITE_URL ?>">
										<i class="ti ti-home"></i>
									</a>
								</span>
								<span class="ttm-bread-sep">&nbsp; : : &nbsp;</span>
								<span><a href="<?php echo SITE_URL.$cat['nav_url'] ?>"><?php echo is_js($cat['nav_name']) ?></a></span>
							</div>  
						</div>
					</div><!-- /.col-md-12 -->  
				</div><!-- /.row -->  
			</div><!-- /.container -->                      
		</div>

		<div class="site-main">
			<!-- sidebar -->
			<div class="sidebar ttm-sidebar ttm-bgcolor-white clearfix">
				<div class="container contain">
					<!-- row -->
					<div class="row">
						<div class="col-lg-9 content-area order-lg-2 kirap">
							<!-- ttm-service-single-content-are -->
							<!-- ttm-service-single-content-are -->
							<div class="ttm-service-single-content-area">
								
								<?php if ($page['page_image']!=""): ?>
									<div class="ttm_single_image-wrapper mb-35">
										<img class="img-fluid" src="<?php echo SITE_UPLOAD_DIR.'page/'.$page['page_image'] ?>" alt="single-img-twelve">
									</div>
								<?php endif ?>



								<div class="ttm-service-description">
									<h2><?php echo is_js($page['page_name']) ?></h2>
									<?php echo is_js($page['page_content']) ?>


								</div>
							</div>
							<!-- ttm-service-single-content-are end -->
						</div>
						<div class="col-lg-3 widget-area">
							<aside class="widget widget-nav-menu">
								
								<ul class="widget-menu">

									<?php foreach ($sidemenu as $key => $side): ?>
										<li class="<?php echo ($side['page_id']==$page['page_id'])?'active':''; ?>">
											<a href="<?php echo SITE_URL.$cat['nav_url'].'/'.$side['page_url'] ?>"><?php echo is_js($side['page_name']) ?></a>
										</li>
									<?php endforeach ?>
								</ul>
							</aside>


					 
						</div>
					</div><!-- row end -->
				</div>
			</div>
			
			<!-- sidebar end -->
		</div>

		<?php include 'theme/footer.php'; ?>

		<a id="totop" href="#top">
			<i class="fa fa-angle-up"></i>
		</a>

	</div><!-- page end -->


	<?php include 'theme/js.php'; ?>


</body>

</html>