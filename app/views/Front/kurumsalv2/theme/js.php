<script src="<?php echo $theme_patch ?>/js/jquery.min.js"></script>
<script src="<?php echo $theme_patch ?>/js/tether.min.js"></script>
<script src="<?php echo $theme_patch ?>/js/bootstrap.min.js"></script>
<script src="<?php echo $theme_patch ?>/js/jquery.easing.js"></script>    
<script src="<?php echo $theme_patch ?>/js/jquery-waypoints.js"></script>    
<script src="<?php echo $theme_patch ?>/js/jquery-validate.js"></script> 
<script src="<?php echo $theme_patch ?>/js/owl.carousel.js"></script>
<script src="<?php echo $theme_patch ?>/js/jquery.prettyPhoto.js"></script>
<script src="<?php echo $theme_patch ?>/js/numinate.min6959.js?ver=4.9.3"></script>
<script src="<?php echo $theme_patch ?>/js/lazysizes.min.js"></script>
<script src="<?php echo $theme_patch ?>/js/main.js"></script>

<!-- Revolution Slider -->
<script src="<?php echo $theme_patch ?>/revolution/js/revolution.tools.min.js"></script>
<script src="<?php echo $theme_patch ?>/revolution/js/rs6.min.js"></script>
<script src="<?php echo $theme_patch ?>/revolution/js/slider.js"></script>
