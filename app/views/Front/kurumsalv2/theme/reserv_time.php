<?php $saat = array(
  '00:00 01:00','01:00 02:00','02:00 03:00','03:00 04:00','04:00 05:00','05:00 06:00','06:00 07:00','07:00 08:00','08:00 09:00',
  '09:00 10:00','10:00 11:00','11:00 12:00','12:00 13:00','13:00 14:00','14:00 15:00','15:00 16:00','16:00 17:00',
  '17:00 18:00','18:00 19:00','19:00 20:00','20:00 21:00','21:00 22:00','22:00 23:00','23:00 00:00'
); ?>
<?php if (count($reservation)>0): ?>

<?php $rezerb = 0; $i = 0; foreach ($saat as $key => $value): ?>
  <?php foreach ($reservation as $keys => $var): ?>
    <?php if ($var['reservation_time'] == $value): $rezerb = 1?>

    <?php endif; ?>
<?php endforeach; ?>
<div class="col-xs-2 p0">
   <button data-saat="<?php echo $value ?>" type="button" <?php echo $rezerb == 1 ? 'disabled':'' ?> class=" btn-default saatbtn " name="button"><?php echo $value ?></button>
  </div>

<?php if ($var['reservation_time'] != $value): $i++;?>

<?php endif; ?>
<?php $rezerb = 0; endforeach; ?>
 <?php else: ?>


   <?php $i = 0; foreach ($saat as $key => $value): ?>

   <div class="col-xs-2 p0">
      <button data-saat="<?php echo $value ?>" type="button"  class=" btn-default saatbtn <?php echo  $i == 0 ? 'active':'' ?>" name="button"><?php echo $value ?></button>
     </div>
   <?php $i++; endforeach; ?>

<?php  endif; ?>
