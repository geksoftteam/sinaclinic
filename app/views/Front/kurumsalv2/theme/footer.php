<footer class="footer widget-footer clearfix">
	
	<div class="second-footer ttm-textcolor-white bg-img2">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 widget-area">
					<div class="widget widget_text  clearfix">
						<h3 class="widget-title"><?= $set['General_title'] ?></h3>
						
						<div class="quicklink-box">
							<!--  featured-icon-box -->
							<div class="featured-icon-box left-icon">
								<div class="featured-icon"><!--  featured-icon -->
									<div class="ttm-icon ttm-icon_element-style-round ttm-icon_element-bgcolor-skincolor ttm-icon_element-size-md ttm-icon_element-style-round">
										<span class="flaticon flaticon-clock"></span><!--  ttm-icon -->
									</div>
								</div>
								<div class="featured-content"><!--  featured-content -->
									<div class="featured-desc"><!--  featured-desc -->
										<p>Bize Ulaşın</p>
									</div>
									<div class="featured-title"><!--  featured-title -->
										<h5><?php echo $set['tel'] ?></h5>
									</div>
									<div class="featured-title"><!--  featured-title -->
										<h5><?php echo $set['gsm'] ?></h5>
									</div>
								</div>
							</div><!--  featured-icon-box END -->
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2 widget-area">
					<div class="widget link-widget clearfix">
						<h3 class="widget-title">Hızlı Erişim</h3>
						<ul id="menu-footer-services">
							<?php foreach ($nav as $key => $menu): ?>
								<li><a href="<?php echo SITE_URL.$menu['nav_url'] ?>"><?php echo is_js($menu['nav_name']) ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 widget-area">
					<div class="widget widget_text clearfix">
						<h3 class="widget-title">Blogdan Başlıklar</h3>
						<ul class="widget-post ttm-recent-post-list">

							<?php  $i=0;

							foreach ($blog as $key => $etkinlik): ?>

								<li>

									<a href="<?php echo SITE_URL.$etkinlik['nav_url'].'/'.$etkinlik['page_url'] ?>"><?php echo is_js($etkinlik['page_name']) ?></a>

								</li>

								<?php $i++; endforeach ?>

							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 widget-area">
						<div class="widget flicker_widget clearfix">
							<h3 class="widget-title">Haberiniz Olsun</h3>
							<div class="textwidget widget-text">
								Yaklaşan etkinliklerimiz, gelişmelerden haberdar olmak için mail adresinizi ekleyebilirsiniz.
								<form id="subscribe-form" class="newsletter-form" method="post" action="#" data-mailchimp="true">
									<div class="mailchimp-inputbox clearfix" id="subscribe-content">
										<i class="fa fa-envelope"></i>
										<input type="email" name="email" placeholder="Mail Adresiniz..." required="">
										<input type="submit" value="">
									</div>
									<div id="subscribe-msg"></div>
								</form>
								<h5 class="mb-20">Bizi Takip Edin</h5>
								<div class="social-icons circle social-hover">
									<ul class="list-inline">

										<?php foreach (json_decode($set['social']) as $key => $value): ?>

											<?php if ($value !='#'): ?>
												<li class="social-<?php echo $key ?>"><a class="tooltip-top" target="_blank" href="<?php echo $value ?>" data-tooltip="<?php echo $key ?>"><i class="fa fa-<?php echo $key ?>" aria-hidden="true"></i></a></li>

											<?php endif ?>
										<?php endforeach ?>

									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-footer-text ttm-bgcolor-darkgrey ttm-textcolor-white">
			<div class="container">
				<div class="row copyright">
					<div class="col-md-6">
						<div class="">
							<span>Copyright © <?php echo date('Y') ?>&nbsp;<?php echo $set['General_Site_Name'] ?></span>
						</div>
					</div>

				</div>
			</div>
		</div>
	</footer>
