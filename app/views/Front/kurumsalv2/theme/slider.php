<!-- START homeclassicmain REVOLUTION SLIDER 6.0.1 -->
<rs-module-wrap id="rev_slider_1_1_wrapper" data-source="gallery">
  <rs-module id="rev_slider_1_1" style="" data-version="6.0.1" class="rev_slider_1_1_height">



   <rs-slides>
      <?php $i = 1; foreach ($slider as $var): ?>
      <!-- rs-slide -->
      <rs-slide data-key="rs-<?php echo $i?>" data-title="Slide" data-thumb="<?php echo SITE_UPLOAD_DIR.'slider/'.$var['slider2_resim'] ?>" data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;">
         <img src="<?php echo SITE_UPLOAD_DIR.'slider/'.$var['slider2_resim'] ?>" title="slider-mainbg-003" width="1920" height="790" class="rev-slidebg" data-no-retina>
         <?php if($var['slider2_link']!='#') :?>
          
         <?php endif ?>
         <rs-layer
         id="slider-2-slide-<?php echo $i?>-layer-3" class="slidertext"
         data-type="text"
         data-color="#263045"
         data-rsp_ch="on"
         data-xy="x:l,l,l,c;xo:50px,50px,40px,0;yo:363px,233px,124px,59px;"
         data-text="w:normal;s:35,52,45,37;l:70,60,50,60;fw:600;"
         data-frame_0="x:-50,-50,-31,-19;"
         data-frame_1="e:Linear.easeNone;st:260;sp:800;sR:260;"
         data-frame_999="o:0;st:w;sR:7940;"
         ><?php echo is_js($var['slider2_name']) ?>

      </rs-layer>




   </rs-slide>

   <?php $i++; endforeach ?>

</rs-slides>
</rs-module>
</rs-module-wrap>
