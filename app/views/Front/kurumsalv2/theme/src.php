    <!-- favicon icon -->
    <link rel="shortcut icon" href="<?php echo $theme_patch ?>/images/favicon.png" />

    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/bootstrap.min.css"/>

    <!-- animate -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/animate.css"/>

    <!-- owl-carousel -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/owl.carousel.css">

    <!-- fontawesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/font-awesome.css"/>

    <!-- themify -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/themify-icons.css"/>

    <!-- flaticon -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/flaticon.css"/>


    <!-- REVOLUTION LAYERS STYLES -->

    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/revolution/css/rs6.css">

    <!-- prettyphoto -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/prettyPhoto.css">

    <!-- shortcodes -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/shortcodes.css"/>

    <!-- main -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/main.css"/>

    <!-- responsive -->
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_patch ?>/css/responsive.css"/>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <input type="hidden" name="url" class="url" id="url" value="<?= SITE_URL ?>">