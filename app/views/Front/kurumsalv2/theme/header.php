      <header id="masthead" class="header ttm-header-style-01">

      	<!-- ttm-header-wrap -->
      	<div class="ttm-header-wrap">
      		<!-- ttm-stickable-header-w -->
      		<div id="ttm-stickable-header-w" class="ttm-stickable-header-w clearfix">
      			<!-- ttm-topbar-wrapper -->
      			<div class="ttm-topbar-wrapper clearfix">
      				<div class="container">
      					<div class="ttm-topbar-content">
      						<ul class="top-contact text-left">
                   <li><a href="tel:<?php echo $set['gsm'] ?>"><i class="fa fa-phone"></i><?php echo $set['gsm'] ?></a></li>
                   <li><i class="fa fa-envelope-o"></i><a href="mailto:<?php echo $set['mail']; ?>"><?php echo $set['mail']; ?></a></li>
                 </ul>
                 <div class="topbar-right text-right">

                   <div class="ttm-social-links-wrapper list-inline">
                    <ul class="social-icons">
                      <?php foreach (json_decode($set['social']) as $key => $value): ?>

                        <?php if ($value !='#'): ?>
                          <li><a href="<?php echo $value ?>" class=" tooltip-bottom" data-tooltip="<?php echo $key ?>"><i class="fa fa-<?php echo $key ?>"></i></a>
                          </li>
                        <?php endif ?>
                      <?php endforeach ?>
                    </ul>
                  </div>
                  <div class="header-btn">

                    <a class="ttm-btn ttm-btn-size-md  ttm-btn-bgcolor-skincolor" href="<?php echo SITE_URL.'Iletisim' ?>">Bize Ulaşın</a>
                    
                  </div>
                </div>
              </div>
            </div>
          </div> 
          <div id="site-header-menu" class="site-header-menu">
            <div class="site-header-menu-inner ttm-stickable-header">
             <div class="container">

              <div class="site-branding">
                <a class="home-link" href="<?php echo SITE_URL ?>" title="<?php echo $set['General_title'] ?>">
                  <img id="logo-img" class="img-center" src="<?= SITE_URL.'logo.png' ?>" alt="logo">
                </a>
              </div> 
              <div id="site-navigation" class="site-navigation">
               <div class="ttm-rt-contact">

                <div class="ttm-header-icons ">


                </div> 
              </div>
              <div class="ttm-menu-toggle">
                <input type="checkbox" id="menu-toggle-form" />
                <label for="menu-toggle-form" class="ttm-menu-toggle-block">
                 <span class="toggle-block toggle-blocks-1"></span>
                 <span class="toggle-block toggle-blocks-2"></span>
                 <span class="toggle-block toggle-blocks-3"></span>
               </label>
             </div>
             <nav id="menu" class="menu">
              <ul class="dropdown">
                <?php   foreach ($nav as $li):  ?>
                  <?php $footer_link[] = $li ?>
                  <?php $link = (count($li['sub']) <2) ? SITE_URL . $li['nav_url'] : SITE_URL .$li['nav_url']  ;?> 
                  <li >
                    <a href="<?php echo $link ?>"  <?php if(count($li['sub']) >=1) { 
                      echo 'class="dropdown"';}?> title="<?php echo is_js($li['nav_name']) ?>">
                      <?php echo is_js($li['nav_name']) ?>  

                    </a>
                    <?php if (count($li['sub']) >= 1): ?>

                      <ul class="sub-menu">
                        <?php if ($li['sub_type'] == 'menu'): ?>
                          <?php foreach ($li['sub'] as $var):   ?>
                            <li><a href="<?php echo SITE_URL . $var['nav_url'] ?>" title="<?php echo is_js($var['nav_name']) ?>"><?php echo kisalt(is_js($var['nav_name']), 30) ?></a>
                              <?php if (isset($var['sub_nav'])): ?>

                                <?php if (count($var['sub_nav']) >= 1): ?>
                                  <ul class="sub-menu">
                                    <?php foreach ($var['sub_nav'] as $value): ?>

                                      <li><a href="<?php echo SITE_URL . $value['nav_url'] ?>" title="<?php echo is_js($value['nav_name']) ?>"><?php echo  kisalt(is_js($value['nav_name']), 40) ?></a>
                                      <?php endforeach ?>

                                    </ul>
                                  <?php endif ?>
                                <?php endif ?>

                              </li>
                            <?php endforeach?>
                          <?php endif?>
                          <?php if ($li['sub_type'] == 'content'): ?>
                            <?php foreach ($li['sub'] as $var): ?>
                              <li><a href="<?php echo SITE_URL . $var['nav_url'] . '/' . $var['page_url'] ?>" title="<?php echo is_js($var['page_name']) ?>"><?php echo kisalt(is_js($var['page_name']), 40) ?></a></li>
                            <?php endforeach?>
                          <?php endif?>
                        </ul>
                      <?php endif?>
                    </li>
                  <?php endforeach?>  
                  <li>
                    <a href="<?= SITE_URL.'Rezervasyon' ?>">
                      <button class="btn btn-success"> Online Rezervasyon </button>
                      
                    </a>
                  </li>
                </ul>
              </nav>
            </div> 
          </div>
        </div>
      </div>
    </div> 
  </div> 

</header> 