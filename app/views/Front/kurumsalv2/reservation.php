<!DOCTYPE html>
<html lang="tr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Rezervasyon Oluştur</title>
	<meta name="keywords" content="<?php echo is_js($cat['nav_keyw']) ?>" />
	<meta name="description" content="<?php echo is_js($cat['nav_desc']) ?>" />

	<?php include 'theme/src.php'; ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

	<link rel="stylesheet" href="<?php echo $theme_patch ?>/css/app.css">
</head>

<body>
	<!--page start-->
	<div class="page">
		<!-- preloader start -->
		<!-- preloader end -->
		<!--header start-->
		<?php include 'theme/header.php'; ?>
		<div class="ttm-page-title-row">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title-box text-center">
							<div class="page-title-heading">
								<h1 class="title">Online Rezervasyon</h1>
							</div><!-- /.page-title-captions -->

						</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div>

		<div class="site-main">
			<!-- sidebar -->
			<section class="ttm-row grid-section clearfix">
				<div class="container">

					<div class="col-xs-12">
						<div class="col-xs-12 p0">

							<div class="kutu">
								<h1 class="rezerv_title">Rezervasyon Yap</h1>
								<hr>
								<input id="txtbas" type="text" class="form-control" style="border-radius: 15px;  display: unset;">
								<hr>
								<select class="form-control saha" name="saha" style="border-radius: 15px 10px 10px 15px;   display: unset;">
									<option value="-1">Saha Seçiniz</option>
									<option value="1">1.Saha</option>
									<option value="2">2.Saha</option>
								</select>
								<hr>

<div class="rezervasyon_saat_area dnone">

								</div>

								<div class="clearfix">  </div>

								<hr>
							 <div id="contact_form_dnone" class="contact_form_dnone dnone">
								<div class="form-group col-xs-4  ">
								    <label>Ad Soyad Giriniz</label>
								    <input type="text" class="form-control reserv_name zor">
								</div>
								<div class="form-group col-xs-4  ">
								    <label>Mail Adresi Giriniz</label>
								    <input type="text" class="form-control reserv_mail zor">
								</div>
								<div class="form-group col-xs-4  ">
								    <label>Telefon Numarası Giriniz</label>
								    <input type="text" class="form-control reserv_gsm zor">
								</div>
								</div>
								<span class="text-danger error_area dnone">Lütfen Saha Seçiniz!</span>
								<span class="text-success succes_area dnone">Randevunuz Oluşturuldu!</span>
								<button type="button" class="rezerv_onay_btn" name="button">Rezervasyon Yap</button>

							</div>


						</div>

					</div>
				</div>
			</section>
		</div>

		<?php include 'theme/footer.php'; ?>

		<a id="totop" href="#top">
			<i class="fa fa-angle-up"></i>
		</a>

	</div><!-- page end -->


	<?php include 'theme/js.php'; ?>

	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo $theme_patch ?>/js/app.js"></script>
<script>

    $('.saha').on('change', function() {
        console.log('sa')
		  $('#contact_form_dnone').removeAttr('class')


		})
</script>

</body>

</html>
