<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $set['General_title'] ?></title>
    <meta name="keywords" content="<?php echo $set['General_keyw'] ?>" />
    <meta name="description" content="<?php echo $set['General_desc'] ?>" />

    <?php include 'theme/src.php'; ?>

</head>

<body>

<!--page start-->
<div class="page">

    <!-- preloader start -->

    <!-- preloader end -->

    <!--header start-->
    <?php include 'theme/header.php'; ?>


    <div class="ttm-page-title-row">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-box text-center">
                        <div class="page-title-heading">
                            <h1 class="title"><?php echo "İletişim" ?></h1>
                        </div><!-- /.page-title-captions -->
                        <div class="breadcrumb-wrapper">
								<span>
									<a title="Ana Sayfa" href="<?php echo SITE_URL ?>">
										<i class="ti ti-home"></i>
									</a>
								</span>
                            <span class="ttm-bread-sep">&nbsp; : : &nbsp;</span>
                            <span><a href="<?php echo SITE_URL.'iletisim' ?>">İletişim</a></span>
                        </div>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

    <div class="site-main">
        <!-- sidebar -->
        <section class="ttm-row zero-padding-section clearfix">
            <div class="container">
                <div class="row no-gutters"><!-- row -->

                    <div class="col-lg-8">
                        <div class="spacing-10 ttm-bgcolor-grey ttm-bg ttm-col-bgcolor-yes ttm-right-span">
                            <div class="ttm-col-wrapper-bg-layer ttm-bg-layer">
                                <div class="ttm-bg-layer-inner"></div>
                            </div>
                            <!-- section title -->
                            <div class="section-title with-desc clearfix">
                                <div class="title-header">
                                    <h5>Rezervasyon </h5>
                                </div>
                            </div><!-- section title end -->
                            <form id="ttm-quote-form" class="row ttm-quote-form clearfix" method="post" action="#">

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">

                                        <select name="" id="" class="form-control ">

                                            <?php foreach ($sahalist as $item) : ?>


                                            <option value="<?php echo $item['saha_id'] ?>"><?php echo $item['saha_name'] ?></option>


                                            <?php endforeach ?>


                                        </select>


                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input name="phone" type="text" placeholder="Telefon Numaranız" required="required" class="form-control bg-white">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input name="address" type="text" placeholder="Mail Adresiniz" required="required" class="form-control bg-white">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input name="subject" type="text" placeholder="Konu" required="required" class="form-control bg-white">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <textarea name="Massage" rows="5" placeholder="Mesajınız..." required="required" class="form-control bg-white"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="text-left">
                                        <button type="submit" id="submit" class="ttm-btn ttm-btn-size-md ttm-btn-bgcolor-skincolor" value="">
                                            Gönder
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- row end -->
                <!-- row -->
                <div class="row">

                </div><!-- row end-->
            </div>
        </section>

    </div>

    <?php include 'theme/footer.php'; ?>

    <a id="totop" href="#top">
        <i class="fa fa-angle-up"></i>
    </a>

</div><!-- page end -->


<?php include 'theme/js.php'; ?>


</body>

</html>