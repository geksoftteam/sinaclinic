$('#txtbas').daterangepicker({
  "timePicker24Hour": true,
  "opens": "center",
  "autoApply": true,
  singleDatePicker: true,
  "locale": {
    "format": "DD/MM/YYYY",
    "separator": " - ",
    "fromLabel": "Dan",
    "toLabel": "a",
    "customRangeLabel": "Seç",
    "daysOfWeek": [
      "Pt",
      "Sl",
      "Çr",
      "Pr",
      "Cm",
      "Ct",
      "Pz"
    ],
    "monthNames": [
      "Ocak",
      "Şubat",
      "Mart",
      "Nisan",
      "Mayıs",
      "Haziran",
      "Temmuz",
      "Ağustos",
      "Eylül",
      "Ekim",
      "Kasım",
      "Aralık"
    ],
    "firstDay": 1
  }
});


function zor(clasi) {
  $('.'+clasi).each(function(index, element) {
    var degert = $(this).val();
    if(degert == '' || degert ==-1){
      $(this).css('border','1px solid red');
      $(this).focus();
      die();
    } else {
      $(this).css('border','1px solid green');
    }
  });
}

$('.saha').on('change', function() {
  $('.rezervasyon_saat_area').removeClass('dnone')
  var saha = $('.saha').val();
  var dateci = $('#txtbas').val();
  var url = $('#url').val();


  $.post(url + 'Rezervasyon/reserv_select', {

    saha: saha,
    dateci: dateci,
  }, function(data) {
    $('.rezervasyon_saat_area').html(data)
  });
})


$('#txtbas').on('change', function() {
  $('.rezervasyon_saat_area').removeClass('dnone')

  var saha = $('.saha').val();
  var dateci = $('#txtbas').val();
  var url = $('#url').val();


  $.post(url + 'Rezervasyon/reserv_select', {

    saha: saha,
    dateci: dateci,
  }, function(data) {
    $('.rezervasyon_saat_area').html(data)


  });
})
$(document).on('click', '.saatbtn', function() {
  var saat = $(this).data('saat')

  $('.saatbtn').each(function() {
    $(this).removeClass('active')
  })

  $(this).addClass('active')
})

$('.saatbtn').on("tabsactivate", function(event, ui) {

  var saat = $(this).data('saat')
  $('.saatbtn').each(function() {
    $(this).removeClass('active')
  })
  $(this).addClass('active')
})




$(document).on('click', '.rezerv_onay_btn', function() {
  zor('zor')


  var dateci = $('#txtbas').val();
  var saha = $('.saha').val()
  var saat = $('.active').data('saat')
  var reserv_name = $('.reserv_name').val()
  var reserv_mail = $('.reserv_mail').val()
  var reserv_gsm = $('.reserv_gsm').val()

  if (saha == -1) {
    $('.error_area').removeClass('dnone')
    die()
  }

  var url = $('#url').val();


  $.post(url + 'Rezervasyon/add', {
    dateci: dateci,
    saha: saha,
    saat: saat,
    reserv_name: reserv_name,
    reserv_mail: reserv_mail,
    reserv_gsm: reserv_gsm
  }, function(data) {
    if (data == 1) {
      $('.succes_area').removeClass('dnone')
      setTimeout(function() {
        location.reload();
      }, 5000);
    }
  });

})
