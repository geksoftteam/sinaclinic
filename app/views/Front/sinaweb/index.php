<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $set['General_title'] ?></title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>


	<?php include "theme/slider.php"; ?>


	<div class="clearfix"></div>



	<div class="container space">
		<div class="row justify-content-center">
			<div>
				<h3 class="p-3"><b>ŞUBELERİMİZ</b></h3>
			</div>
			<div class="wrapper " >

				<?php foreach($subeler as $item) :?>
					<div class="snip">
						<img src="<?= SITE_UPLOAD_DIR.'page/'.$item['sube_image'] ?>">
						<div>
							<a href="<?= SITE_URL.'sube/detay/'.$item['sube_seo_name'] ?>">
								<h2 class="align-items-center"><?php echo is_js($item['sube_name'])  ?></h2>
							</a>

						</div>
					</div>

				<?php endforeach; ?>
			</div>
		</div>
	</div>


	<div class="col-md-12 space">
		<h3 class="p-3 	text-center"><b>BÖLÜMLERİMİZ</b></h3>
	</div>
	<section class="sliderarea1">
		<div class="container-fluid icon">

			<img class="w-100" src="<?php echo $theme_patch ?>/assets/img/bolumler.png" alt="First slide">

			<div class="owl-carousel owl2 owl-theme owlnavstyle owlnavstlow">

				<?php foreach ($bolumler as $blm): ?>
					<div class="item p-4">
						<a href="<?= SITE_URL.'bolum/detay/'.$blm['bolum_seo_name'] ?>" class="btn effect-1">


							<h5 class="text-center mt-3 text-white"><?= is_js($blm['bolum_name']) ?></h5>
						</a>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</section>



	<div class="col-md-12 space">
		<h3 class="p-3 	text-center"><b>DOKTORLARIMIZ</b></h3>
	</div>
	<div class="container-fluid">
		<div class="sliderarea1 sliderarea row">
			<div class="owl-carousel round owl6 owl-theme owlnavstyle py-4">
				
				<?php foreach ($doktorlar as $dr): ?>
					<div class="item ">
						<div class="round-image">
							<img src="<?= SITE_UPLOAD_DIR.'page/'.$dr['doktor_image'] ?>" alt="">
						</div>

						<div class="p-3">
							<a href="<?= SITE_URL.'doktor/detay/'.$dr['doktor_seo_name'] ?>">
								<h6 class="text-center"><?= is_js($dr['doktor_name']) ?></h6>
								

							</a>
							<p><b><?= is_js($dr['doktor_desc']) ?></b></p>
						</div>
					</div>

				<?php endforeach ?>

			</div>
		</div>
	</div>

	<div class="clearfix"></div>


	<section class="studiosec sliderarea1">

		<div class="container-fluid">

			<div class="row">



				<div class="owl-carousel owl3 owl-theme owlnavstyle">
					<?php foreach ($blog as $blg): ?>
						<div class="item">
							<a href="<?php echo SITE_URL.$blg['nav_url'].'/'.$blg['page_url'] ?>">
								<img src="<?= SITE_UPLOAD_DIR.'page/'.$blg['page_image'] ?>" alt="">
								<div class="write">
									<h5 class="text-white"><?= is_js($blg['page_name']) ?></h5>
									<p class="text-white"><?= is_js($blg['page_jenerik']) ?></p>
								</div>
							</a>
						</div>



					<?php endforeach ?>



				</div>

			</div>


		</div>




	</section>


	<div class="clearfix"></div>




	<?php include 'theme/footer.php'; ?>

	<?php include 'theme/js.php'; ?>
</body>
</html>