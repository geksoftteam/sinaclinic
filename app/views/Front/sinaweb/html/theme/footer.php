<footer>
	<div class="container">
		<div class="row footer">
			<div class="col-lg-2 col-md-3 mt-5">
				<a href="index.php"><img src="assets/img/logo.png" class="logo" alt=""></a>

				<div class="footersocialmedia">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-youtube"></i></a>
					<a href="#"><i class="fab fa-instagram"></i></a>
				</div>
			</div>
			<div class="col-lg-2 col-md-3 mt-5">
				<h5><b>Kurumsal</b></h5>
				<ul>
					<li>Hakkımızda</li>
					<li>İnsan Kaynakları</li>
					<li>Hasta Hakları</li>
					<li>Refakat Kuralları</li>
					<li>Check-Up Hizmetlerimiz</li>
				</ul>
			</div>

			<div class="col-lg-2 col-md-3 mt-5">
				<h5><b>Bölümler</b></h5>
				<ul>
					<li>Ağız ve Diş Sağlığı</li>
					<li>Psikiyatri</li>
					<li>Beslenme ve Diyetetik</li>
					<li>Cildiye</li>
				</ul>
			</div>

			<div class="col-lg-2 offset-lg-0 col-md-5 offset-md-2 mt-5">
				<h5><b>Bize Ulaşın</b></h5>
				<ul>
					<li>Sina Clinic</li>
					<li>Sina Clinic 2</li>
					<li>Sina Şube 3</li>
					<li>Sina Şube 4</li>
					<li>Alibey Hospital</li>
					<li></li>
				</ul>
			</div>

			<div class="col-lg-2 col-md-5 mt-5">
				<h5><b>ÇAĞRI MERKEZİ</b></h5>
				<ul>
					<li>0212 627 79 79</li>
					<li>0212 627 79 79</li>
					<li>Karadolap Mh, Neşeli Sk</li>
					<li>Yağmur Ap. Mo:24A</li>
					<li>Eyüp/istanbul</li>
					<li></li>
				</ul>
			</div>

		</div>

		
	</div>


	<div class="container-fluid">
		<p class="text-center mt-5 text-white  mb-0" style="padding: 15px;
		border-top: 1px solid;"> ©2021 Osman Çetin Resmi Web Sitesi</p>
	</div>
</footer>
