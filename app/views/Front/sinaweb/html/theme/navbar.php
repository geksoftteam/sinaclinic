<div class="navbar">
	<div>
		<a href="index.php"><img src="assets/img/logo.png" alt=""></a>
	</div>

	<div>
		<nav class="navbar navbar-expand-lg navbar-light">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="true" aria-label="Toggle navigation">
				<span class="text-white"><i class="fa fa-bars"></i></span>
			</button>

			<div class="collapse navbar-collapse " id="navbarNav">

				<ul class="navbar-nav">
					<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fa fa-close text-white"></i>
					</button>

					<div id="logo"></div>

					<li class="nav-item">
						<a href="#">Diş</a>
					</li>
					<li class="nav-item">
						<a href="#">Psikoloji</a>
					</li>
					<li class="nav-item">
						<a href="hastaneler.php">Hastaneler </a>
					</li>
					<li class="nav-item">
						<a href="hekimler.php">Hekimler</a>
					</li>
					<li class="nav-item">
						<a href="#">Hasta Kaydı</a>
					</li>
					

				</ul>
				

			</div>

		</nav>


	</div>

	<div class="navright ">
		<div class=" navbutton ">	
			<button type="button" class="btn info btn-md"><p>Görüntülü </p><p>Görüşme</p></button>
			<button type="button" class="btn btn-light btn-md"><p>Hızlı</p> <p>Randevu</p></button>
		</div>	
		<div class="searchbar">
			<input type="text">
			<button class="btn"><i class="fas fa-search"></i></button>
		</div>

	</div>

</div>
