<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h3><b>HAKKIMIZDA</b></h3>
	</div>



	<div class="container p-0">		
		<nav class="nav">
			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="#">HAKKIMIZDA</a>
		</nav>

	</div>


	<div class="clearfix"></div>

	<div class="container mt-3">
		<div class="row">
			<div class="news col-md-8">
				<div class="yazi-detay mt-3">
					<p>Özel Alibey Hospital hastanesi 1989 yılında Çetinler Polikliniği olarak sağlık hizmetine başlamış olup,2007 yılında Çetinler Cerrahi Tıp Merkezi, 2011 yılında ise Alibey Hospital hastanesine dönüşerek gelişimini sürdürmüştür..</p>

					<p>İstanbul Avrupa Yakasında , uluslararası  standart  ve kalitede  hizmet veren   Alibey  Hospital hastanesi olarak; 18 uzman doktor, 20 sağlık personeli, 3 Kadın Doğum Uzmanı, 3 Diş Hekimi, 2 Genel Cerrahi Uzmanı, 1 Dahiliye ,1 Ortopedi ve travmatoloji,1 cildiye,1 Çocuk,1 Nöroloji,1 Üroloji,1 Göz, 1 KBB, Anestezi ve Reanimasyon Uzmanı1 Estetisyen, 1 Diyetisyen , 3 Genel Yoğun Bakım, 5 Yeni Doğan Bakımı olmak üzere  31  yatak kapasitesi ile geniş  bir teknoloji  imkanına sahiptir.</p>
					<br>
					<h4><b>VİZYON</b></h4>
					<br>
					<p>Özel  Alibey  Hospital Hastanesi, ülkemizin en güvenilir sağlık kuruluşlarından biri olmayı hedeflemektedir. </p>
					<br>
					<h4><b>MİSYON</b></h4>
					<br>

					<p >Özel Alibey  Hospital Hastanesi tüm çalışanlarıyla,uluslararası standartlarda verdiği hizmetle zirveyi hedefler, hasta mahremiyetini korur, güvenliğini  sağlar,memnuniyetini kazanır,insana saygı duyar ve ahlâkı esas alır.</p>	
				</div>
			</div>
			<div class="news col-md-4">
				<div class="blog-card card" style="border: none;">
					<h3 class="card-header">BÖLÜMLER</h3>

					<div class="col-md-12 list">
						<ul>
							<li><a href="#"></a>Psikiyatri</li>
							<li><a href="#"></a>Beslenme ve Diyetetik</li>
							<li><a href="#"></a>Cildiye</li>
						</ul>
					</div>
				</div>	
			</div>
		</div>
	</div>


	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>