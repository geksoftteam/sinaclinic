<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b>Hastaneler</b></h4>
	</div>


	<div class="container  p-0">
		
		<nav class="nav">
			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="hastaneler.php">Hastaneler</a>
		</nav>
	</div>
	<div class="content-yazi container mt-3">
		<div class="row">
			<div class="col-md-12">
				<div class="hospital col-md-3">
					<img src="assets/img/pexels-pixabay-269077.jpg">
				</div>
				<div class="hospital col-md-3">
					<h5>SİNA CLINIC</h5>
					<p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit. </p>
				</div>
				<div class="hospital col-md-3">
					<img src="assets/img/national-cancer-institute-1c8sj2IO2I4-unsplash.jpg">
				</div>
				<div class="hospital col-md-3">
					<h5>SİNA CLINIC</h5>
					<p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit. </p>
				</div>

			</div>

			<div class="col-md-12">
				<div class="hospital col-md-3">
					<img src="assets/img/graham-ruttan-aMNLYoT2z_I-unsplash.jpg">
				</div>
				<div class="hospital col-md-3">
					<h5>SİNA CLINIC</h5>
					<p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit. </p>
				</div>
				<div class="hospital col-md-3">
					<img src="assets/img/350x276_tralibey-hospitalenalibey-hospital_77.png">
				</div>
				<div class="hospital col-md-3">
					<h5>SİNA CLINIC</h5>
					<p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit. </p>
				</div>

			</div>

		</div>
	</div>
</div>

</div>

<div class="clearfix"></div>



<?php include 'theme/footer.php'; ?>


<?php include 'theme/js.php'; ?>

</body>
</html>