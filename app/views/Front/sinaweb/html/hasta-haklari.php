<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h3><b>HASTA HAKLARI</b></h3>
	</div>


	<div class="container p-0">		
		<nav class="nav">
			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="#">HAKKIMIZDA</a>
		</nav>

	</div>


	<div class="clearfix"></div>

	<div class="container mt-3">
		<div class="row">
			<div class="news col-md-8">
				<div>

					<h6><b>Hizmetlerden Yararlanma Hakkı</b></h6>
					<p>Hastanemize başvuran tüm hastalarımız, düşünce ve sosyal özellikleri ne olursa olsun tüm hizmetlerimizden yararlanma hakkına sahiptir.</p>
					<br>
					<h6><b>Bilgi isteme Hakkı</b></h6>
					<p>Hastalarımız Sağlık Kurumu tarafından verilen her türlü hizmetin ve imkânın ne olduğunu ve bu hizmetlerden faydalanma koşullarını öğrenme hakkına sahiptir.</p>
					<br>
					<h6><b>Sağlık kuruluşunu Seçme ve Değiştirme Hakkı</b></h6>
					<p>Hayati tehlike bakımından sağlık kuruluşunu değiştirilmesinde tıbben sakınca görülmemesi halinde hastalarımız istedikleri sağlık kuruluşunu seçerek tedavi görebilirler.</p>
					<br>
					<h6><b>Personeli Tanıma Hakkı</b></h6>
					<p>Hastalarımız kendisine sağlık hizmeti verecek veya vermekte olan doktorların ve diğer tıbbi personelin kimlikleri, görev ve unvanları hakkında bilgi sahibi olma hakkına sahiptirler.</p>
					<br>
					<h6><b>Bilgi Verilmesini Yasaklama Hakkı</b></h6>
					<p>Hastalarımız sağlık durumları hakkında kendilerine, ailelerine veya yakınlarına bilgi verilmemesini talep edebilirler.</p>
					<br>
					<h6><b>Danışma (Konsültasyon) Hakkı</b></h6>
					<p>Hastalarımızın kendi talebi ve ilave ödemeyi kabul etmesi halinde, başka bir uzman ile konsültasyon yapılmasını isteme hakları vardır.</p>
					<br>
					<h6><b>Mahremiyet Hakkı</b></h6>
					<p>Sağlık hizmetinin verilmesi nedeniyle elde edilen bilgilerin kanun ile izin verilen haller dışında kesinlikle açıklanmaması; tüm tıbbi değerlendirilmelerin, sağlık harcamalarının kaynağının gizli tutulması; tedavisi ile doğrudan ilgili olmayan kimselerin tıbbi müdahale sırasında bulunmaması ve hastalığın içeriği gerektirmedikçe şahsi ve ailevi hayatına müdahalede bulunulmamasını hastalarımız talep edebilirler.</p>
					<br>
					<h6><b>Hastadan Rıza Alınması</b></h6>
					<p>Hasta tıbbi ve yasal zorunluluklar dışında, hastalığı ile ilgili bir ölüm ya da ciddi yan etki riski, nekahat ile ilgili problemler ve sonuçta elde edilecek başarı şansı gibi konularda mümkün olan bilgilendirme yapıldıktan sonra, tedavisi ile ilgili olarak varılacak kararlara katılma hakkına sahiptir.</p>
					<br>
					<h6><b>Tedaviyi Reddetme ve Durdurma Hakkı</b></h6>
					<p>Kanunen zorunlu haller dışında, ortaya çıkabilecek olumsuz sonuçların doktor tarafından tümüyle anlatılması sonrası, bunların sorumluluğunu üstlenmek koşuluyla hastalarımız uygulanacak tedaviyi reddedebilir veya durdurabilir. </p> 
					<p>(Girişim veya tedavi başlandıktan sonra rızayı geri alarak tedaviyi durdurmak ancak tıbbi yönden sakınca bulunmaması koşuluna bağlıdır.)</p>
					<br>
					<h6><b>Saygı ve İtibar Görme Hakkı</b></h6>
					<p>Hastalarımız ve aileleri her zaman ve her türlü koşulda, bireysel itibarlarının korunduğu saygılı, nazik, şefkatli ve güler yüzlü bir ortamda hizmet alma hakkına sahiptir.</p>
					<br>
					<h6><b>Ziyaretçi, Refakatçi Bulundurma Hakkı</b></h6>
					<p>Hastalarımız kurum tarafından belirlenen usul ve esaslara uygun şekilde ziyaretçi kabul edebilir, refakatçi bulundurabilirler.</p>
					<br>
					<h6><b>Güvenli Ortam Hakkı</b></h6>
					<p>Hastalarımız güvenlik içinde olmayı talep edebilirler. Herkesin sağlık kurumunda güvenlik içinde olmayı bekleme ve bunu isteme hakkı vardır. Hastaların, ziyaretçi ve refakatçi gibi yakınlarının can ve mal güvenliklerinin korunması ve sağlanması için gerekli tedbirler hastanemizce alınır.</p>
					<br>
					<h6><b>Dini Gerekleri Yerine Getirme ve Dini Hizmetlerden Faydalanma Hakkı</b></h6>
					<p>Hastalarımız sağlık kurum ve kuruluşlarının imkanları ölçüsünde kurum hizmetlerinde aksamalara neden olmamak koşuluyla dini koşulları serbestçe yerine getirme hakkına sahiptir.</p>
					<br>
					<h6><b>Tetkik ve Tedavi Bedellerini Öğrenme Hakkı</b></h6>
					<p>Hastanın sağlık kuruluşundan sağlanan hizmet karşılığında ödeyeceği bedellerin açık ve detaylı bir faturasını isteme ve alma hakkı vardır.</p>
					<br>
					<h5><b> HASTA SORUMLULUKLARI</b></h5>
					<p>Hasta sorumlulukları, tıbbi tedavilerde alınan sonuçların en iyi olmasını ve sağlıkta kalitenin arttırılmasını sağlayacak en önemli koşullarından biridir. Hastalarımızın sorumlulukları aşağıdaki gibidir:</p>
					<br>
					<ol>
						<li>Tıbbi hizmet vermekle görevli doktor ve hemşirelere sağlık durumuyla ilgili tam ve doğru bilgi vermek.</li>
						<li>  Tüm sağlık çalışanları ile açık bir iletişimde bulunmak ve iyimser bir yaklaşım içinde olmak</li>
						<li>Tıbbi personelle kendilerine verilecek bilgiler, sağlık ile ilgili alınan kararlar ve tüm konularda işbirliği içinde çalışmak</li>
						<li>Tıp  biliminin olası risk ve sınırlarını tanımak ve bu doğrultuda gerçekçi yaklaşımı korumak.</li>
						<li>Verilen sağlık hizmetleri hakkında önemli konularda karar ve rıza vermek</li>
						<li>Sağlık harcamalarının gerektireceği maddi yükümlülükleri vaktinde karşılamak.</li>
						<li>Düzenli egzersiz, sigara içmemek, sağlıklı bir beslenme alışkanlığı edinmek gibi önlemlerle sağlıklarını en üst düzeyde tutma çabasında olmak.</li>
						<li> Enfeksiyon hastalıklarının yayılmaması için alınmış ve kendilerine önerilen tüm önlemler konusunda hassas davranmak.</li>
						<li>Diğer hastaların ve hasta yakınlarının haklarına saygı göstermek.</li>
						<li>Sağlık hizmeti veren kişilerin diğer hastalara ve topluma karşı olan etkin tedavi sağlama zorunluluğunun farkında olmak.</li>
					</ol>


				</div>
			</div>
			<div class="news col-md-4">
				<div class="blog-card card" style="border: none;">
					<h3 class="card-header">BÖLÜMLER</h3>

					<div class="col-md-12 list">
						<ul>
							<li><a href="#"></a>Psikiyatri</li>
							<li><a href="#"></a>Beslenme ve Diyetetik</li>
							<li><a href="#"></a>Cildiye</li>
						</ul>
					</div>
				</div>	
			</div>
		</div>
	</div>


	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>

