<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>


	<div class="container-fluid p-0">
		<div class="row m-0 p-0">
			<div id="carouselExampleIndicators" class="carousel slide slider align-items-center" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block" src="assets/img/slider-1.png" alt="First slide">
						<div class="flexboxcapsul col-md-12">
							<div class="p-0 col-xl-4 col-lg-5 col-md-6">
								<div class="flex">	
									<button class="flexbox btn-light">
										<i class="fas fa-video"></i>
										<p><b>Görüntülü Görüşme Randevusu</b></p>
									</button>

									<button class="flexbox info">
										<i class="fas fa-briefcase-medical"></i>
										<p><b>Muayne Randevusu</b></p>
									</button>
								</div>
								<div class="flex">


									<button class="flexbox info">
										<i class="fas fa-file-medical"></i>
										<p><b>E-Sonuç</b></p>
									</button>

									<button class="flexbox btn-light">
										<i class="fas fa-heartbeat"></i>
										<p><b>Check-Up</b></p>
									</button>
								</div>
							</div>
							<div class="p-0 col-xl-8 col-lg-7 col-md-6">
								<h3>LOREM IPSUM</h3>
								<h2 class="text-white">DOLOR SIT AMET,</h2>
								<h2>CONSECTETUR</h2>
								<h2 class="text-white">ADIPISCING ELIT</h2>
								<p><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </b></p>

								<button type="button" class="btn btn-light dty-bilgi"><b>DETAYLI BİLGİ </b><img src="assets/İcon/Arrow icon.png" style="width: 25px;
								float: right;"></button>

							</div>
						</div>
						<div>
							<a href="#" class="dr"> <img src="assets/İcon/doktorunuza_sorun_button.png" alt=""></a>
						</div>
					</div>

					<div class="carousel-item">
						<img class="d-block" src="assets/img/slider-1.png" alt="First slide">
						<div class="flexboxcapsul col-md-12">
							<div class="p-0 col-xl-4 col-lg-5 col-md-6">
								<div class="flex">	
									<button class="flexbox btn-light">
										<i class="fas fa-video"></i>
										<p><b>Görüntülü Görüşme Randevusu</b></p>
									</button>

									<button class="flexbox info">
										<i class="fas fa-briefcase-medical"></i>
										<p><b>Muayne Randevusu</b></p>
									</button>
								</div>
								<div class="flex">


									<button class="flexbox info">
										<i class="fas fa-file-medical"></i>
										<p><b>E-Sonuç</b></p>
									</button>

									<button class="flexbox btn-light">
										<i class="fas fa-heartbeat"></i>
										<p><b>Check-Up</b></p>
									</button>
								</div>
							</div>
							<div class="p-0 col-xl-8 col-lg-7 col-md-6">
								<h3>LOREM IPSUM</h3>
								<h2 class="text-white">DOLOR SIT AMET,</h2>
								<h2>CONSECTETUR</h2>
								<h2 class="text-white">ADIPISCING ELIT</h2>
								<p><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </b></p>

								<button type="button" class="btn btn-light dty-bilgi"><b>DETAYLI BİLGİ </b><img src="assets/İcon/Arrow icon.png" style="width: 25px;
								float: right;"></button>

							</div>
						</div>
						<div>
							<a href="#" class="dr"> <img src="assets/İcon/doktorunuza_sorun_button.png" alt=""></a>
						</div>
					</div>


					<div class="carousel-item">
						<img class="d-block" src="assets/img/slider-1.png" alt="First slide">
						<div class="flexboxcapsul col-md-12">
							<div class="p-0 col-xl-4 col-lg-5 col-md-6">
								<div class="flex">	
									<button class="flexbox btn-light">
										<i class="fas fa-video"></i>
										<p><b>Görüntülü Görüşme Randevusu</b></p>
									</button>

									<button class="flexbox info">
										<i class="fas fa-briefcase-medical"></i>
										<p><b>Muayne Randevusu</b></p>
									</button>
								</div>
								<div class="flex">


									<button class="flexbox info">
										<i class="fas fa-file-medical"></i>
										<p><b>E-Sonuç</b></p>
									</button>

									<button class="flexbox btn-light">
										<i class="fas fa-heartbeat"></i>
										<p><b>Check-Up</b></p>
									</button>
								</div>
							</div>
							<div class="p-0 col-xl-8 col-lg-7 col-md-6">
								<h3>LOREM IPSUM</h3>
								<h2 class="text-white">DOLOR SIT AMET,</h2>
								<h2>CONSECTETUR</h2>
								<h2 class="text-white">ADIPISCING ELIT</h2>
								<p><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </b></p>

								<button type="button" class="btn btn-light dty-bilgi"><b>DETAYLI BİLGİ </b><img src="assets/İcon/Arrow icon.png" style="width: 25px;
								float: right;"></button>

							</div>
						</div>
						<div>
							<a href="#" class="dr"> <img src="assets/İcon/doktorunuza_sorun_button.png" alt=""></a>
						</div>
					</div>


					<div class="carousel-item">
						<img class="d-block" src="assets/img/slider-1.png" alt="First slide">
						<div class="flexboxcapsul col-md-12">
							<div class="p-0 col-xl-4 col-lg-5 col-md-6">
								<div class="flex">	
									<button class="flexbox btn-light">
										<i class="fas fa-video"></i>
										<p><b>Görüntülü Görüşme Randevusu</b></p>
									</button>

									<button class="flexbox info">
										<i class="fas fa-briefcase-medical"></i>
										<p><b>Muayne Randevusu</b></p>
									</button>
								</div>
								<div class="flex">


									<button class="flexbox info">
										<i class="fas fa-file-medical"></i>
										<p><b>E-Sonuç</b></p>
									</button>

									<button class="flexbox btn-light">
										<i class="fas fa-heartbeat"></i>
										<p><b>Check-Up</b></p>
									</button>
								</div>
							</div>
							<div class="p-0 col-xl-8 col-lg-7 col-md-6">
								<h3>LOREM IPSUM</h3>
								<h2 class="text-white">DOLOR SIT AMET,</h2>
								<h2>CONSECTETUR</h2>
								<h2 class="text-white">ADIPISCING ELIT</h2>
								<p><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </b></p>

								<button type="button" class="btn btn-light dty-bilgi"><b>DETAYLI BİLGİ </b><img src="assets/İcon/Arrow icon.png" style="width: 25px;
								float: right;"></button>

							</div>
						</div>
						<div>
							<a href="#" class="dr"> <img src="assets/İcon/doktorunuza_sorun_button.png" alt=""></a>
						</div>
					</div>




					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>


	<div class="clearfix"></div>


	<div class="container-fluid space">
		<div class="div  mt-5">
			<h3 class="text-center p-3"><b>SAĞLIK REHBERİ</b></h3>
		</div>
		<div class=" sliderarea slideimg row">
			<div class="owl-carousel owl1 owl-theme owlnavstyle py-4">
				<div class="item">
					<img src="assets/img/medical-equipment-JQ23CTL.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-outline-primary"><b>Lorem İpsum Dolor?</b></button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/repairing-tooth-EEBYC32.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-primary">Lorem İpsum Dolor?</button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/teeth.png" at="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-outline-primary"><b>Lorem İpsum Dolor?</b></button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/dental-surgeon-examining-patient-MDUG5LB.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-primary">Lorem İpsum Dolor?</button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/part-of-dentist-choosing-color-teeth-from-palette-QPDEEAD.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-outline-primary">Lorem İpsum Dolor?</button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/anatomy-of-healthy-teeth-and-tooth-dental-implant--PAF6ZMW.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-primary">Lorem İpsum Dolor?</button>
					</div>
				</div>
			</div>

		</div>
	</div>



	<div class="container space">
		<div class="row justify-content-center">
			<div>
				<h3 class="p-3"><b>ŞUBELERİMİZ</b></h3>
			</div>
			<div class="wrapper " >
				<div class="snip">
					<img src="assets/img/pexels-pixabay-269077.jpg">
					<div>
						<h2 class="align-items-center">SİNA CLINIC</h2>

					</div>
				</div>

				<div class="snip">
					<img src="assets/img/graham-ruttan-aMNLYoT2z_I-unsplash.jpg">
					<div>
						<h2 class="align-items-center">SİNA CLINIC</h2>
					</div>
				</div>


				<div class="snip">
					<img src="assets/img/national-cancer-institute-1c8sj2IO2I4-unsplash.jpg">
					<div>
						<center><h2>ALİBEY HOSPİTAL</h2></center>
					</div>
				</div>

				<div class="snip">
					<img src="assets/img/350x276_tralibey-hospitalenalibey-hospital_77.png">
					<div>
						<h2 class="text-center">ALİBEY HOSPİTAL</h2>
					</div>
				</div>

			</div>
		</div>
	</div>


	<div class="col-md-12 space">
		<h3 class="p-3 	text-center"><b>BÖLÜMLERİMİZ</b></h3>
	</div>
	<section class="sliderarea1">
		<div class="container-fluid icon">

			<img class="w-100" src="assets/img/bolumler.png" alt="First slide">

			<div class="owl-carousel owl2 owl-theme owlnavstyle owlnavstlow">
				<div class="item p-4">
					<a href="#" class="btn effect-1">
						<img src="assets/İcon/dis.png" alt="">
					</a>
					<h5 class="text-center mt-3 text-white">Diş</h5>

				</div>
				<div class="item p-4">
					<a href="#" class="btn effect-1">
						<img src="assets/İcon/psikiyatri2.png" alt="">
					</a>				
					<h5 class="text-center mt-3 text-white">Psikiyatri</h5>
				</div>

				<div class="item p-4">
					<a href="#" class="btn effect-1">
						<img src="assets/İcon/diyetetik.png" alt="">
					</a>					
					<h5 class="text-center mt-3 text-white">Beslenme ve Diyetetik</h5>
				</div>
				<div class="item p-4">
					<a href="#" class="btn effect-1">
						<img src="assets/İcon/cild.png" alt="">
					</a>					
					<h5 class="text-center mt-3 text-white">Cildiye</h5>
				</div>
				<div class="item p-4">
					<a href="#" class="btn effect-1">icon</a>	
					<h5 class="text-center mt-3 text-white">Lorem</h5>
				</div>
				<div class="item p-4">
					<a href="#" class="btn effect-1">icon</a>		
					<h5 class="text-center mt-3 text-white">Lorem</h5>
				</div>
			</div>
		</div>
	</section>



	<div class="col-md-12 space">
		<h3 class="p-3 	text-center"><b>DOKTORLARIMIZ</b></h3>
	</div>
	<div class="container-fluid">
		<div class="sliderarea1 sliderarea row">
			<div class="owl-carousel round owl6 owl-theme owlnavstyle py-4">
				<div class="item ">
					<div class="round-image">
						<img src="assets/img/Omsan Çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Osman Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>


				<div class="item">
					<div class="round-image">
						<img src="assets/img/ali çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Ali Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>

				<div class="item ">
					<div class="round-image">
						<img src="assets/img/Omsan Çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Osman Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>

				<div class="item ">
					<div class="round-image">
						<img src="assets/img/ali çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Ali Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>


				<div class="item ">
					<div class="round-image">
						<img src="assets/img/Omsan Çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Osman Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>

				<div class="item ">
					<div class="round-image">
						<img src="assets/img/ali çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Ali Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="clearfix"></div>


	<section class="studiosec sliderarea1">
		
		<div class="container-fluid">
			
			<div class="row">
				
				<ul class="nav nav-pills flex-column flex-sm-row mb-5 text-center" id="pills-tab" role="tablist">
					<li class="nav-item flex-sm-fill text-sm-center" role="presentation">
						<a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">TÜM HABERLER</a>
					</li>
					<li class="nav-item flex-sm-fill text-sm-center" role="presentation">
						<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">HASTANELERDEN HABERLER</a>
					</li>
					<li class="nav-item flex-sm-fill text-sm-center" role="presentation">
						<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">KURUMSAL HABERLER</a>
					</li>
				</ul>

				<div class="tab-content col-md-12" id="pills-tabContent">

					
					<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
						
						
							<div class="owl-carousel owl3 owl-theme owlnavstyle">
								
								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-Hoca.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-hoca-2.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/white-teeth-Z3KUYBD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/tooth-implant-in-the-model-human-teeth-gums-and-de-PQ9Z6GD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-Hoca.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-hoca-2.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/white-teeth-Z3KUYBD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/tooth-implant-in-the-model-human-teeth-gums-and-de-PQ9Z6GD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>						

							</div>


						

					</div>

					<!---->



					<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-home-tab">
						
						
							<div class="owl-carousel owl3 owl-theme owlnavstyle">
								
								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-Hoca.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-hoca-2.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/white-teeth-Z3KUYBD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/tooth-implant-in-the-model-human-teeth-gums-and-de-PQ9Z6GD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-Hoca.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-hoca-2.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/white-teeth-Z3KUYBD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/tooth-implant-in-the-model-human-teeth-gums-and-de-PQ9Z6GD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>						

							</div>


						</div>

					

					<!---->



					<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-home-tab">
						
						
							<div class="owl-carousel owl3 owl-theme owlnavstyle">
								
								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-Hoca.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-hoca-2.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/white-teeth-Z3KUYBD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/tooth-implant-in-the-model-human-teeth-gums-and-de-PQ9Z6GD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-Hoca.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/Osman-hoca-2.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/white-teeth-Z3KUYBD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>

								<div class="item">
									<a href="#">
										<img src="assets/img/tooth-implant-in-the-model-human-teeth-gums-and-de-PQ9Z6GD.png" alt="">
										<div class="write">
											<h5 class="text-white">Lorem Ipsum is simpl</h5>
											<p class="text-white">Lorem Ipsum is simply	dummy text of the printing</p>
										</div>
									</a>
								</div>						

							</div>


						</div>

					

					<!---->


				</div>

			</div>


		</div>




	</section>


	<div class="clearfix"></div>




	<?php include 'theme/footer.php'; ?>

	<?php include 'theme/js.php'; ?>
</body>
</html>