<!DOCTYPE html>
<html lang="tr">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic - Dr. Osman Çetin</title>
	
	<?php include 'theme/src.php' ?>

</head>
<body>

	<?php include 'theme/navbar.php' ?>

	<div class="container mt-5">
		<h4><b>Osman Çetin</b></h4>
	</div>


	<div class="container p-0">
		<nav class="nav">

			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="hekimler.php">Hekimler</a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="hastahane.php">Dr. Osman Çetin</a>
		</nav>
	</div>

	<div class="container-fluid dr-bg">
		<div class="col-md-6 offset-md-2">
			<div class="row">
				<img src="assets/img/Omsan Çetin.png" alt="">
				<div>
					<h6>Uzman Doktor</h6>
					<h2>OSMAN ÇETİN</h2>
					<button type="button" class="btn info btn-md">Lorem ipsum</button>
				</div>
			</div>

		</div>
	</div>


	<div class="container">
		
		<div class="col-md-9 offset-md-1">
			<div class="yazi-title">
				<h3 class="card-title">Osman Çetin Kimdir? </h3>
				<h5 class="card-title">Lorem Ipsum</h5>
			</div>


			<h3>Lorem Ipsum</h3>

			<p>Ağız, Diş ve Çene Cerrahisi Uzmanı Dr. Osman Çetin 1984 yılında Burdur’da doğdu. İlk, orta ve lise eğitimlerini İstanbul’da tamamladı. 2010 yılında Yeditepe Üniversitesi Diş Hekimliği Fakültesi’nden mezun oldu. 2017 yılında Yeditepe Üniversitesi Diş Hekimliği Fakültesi Ağız, Diş ve Çene Cerrahisi Anabilim Dalında Uzmanlık eğitimini tamamladı.</p>

			<p>Uzmanlığı sırasında birçok bilimsel kongre ve toplantılarda yer aldı. Uzmanlık tezi geniş yerleşimli kistler üzerinedir. Uzmanlığı sırasında çene eklemi hastalıkları ve tedavisi alanında özellikle kendini geliştirdi.</p>

			<p>Uzmanlığı sırasında birçok ileri cerrahi gerektiren implant vakasını başarıyla tamamladı. 2010 yılında Eyüp’te Alibey Hospital Sağlık Grubunun diş bolumunum kurdu.</p>

			<p>2013 yılında Sarıyer’de Alibey Tıp Merkezi’nin diş bölümünü kurd. 2014 yılında Suadiye’de Dent Avrasya Ağız ve Diş Sağlığı Merkezi’ni kurmuş olup 2017 yılına kadar Yönetim Kurulu Başkanlığını yaptı.</p>

			<p>2016 yılında Esenyurt’ta özel bir hastanenin diş bölümünü kurdu ve işletmesini yönetti. 2015 yılından beri Alibey Sağlık Grubu’na Yönetim Kurulu Başkanlığı yapmaktadır. Dr. Osman Çetin birçok dernek ve kuruluşta yönetici konumundadır. Ayrıca birçok sosyal projede yer almaktadır.</p>

			<p>Kendisi Uluslararası Diplomatlar Birliği Yönetim Kurulu üyesidir. Aynı zamanda TUMSAD (Tüm Sağlık Kuruluşları) Derneği Yönetim Kurulu Üyesidir.</p>

			<p>Türk Diş Hekimleri Birliği, Türk Oral İmplantoloji Derneği üyelikleri de devam etmektedir. Engel atlama ve binicilik dalında lisanslı sporcu olan Dr. Osman Çetin sporun tüm branşlarıyla ilgilidir.</p>



		</div>
	</div>

	<div class="clearfix"></div>



	<div class="col-md-12 space">
		<h3 class="p-3 	text-center"><b>DOKTORLARIMIZ</b></h3>
	</div>
	<div class="container-fluid">
		<div class="sliderarea1 sliderarea row">
			<div class="owl-carousel round owl1 owl-theme owlnavstyle py-4">
				<div class="item ">
					<div class="round-image">
						<img src="assets/img/Omsan Çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Osman Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>


				<div class="item">
					<div class="round-image">
						<img src="assets/img/ali çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Ali Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>

				<div class="item ">
					<div class="round-image">
						<img src="assets/img/Omsan Çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Osman Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>

				<div class="item ">
					<div class="round-image">
						<img src="assets/img/ali çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Ali Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>


				<div class="item ">
					<div class="round-image">
						<img src="assets/img/Omsan Çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Osman Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>

				<div class="item ">
					<div class="round-image">
						<img src="assets/img/ali çetin.png" alt="">
					</div>

					<div class="p-3">
						<h6 class="text-center">Uzm. Dr. Ali Çetin</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
					</div>
				</div>

			</div>
		</div>
	</div>



	<?php include 'theme/footer.php' ?>


	<?php include 'theme/js.php' ?>

</body>
</html>