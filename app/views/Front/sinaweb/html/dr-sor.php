<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b>DOKTORUNUZA SORUN </b></h4>
	</div>


	<div class="container p-0">
		<nav class="nav">
			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="dr-sor.php">Doktorunuza Sorun</a>
		</nav>
	</div>




	<div class="container mt-5">
		<div class="row">
			<div class="col-md-8 ques">
				<form>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputPassword4"> <b>İsim / Soyisim  </b></label>
							<input type="text" class="form-control" id="inputPassword4">
						</div>
						<div class="form-group col-md-6">
							<label for="inputEmail4"><b>Email</b></label>
							<input type="email" class="form-control" id="inputEmail4" >
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputPassword4"><b>Telefon / GSM</b></label>
							<input type="text" class="form-control" id="inputPassword4">
						</div>
						<div class="form-group col-md-6">
							<label for="exampleFormControlSelect1"><b>Doktor</b></label>
							<select class="form-control" id="exampleFormControlSelect1">
								<option>Uzm.Dr.Osman Çetin</option>
								<option>Uzm.Dr.Ali Çetin</option>
								<option>Uzm.Dr.Osman Çetin</option>
								<option>Uzm.Dr.Ali Çetin</option>
								<option>Uzm.Dr.Osman Çetin</option>
							</select>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputPassword4"><b>Talep Edilen Pozisyon</b> </label>
							<input type="text" class="form-control" id="inputPassword4">
						</div>
					</div>  

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="exampleFormControlTextarea1"><b>Mesajınız</b></label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="7"></textarea>
						</div>
					</div>  

					<div class="form-row">
						<div class="form-group col-md-12">
							<button type="button" class="btn info btn-md float-right">Gönder</button>
						</div> 
					</div>
				</form>								
			</div>


			<div class="col-md-3 offset-md-1">
				<div class="sliderarea row">
					<h4 class="m-auto p-3"><b>DOKTORLAR</b></h4>
					<div class="owl-carousel round owl4 owl-theme owlnavstyle py-4">
						<div class="item ">
							<div class="round-image">
								<img src="assets/img/Omsan Çetin.png" alt="">
							</div>

							<div class="p-3">
								<h5 class="text-center">Uzm. Dr. Osman Çetin</h5>
								<div class="text-center">
									<button type="button" class="btn info btn-md ">Doktora Sorun</button>
								</div>
							</div>
						</div>


						<div class="item">
							<div class="round-image">
								<img src="assets/img/ali çetin.png" alt="">
							</div>

							<div class="p-3">
								<h5 class="text-center">Uzm. Dr. Ali Çetin</h5>
								<button type="button" class="btn info btn-md">Doktora Sorun</button>
							</div>
						</div>

						<div class="item ">
							<div class="round-image">
								<img src="assets/img/Omsan Çetin.png" alt="">
							</div>

							<div class="p-3">
								<h5 class="text-center">Uzm. Dr. Osman Çetin</h5>
								<button type="button" class="btn info btn-md">Doktora Sorun</button>
							</div>
						</div>

						<div class="item ">
							<div class="round-image">
								<img src="assets/img/ali çetin.png" alt="">
							</div>

							<div class="p-3">
								<h5 class="text-center">Uzm. Dr. Ali Çetin</h5>
								<button type="button" class="btn info btn-md">Doktora Sorun</button>
							</div>
						</div>


						<div class="item ">
							<div class="round-image">
								<img src="assets/img/Omsan Çetin.png" alt="">
							</div>

							<div class="p-3">
								<h5 class="text-center">Uzm. Dr. Osman Çetin</h5>
								<button type="button" class="btn info btn-md">Doktora Sorun</button>
							</div>
						</div>

						<div class="item ">
							<div class="round-image">
								<img src="assets/img/ali çetin.png" alt="">
							</div>

							<div class="p-3">
								<h5 class="text-center">Uzm. Dr. Ali Çetin</h5>
								<button type="button" class="btn info btn-md">Doktora Sorun</button>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>


	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>