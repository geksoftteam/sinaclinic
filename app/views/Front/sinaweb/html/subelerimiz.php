<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b>Alibey Hospital</b></h4>
	</div>



	<div class="container p-0 ">		
		<nav class="nav">
			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="#">Şubeler</a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="saglik-rehberi.php">Alibey Hospital</a>
		</nav>

	</div>


	<div class="clearfix"></div>


	<div class="container p-4">
		<div class="row guide">
			<div class="news col-md-7">
				<img src="assets/img/alibey-hospital-zoom.png" alt="">


				<div class="yazi-detay">
					<div class="yazi-detay mt-3">
						<h3>Alibey Hospital</h3>
						<p>İstanbul Avrupa Yakasında , uluslararası  standart  ve kalitede  hizmet veren   Alibey  Hospital hastanesi olarak; 18 uzman doktor, 20 sağlık personeli, 3 Kadın Doğum Uzmanı, 3 Diş Hekimi, 2 Genel Cerrahi Uzmanı, 1 Dahiliye ,1 Ortopedi ve travmatoloji,1 cildiye,1 Çocuk,1 Nöroloji,1 Üroloji,1 Göz, 1 KBB, Anestezi ve Reanimasyon Uzmanı1 Estetisyen, 1 Diyetisyen , 3 Genel Yoğun Bakım, 5 Yeni Doğan Bakımı olmak üzere  31  yatak kapasitesi ile geniş  bir teknoloji  imkanına sahiptir.</p>
						<br>
						<h4><b>VİZYON</b></h4>
						<br>
						<p>Özel  Alibey  Hospital Hastanesi, ülkemizin en güvenilir sağlık kuruluşlarından biri olmayı hedeflemektedir. </p>
						<br>
						<h4><b>MİSYON</b></h4>
						<br>

						<p >Özel Alibey  Hospital Hastanesi tüm çalışanlarıyla,uluslararası standartlarda verdiği hizmetle zirveyi hedefler, hasta mahremiyetini korur, güvenliğini  sağlar,memnuniyetini kazanır,insana saygı duyar ve ahlâkı esas alır.</p>	
					</div>

					<div class="col-md-12">
						<div class="owl-carousel owl5 owl-theme owlnavstyle">
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
						</div>
					</div>


				</div>
			</div>
			<div class="news col-md-5">
				<div class="blog-card card" style="border: none;">
					<h3 class="card-header">RANDEVU AL</h3>
					<br>
					<h3 class="card-header">ŞUBELER</h3>

					<div class="card-body yesil">
						<img src="assets/img/pexels-pixabay-269077.jpg">
						<h5>SİNA CLINIC</h5>
						<p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit. </p>
					</div>

					<div class="card-body">
						<img src="assets/img/national-cancer-institute-1c8sj2IO2I4-unsplash.jpg">
						<h5>SİNA CLINIC</h5>
						<p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit. </p>
					</div>

					<div class="card-body">
						<img src="assets/img/graham-ruttan-aMNLYoT2z_I-unsplash.jpg">
						<h5>SİNA CLINIC</h5>
						<p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit. </p>
					</div>
				</div>	
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="container">
		<div class="row contact-item">

			<div class="item">
				<h5 class="">Alibey Hospital İletişim Bilgileri </h5>
				<p class="">Adres: Karadolap Mah. Neşeli Sok. Yağmur </p>
				<p class="">Telefon:  +90 0553 004 67 97  </p>
				<p class="">Fax: +90 0553 004 67 97 </p>
			</div>
			<div class="item">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3007.54478831756!2d28.925163315857848!3d41.07894202302147!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab0e0dd4826eb%3A0x2bf08a0f35f5cb1c!2zS2FyYWRvbGFwLCBOZcWfZWxpIFNrLiBObzoyMiwgMzQwNjUgRXnDvHBzdWx0YW4vxLBzdGFuYnVs!5e0!3m2!1str!2str!4v1618058638217!5m2!1str!2str" width="396" height="192" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			</div>

		</div>
	</div>



	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>