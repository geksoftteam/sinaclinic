$('.owl1').owlCarousel({
    stagePadding:60,
    loop:false,
    margin:30,
    navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:true
        },
        900:{
            items:3,
            nav:true
        },
        1100:{
            items:4,
            nav:true,
            loop:false
        }
    }
})

$('.owl2').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:true
        },

        700:{
            items:3,
            nav:true
        },
        1000:{
            items:4,
            nav:true,
            loop:false
        }
    }
})

$('.owl3').owlCarousel({
    loop:true,
    margin:20,
    nav:true,
    dots:false,
    navText: ["<i class='fa fa-caret-left'></i>", "<i class='fa fa-caret-right'></i>"],
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:true
        },
        900:{
            items:3,
            nav:true
        },
        1100:{
            items:5,
            nav:true,
            loop:false
        }
    }
})


$('.owl4').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    items:1,
    navText: ["<i class='fa fa-caret-left'></i>", "<i class='fa fa-caret-right'></i>"],
})


$('.owl5').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    items:3,
    navText: ["<i class='fa fa-caret-left'></i>", "<i class='fa fa-caret-right'></i>"]
})

$('.owl6').owlCarousel({
    stagePadding:60,
    loop:false,
    margin:30,
    navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:true
        },
        900:{
            items:3,
            nav:true
        },
        1100:{
            items:4,
            nav:true,
            loop:false
        }
    }
})


$('.owl7').owlCarousel({
    loop:true,
    margin:20,
    nav:false,
    dots:false,
    items:4,
})

var lightbox = $('.gallery a').simpleLightbox({ /* options */ });