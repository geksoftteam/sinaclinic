<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b>Diş</b></h4>
	</div>


	<div class="container p-0">		
		<nav class="nav">
			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="#">Bölümlerimiz</a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="saglik-rehberi.php">Diş</a>
		</nav>

	</div>


	<div class="clearfix"></div>


	<div class="container p-4">
		<div class="row guide">
			<div class="news col-md-7">
				<img src="assets/img/white-teeth-Z3KUYBD.jpg" alt="">


				<div class="yazi-detay">

					<h5 class="mt-5"><b>Diş Bölümü Nedir?</b></h5>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,ficia deserunt mollit anim id est laborum.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<div class="col-md-12">
						<div class="owl-carousel owl5 owl-theme owlnavstyle">
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
						</div>
					</div>


				</div>
			</div>
			<div class="news col-md-5">
				<div class="blog-card card" style="border: none;">
					<h3 class="card-header">HABERLER</h3>

					<div class="card-body">
						<img src="assets/img/dental-surgeon-examining-patient-MDUG5LB.jpg">
						<h5>Special title treatment</h5>
						<p>With supporting text below as a natural lead-in to additional content.</p>
						<a href="#" class="btn btn-primary">Lorem ipsum dolor ?</a>
					</div>

					<div class="card-body">
						<img src="assets/img/part-of-dentist-choosing-color-teeth-from-palette-QPDEEAD.jpg">
						<h5>Special title treatment</h5>
						<p>With supporting text below as a natural lead-in to additional content.</p>
						<a href="#" class="btn btn-primary">Lorem ipsum dolor ?</a>
					</div>

					<div class="card-body">
						<img src="assets/img/teeth.png">
						<h5>Special title treatment</h5>
						<p>With supporting text below as a natural lead-in to additional content.</p>
						<a href="#" class="btn btn-primary">Lorem ipsum dolor ?</a>
					</div>

					<div class="card-body">
						<img src="assets/img/repairing-tooth-EEBYC32.jpg">
						<h5>Special title treatment</h5>
						<p>With supporting text below as a natural lead-in to additional content.</p>
						<a href="#" class="btn btn-primary">Lorem ipsum dolor ?</a>
					</div>

					<div class="card-body">
						<img src="assets/img/medical-equipment-JQ23CTL.jpg">
						<h5>Special title treatment</h5>
						<p>With supporting text below as a natural lead-in to additional content.</p>
						<a href="#" class="btn btn-primary">Lorem ipsum dolor ?</a>
					</div>

				</div>	
			</div>
		</div>
	</div>

	<div class="clearfix"></div>
	<div class="container-fluid space">
		<div class="div  mt-5">
			<h3 class="text-center p-3"><b>SAĞLIK REHBERİ</b></h3>
		</div>
		<div class=" sliderarea slideimg row">
			<div class="owl-carousel owl1 owl-theme owlnavstyle py-4">
				<div class="item">
					<img src="assets/img/medical-equipment-JQ23CTL.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-outline-secondary"><b>Lorem İpsum Dolor?</b></button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/repairing-tooth-EEBYC32.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-primary">Lorem İpsum Dolor?</button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/teeth.png" at="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-outline-secondary"><b>Lorem İpsum Dolor?</b></button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/dental-surgeon-examining-patient-MDUG5LB.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-primary">Lorem İpsum Dolor?</button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/part-of-dentist-choosing-color-teeth-from-palette-QPDEEAD.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-outline-secondary">Lorem İpsum Dolor?</button>
					</div>
				</div>
				<div class="item">
					<img src="assets/img/anatomy-of-healthy-teeth-and-tooth-dental-implant--PAF6ZMW.jpg" alt="">
					<div class="p-3">
						<h6>Lorem Ipsum is simpl</h6>
						<p><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<button type="button" class="btn btn-primary">Lorem İpsum Dolor?</button>
					</div>
				</div>
			</div>

		</div>
	</div>



	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>