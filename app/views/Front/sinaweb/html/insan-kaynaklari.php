<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b>İnsan Kaynakları</b></h4>
	</div>



	<div class="container p-0">
		
		<nav class="nav">
			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="dr-sor.php">İnsan Kaynakları</a>
		</nav>
	</div>

</div>

<div class="clearfix"></div>


<div class="container mt-5">
	<div class="row">
		<div class="col-md-8 ques">
			<form>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputPassword4"><b>İsim / Soyisim</b>  </label>
						<input type="text" class="form-control" id="inputPassword4">
					</div>
					<div class="form-group col-md-6">
						<label for="inputEmail4"> <b>Email</b> </label>
						<input type="email" class="form-control" id="inputEmail4" >
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputPassword4"><b>Telefon</b></label>
						<input type="text" class="form-control" id="inputPassword4">
					</div>
					<div class="form-group col-md-6">
						<label for="inputEmail4"><b>Gsm</b></label>
						<input type="email" class="form-control" id="inputEmail4" >
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputPassword4"><b>Talep Edilen Pozisyon </b> </label>
						<input type="text" class="form-control" id="inputPassword4">
					</div>
				</div>  

				<div class="form-row">

					<div class="form-group col-md-6">
						<label for="exampleFormControlTextarea1">
							<b> Neden Bizimle Çalışmak İstiyorsunuz ?</b>
						</label>
						<textarea class="form-control" id="exampleFormControlTextarea1" rows="7"></textarea>
					</div>
					<div class="form-group col-md-6" style="position: relative; padding-top: 4%; padding-left: 20px;">

						<label for="exampleFormControlFile1">CV (Sadece doc,docx,pdf dökümanları yüklenebilir)</label>
						<input type="file" class="form-control-file" id="exampleFormControlFile1">
					</div> 		

					<button type="button" class="m-3 btn  info btn-md">GÖNDER</button>

				</div>  





			</form>									

		</div>


		<div class="news col-md-4">
			<div class="blog-card card" style="border: none;">
				<h3 class="card-header">BÖLÜMLER</h3>

				<div class="col-md-12 list">
					<ul>
						<li><a href="#"></a>Psikiyatri</li>
						<li><a href="#"></a>Beslenme ve Diyetetik</li>
						<li><a href="#"></a>Cildiye</li>
					</ul>
				</div>
			</div>	
		</div>
	</div>
</div>
<?php include 'theme/footer.php'; ?>


<?php include 'theme/js.php'; ?>

</body>
</html>