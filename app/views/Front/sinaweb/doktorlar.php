<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= 'Doktorlarımız' ?></title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>


	<div class="container mt-5">
		<h4><b>Hekimler</b></h4>
	</div>



	<div class="container p-0">

		<nav class="nav">
			<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
			<i class="fas fa-angle-right"></i>
			Doktorlarımız

		</nav>
	</div>

	<div class="container mt-3">
		<div class="row justify-content-center mt-2">
			<div class="content-yazi">
				<?php foreach ($doktorlar as $dr): ?>
					<div class="item">
						<img src="<?= SITE_UPLOAD_DIR.'page/'.$dr['doktor_image'] ?>" alt="">
						<div class="item-icerik"><p class="mb-0"><b><?= is_js($dr['doktor_name']) ?></b></p>
							<a href="<?= SITE_URL.'doktor/detay/'.$dr['doktor_seo_name'] ?>" class="button1 btn">Profili Gör</a>

						</div>
					</div>

				<?php endforeach ?>
				


			</div>
		</div>

	</div>



	<div class="clearfix"></div>



	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>