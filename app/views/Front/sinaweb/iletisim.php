<!DOCTYPE html>
<html lang="tr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b>İletişim</b></h4>
	</div>



	<div class="container p-0">
		
		<nav class="nav">
			<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
			<a href="<?= SITE_URL.'Iletisim' ?>">İletişim</a>
		</nav>

	</div>

</div>
<?php foreach ($subeler as $item): ?>
	<div class="container">
		<div class="row contact-item">

			<div class="item">
				<h5 class=""><?= is_js($item['sube_name']) ?></h5>
				<p class="">Telefon : <?= is_js($item['sube_telefon']) ?></p>
				<p class="">Faks : <?= is_js($item['sube_fax']) ?></p>
				<p class="">Adres : <?= is_js($item['sube_adres']) ?></p>
			</div>
			<div class="item right" >
				<?= is_js($item['sube_maps']) ?>
			</div>

		</div>
	</div>
<?php endforeach ?>



<?php include 'theme/footer.php'; ?>


<?php include 'theme/js.php'; ?>

</body>
</html>