	<div class="container-fluid p-0">
		<div class="row m-0 p-0">
			<div id="carouselExampleIndicators" class="carousel slide slider align-items-center" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
				</ol>
				<div class="carousel-inner">
					<?php $i = 1; foreach ($slider as $var): ?>
					<div class="carousel-item <?= $i==1 ? "active": ""; ?>">
						<img class="d-block" src="<?php echo SITE_UPLOAD_DIR.'slider/'.$var['slider2_resim'] ?>" alt="First slide">
						<div class="flexboxcapsul col-md-12">
							<div class="p-0 col-xl-4 col-lg-5 col-md-6">
								<div class="flex">	
									<a href="<?= SITE_URL.'Onlinerandevu' ?>">
										<button class="flexbox btn-light">
											<i class="fas fa-video"></i>
											<p><b>Görüntülü Görüşme Randevusu</b></p>
										</button>
									</a>

									<a href="<?= SITE_URL.'Onlinerandevu' ?>">
										<button class="flexbox info">
											<i class="fas fa-briefcase-medical"></i>
											<p><b>Muayne Randevusu</b></p>
										</button>
									</a>
								</div>
								<div class="flex">
									<a href="<?= SITE_URL.'LabSonuclar' ?>">
										<button class="flexbox info">
											<i class="fas fa-file-medical"></i>
											<p><b>E-Sonuç</b></p>
										</button>
									</a>
									<a href="<?= SITE_URL.'bolum' ?>">
										<button class="flexbox btn-light">
											<i class="fas fa-heartbeat"></i>
											<p><b>Bölümlerimiz</b></p>
										</button>
									</a>
								</div>
							</div>
							<div class="p-0 col-xl-8 col-lg-7 col-md-6">

								<h2 class="text-white"><?php echo is_js($var['slider2_name']) ?></h2>

								<p><b><?php echo is_js($var['slider2_name']) ?></b></p>

								<a href="<?php echo SITE_URL.is_js($var['slider2_link']) ?>">
									<button type="button" class="btn btn-light dty-bilgi"><b>DETAYLI BİLGİ </b>
										<img src="<?php echo $theme_patch ?>/assets/icon/Arrowicon.png" style="width: 25px;
										float: right;">
									</button>
								</a>


							</div>
						</div>
						<div>
							<!--<a href="#" class="dr"> <img src="<?php echo $theme_patch ?>/assets/icon/doktorunuza_sorun_button.png" alt=""></a>-->
						</div>
					</div>

					<?php $i++; endforeach ?>


					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>