<footer>
	<div class="container">
		<div class="row footer">
			<div class="col-lg-2 col-md-3 mt-5">
				<a href="<?= SITE_URL ?>"><img src="<?= SITE_URL . 'public/files/logo/' . $set['site_logo'] ?>" alt="<?= $set['General_title'] ?>"></a>

				<div class="footersocialmedia">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-youtube"></i></a>
					<a href="#"><i class="fab fa-instagram"></i></a>
				</div>
			</div>
			<div class="col-lg-2 col-md-3 mt-5">
				<h5><b>Kurumsal</b></h5>
				<ul>
					
					<?php foreach ($nav as $item): ?>

						<li class="fmenu"><a href="<?= SITE_URL.$item['nav_url'] ?>"><?= is_js($item['nav_name']) ?></a></li>
						
					<?php endforeach ?>
					
				</ul>
			</div>

			<div class="col-lg-2 col-md-3 mt-5">
				<h5><b>Bölümler</b></h5>
				<ul>
					<li>Ağız ve Diş Sağlığı</li>
					<li>Psikiyatri</li>
					<li>Beslenme ve Diyetetik</li>
					<li>Cildiye</li>
				</ul>
			</div>


			<div class="col-lg-4 col-md-5 mt-5">
				<h5><b>Bize Ulaşın</b></h5>
				<ul>
					<li><?= $set['tel'] ?></li>
					<li><?= $set['mail'] ?></li>
					<li><?= $set['adres'] ?></li>

				</ul>
			</div>

		</div>

		
	</div>


	<div class="container-fluid">
		<p class="text-center mt-5 text-white  mb-0" style="padding: 15px;
		border-top: 1px solid;"> © <?= date('Y'). ' '. $set['General_title'] ?></p>
	</div>
</footer>
