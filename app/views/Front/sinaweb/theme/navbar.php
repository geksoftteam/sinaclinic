<?php
$ds = DIRECTORY_SEPARATOR;
$mainMenuPath = ROOT . $ds . 'app' . $ds . 'views' . $ds . "Main_Menu.config.xml";

$manager = new \App\Libs\MenuManager();
$menu = $manager->loadMenu($mainMenuPath);

/**
 * @param \App\Libs\MenuItem $menuItem
 * @param int $level
 * @return \App\Libs\Html\HtmlElement
 */
function renderMainMenuItem($menuItem, $level = 0){
    $li = new \App\Libs\Html\HtmlElement('li');
    if($level == 0){
        $li->addClass('nav-item');
    }
    $a = new \App\Libs\Html\HtmlElement('a');
    $a->setAttr('href',$menuItem->getLinkUrl());
    $a->setAttr('title',$menuItem->getText());
    if($menuItem->hasChilds()){
        $a->setAttr('data-toggle','dropdown');
    }
    $a->addChild(new \App\Libs\Html\TextNode($menuItem->getText()));
    $li->addChild($a);
    if($menuItem->hasChilds()){
        $ul = new \App\Libs\Html\HtmlElement('ul');
        $ul->addClass('dropdown-menu');
        foreach ($menuItem->getChilds() as $chld){
            $ul->addChild(renderMainMenuItem($chld,$level + 1));
        }
        $li->addChild($ul);
    }
    return $li;
} 

?>

<div class="navbar">
    <div>
        <a href="<?= SITE_URL ?>"><img src="<?= SITE_URL . 'public/files/logo/' . $set['site_logo'] ?>" alt=""></a>
    </div>

    <div>
        <nav class="navbar navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="true" aria-label="Toggle navigation">
            <span class="text-white"><i class="fa fa-bars"></i></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarNav">

            <ul class="navbar-nav">
                <?php
                foreach ($menu->getMenuItems() as $mi){
                    $liElem =  renderMainMenuItem($mi);
                    echo $liElem->toHtmlString();
                }
                ?>
            </ul>



        </div>

    </nav>

</div>

<div class="navright ">
    <div class="navbutton">
        <button type="button" class="btn info btn-md"><p>Görüntülü </p>
            <p>Görüşme</p></button>
            <button type="button" class="btn btn-light btn-md"><p>Hızlı</p>
                <p>Randevu</p></button>
            </div>

        </div>

    </div>
