<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Şubelerimiz</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b>Hastaneler</b></h4>
	</div>
	
	<div class="container  p-0">
		
		<nav class="nav">
			<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>

			<a href="<?= SITE_URL.'sube' ?>">Hastaneler</a>
		</nav>
	</div>
	<div class="content-yazi container mt-3">
		<div class="row">
			<?php foreach($subeler as $item) :?>
				<div class="hospital col-md-3">
					<img src="<?= SITE_UPLOAD_DIR.'page/'.$item['sube_image'] ?>">
				</div>
				<div class="hospital col-md-3">
					<a href="<?= SITE_URL.'sube/detay/'.$item['sube_seo_name'] ?>">
						<h5><?php echo is_js($item['sube_name'])  ?></h5>
					</a>
					<p><?php echo is_js($item['sube_adres']) ?> </p>
				</div>

			<?php endforeach; ?>

		</div>
	</div>
</div>

</div>

<div class="clearfix"></div>



<?php include 'theme/footer.php'; ?>


<?php include 'theme/js.php'; ?>

</body>
</html>