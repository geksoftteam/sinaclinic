<!DOCTYPE html>
<html lang="tr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= is_js($page['page_name']) ?></title>



	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h3><b><?= is_js($page['page_name']) ?></b></h3>
	</div>

	<div class="container p-0">		
		<nav class="nav">
			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="<?= SITE_URL.$cat['nav_url'] ?>"><?= is_js($cat['nav_name']) ?> > </a>
			<p class="linkss"><?= is_js($page['page_name']) ?></p>
		</nav>
	</div>

	<div class="clearfix"></div>

	<div class="container mt-3">
		<div class="row">
			<div class="news col-md-8">
				<div class="yazi-detay mt-3">
					<?php echo is_js($page['page_content']) ?>
				</div>
			</div>
			<div class="news col-md-4">
				<div class="blog-card card" style="border: none;">
					<h3 class="card-header"><?= is_js($cat['nav_name']) ?></h3>

					<div class="col-md-12 list">
						<ul>
							<?php foreach ($sidemenu as $value): ?>
								<li>
									<a href="<?= SITE_URL.$cat['nav_url'].'/'.$value['page_url'] ?>" title="<?= is_js($value['page_name']) ?>">
										<?= is_js($value['page_name']) ?>
									</a>
								</li>
							<?php endforeach ?>

						</ul>
					</div>
				</div>	
			</div>
		</div>
	</div>


	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>