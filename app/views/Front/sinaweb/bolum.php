<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= 'Bölümlerimiz' ?></title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b><?= 'Bölümlerimiz'; ?></b></h4>
	</div>
	<div class="container p-0">		
		<nav class="nav">
			<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
			<i class="fas fa-angle-right"></i>
			Bölümlerimiz
		</nav>
	</div>
	<div class="clearfix"></div>

	<div class="container mt-4">
		<div class="row">
			<div class="news col-md-9">
				<div class="container mt-3">
					<div class="row justify-content-center mt-2">
						<div class="content-yazi">
							<?php foreach ($bolumler as $blm): ?>

								<div class="item">
									<div class="item-icerik"><p class="mb-0"><b><?= is_js($blm['bolum_name']) ?></b></p>
										<a href="<?= SITE_URL.'bolum/detay/'.$blm['bolum_seo_name'] ?>" class="button1 btn">Detay</a>

									</div>
								</div>

							<?php endforeach ?>



						</div>
					</div>

				</div>

			</div>
			<div class="news col-md-3">
				<div class="blog-card card" style="border: none;">



					<div class="sliderarea row p-1 mt-2">
						<h4 class="m-auto p-3"><b>DOKTORLAR</b></h4>
						<div class="owl-carousel round owl4 owl-theme owlnavstyle py-4">

							<?php foreach ($doktorlar as $value): ?>
								

								<div class="item ">
									<div class="round-image">
										<img src="<?= SITE_UPLOAD_DIR.'page/'.$value['doktor_image'] ?>" alt="">
									</div>

									<div class="p-3 text-center">
										<a href="<?= SITE_URL.'doktor/detay/'.$value['doktor_seo_name'] ?>">
											<h5><?= is_js($value['doktor_name']) ?></h5>
										</a>
										<p><b><?= is_js($value['doktor_desc']) ?></b></p>

									</div>
								</div>

							<?php endforeach ?>

						</div>
					</div>

				</div>	
			</div>
		</div>
	</div>

	<div class="clearfix"></div>


	
	<?php if (count($blog)>0): ?>

		<div class="container-fluid space">
			<div class="div  mt-5">
				<h3 class="text-center p-3"><b>SAĞLIK REHBERİ</b></h3>
			</div>
			<div class=" sliderarea slideimg row">
				<div class="owl-carousel owl1 owl-theme owlnavstyle py-4">

					<?php foreach ($blog as $blg): ?>

						<div class="item">
							<img src="<?= SITE_UPLOAD_DIR.'page/'.$blg['page_image'] ?>" alt="">
							<div class="p-3">
								<a href="<?php echo SITE_URL.$blg['nav_url'].'/'.$blg['page_url'] ?>">
									<h6><?= is_js($blg['page_name']) ?></h6>

								</a>
								<p><b><?= is_js($blg['page_jenerik']) ?></b></p>
							</div>
						</div>

					<?php endforeach ?>

				</div>

			</div>
		</div>
	<?php endif ?>
	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>