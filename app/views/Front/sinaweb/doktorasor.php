<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b>DOKTORUNUZA SORUN </b></h4>
	</div>


	<div class="container p-0">
		<nav class="nav">
			<a  href="index.php"><i class="fas fa-home"></i></a>
			<a  href="index.php"><i class="fas fa-angle-right"></i></a>
			<a href="dr-sor.php">Doktorunuza Sorun</a>
		</nav>
	</div>




	<div class="container mt-5">
		<div class="row">
			<div class="col-md-8 ques">
				<form action="#" id="doktorasor" class="doktorasor">
					<div class="form-row">
						<div class="form-group col-md-6">
							<label > <b>İsim / Soyisim  </b></label>
							<input type="text" class="form-control zorunlu"  name="isimsoyisim">
						</div>
						<div class="form-group col-md-6">
							<label><b>Email</b></label>
							<input type="email" class="form-control zorunlu" name="mailadresi">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-6">
							<label ><b>Telefon / GSM</b></label>
							<input type="text" class="form-control zorunlu" name="telefon">
						</div>
						<div class="form-group col-md-6">
							<label for="exampleFormControlSelect1"><b>Doktor</b></label>
							<select class="form-control" id="exampleFormControlSelect1" name="doktor">
								<?php foreach ($doktorlar as $value): ?>
									<option value="<?= is_js($value['doktor_name']) ?>"><?= is_js($value['doktor_name']) ?></option>
								<?php endforeach ?>


							</select>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="exampleFormControlTextarea1"><b>Mesajınız</b></label>
							<textarea class="form-control zorunlu" id="exampleFormControlTextarea1" rows="7" name="mesaj"></textarea>
						</div>
					</div>  

					<div class="form-row">
						<div class="form-group col-md-12">
							<button type="button" class="btn info btn-md float-right doktorasorBtn">Gönder</button>
						</div> 
					</div>
				</form>								
			</div>


			<div class="col-md-3 offset-md-1">
				<div class="sliderarea row">
					<h4 class="m-auto p-3"><b>DOKTORLAR</b></h4>
					<div class="owl-carousel round owl4 owl-theme owlnavstyle py-4">
						<?php foreach ($doktorlar as $dr): ?>
							<div class="item ">
								<div class="round-image">
									<img src="<?= SITE_UPLOAD_DIR.'page/'.$dr['doktor_image'] ?>" alt="">
								</div>

								<div class="p-3">
									<a href="<?= SITE_URL.'doktor/detay/'.$dr['doktor_seo_name'] ?>">
										<h5 class="text-center"><?= is_js($dr['doktor_name']) ?></h5>
										
									</a>

								</div>
							</div>


						<?php endforeach ?>

					</div>

				</div>
			</div>
		</div>
	</div>


	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
	<script>
		
		$('.doktorasorBtn').click(function(){
			zor('zorunlu')
			siteUrl = $('.site_url').val();

			$.post(siteUrl+'Iletisim/Send', $('#doktorasor').serialize(), function(data) {
				if (data==0) {
					swal('Hata', 'Bir Sorun Oluştu', 'error')

				}else{
					$('.doktorasorBtn').prop('disabled', 'true')
					$('.doktorasorBtn').html('Mesajınız Gönderilmiştir.')
					swal('Başarılı', 'Sorunuz İlgili Doktora İletilmiştir.', 'success')

				}
			});

		})

		function zor(clasi) {
			$('.'+clasi).each(function(index, element) {
				var degert = $(this).val();
				if(degert == '' || degert ==-1){
					$(this).css('border','1px solid red');
					$(this).focus();
					die();
				} else {
					$(this).css('border','1px solid green');
				}
			});
		}
	</script>

</body>
</html>