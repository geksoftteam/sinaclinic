<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= 'Bölümlerimiz' ?></title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b>
			<?= isset($cat['nav_name']) ? is_js($cat['nav_name']) : "Bölümlerimiz" ?>

		</b></h4>
	</div>
	<div class="container p-0">		
		<nav class="nav">
			<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
			<i class="fas fa-angle-right"></i>
			<?= isset($cat['nav_name']) ? is_js($cat['nav_name']) : "Bölümlerimiz" ?>
		</nav>
	</div>
	<div class="clearfix"></div>



	<div class="container space">

		<div class=" sliderarea slideimg row">
			<div class="owl-carousel owl1 owl-theme owlnavstyle py-4">
				<?php  foreach ($cat_page as $g):  ?>

					<div class="item">
						<img src="<?php echo SITE_UPLOAD_DIR.'page/'.$g['page_image'] ?>" alt="">
						<div class="p-3">
							<a href="<?php echo SITE_URL.$g['nav_url'].'/'.$g['page_url'] ?>">
								<h6><?php echo is_js($g['page_name']) ?></h6>

							</a>
							<p><b><?php echo kisalt(is_js($g['page_jenerik']),70) ?></b></p>
						</div>
					</div>

				<?php endforeach ?>
			</div>

		</div>
	</div>
	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>