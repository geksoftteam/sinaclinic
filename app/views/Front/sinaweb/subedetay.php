<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= is_js($sube['sube_name']) ?> - Şubelerimiz</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b><?= is_js($sube['sube_name']) ?></b></h4>
	</div>



	<div class="container p-0 ">		
		<nav class="nav">
			<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
			<i class="fas fa-angle-right"></i> 
			<a href="<?= SITE_URL.'sube' ?>">Şubeler</a>
			<i class="fas fa-angle-right"></i> 
			<?= is_js($sube['sube_name']) ?> 
		</nav>

	</div>


	<div class="clearfix"></div>


	<div class="container p-4">
		<div class="row guide">
			<div class="news col-md-7">
				<img src="assets/img/alibey-hospital-zoom.png" alt="">


				<div class="yazi-detay">
					<div class="yazi-detay mt-3">
						<h3><?= is_js($sube['sube_name']) ?></h3>
						<?= is_js($sube['sube_content']) ?>
					</div>
					<!--
					<div class="col-md-12">
						<div class="owl-carousel owl5 owl-theme owlnavstyle">
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="assets/img/Slider-1.png" alt="">
								</a>
							</div>
						</div>
					</div>
				-->

			</div>
		</div>
		<div class="news col-md-5">
			<div class="blog-card card" style="border: none;">

				<h3 class="card-header">ŞUBELER</h3>
				<?php foreach ($subeler as $item): ?>
					

					<div class="card-body yesil">
						<img src="<?= SITE_UPLOAD_DIR.'page/'.$item['sube_image'] ?>">
						<a href="<?= SITE_URL.'sube/detay/'.$item['sube_seo_name'] ?>">
							<h5><?php echo is_js($item['sube_name'])  ?></h5>
						</a>
						<span>Telefon : <?php echo is_js($item['sube_telefon'])  ?></span>
						<br>
						<span>Faks : <?php echo is_js($item['sube_fax'])  ?></span>
						<br>
						<span>Adres : <?php echo is_js($item['sube_adres'])  ?></span>
					</div>
				<?php endforeach ?>

			</div>	
		</div>
	</div>
</div>

<div class="clearfix"></div>

<div class="container">
	<div class="row contact-item">

		<div class="item">
			<h5 class=""><?= is_js($sube['sube_name']) ?></h5>
			<p class="">Telefon:  <?php echo is_js($sube['sube_telefon'])  ?> </p>
			<p class="">Fax: <?php echo is_js($sube['sube_fax'])  ?> </p>
			<p class="">Adres:  <?php echo is_js($sube['sube_adres'])  ?></p>
			
		</div>
		<div class="item">
			<?php echo is_js($sube['sube_maps'])  ?>
		</div>

	</div>
</div>



<?php include 'theme/footer.php'; ?>


<?php include 'theme/js.php'; ?>

</body>
</html>