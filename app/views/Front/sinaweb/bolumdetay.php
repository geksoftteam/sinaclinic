<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<div class="container mt-5">
		<h4><b><?= is_js($bolum['bolum_name']) ?></b></h4>
	</div>

	<div class="container p-0">		
		<nav class="nav">
			<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
			<i class="fas fa-angle-right"></i> 
			<a href="<?= SITE_URL.'bolum' ?>">Bölümlerimiz</a>
			<i class="fas fa-angle-right"></i> 
			<a href="<?= SITE_URL.'bolum/detay/'.$bolum['bolum_seo_name'] ?>"><?= is_js($bolum['bolum_name']) ?></a>
		</nav>

	</div>

	<div class="clearfix"></div>

	<div class="container mt-4">
		<div class="row">
			<div class="news col-md-9">
				<div>
					<img src="assets/img/HASTANE iç görsel.jpg" alt="">
				</div>

				<div class="yazi-detay mt-5">
					<h5><?= is_js($bolum['bolum_name']) ?></h5>
					<?= is_js($bolum['bolum_content']) ?>
				</div>
				<?php if (count($bolumDoktor)>0): ?>
					<div class="content-yazi">
						<?php foreach ($bolumDoktor as $item): ?>
							<div class="item">
								<img src="<?= SITE_UPLOAD_DIR.'page/'.$item['doktor_image'] ?>" alt="">
								<div class="item-icerik"><p class="mb-0"><b><?= is_js($item['doktor_name']) ?></b></p>
									<a href="<?= SITE_URL.'doktor/detay/'.$item['doktor_seo_name'] ?>" class="button1 btn">
										Profili Gör
									</a>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				<?php endif ?>

			</div>

			<div class="news col-md-3">

				<div class="blog-card card" style="border: none;">

					<h3 class="card-header">BÖLÜMLER</h3>

					<div class="col-md-12 list">
						<ul>
							<?php foreach ($bolumler as $value): ?>
								
								<li><a href="<?= SITE_URL.'bolum/detay/'.$value['bolum_seo_name'] ?>"><?= is_js($value['bolum_name']) ?></a></li>
							<?php endforeach ?>

						</ul>

					</div>





				</div>	
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<?php if (count($blog)>0): ?>

		<div class="container-fluid space">
			<div class="div  mt-5">
				<h3 class="text-center p-3"><b>SAĞLIK REHBERİ</b></h3>
			</div>
			<div class=" sliderarea slideimg row">
				<div class="owl-carousel owl1 owl-theme owlnavstyle py-4">

					<?php foreach ($blog as $blg): ?>

						<div class="item">
							<img src="<?= SITE_UPLOAD_DIR.'page/'.$blg['page_image'] ?>" alt="">
							<div class="p-3">
								<a href="<?php echo SITE_URL.$blg['nav_url'].'/'.$blg['page_url'] ?>">
									<h6><?= is_js($blg['page_name']) ?></h6>

								</a>
								<p><b><?= is_js($blg['page_jenerik']) ?></b></p>
							</div>
						</div>

					<?php endforeach ?>

				</div>

			</div>
		</div>
	<?php endif ?>

	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>