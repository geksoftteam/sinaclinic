<!DOCTYPE html>
<html lang="tr">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= is_js($doktor['doktor_name']) ?></title>
	
	<?php include 'theme/src.php' ?>

</head>
<body>

	<?php include 'theme/navbar.php' ?>

	<div class="container mt-5">
		<h4><b><?= is_js($doktor['doktor_name']) ?></b></h4>
	</div>


	<div class="container p-0">
		<nav class="nav">

			<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
			<i class="fas fa-angle-right"></i>
			<a href="<?= SITE_URL.'Doktor' ?>">Doktorlar</a>
			<i class="fas fa-angle-right"></i> 
			<?= is_js($doktor['doktor_name']) ?> 
		</nav>
	</div>

	<div class="container-fluid dr-bg">
		<div class="col-md-6 offset-md-2">
			<div class="row">
				<img src="assets/img/Omsan Çetin.png" alt="">
				<div>
					<h6><?= is_js($doktor['doktor_name']) ?></h6>
					<a href="<?= SITE_URL.'doktor/sorusor' ?>">
						<button type="button" class="btn info btn-md"><?= 'Doktorunuza Sorun' ?></button>

					</a>
				</div>
			</div>

		</div>
	</div>


	<div class="container">
		
		<div class="col-md-9 offset-md-1">
			<div class="yazi-title">
				<h3 class="card-title"><?= is_js($doktor['doktor_name']) ?> </h3>

			</div>


			<?= is_js($doktor['doktor_content']) ?>


		</div>
	</div>

	<div class="clearfix"></div>



	<div class="col-md-12 space">
		<h3 class="p-3 	text-center"><b>DOKTORLARIMIZ</b></h3>
	</div>
	<div class="container">
		<div class="sliderarea1 sliderarea row">
			<div class="owl-carousel round owl1 owl-theme owlnavstyle py-4">

				<?php foreach ($doktorlar as $dr): ?>
					
					<div class="item ">
						<div class="round-image">
							<img src="<?= SITE_UPLOAD_DIR.'page/'.$dr['doktor_image'] ?>" alt="">
						</div>
						<div class="p-3">
							<a href="<?= SITE_URL.'doktor/detay/'.$dr['doktor_seo_name'] ?>">
								<h6 class="text-center"><?= is_js($dr['doktor_name']) ?></h6>
								
							</a>
							<p><b><?= is_js($dr['doktor_desc']) ?></b></p>
						</div>
					</div>

				<?php endforeach ?>
			</div>
		</div>
	</div>



	<?php include 'theme/footer.php' ?>


	<?php include 'theme/js.php' ?>

</body>
</html>