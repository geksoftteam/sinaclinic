<!DOCTYPE html>
<html lang="tr">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>


</head>
<body>


	<?php include 'theme/navbar.php'; ?>

	<nav class="nav">
		<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
		<a  href="<?= SITE_URL ?>"><i class="fas fa-chevron-right"></i></a>
		<a href="<?= SITE_URL.'Iletisim' ?>">İletişim</a>
	</nav>

	<div class="clearfix"></div>

	<div class="container" style="width: 90%;">

		<div class="contact">

			<?php foreach ($subeler as $item): ?>

				<div class="card">

					<div class="card-body">
						<h5 class="card-title"><?= is_js($item['sube_name']) ?></h5>
					</div>
					<ul class="list-group list-group-flush">
						<li class="list-group-item">Telefon : <?= is_js($item['sube_telefon']) ?>  </li>
						<li class="list-group-item">Faks : <?= is_js($item['sube_fax']) ?></li>
						<li class="list-group-item">Adres : <?= is_js($item['sube_adres']) ?> </li>
					</ul>
					<div class="card-body" style="width: 100%;, padding: 0;">
						<?= is_js($item['sube_maps']) ?>
					</div>

				</div>

			<?php endforeach ?>

			

		</div>
		


	</div>

	<div class="clearfix"></div>


	<?php include 'theme/footer.php'; ?>


























	<?php include 'theme/js.php'; ?>

</body>
</html>