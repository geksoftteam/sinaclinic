<!DOCTYPE html>
<html lang="tr">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= is_js($page['page_name']) ?></title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	<nav class="nav">

		<a  href="index.php"><i class="fas fa-home"></i></a>
		<a  href="index.php"><i class="fas fa-chevron-right"></i></a>
		<a  href="<?= SITE_URL.$cat['nav_url'] ?>"><?= is_js($cat['nav_name']) ?></a>
		<a  href="index.php"><i class="fas fa-chevron-right"></i></a>
		<a class="linkss"><?= is_js($page['page_name']) ?></a>

	</nav>

	<div class="clearfix"></div>


	<div class="blog-img">
		
		<img src="<?php echo SITE_UPLOAD_DIR.'page/'.$value['page_image'] ?>" alt="">

	</div>
	
	
	<div class="blog">

		<div class="container">
			
			<h1 class="blog_title"><?= is_js($page['page_name']) ?></h1>

			<div class="blog_content">
				
				<?php echo is_js($page['page_content']) ?>

			</div>

		</div>

	</div>

	<div class="clearfix"></div>


	


	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>

</body>
</html>