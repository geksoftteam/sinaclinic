<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Sina Clinic</title>

  <?php include 'theme/src.php'; ?>



</head>
<body>

  <?php include 'theme/navbar.php'; ?>


  <nav class="nav">

   <a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
   <a  href="<?= SITE_URL ?>"><i class="fas fa-chevron-right"></i></a>
   <a href="<?= SITE_URL.'bolum' ?>">Bölümlerimiz</a>
   <a  href="<?= SITE_URL ?>"><i class="fas fa-chevron-right"></i></a>
   <a href="<?= SITE_URL.'bolum/detay/'.$bolum['bolum_seo_name'] ?>"><?= is_js($bolum['bolum_name']) ?></a>

 </nav>

 <div class="clearfix"></div>


 <div class="blog">

  <div class="container">

    <h1 class="blog_title"><?= is_js($bolum['bolum_name']) ?></h1>

    <div class="blog_content">

     <?= is_js($bolum['bolum_content']) ?>

   </div>

 </div>

</div>



<div class="clearfix"></div>


<?php if (count($bolumDoktor)>0): ?>

  <div class="container-fluid" style="width: 100%; padding: 0; margin: 0; margin-bottom: 15%;" >

    <div class="sliderarea5" >

      <div class="owl-carousel owl3 owl-theme owlnavstyle owl-loaded owl-drag">

        <div class="owl-stage-outer">
          <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1920px; padding-top: 15px;">

            <?php foreach ($bolumDoktor as $item): ?>

              <div class="owl-item active" style="width: 230px; height: 350px; margin-right: 10px; ">
                <a href="<?= SITE_URL.'doktor/detay/'.$item['doktor_seo_name'] ?>">

                  <div class="item">
                    <div class="icerik_img">
                      <img src="<?= SITE_UPLOAD_DIR.'page/'.$item['doktor_image'] ?>" alt="">
                    </div>
                    <div class="item-icerik">
                      <p><b><?= is_js($item['doktor_name']) ?></b></p>
                      <a href="<?= SITE_URL.'doktor/detay/'.$item['doktor_seo_name'] ?>" class="button1 btn">Profili Gör</a>

                    </div>
                  </div>
                </a>
              </div>

            <?php endforeach ?>

          </div>

        </div>

        <div class="owl-nav">
          <button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button>
          <button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button>
        </div>
        <div class="owl-dots disabled"></div>
      </div>

    </div>

  <?php endif ?>

</div>

<div class="clearfix"></div>

<div class="container ik">

  <div class="card">
    <div class="card-header">
     Bölümler
   </div>
   <ul class="list-group list-group-flush">

    <?php foreach ($bolumler as $value): ?>
     <li>
      <a class="list-group-item" href="<?= SITE_URL.'bolum/detay/'.$value['bolum_seo_name'] ?>"><?= is_js($value['bolum_name']) ?></a>
    </li>
  <?php endforeach ?>
</ul>

</div>


</div>

<div class="clearfix"></div>


<?php include 'theme/footer.php'; ?>























<?php include 'theme/js.php'; ?>

</body>
</html>