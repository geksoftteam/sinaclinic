<!DOCTYPE html>
<html lang="tr">
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Sina Clinic</title>

  <?php include 'theme/src.php'; ?>



</head>
<body>

  <?php include 'theme/navbar.php'; ?>


  <nav class="nav">
    <a  href="index.php"><i class="fas fa-home"></i></a>
    <a  href="index.php"><i class="fas fa-chevron-right"></i></a>
    <a href="doktora-sor.php">Doktorunuza Sorun</a>
  </nav>

  <div class="clearfix"></div>


  <div class="container" style="width: 90%;">

    <form action="#" id="doktorasor" class="doktorasor">
      
      <div class="form-row">

        <div class="form-group col-md-6">

          <label for="inputPassword4">İsim / Soyisim  </label>
          <input type="text" class="form-control" id="inputPassword4">

        </div>

        <div class="form-group col-md-6">

          <label for="inputEmail4">Email</label>
          <input type="email" class="form-control" id="inputEmail4">

        </div>

      </div>

      <div class="form-row">

        <div class="form-group col-md-6">

          <label for="inputPassword4">Telefon/GSM</label>
          <input type="text" class="form-control" id="inputPassword4">

        </div>

        <div class="form-group col-md-6">

          <label for="exampleFormControlSelect1">Doktor</label>
          <select class="form-control" id="exampleFormControlSelect1">
           <?php foreach ($doktorlar as $value): ?>
            <option value="<?= is_js($value['doktor_name']) ?>"><?= is_js($value['doktor_name']) ?></option>
          <?php endforeach ?>
        </select>

      </div>

    </div>

    <div class="form-row">

      <div class="form-group col-md-6">

        <label for="inputPassword4">Talep Edilen Pozisyon  </label>
        <input type="text" class="form-control" id="inputPassword4">

      </div>

    </div>  

    <div class="form-row">

      <div class="form-group col-md-6">

        <label for="exampleFormControlTextarea1">Neden Bizimle Çalışmak İstiyorsunuz ?</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="8"></textarea>

      </div>
      <div class="form-group col-md-6" style="position: relative; padding-top: 4%; padding-left: 20px;">

        <button type="button" class="btn btn-primary" style="background: #b6dce1; border: none; color: #1e3253; float: right; width: 160px;height: 47px;">Gönder</button>


      </div>    

    </div>  





  </form>   

</div>

<div class="clearfix"></div>


<div class="container-fluid" style="width: 100%; padding: 0; margin: 0;" >

  <div class="sliderarea4" >
    <h2><b>DOKTORLARIMIZ</b></h2>
    <div class="owl-carousel owl3 owl-theme owlnavstyle owl-loaded owl-drag" style="background: linear-gradient( 170deg, #f8f9fb 0%, #c2c2c2 100%); ">
      <div class="owl-stage-outer">
        <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; padding-top: 15px;">

          <?php foreach ($doktorlar as $dr): ?>
            <div class="owl-item active" style="">
              <a href="<?= SITE_URL.'doktor/detay/'.$dr['doktor_seo_name'] ?>">

                <div class="item">
                  <div class="icerik_img">
                    <img src="<?= SITE_UPLOAD_DIR.'page/'.$dr['doktor_image'] ?>" alt="">
                  </div>
                  <div class="item-icerik">
                    <a href="<?= SITE_URL.'doktor/detay/'.$dr['doktor_seo_name'] ?>" class="button1 btn"><?= is_js($dr['doktor_name']) ?></a>
                    <p class="mb-0"><b>L<?= is_js($dr['doktor_desc']) ?></b></p>
                  </div>
                </div>
              </a>
            </div>
          <?php endforeach ?>
        </div>
      </div>

      <div class="owl-nav">
        <button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button>
        <button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button>
      </div>
      <div class="owl-dots disabled"></div>
    </div>

  </div>

</div>

<div class="clearfix"></div>


<?php include 'theme/footer.php'; ?>























<?php include 'theme/js.php'; ?>



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script>

  $('.doktorasorBtn').click(function(){
    zor('zorunlu')
    siteUrl = $('.site_url').val();

    $.post(siteUrl+'Iletisim/Send', $('#doktorasor').serialize(), function(data) {
      if (data==0) {
        swal('Hata', 'Bir Sorun Oluştu', 'error')

      }else{
        $('.doktorasorBtn').prop('disabled', 'true')
        $('.doktorasorBtn').html('Mesajınız Gönderilmiştir.')
        swal('Başarılı', 'Sorunuz İlgili Doktora İletilmiştir.', 'success')

      }
    });

  })

  function zor(clasi) {
    $('.'+clasi).each(function(index, element) {
      var degert = $(this).val();
      if(degert == '' || degert ==-1){
        $(this).css('border','1px solid red');
        $(this).focus();
        die();
      } else {
        $(this).css('border','1px solid green');
      }
    });
  }
</script>

</body>
</html>