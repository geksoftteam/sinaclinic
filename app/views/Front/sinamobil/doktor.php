<!DOCTYPE html>
<html>
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= is_js($doktor['doktor_name']) ?>r</title>

	<?php include 'theme/src.php'; ?>


</head>
<body>

	<?php include 'theme/navbar.php'; ?>


	<nav class="nav">
		<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
		<a  href="<?= SITE_URL ?>"><i class="fas fa-chevron-right"></i></a>
		<a href="<?= SITE_URL.'Doktor' ?>">Doktorlar</a>
		<a  href="<?= SITE_URL ?>"><i class="fas fa-chevron-right"></i></a>
		<a href="doktor-ic.php"><?= is_js($doktor['doktor_name']) ?> </a>
	</nav>

	<div class="clearfix"></div>

	<div class="doktor-nav">
		<div class="container">
			
			<div class="card mb-3 item">
				<div class="row g-0">

					<div class="col-md-12">
						<img src="assets/img/doktor1.png" alt="">
					</div>


				</div>

				<div class="row">
					
					<div class="col-md-12">
						<div class="card-body">
							<h3 class="card-title"><?= is_js($doktor['doktor_name']) ?></h3>
							<button type="button" class="card-title2"><?= 'Doktorunuza Sorun' ?></button>
						</div>
					</div>

				</div>
			</div>


		</div>

	</div>

	<div class="clearfix"></div>

	<div class="blog">

		<div class="container">
			
			<h1 class="blog_title"><?= is_js($doktor['doktor_name']) ?></h1>

			<div class="blog_content">
				
				<?= is_js($doktor['doktor_content']) ?>

			</div>

		</div>

	</div>

	<div class="clearfix"></div>

	<div class="container-fluid" style="width: 100%; padding: 0; margin: 0;" >

		<div class="sliderarea4" >
			<h2><b>DOKTORLARIMIZ</b></h2>
			<div class="owl-carousel owl3 owl-theme owlnavstyle owl-loaded owl-drag" style="background: linear-gradient( 170deg, #f8f9fb 0%, #c2c2c2 100%); ">
				<div class="owl-stage-outer">
					<div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; padding-top: 15px;">

						<?php foreach ($doktorlar as $dr): ?>
							<div class="owl-item active" style="">
								<a href="<?= SITE_URL.'doktor/detay/'.$dr['doktor_seo_name'] ?>">

									<div class="item">
										<div class="icerik_img">
											<img src="<?= SITE_UPLOAD_DIR.'page/'.$dr['doktor_image'] ?>" alt="">
										</div>
										<div class="item-icerik">
											<a href="<?= SITE_URL.'doktor/detay/'.$dr['doktor_seo_name'] ?>" class="button1 btn"><?= is_js($dr['doktor_name']) ?></a>
											<p class="mb-0"><b>L<?= is_js($dr['doktor_desc']) ?></b></p>
										</div>
									</div>
								</a>
							</div>
						<?php endforeach ?>
					</div>
				</div>

				<div class="owl-nav">
					<button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button>
					<button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button>
				</div>
				<div class="owl-dots disabled"></div>
			</div>

		</div>

	</div>

	<div class="clearfix"></div>


	<?php include 'theme/footer.php'; ?>



	<?php include 'theme/js.php'; ?>

</body>
</html>