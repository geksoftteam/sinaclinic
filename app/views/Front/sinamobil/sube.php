<!DOCTYPE html>
<html lang="tr">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Şubelerimiz</title>

	<?php include 'theme/src.php'; ?>


</head>
<body>


	<?php include 'theme/navbar.php'; ?>


	<nav class="nav">
		<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
		<a  href="<?= SITE_URL ?>"><i class="fas fa-chevron-right"></i></a>
		<<a href="<?= SITE_URL.'sube' ?>">Hastahaneler</a>
	</nav>

	<div class="clearfix"></div>

	<div class="container hospital">


		<?php foreach($subeler as $item) :?>

			<div class="card">
				<img src="<?= SITE_UPLOAD_DIR.'page/'.$item['sube_image'] ?>" class="card-img-top" alt="">
				<div class="card-body">
					<h5 class="card-title"><?php echo is_js($item['sube_name'])  ?></h5>
					<p class="card-text"><?php echo is_js($item['sube_adres']) ?>  </p>
				</div>
			</div>

		<?php endforeach ?>

	</div>


	<div class="clearfix"></div>


	<?php include 'theme/footer.php'; ?>

	<?php include 'theme/js.php'; ?>

</body>
</html>