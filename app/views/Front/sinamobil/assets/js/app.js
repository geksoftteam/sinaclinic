$(".navbarcollapsebtn").click(function(){
	$(".navbar-collapsed").toggleClass('navbarshow');;
});



$(document).ready(function(){
	
    $(".owlhomeslider").owlCarousel({
        navigation : true,
        margin:10,
        nav:false,
        dots:false,
        items:1,
        loop: false,
        rewind: true,

    });


    $('.owl1').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots:false,
        items:5,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },

            750:{
                items:2,
            },

            1023:{
                items:3,
            },

        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"]
    });



    $('.owl2').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots:false,
        items:5,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },

            750:{
                items:2,
                nav:true,
            },

            1000:{
                items:3,
                nav:true,
            },


            1600:{
                items:5,
                nav:true,
                loop:false
            }
        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"]
    });



    $('.owl3').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots:false,
        items:5,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },

            750:{
                items:2,
                nav:true,
            },

            1000:{
                items:3,
                nav:true,
            },


            1600:{
                items:5,
                nav:true,
                loop:false
            }
        },

        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"]
    });



});


