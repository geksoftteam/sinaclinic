<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Sina Clinic - İnsan Kaynakları</title>

  <?php include 'theme/src.php'; ?>



</head>
<body>

  <?php include 'theme/navbar.php'; ?>


  <nav class="nav">
    <a  href="index.php"><i class="fas fa-home"></i></a>
    <a  href="index.php"><i class="fas fa-chevron-right"></i></a>
    <a href="doktora-sor.php">Doktorunuza Sorun</a>
  </nav>

  <div class="clearfix"></div>


  <div class="container" style="width: 90%;">

    <form>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputPassword4">İsim / Soyisim  </label>
          <input type="text" class="form-control" id="inputPassword4">
        </div>
        <div class="form-group col-md-6">
          <label for="inputEmail4">Email</label>
          <input type="email" class="form-control" id="inputEmail4" >
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputPassword4">Telefon/GSM</label>
          <input type="text" class="form-control" id="inputPassword4">
        </div>
        <div class="form-group col-md-6">
          <label for="exampleFormControlSelect1">Doktor</label>
          <select class="form-control" id="exampleFormControlSelect1">
            <option>Uzm.Dr.Osman Çetin</option>
            <option>Uzm.Dr.Osman Çetin</option>
            <option>Uzm.Dr.Osman Çetin</option>
            <option>Uzm.Dr.Osman Çetin</option>
            <option>Uzm.Dr.Osman Çetin</option>
          </select>
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputPassword4">Talep Edilen Pozisyon  </label>
          <input type="text" class="form-control" id="inputPassword4">
        </div>
      </div>  

      <div class="form-row">

        <div class="form-group col-md-6">
          <label for="exampleFormControlTextarea1">Neden Bizimle Çalışmak İstiyorsunuz ?</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="8"></textarea>
        </div>
        <div class="form-group col-md-6" style="position: relative; padding-top: 4%; padding-left: 20px;">

        <button type="button" class="btn btn-primary" style="background: #b6dce1; border: none; color: #1e3253; float: right; width: 160px;height: 47px;">Gönder</button>


        </div>    

      </div>  





    </form>   

  </div>

  <div class="clearfix"></div>

  <div class="container-fluid" style="width: 100%; padding: 0;margin: 6% 0 42% 0 ;" >

      <div class="sliderarea4" >
        <h2><b>DOKTORLARIMIZ</b></h2>
        <div class="owl-carousel owl3 owl-theme owlnavstyle owl-loaded owl-drag" style="background: linear-gradient( 170deg, #f8f9fb 0%, #c2c2c2 100%); ">

          <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1920px; padding-top: 15px;"><div class="owl-item active" style="width: 310px; height: 380px; margin-right: 10px; "><a href="#">

            <div class="item">
              <div class="icerik_img">
                <img src="assets/img/doktor1.png" alt="">
              </div>
              <div class="item-icerik">
                <a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
                <p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor</b></p>
              </div>
            </div>
          </a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

            <div class="item">
              <div class="icerik_img">
                <img src="assets/img/doktor1.png" alt="">
              </div>
              <div class="item-icerik">
                <a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
                <p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor</b></p>
              </div>
            </div>
          </a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

            <div class="item">
              <div class="icerik_img">
                <img src="assets/img/doktor1.png" alt="">
              </div>
              <div class="item-icerik">
                <a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
                <p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor</b></p>
              </div>
            </div>
          </a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

            <div class="item">
              <div class="icerik_img">
                <img src="assets/img/doktor1.png" alt="">
              </div>
              <div class="item-icerik">
                <a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
                <p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor</b></p>
              </div>
            </div>
          </a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

            <div class="item">
              <div class="icerik_img">
                <img src="assets/img/doktor1.png" alt="">
              </div>
              <div class="item-icerik">
                <a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
                <p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor</b></p>
              </div>
            </div>
          </a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

            <div class="item">
              <div class="icerik_img">
                <img src="assets/img/doktor1.png" alt="">
              </div>
              <div class="item-icerik">
                <a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
                <p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor</b></p>
              </div>
            </div>
          </a></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button></div><div class="owl-dots disabled"></div></div>

        </div>

      </div>

    </div>

    <div class="clearfix"></div>


<?php include 'theme/footer.php'; ?>























<?php include 'theme/js.php'; ?>

</body>
</html>