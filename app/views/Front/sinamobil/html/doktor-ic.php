<!DOCTYPE html>
<html>
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic - Doktor</title>

	<?php include 'theme/src.php'; ?>


</head>
<body>

	<?php include 'theme/navbar.php'; ?>


	<nav class="nav">
		<a  href="index.php"><i class="fas fa-home"></i></a>
		<a  href="index.php"><i class="fas fa-chevron-right"></i></a>
		<a href="hekimler.php">Hekimler</a>
		<a  href="index.php"><i class="fas fa-chevron-right"></i></a>
		<a href="doktor-ic.php">Dr. Osman Çetin</a>
	</nav>

	<div class="clearfix"></div>

	<div class="doktor-nav">
		<div class="container">
			
			<div class="card mb-3 item">
				<div class="row g-0">

					<div class="col-md-12">
						<img src="assets/img/doktor1.png" alt="">
					</div>


				</div>

				<div class="row">
					
					<div class="col-md-12">
						<div class="card-body">
							<h5 class="card-title">Uzman Doktor</h5>
							<h3 class="card-title">OSMAN ÇETİN</h3>
							<h3 class="card-title2">Lorem ipsum</h3>
						</div>
					</div>

				</div>
			</div>


		</div>

	</div>

	<div class="clearfix"></div>

	<?php include 'blog_template.php'; ?>

	<div class="clearfix"></div>

	<div class="container-fluid" style="width: 100%; padding: 0; margin: 0; margin-bottom: 40%;" >

		<div class="sliderarea4" >
			<h2><b>DOKTORLARIMIZ</b></h2>
			<div class="owl-carousel owl3 owl-theme owlnavstyle owl-loaded owl-drag" style="background: linear-gradient( 170deg, #f8f9fb 0%, #c2c2c2 100%); ">

				<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1920px; padding-top: 15px;"><div class="owl-item active" style="width: 310px; height: 380px; margin-right: 10px; "><a href="#">

					<div class="item">
						<div class="icerik_img">
							<img src="assets/img/doktor1.png" alt="">
						</div>
						<div class="item-icerik">
							<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
							<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor</b></p>
						</div>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

					<div class="item">
						<div class="icerik_img">
							<img src="assets/img/doktor1.png" alt="">
						</div>
						<div class="item-icerik">
							<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
							<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor</b></p>
						</div>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

					<div class="item">
						<div class="icerik_img">
							<img src="assets/img/doktor1.png" alt="">
						</div>
						<div class="item-icerik">
							<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
							<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor</b></p>
						</div>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

					<div class="item">
						<div class="icerik_img">
							<img src="assets/img/doktor1.png" alt="">
						</div>
						<div class="item-icerik">
							<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
							<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor</b></p>
						</div>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

					<div class="item">
						<div class="icerik_img">
							<img src="assets/img/doktor1.png" alt="">
						</div>
						<div class="item-icerik">
							<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
							<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor</b></p>
						</div>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 346px; margin-right: 10px;"><a href="#">

					<div class="item">
						<div class="icerik_img">
							<img src="assets/img/doktor1.png" alt="">
						</div>
						<div class="item-icerik">
							<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
							<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor</b></p>
						</div>
					</div>
				</a></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button></div><div class="owl-dots disabled"></div></div>

			</div>

		</div>

	</div>

	<div class="clearfix"></div>


	<?php include 'theme/footer.php'; ?>



	<?php include 'theme/js.php'; ?>

</body>
</html>