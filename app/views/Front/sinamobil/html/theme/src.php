<!--CSS-->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/radikal_font.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">


<link rel="stylesheet" href="assets/libs/owl/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
<link rel="stylesheet" href="assets/libs/owl/OwlCarousel2-2.3.4/dist/assets/owl.theme.green.min.css">
<link rel="stylesheet" href="assets/libs/fontawesome-free-5.15.3-web/css/all.css">