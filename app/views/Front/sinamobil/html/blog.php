<!DOCTYPE html>
<html>
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	
	<nav class="nav">
		<a  href="index.php"><i class="fas fa-home"></i></a>
		<a  href="index.php"><i class="fas fa-chevron-right"></i></a>
		<a href="blog.php">Blog</a>

	</nav>

	<div class="clearfix"></div>


	<div class="blog-img">
		
		<img src="assets/img/blog-img.jpg">

	</div>

	
	
	<?php include 'theme/blog_template.php'; ?>


	<div class="container">

		<div class="sliderarea2" style="margin-top: 0!important; padding-top: 0;">
			<div class="owl-carousel owl2 owl-theme owlnavstyle owl-loaded owl-drag">

				<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1920px;"><div class="owl-item active" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/rhbr-1.jpg" alt="">
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/rhbr-2.jpg" alt="">

					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/rhbr-3.jpg" alt="">
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/rhbr-4.jpg" alt="">
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/rhbr-1.jpg" alt="">
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/rhbr-2.jpg" alt="">
					</div>
				</a></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button></div><div class="owl-dots disabled"></div></div>

			</div>

		</div>


		<div class="clearfix"></div>


		<div class="container" style="margin-top: 30px;"> 

			<div class="sliderarea1">
				<h2><b>SAĞLIK REHBERİ</b></h2>
				<div class="owl-carousel owl1 owl-theme owlnavstyle owl-loaded owl-drag">

					<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2160px;"><div class="owl-item active" style="width: 350px; margin-right: 10px;"><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button1 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style="width: 350px; margin-right: 10px;"><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button2 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style="width: 350px; margin-right: 10px;"><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button1 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style="width: 350px; margin-right: 10px;"><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button2 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style="width: 350px; margin-right: 10px;"><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button1 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style="width: 350px; margin-right: 10px;"><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button2 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button></div><div class="owl-dots disabled"></div></div>

				</div>

			</div>

			<div class="clearfix"></div>


























































			<?php include 'theme/footer.php'; ?>


			<? php include 'theme/js.php'; ?>


		</body>
		</html>