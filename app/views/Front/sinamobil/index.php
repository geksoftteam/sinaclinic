<!DOCTYPE html>
<html lang="tr">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sina Clinic</title>

	<?php include 'theme/src.php'; ?>

</head>
<body>


	<?php include 'theme/navbar.php'; ?>

	<div class="owl-carousel owlhomeslider owl-theme owl-loaded owl-drag">
		

		<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-808px, 0px, 0px); transition: all 0s ease 0s; width: 2424px;">


			<div class="owl-item " style="width: 350px; margin-right: 5px;"><div class="item">
				<img src="assets/img/slider_1_sina.png" alt="">
				<div class="homesliderbtn">
					<a href="#" class="button1 btn"><span style="float: left;">Detaylı Bilgi</span><i class="fas fa-long-arrow-alt-right"></i></a>			
				</div>


			</div>
		</div>

		<div class="owl-item " style="width: 350px; margin-right: 5px;"><div class="item">
			<img src="assets/img/slider_1_sina.png" alt="">
			<div class="homesliderbtn">
				<a href="#" class="button1 btn"><span style="float: left;">Detaylı Bilgi</span><i class="fas fa-long-arrow-alt-right"></i></a>			
			</div>

		</div></div>

		<div class="owl-item " style="width: 350px; margin-right: 5px;"><div class="item">
			<img src="assets/img/slider_1_sina.png" alt="">
			<div class="homesliderbtn">
				<a href="#" class="button1 btn"><span style="float: left;">Detaylı Bilgi</span><i class="fas fa-long-arrow-alt-right"></i></a>			
			</div>

		</div></div>

		<div class="owl-item " style="width: 350px; margin-right: 5px;"><div class="item">
			<img src="assets/img/slider_1_sina.png" alt="">
			<div class="homesliderbtn">
				<a href="#" class="button1 btn"><span style="float: left;">Detaylı Bilgi</span><i class="fas fa-long-arrow-alt-right"></i></a>			
			</div>

		</div></div>

		<div class="owl-item " style="width: 350px; margin-right: 5px;"><div class="item">
			<img src="assets/img/slider_1_sina.png" alt="">
			<div class="homesliderbtn">
				<a href="#" class="button1 btn"><span style="float: left;">Detaylı Bilgi</span><i class="fas fa-long-arrow-alt-right"></i></a>			
			</div>
		</div>
	</div>
	
</div>

</div>
</div>

<div class="clearfix"></div>


<div class="container buttons">
	
	
	<div class="row">

		<div class="card col-md-4 button1">
			<i class="fas fa-video"></i>
			<div class="card-body">
				<a href="#">Görüntülü Görüşme Randevusu</a>
			</div>
		</div>

		<div class="card col-md-4" >
			<i class="fas fa-briefcase-medical"></i>
			<div class="card-body">
				<a href="#">Muayne Randevusu</a>
			</div>
		</div>

		<div class="card col-md-4 button1" >
			<i class="fas fa-file-medical"></i>
			<div class="card-body">
				<a href="#">E - Sonuç</a>
			</div>
		</div>

		<div class="card col-md-4 " >
			<i class="fas fa-heartbeat"></i>
			<div class="card-body">
				<a href="#">Check - Up</a>
			</div>
		</div>

	</div>

</div>

<div class="clearfix"></div>


<div class="container">
	
	<div class="sliderarea1">
		<h2><b>SAĞLIK REHBERİ</b></h2>
		<div class="owl-carousel owl1 owl-theme owlnavstyle owl-loaded owl-drag">

			<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s;"><div class="owl-item" style=""><a href="#">

				<div class="item">
					<img src="assets/img/rhbr-1.jpg" alt="">
					<div class="item-icerik">
						<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<p>Lorem Ipsum is simply</p>
						<a href="#" class="button1 btn">Lorem ipsum dolor ?</a>
					</div>
				</div>
			</a></div><div class="owl-item" style=""><a href="#">

				<div class="item">
					<img src="assets/img/rhbr-1.jpg" alt="">
					<div class="item-icerik">
						<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<p>Lorem Ipsum is simply</p>
						<a href="#" class="button2 btn">Lorem ipsum dolor ?</a>
					</div>
				</div>
			</a></div><div class="owl-item" style=""><a href="#">

				<div class="item">
					<img src="assets/img/rhbr-1.jpg" alt="">
					<div class="item-icerik">
						<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<p>Lorem Ipsum is simply</p>
						<a href="#" class="button1 btn">Lorem ipsum dolor ?</a>
					</div>
				</div>
			</a></div><div class="owl-item" style=""><a href="#">

				<div class="item">
					<img src="assets/img/rhbr-1.jpg" alt="">
					<div class="item-icerik">
						<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<p>Lorem Ipsum is simply</p>
						<a href="#" class="button2 btn">Lorem ipsum dolor ?</a>
					</div>
				</div>
			</a></div><div class="owl-item" style=""><a href="#">

				<div class="item">
					<img src="assets/img/rhbr-1.jpg" alt="">
					<div class="item-icerik">
						<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<p>Lorem Ipsum is simply</p>
						<a href="#" class="button1 btn">Lorem ipsum dolor ?</a>
					</div>
				</div>
			</a></div><div class="owl-item" style=""><a href="#">

				<div class="item">
					<img src="assets/img/rhbr-1.jpg" alt="">
					<div class="item-icerik">
						<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
						<p>Lorem Ipsum is simply</p>
						<a href="#" class="button2 btn">Lorem ipsum dolor ?</a>
					</div>
				</div>
			</a></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button></div><div class="owl-dots disabled"></div></div>

		</div>

	</div>


	<div class="clearfix"></div>


	<div class="container">

		<div class="sliderarea2">
			<h2><b>ŞUBELERİMİZ</b></h2>
			<div class="owl-carousel owl2 owl-theme owlnavstyle owl-loaded owl-drag">

				<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1920px;"><div class="owl-item active" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/sube-1.jpg" alt="">
						<p>SİNA CLINIC</p>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/sube-1.jpg" alt="">
						<p>SİNA CLINIC</p>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/sube-1.jpg" alt="">
						<p>SİNA CLINIC</p>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/sube-1.jpg" alt="">
						<p>SİNA CLINIC</p>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/sube-1.jpg" alt="">
						<p>SİNA CLINIC</p>
					</div>
				</a></div><div class="owl-item" style="width: 310px; height: 195px; margin-right: 10px;"><a href="#">

					<div class="item">
						<img src="assets/img/sube-1.jpg" alt="">
						<p>SİNA CLINIC</p>
					</div>
				</a></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button></div><div class="owl-dots disabled"></div></div>

			</div>

		</div>

		<div class="clearfix"></div>

		<div class="container-fluid" style="width: 100%; padding: 0; margin: 0;" >

			<div class="sliderarea3" >
				<h2><b>BÖLÜMLERİMİZ</b></h2>
				<div class="owl-carousel owl3 owl-theme owlnavstyle owl-loaded owl-drag" style="background: url('assets/img/bolumler.png');" >

					<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1920px;"><div class="owl-item active" style="width: 310px; height: 300px; margin-right: 10px; "><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/bolum1-1.png" alt="">
							</div>
							<p>Cildiye</p>
						</div>
					</a></div><div class="owl-item" style="width: 310px; height: 300px; margin-right: 10px;"><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/bolum1-2.png" alt="">
							</div>
							<p>Psikiyatri</p>
						</div>
					</a></div><div class="owl-item" style="width: 310px; height: 300px; margin-right: 10px;"><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/bolum1-3.png" alt="">
							</div>
							<p>DİŞ</p>
						</div>
					</a></div><div class="owl-item" style="width: 310px; height: 300px; margin-right: 10px;"><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/bolum1-4.png" alt="">
							</div>
							<p>Beslenme ve Diyetetik</p>
						</div>
					</a></div><div class="owl-item" style="width: 310px; height: 300px; margin-right: 10px;"><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/bolum1-1.png" alt="">
							</div>
							<p>Cildiye</p>
						</div>
					</a></div><div class="owl-item" style="width: 310px; height: 300px; margin-right: 10px;"><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/bolum1-2.png" alt="">
							</div>
							<p>Psikiyatri</p>
						</div>
					</a></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button></div><div class="owl-dots disabled"></div></div>

				</div>

			</div>

		</div>

		<div class="clearfix"></div>

		<div class="container-fluid" style="width: 100%; padding: 0; margin: 0;" >

			<div class="sliderarea4" >
				<h2><b>DOKTORLARIMIZ</b></h2>
				<div class="owl-carousel owl3 owl-theme owlnavstyle owl-loaded owl-drag" style="background: linear-gradient( 170deg, #f8f9fb 0%, #c2c2c2 100%); ">

					<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; padding-top: 15px;"><div class="owl-item active" style=""><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/doktor1.png" alt="">
							</div>
							<div class="item-icerik">
								<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
								<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor</b></p>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/doktor1.png" alt="">
							</div>
							<div class="item-icerik">
								<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
								<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor</b></p>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/doktor1.png" alt="">
							</div>
							<div class="item-icerik">
								<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
								<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor</b></p>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/doktor1.png" alt="">
							</div>
							<div class="item-icerik">
								<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
								<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor</b></p>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/doktor1.png" alt="">
							</div>
							<div class="item-icerik">
								<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
								<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor</b></p>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<div class="icerik_img">
								<img src="assets/img/doktor1.png" alt="">
							</div>
							<div class="item-icerik">
								<a href="#" class="button1 btn">Uzm. Dr. Osman Çetin</a>
								<p class="mb-0"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor</b></p>
							</div>
						</div>
					</a></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button></div><div class="owl-dots disabled"></div></div>

				</div>

			</div>

		</div>

		<div class="clearfix"></div>




		<div class="container">

			<div class="sliderarea1">
				<h2><b>TÜM HABERLER</b></h2>
				<div class="owl-carousel owl1 owl-theme owlnavstyle owl-loaded owl-drag">

					<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2160px;"><div class="owl-item active" style=""><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button1 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button2 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button1 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button2 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button1 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div><div class="owl-item" style=""><a href="#">

						<div class="item">
							<img src="assets/img/rhbr-1.jpg" alt="">
							<div class="item-icerik">
								<p class="mb-0"><b>Lorem Ipsum is simply	dummy text of the printing</b></p>
								<p>Lorem Ipsum is simply</p>
								<a href="#" class="button2 btn">Lorem ipsum dolor ?</a>
							</div>
						</div>
					</a></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button></div><div class="owl-dots disabled"></div></div>

				</div>

			</div>


			<div class="clearfix"></div>


			<?php include 'theme/footer.php'; ?>































































			<?php include 'theme/js.php'; ?>

		</body>
		</html>