<!DOCTYPE html>
<html lang="tr">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= 'Bölümlerimiz' ?></title>

	<?php include 'theme/src.php'; ?>

</head>
<body>

	<?php include 'theme/navbar.php'; ?>

	
	<nav class="nav">
		<a  href="<?= SITE_URL ?>"><i class="fas fa-home"></i></a>
		<a  href="#"><i class="fas fa-chevron-right"></i></a>
		<a href="<?= SITE_URL.'bolum/detay/'.$blm['bolum_seo_name'] ?>">Bölümlerimiz</a>

	</nav>

	<div class="clearfix"></div>
	
	


	<div class="container">

		<div class="sliderarea1" style="margin-top: 0!important; padding-top: 0;">
			<div class="owl-carousel owl1 owl-theme owlnavstyle owl-loaded owl-drag">

				<div class="owl-stage-outer">
					<div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2160px;">

						<?php foreach ($bolumler as $blm): ?>

							<div class="owl-item active" style="width: 350px; margin-right: 10px;">
								<a href="#">

									<div class="item">
										<div class="item-icerik">
											<p class="mb-0"><b><?= is_js($blm['bolum_name']) ?></b></p>
											<a href="<?= SITE_URL.'bolum/detay/'.$blm['bolum_seo_name'] ?>" class="button1 btn">Detay</a>
										</div>
									</div>
								</a>
							</div>

						<?php endforeach ?>

					</div>
				</div>
				<div class="owl-nav">
					<button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button>
					<button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button>
				</div>
				<div class="owl-dots disabled"></div>
			</div>

		</div>

	</div>


	<div class="clearfix"></div>



	<?php if (count($blog)>0): ?>

		<div class="container" style="margin-top: 30px;"> 

			<div class="sliderarea1">
				<h2><b>SAĞLIK REHBERİ</b></h2>
				<div class="owl-carousel owl1 owl-theme owlnavstyle owl-loaded owl-drag">

					<div class="owl-stage-outer">
						<div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2160px;">

							<?php foreach ($blog as $blg): ?>

							<div class="owl-item active" style="width: 350px; margin-right: 10px;">
								<a href="<?php echo SITE_URL.$blg['nav_url'].'/'.$blg['page_url'] ?>">

									<div class="item">
										<img src="<?= SITE_UPLOAD_DIR.'page/'.$blg['page_image'] ?>" alt="">
										<div class="item-icerik">
											<p class="mb-0"><b><?= is_js($blg['page_name']) ?></b></p>
											<p><?= is_js($blg['page_jenerik']) ?></p>
											<a href="<?php echo SITE_URL.$blg['nav_url'].'/'.$blg['page_url'] ?>" class="button1 btn">Detay</a>
										</div>
									</div>
								</a>
							</div>

							<?php endforeach ?>

						</div>
					</div>
					<div class="owl-nav">
						<button type="button" role="presentation" class="owl-prev disabled"><i class="fas fa-chevron-left" aria-hidden="true"></i></button>
						<button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right" aria-hidden="true"></i></button>
					</div>
					<div class="owl-dots disabled"></div>
				</div>

			</div>

		</div>


	<?php endif ?>

	<div class="clearfix"></div>


























































	<?php include 'theme/footer.php'; ?>


	<?php include 'theme/js.php'; ?>


</body>
</html>