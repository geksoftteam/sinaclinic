<?php

class Form {
	public $currentValue;
	public $values = array();
	public $errors = array();

	public function __construct() {}

	public function post($key) {
		if (is_array($_POST[$key])) {

			$this->values[$key] = $this->jsoncu($_POST[$key]);
			$this->currentValue = $key;
			return $this;

		} else {

			$this->values[$key] = trim($_POST[$key]);
			$this->currentValue = $key;
			return $this;
		}
	}

	public function datapost($key) {
		if (is_array($_POST[$key]) && !empty($_POST[$key]) && $_POST[$key] > 0) {

			$this->values[$key] = $this->jsoncu($_POST[$key]);
			$this->currentValue = $key;
			return $this;

		} else {

			$this->values[$key] = trim($_POST[$key]);
			$this->currentValue = $key;
			return $this;
		}
	}

	public function p($key) {

		return trim($_POST[$key]);
	}

	public function postresim($key) {

		if (isset($_FILES[$key]['tmp_name']) && !empty($_FILES[$key]['tmp_name'])) {
			return $_FILES[$key];
		} else {
			return false;
		}
	}

	public function isEmpty($label = "İsim") {

		if (empty($this->values[$this->currentValue])) {
			$this->errors[] = "Lütfen <b>" . $label . "</b> alanını boş bırakmayınız.";
		}
		return $this;

	}
	public function length($min = 0, $max, $label) {
		if (strlen($this->values[$this->currentValue]) < $min OR strlen($this->values[$this->currentValue]) > $max) {
			$this->errors[] = "Lütfen " . $label . " alanına " . $min . " ve " . $max . " karakter arasında bir yazı giriniz.";
		}
		return $this;
	}
	public function isMail($label = "Mail") {
		if (!filter_var($this->values[$this->currentValue], FILTER_VALIDATE_EMAIL)) {
			$this->errors[] = "Lütfen " . $label . " alanına geçerli bir mail adresi giriniz.";
		}
	}

	public function isInt($label = "Sayı") {
		if (!is_numeric($this->values[$this->currentValue])) {
			$this->errors[] = "Lütfen <b>" . $label . "</b> alanına sayısal bir değer giriniz.";
		}
	}

	public function submit() {
		if (empty($this->errors)) {
			return true;
		} else {
			return false;
		}
	}

	public function jsoncu($input) {

		return preg_replace_callback(
			'/\\\\u([0-9a-zA-Z]{4})/',
			function ($matches) {
				return mb_convert_encoding(pack('H*', $matches[1]), 'UTF-8', 'UTF-16');
			},
			json_encode($input)
		);

	}

}
