<?php


namespace App\Libs;


class UriObject
{

    #region fields

    /** @var array<string,int>|int[]  */
    protected $default_ports = [
        'http' => 80,
        'https' => 443,
        'smtp' => 25,
        'ftp' => 21,
        'telnet' => 23,
        'imap' => 143,
        'rdp' => 3389,
        'ssh' => 22,
        'dns' => 53,
        'pop3' => 110,
    ];

    /** @var string|null */
    protected $rawUrl = null;

    /** @var string */
    protected $protocol = 'http';

    /** @var int */
    protected $port = 80;

    /** @var string */
    protected $host = '';

    /** @var array|string[] */
    protected $segments = array();

    /** @var array<string,string> */
    protected $queryStrings = array();

    /** @var string|null */
    protected $hash = null;

    #endregion fields

    #region ctor

    /**
     * UriObject constructor.
     * @param string|null $url
     */
    public function __construct($url = null)
    {
        $this->rawUrl = $url;
        $this->initialize();
    }


    #endregion ctor

    #region props

    /**
     * @return string|null
     */
    public function getRawUrl()
    {
        return $this->rawUrl;
    }

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @param string $protocol
     * @return UriObject
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
        return $this;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param int $port
     * @return UriObject
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return UriObject
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getSegments()
    {
        return $this->segments;
    }

    /**
     * @param array|string[] $segments
     * @return UriObject
     */
    public function setSegments($segments)
    {
        $this->segments = $segments;
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryStrings()
    {
        return $this->queryStrings;
    }

    /**
     * @param array $queryStrings
     * @return UriObject
     */
    public function setQueryStrings($queryStrings)
    {
        $this->queryStrings = $queryStrings;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string|null $hash
     * @return UriObject
     */
    public function setHash($hash)
    {
        if (!empty($hash)) {
            $hash = str_replace('#', '', $hash);
        }
        $this->hash = $hash;
        return $this;
    }


    #endregion props

    #region methods

    /**
     * @param int $index
     * @param string $segment
     * @return $this
     */
    public function setSegment($index, $segment)
    {
        $this->segments[$index] = $segment;
        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function setQuery($key, $value)
    {
        $this->queryStrings[$key] = $value;
        return $this;
    }

    /**
     * @param string $segment
     * @return $this,
     */
    public function addSegment($segment)
    {
        $this->segments[] = $segment;
        return $this;
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getQuery($key)
    {
        if (isset($this->queryStrings[$key])) {
            return $this->queryStrings[$key];
        }
        return null;
    }

    /**
     * @param int $index
     * @return string|null
     */
    public function getSegment($index)
    {
        if (isset($this->segments[$index])) {
            return $this->segments[$index];
        }
        return null;
    }

    public function __toString()
    {
        $str = '';
        if (!empty($this->protocol)) {
            $str .= strtolower($this->protocol) . ':';
        }
        $str .= '//' . $this->host;

        $incPort = false;
        if(empty($this->protocol)){
            if($this->default_ports['http'] != $this->port && $this->default_ports['https'] != $this->port){
                $incPort = true;
            }
        }else{
            if(isset($this->default_ports[strtolower($this->protocol)])){
                if($this->default_ports[strtolower($this->protocol)] != $this->port){
                    $incPort = true;
                }
            }else{
                $incPort = true;
            }
        }
        if($incPort){
            $str .= ':' . $this->port;
        }

        if(!empty($this->segments)){
            $str .= '/' . implode('/',$this->segments);
        }

        if(!empty($this->queryStrings)){
            $qsArr = array();
            foreach ($this->queryStrings as $k => $v){
                $qsArr[] = $k . '=' . $v;
            }
            $str .= '?' . implode('&',$qsArr);
        }
        if(!empty($this->hash)){
            $str .= '#' . $this->hash;
        }

        return $str;

    }

    #endregion methods

    #region utils

    protected function initialize()
    {
        if (empty($this->rawUrl)) {
            return;
        }
        $rs = parse_url($this->rawUrl);
        $this->host = $rs['host'];
        if (isset($rs['scheme'])) {
            $this->protocol = strtolower($rs['scheme']);
        } else {
            $this->protocol = '';
        }
        if (isset($rs['port'])) {
            $this->port = intval($rs['port']);
        } else {
            if (!empty($this->protocol)) {
                if (isset($this->default_ports[$this->protocol])) {
                    $this->port = $this->default_ports[$this->protocol];
                }
            }
        }

        if (isset($rs['path'])) {
            $this->segments = array();
            $paths = explode('/', $rs['path']);
            foreach ($paths as $pt){
                if(!empty($pt)){
                    $this->segments[] = $pt;
                }
            }
        }

        if (isset($rs['query'])) {
            $this->queryStrings = array();
            $query = explode('&', $rs['query']);

            foreach ($query as $qItm) {
                $keyVal = explode('=', $qItm);

                $this->queryStrings[$keyVal[0]] = isset($keyVal[1]) ? $keyVal[1] : '';
            }
        }


        if (isset($rs['fragment'])) {
            $this->hash = $rs['fragment'];
        }

    }

    #endregion utils

    #region static

    /**
     * @param string $url
     * @return UriObject
     */
    public static function parse($url)
    {
        return new UriObject($url);
    }

    #endregion static

}