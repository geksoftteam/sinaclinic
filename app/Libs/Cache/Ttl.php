<?php


namespace App\Libs\Cache;


class Ttl
{
    /**
     * @param int $minutes
     * @return int
     */
    public static function fromMinutes($minutes){
        return $minutes * 60;
    }

    /**
     * @param int $hours
     * @return int
     */
    public static function fromHours($hours){
        return static::fromMinutes($hours * 60);
    }

    /**
     * @param int $days
     * @return int
     */
    public static function fromDays($days){
        return static::fromHours($days * 24);
    }

    /**
     * @param int $weeks
     * @return int
     */
    public static function fromWeeks($weeks){
        return static::fromDays($weeks * 7);
    }

    /**
     * @param int $months
     * @return int
     */
    public static function fromMonths($months){
        return static::fromDays($months * 30);
    }
}