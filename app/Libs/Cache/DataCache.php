<?php


namespace App\Libs\Cache;


class DataCache implements IDataCache
{
    #region fields

    /** @var bool */
    protected $cacheEnabled = true;

    /** @var bool */
    protected $enable2ndCache = true;

    /** @var string */
    protected $cacheFilesPath = "";

    protected $mode = 0640;

    /**
     * 2. ön bellek aynı request içinde gelen çağrıları ramden verir.
     * @var array
     */
    protected $_2ndCache = array();

    /** @var string */
    protected $prefix = '';


    #endregion fields

    #region ctor

    public function __construct()
    {
        $this->cacheEnabled = CACHE_ON;
        $this->enable2ndCache = ENABLE_2ND_CACHE_ON;
        $this->prefix = CACHE_PREFIX;
        $this->cacheFilesPath = CACHE_PATH;
        $this->mode = CACHE_FILE_MODE;

        if (!empty($this->cacheFilesPath)) {
            $this->cacheFilesPath = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $this->cacheFilesPath);
        }

    }

    #endregion ctor

    #region IDataCache

    /**
     * @param string $key
     * @return mixed|null
     */
    public function get($key)
    {
        if (false == $this->cacheEnabled) {
            return null;
        }

        if ($this->enable2ndCache && array_key_exists($key, $this->_2ndCache)) {
            return $this->_2ndCache[$key];
        }

        $key = $this->prefix . $key;
        $data = $this->getItem($key);

        return is_array($data) ? $data['data'] : null;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return bool
     */
    public function save($key, $value, $ttl = 60)
    {
        if (false == $this->cacheEnabled) {
            return true;
        }

        $key = $this->prefix . $key;

        $contents = [
            'time' => time(),
            'ttl' => $ttl,
            'data' => $value,
        ];

        if ($this->writeFile($this->cacheFilesPath . $key, serialize($contents))) {
            chmod($this->cacheFilesPath . $key, $this->mode);

            if ($this->enable2ndCache) {
                $this->_2ndCache[$key] = $value;
            }

            return true;
        }

        return false;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete($key)
    {
        if (false == $this->cacheEnabled) {
            return true;
        }

        $key = $this->prefix . $key;

        $res = is_file($this->cacheFilesPath . $key) && unlink($this->cacheFilesPath . $key);
        if ($this->enable2ndCache && array_key_exists($key, $this->_2ndCache)) {
            unset($this->_2ndCache[$key]);
        }
        return $res;

    }

    /**
     * @param string $key
     * @param int $offset
     * @return bool|int|mixed
     */
    public function increment($key, $offset = 1)
    {
        if (false == $this->cacheEnabled) {
            return true;
        }
        $key = $this->prefix . $key;

        $data = $this->getItem($key);

        if ($data === false) {
            $data = [
                'data' => 0,
                'ttl' => 60,
            ];
        } elseif (!is_int($data['data'])) {
            return false;
        }

        $newValue = $data['data'] + $offset;

        $res = $this->save($key, $newValue, $data['ttl']) ? $newValue : false;
        if ($res && $this->enable2ndCache && array_key_exists($key, $this->_2ndCache)) {
            $this->_2ndCache[$key]++;
        }
        return $res;
    }

    /**
     * @param string $key
     * @param int $offset
     * @return bool|int|mixed
     */
    public function decrement($key, $offset = 1)
    {
        if (false == $this->cacheEnabled) {
            return true;
        }
        $key = $this->prefix . $key;

        $data = $this->getItem($key);

        if ($data === false) {
            $data = [
                'data' => 0,
                'ttl' => 60,
            ];
        } elseif (!is_int($data['data'])) {
            return false;
        }

        $newValue = $data['data'] - $offset;
        $res = $this->save($key, $newValue, $data['ttl']) ? $newValue : false;
        if ($res && $this->enable2ndCache && array_key_exists($key, $this->_2ndCache)) {
            $this->_2ndCache[$key]--;
        }
        return $res;

    }

    /**
     * @return bool
     */
    public function clean()
    {
        if (false == $this->cacheEnabled) {
            return true;
        }
        $res = $this->deleteFiles($this->cacheFilesPath, false, true);
        if ($res && $this->enable2ndCache) {
            $this->_2ndCache = array();
        }
        return $res;
    }

    /**
     * @return array|bool|false|mixed
     */
    public function getCacheInfo()
    {
        if (false == $this->cacheEnabled) {
            return null;
        }
        return $this->getDirFileInfo($this->cacheFilesPath);
    }

    /**
     * @param string $key
     * @return array|bool|mixed
     */
    public function getMetaData($key)
    {
        if (false == $this->cacheEnabled) {
            return null;
        }
        $key = $this->prefix . $key;

        if (!is_file($this->cacheFilesPath . $key)) {
            return false;
        }

        $data = @unserialize(file_get_contents($this->cacheFilesPath . $key));

        if (is_array($data)) {
            $mtime = filemtime($this->cacheFilesPath . $key);

            if (!isset($data['ttl'])) {
                return false;
            }

            return [
                'expire' => $mtime + $data['ttl'],
                'mtime' => $mtime,
                'data' => $data['data'],
            ];
        }

        return false;
    }

    /**
     * @param string $key
     * @param callable $func
     * @param array $args
     * @param int $ttl
     * @return mixed
     */
    public function getOrSave($key, $func, $args = [], $ttl = 60)
    {
        $result = $this->cacheEnabled ?
            $this->get($key) :
            null;
        if ($result === null) {
            $result = call_user_func_array($func, $args);
            if ($this->cacheEnabled) {
                $this->save($key, $result, $ttl);
            }
        }
        return $result;
    }

    /**
     * @param string $keyRegex
     * @return bool
     */
    public function deleteByRegex($keyRegex)
    {
        if(false == $this->cacheEnabled){
            return false;
        }
        $cacheInfos = $this->getCacheInfo();
        $foundedKeys = array();
        foreach ($cacheInfos as $key => $val) {
            if (preg_match($keyRegex, $key)) {
                $foundedKeys[] = $key;
            }
        }

        foreach ($foundedKeys as $cKey) {
            $this->delete($cKey);
        }
        return !empty($foundedKeys);
    }

    public function deleteByStartWith($keyStartWith)
    {
        if(false == $this->cacheEnabled){
            return false;
        }
        $foundedKeys = array();

        $cacheInfos = $this->getCacheInfo();
        $length = strlen($keyStartWith);


        foreach ($cacheInfos as $key => $val) {

            if (substr($key,0,$length) == $keyStartWith) {
                $foundedKeys[] = $key;
            }
        }

        foreach ($foundedKeys as $cKey) {
            $this->delete($cKey);
        }
        return !empty($foundedKeys);
    }

    #endregion IDataCache

    #region Utils

    /**
     * @param string $key
     * @return bool|mixed
     */
    protected function getItem($key)
    {
        if (!is_file($this->cacheFilesPath . $key)) {
            return false;
        }

        $data = unserialize(file_get_contents($this->cacheFilesPath . $key));


        if ($data['ttl'] > 0 && time() > $data['time'] + $data['ttl']) {
            if (is_file($this->cacheFilesPath . $key)) {
                unlink($this->cacheFilesPath . $key);
            }

            return false;
        }

        return $data;
    }

    /**
     * @param string $path
     * @param string $data
     * @param string $mode
     * @return bool
     */
    protected function writeFile($path, $data, $mode = 'wb')
    {
        if (($fp = @fopen($path, $mode)) === false) {
            return false;
        }

        flock($fp, LOCK_EX);

        for ($result = $written = 0, $length = strlen($data); $written < $length; $written += $result) {
            if (($result = fwrite($fp, substr($data, $written))) === false) {
                break;
            }
        }

        flock($fp, LOCK_UN);
        fclose($fp);

        return is_int($result);
    }

    /**
     * @param string $path
     * @param bool $delDir
     * @param bool $htdocs
     * @param int $_level
     * @return bool
     */
    protected function deleteFiles($path, $delDir = false, $htdocs = false, $_level = 0)
    {
        // Trim the trailing slash
        $path = rtrim($path, '/\\');

        if (!$currentDir = @opendir($path)) {
            return false;
        }

        while (false !== ($filename = @readdir($currentDir))) {
            if ($filename !== '.' && $filename !== '..') {
                if (is_dir($path . DIRECTORY_SEPARATOR . $filename) && $filename[0] !== '.') {
                    $this->deleteFiles($path . DIRECTORY_SEPARATOR . $filename, $delDir, $htdocs, $_level + 1);
                } elseif ($htdocs !== true || !preg_match('/^(\.htaccess|index\.(html|htm|php)|web\.config)$/i', $filename)) {
                    @unlink($path . DIRECTORY_SEPARATOR . $filename);
                }
            }
        }

        closedir($currentDir);

        return ($delDir === true && $_level > 0) ? @rmdir($path) : true;
    }

    /**
     * @param string $sourceDir
     * @param bool $topLevelOnly
     * @param bool $_recursion
     * @return array|false
     */
    protected function getDirFileInfo($sourceDir, $topLevelOnly = true, $_recursion = false)
    {
        static $_filedata = [];
        $relativePath = $sourceDir;

        if ($fp = @opendir($sourceDir)) {
            // reset the array and make sure $source_dir has a trailing slash on the initial call
            if ($_recursion === false) {
                $_filedata = [];
                $sourceDir = rtrim(realpath($sourceDir) ?: $sourceDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
            }

            // Used to be foreach (scandir($source_dir, 1) as $file), but scandir() is simply not as fast
            while (false !== ($file = readdir($fp))) {
                if (is_dir($sourceDir . $file) && $file[0] !== '.' && $topLevelOnly === false) {
                    $this->getDirFileInfo($sourceDir . $file . DIRECTORY_SEPARATOR, $topLevelOnly, true);
                } elseif ($file[0] !== '.') {
                    $_filedata[$file] = $this->getFileInfo($sourceDir . $file);
                    $_filedata[$file]['relative_path'] = $relativePath;
                }
            }

            closedir($fp);

            return $_filedata;
        }

        return false;
    }

    /**
     * @param string $file
     * @param string[] $returnedValues
     * @return array|false
     */
    protected function getFileInfo($file, $returnedValues = ['name', 'server_path', 'size', 'date'])
    {
        if (!is_file($file)) {
            return false;
        }

        if (is_string($returnedValues)) {
            $returnedValues = explode(',', $returnedValues);
        }

        foreach ($returnedValues as $key) {
            switch ($key) {
                case 'name':
                    $fileInfo['name'] = basename($file);
                    break;
                case 'server_path':
                    $fileInfo['server_path'] = $file;
                    break;
                case 'size':
                    $fileInfo['size'] = filesize($file);
                    break;
                case 'date':
                    $fileInfo['date'] = filemtime($file);
                    break;
                case 'readable':
                    $fileInfo['readable'] = is_readable($file);
                    break;
                case 'writable':
                    $fileInfo['writable'] = is_writable($file);
                    break;
                case 'executable':
                    $fileInfo['executable'] = is_executable($file);
                    break;
                case 'fileperms':
                    $fileInfo['fileperms'] = fileperms($file);
                    break;
            }
        }

        return $fileInfo; // @phpstan-ignore-line
    }


    #endregion Utils

    #region statics

    /**
     * @return DataCache
     */
    public static function instance(){
        static $instance = null;
        if($instance === null){
            $instance = new self();
        }
        return $instance;
    }

    #endregion statics

}