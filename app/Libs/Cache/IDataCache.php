<?php


namespace App\Libs\Cache;


interface IDataCache
{

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key);

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return mixed
     */
    public function save($key, $value,  $ttl = 60);

    /**
     * @param string $key
     * @return mixed
     */
    public function delete($key);

    /**
     * @param string $key
     * @param int $offset
     * @return mixed
     */
    public function increment($key, $offset = 1);

    /**
     * @param string $key
     * @param int $offset
     * @return mixed
     */
    public function decrement($key, $offset = 1);

    /**
     * @return mixed
     */
    public function clean();

    /**
     * @return mixed
     */
    public function getCacheInfo();

    /**
     * @param string $key
     * @return mixed
     */
    public function getMetaData($key);

    /**
     * @param string $key
     * @param callable $func
     * @param array $args
     * @param int $ttl
     * @return mixed
     */
    public function getOrSave($key, $func, $args = [], $ttl = 60);

    /**
     * @param string $keyRegex
     * @return bool
     */
    public function deleteByRegex($keyRegex);

    /**
     * @param string $keyStartWith
     * @return bool
     */
    public function deleteByStartWith($keyStartWith);


}