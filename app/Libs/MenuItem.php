<?php


namespace App\Libs;


use App\Libs\Xml\ItoXml;
use App\Libs\Xml\XmlNode;

class MenuItem implements ItoXml, \JsonSerializable
{

    #region fields

    /**
     * @var string|null
     */
    protected $text = null;

    /**
     * @var string|null
     */
    protected $url = null;

    /**
     * @var string|null
     */
    protected $menuType = null;

    /**
     * @var string|null
     */
    protected $iconClass = null;

    /**
     * @var bool
     */
    protected $loginRequired = false;

    /** @var string|null */
    protected $tableName = null;

    /** @var string|null */
    protected $tableId = null;

    /**
     * @var array
     */
    protected $attributes = array();


    /**
     * @var array|MenuItem[]
     */
    protected $childs = array();

    /**
     * @var bool|null
     */
    protected $active = null;

    /** @var bool */
    protected $renderAsComment = false;

    #endregion fields

    #region ctor
    /**
     * MenuItem constructor.
     * @param bool $renderAsComment
     */
    public function __construct($renderAsComment = false)
    {
        $this->renderAsComment = $renderAsComment;
    }

    #endregion ctor

    #region properties

    /**
     * @return string|null
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return MenuItem
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     * @return MenuItem
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMenuType()
    {
        return $this->menuType;
    }

    /**
     * @param string|null $menuType
     * @return MenuItem
     */
    public function setMenuType($menuType)
    {
        $this->menuType = $menuType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIconClass()
    {
        return $this->iconClass;
    }

    /**
     * @param string|null $iconClass
     * @return MenuItem
     */
    public function setIconClass($iconClass)
    {
        $this->iconClass = $iconClass;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLoginRequired()
    {
        return $this->loginRequired;
    }

    /**
     * @param bool $loginRequired
     * @return MenuItem
     */
    public function setLoginRequired($loginRequired = true)
    {
        $this->loginRequired = $loginRequired;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     * @return MenuItem
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * @return array|MenuItem[]
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * @param array|MenuItem[] $childs
     * @return MenuItem
     */
    public function setChilds($childs)
    {
        $this->childs = $childs;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string|null $tableName
     * @return MenuItem
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableId()
    {
        return $this->tableId;
    }

    /**
     * @param string|null $tableId
     * @return MenuItem
     */
    public function setTableId($tableId)
    {
        $this->tableId = $tableId;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     * @return MenuItem
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRenderAsComment()
    {
        return $this->renderAsComment;
    }

    /**
     * @param bool $renderAsComment
     * @return MenuItem
     */
    public function setRenderAsComment($renderAsComment)
    {
        $this->renderAsComment = $renderAsComment;
        return $this;
    }

    #endregion properties

    #region Methods


    /**
     * @param MenuItem $menuItem
     * @return MenuItem
     */
    public function addChild($menuItem)
    {
        $this->childs[] = $menuItem;
        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return MenuItem
     */
    public function addAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasChilds()
    {
        return !empty($this->childs);
    }

    /**
     * @return bool
     */
    public function hasAttributes()
    {

        return !empty($this->attributes);
    }

    /**
     * @param string $systemName
     * @return bool
     */
    public function isActive($systemName)
    {
        if ($this->active !== null) {
            return $this->active;
        }
        if (empty($systemName)) {
            $this->active = false;
            return false;
        }
        $this->active = $this->getMenuType() == $systemName;
        if ($this->active == false && $this->hasChilds()) {
            foreach ($this->getChilds() as $child) {
                $res = $child->isActive($systemName);
                if ($res) {
                    $this->active = $res;
                    break;
                }
            }
        }
        return $this->active;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = array();
        $data['text'] = $this->getText();
        $data['url'] = $this->getUrl();
        $data['menutype'] = $this->getMenuType();
        $data['iconclass'] = $this->getIconClass();


        if ($this->isLoginRequired()) {
            $data['loginrequired'] = '1';
        } else {
            $data['loginrequired'] = '0';
        }


        $data['tablename'] = $this->getTableName();


        $data['tableid'] = $this->getTableId();


        if ($this->hasAttributes()) {
            foreach ($this->attributes as $key => $val) {
                $data[$key] = $val;
            }
        }
        return $data;
    }

    public function getLinkUrl(){
        if(empty($this->url)){
            return '';
        }
        $parsed = parse_url($this->url);

        if(isset($parsed['host']) && !empty($parsed['host'])){
            return $this->url;
        }
        $res = $this->url;
        $res = trim($res,"/");
        //var_dump($res);
        //exit(0);
        $res = trim(SITE_URL,"/") . '/' .$res;
        return $res;
    }

    #endregion Methods

    #region ItoXml

    /**
     * @return XmlNode
     */
    public function toXmlNode()
    {
        $node = new XmlNode('item');
        $node->addAttribute('text', $this->getText());
        if ($this->getUrl()) {
            $node->addAttribute('url', $this->getUrl());
        }

        if ($this->getMenuType()) {
            $node->addAttribute('menuType', $this->getMenuType());
        }

        if ($this->getIconClass()) {
            $node->addAttribute('iconClass', $this->getIconClass());
        }
        if ($this->isLoginRequired()) {
            $node->addAttribute('loginRequired', '1');
        }
        if (!empty($this->getTableName())) {
            $node->addAttribute('tableName', $this->getTableName());
        }
        if (!empty($this->getTableId())) {
            $node->addAttribute('tableId', $this->getTableId());
        }


        if ($this->hasAttributes()) {
            foreach ($this->attributes as $key => $val) {
                $node->addAttribute($key, $val);
            }
        }
        if ($this->hasChilds()) {
            foreach ($this->childs as $mi) {
                $node->addChild($mi->toXmlNode());
            }
        }

        $node->setRenderAsComment($this->renderAsComment);


        return $node;

    }

    /**
     * @return string
     */
    public function toXml()
    {
        return $this->toXmlNode()->toXmlString(0);
    }

    #endregion ItoXml

    #region JsonSerializable

    public function jsonSerialize()
    {
        return (object)array(
            'text' => $this->text,
            'url' => $this->url,
            'menuType' => $this->menuType,
            'iconClass' => $this->iconClass,
            'loginRequired' => $this->loginRequired,
            'tableName' => $this->tableName,
            'tableId' => $this->tableId,
            'attributes' => $this->attributes,
            'childs' => $this->childs,
            'active' => $this->active,
        );
    }

    #endregion JsonSerializable
}