<?php


namespace App\Libs\Xml;

class XmlNode
{
    /**
     * @var bool
     */
    protected $renderAsComment = false;

    #region ctor

    /**
     * XmlNode constructor.
     * @param string|null $tagName
     * @param string|null $text
     * @param bool|null $ignoreNs
     */
    public function __construct($tagName = null, $text = null, $ignoreNs = null)
    {
        $this->childNodes = array();
        $this->attributes = array();
        if (!is_null($tagName)) {
            if (strpos($tagName, ':') !== false) {
                $tgNms = explode(':', $tagName);
                $this->prefixNs = $tgNms[0];
                $this->tagName = $tgNms[1];
            } else {
                $this->tagName = $tagName;
            }

        }
        if (!is_null($text)) {
            $this->text = $text;
        }
        if (!is_null($ignoreNs)) {
            $this->ignoreNs = $ignoreNs;
        }
    }

    #endregion ctor

    #region properties

    /**
     * @var string
     */
    public $tagName;

    /**
     * @var bool
     */
    public $isSelfClose = false;

    /**
     * @var bool
     */
    public $isRoot = false;

    /**
     * @var string|null
     */
    public $prefixNs = null;

    /**
     * @var array<string,string>
     */
    public $attributes;


    /**
     * @var XmlNode|null
     */
    public $parentNode = null;

    /**
     * @var array|XmlNode[]
     */
    public $childNodes;

    /**
     * @var string|null
     */
    public $text = null;

    /**
     * @var string
     */
    public $comments;

    /**
     * @param bool $renderAsComment
     */
    public function setRenderAsComment($renderAsComment = true)
    {
        $this->renderAsComment = $renderAsComment;
    }

    #endregion properties

    #region methods

    public function __toString()
    {
        return $this->toXmlString(0);
    }

    /**
     * @param int $indentLevel
     * @return string
     */
    public function toXmlString($indentLevel = 0)
    {

        $indentStr = '    ';
        $xmlRes = "";
        if ($this->renderAsComment) {
            $xmlRes .= str_repeat($indentStr, $indentLevel) . '<!-- ' . PHP_EOL;
        }

        if (empty($this->text) && empty($this->childNodes)) {
            $this->isSelfClose = true;
        }
        $tagName = $this->getTagName();
        $xmlRes .= $this->renderComments(str_repeat($indentStr, $indentLevel));
        $xmlRes .= str_repeat($indentStr, $indentLevel) . "<" . $tagName;
        if (!empty($this->attributes)) {
            foreach ($this->attributes as $key => $value) {
                $xmlRes .= ' ' . $key;
                $xmlRes .= '=' . '"' . str_replace('"', '\"', $value) . '"';
            }
        }
        if ($this->isSelfClose) {
            $xmlRes .= '/>' . PHP_EOL;
        } else {
            $xmlRes .= '>';
            if ($this->text !== null) {
                $xmlRes .= $this->text;
            } elseif (!empty($this->childNodes)) {
                $xmlRes .= PHP_EOL;
                foreach ($this->childNodes as $node) {
                    if ($node !== null) {
                        $xmlRes .= $node->toXmlString($indentLevel + 1);
                    }
                }
                $xmlRes .= str_repeat($indentStr, $indentLevel);
                // $xmlRes .= PHP_EOL;
            }

            $xmlRes .= '</' . $tagName . '>' . PHP_EOL;
        }
        if ($this->renderAsComment) {
            $xmlRes .= str_repeat($indentStr, $indentLevel) . ' -->' . PHP_EOL;
        }
        if ($this->isRoot === true) {
            $xmlRes = '<?xml version="1.0" encoding="utf-8"?>' . PHP_EOL . $xmlRes;
        }
        return $xmlRes;
    }

    /**
     * @param string $strAttributes
     */
    public function parseAttributes($strAttributes)
    {
        $strLinesArr = array();
        if (strpos($strAttributes, "\n") !== false) {
            $strLinesArr = explode("\n", $strAttributes);
        } else {
            $strLinesArr[] = $strAttributes;
        }
        foreach ($strLinesArr as $strLine) {
            if (empty($strLine)) {
                continue;
            }
            $lnArr = explode('=', $strLine);
            $key = $lnArr[0];
            $key = trim($key);
            $val = $lnArr[1];
            $val = trim($val);
            $val = trim($val, '\'');
            $val = trim($val, '"');
            $this->attributes[$key] = $val;
        }
    }


    /**
     * @param XmlNode $xmlNode
     */
    public function addChild($xmlNode)
    {
        $this->childNodes[] = $xmlNode;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    #endregion methods

    #region utils

    /**
     * @param string $indent
     * @return string
     */
    private function renderComments($indent = '')
    {
        $cmnt = '';
        if (isset($this->comments) && !empty($this->comments)) {
            $cmnt .= $indent . '<!-- ' . $this->comments . ' -->' . PHP_EOL;
        }
        return $cmnt;
    }

    /**
     * @return string
     */
    private function getTagName()
    {
        $tagname = $this->tagName;

        if (!empty($this->prefixNs) && ($this->ignoreNs == false)) {
            $tagname = $this->prefixNs . ':' . $this->tagName;
        }
        return $tagname;
    }


    #endregion utils
}