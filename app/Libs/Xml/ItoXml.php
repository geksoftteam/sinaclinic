<?php


namespace App\Libs\Xml;


interface ItoXml
{

    /**
     * @return XmlNode
     */
    public function toXmlNode();

    /**
     * @return string
     */
    public function toXml();

}