<?php


namespace App\Libs\Filesystem;


use App\Libs\Filesystem\Exceptions\FileNotFoundException;
use App\Libs\Filesystem\Exceptions\IOException;
use Exception;
use FilesystemIterator;
use InvalidArgumentException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Filesystem
{

    #region fields

    const MAX_PATH_LEN = PHP_MAXPATHLEN - 2;

    /**
     * Son Hata Mesajı
     * @var string|null
     */
    private static  $lastErrorMessage;

    #endregion fields

    #region methods

    /**
     * @param string $dir
     * @param int $mode
     * @throws \Throwable
     * @throws IOException
     */
    public function mkdir($dir, $mode = 0777)
    {
        if (is_dir($dir)) {
            return;
        }

        if (!self::errorHandleInvoke('mkdir', $dir, $mode, true)) {
            if (!is_dir($dir)) {
                if (self::$lastErrorMessage) {
                    $msg = 'Dizin oluşturulamadı. ("'.$dir.'") : '. self::$lastErrorMessage;
                    throw new IOException($msg, 0, null, $dir);
                }
                $msg = 'Dizin oluşturulamadı. ("'.$dir.'")';
                throw new IOException($msg, 0, null, $dir);
            }
        }
    }

    /**
     * @param iterable $dirs
     * @param int $mode
     * @throws \Throwable
     */
    public function mkdirs($dirs,  $mode = 0777)
    {
        foreach ($dirs as $dir) {
            $this->mkdir($dir, $mode);
        }
    }

    /**
     * @param string $file
     * @return bool
     * @throws IOException
     */
    public function exists($file)
    {
        if (strlen($file) > self::MAX_PATH_LEN) {
            throw new IOException(
                'Dosya yolu uzunluğu '.self::MAX_PATH_LEN.' karakteri aştığı için dosyanın var olup olmadığı kontrol edilemedi.',
                0,
                null,
                $file
            );
        }
        return file_exists($file);

    }

    /**
     * @param iterable|string[] $files
     * @return bool
     * @throws IOException
     */
    public function existsAll($files)
    {
        foreach ($files as $file) {
            if (!$this->exists($file)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param string $originFile
     * @param string $targetFile
     * @param bool $overwriteNewerFiles
     * @throws FileNotFoundException
     * @throws IOException
     * @throws \Throwable
     *
     */
    public function copy($originFile, $targetFile, $overwriteNewerFiles = false)
    {
        $originIsLocal = stream_is_local($originFile) || 0 === stripos($originFile, 'file://');
        if ($originIsLocal && !is_file($originFile)) {
            throw new FileNotFoundException(
                'Dosya bulunamadığı için kopyalanamadı. Dosya yolu: ' . $originFile,
                0, null, $originFile
            );
        }

        $this->mkdir(dirname($targetFile));

        $doCopy = true;
        if (!$overwriteNewerFiles && null === parse_url($originFile, PHP_URL_HOST) && is_file($targetFile)) {
            $doCopy = filemtime($originFile) > filemtime($targetFile);
        }

        if ($doCopy) {
            // https://bugs.php.net/64634
            if (false === $source = @fopen($originFile, 'r')) {
                throw new IOException(
                    'Kaynak dosya ("'.$originFile.'") okunamadığı için, "'.$targetFile.'" klasörüne kopyalanamadı.', 0, null, $originFile);
            }

            // FTP akış sarmalayıcısı kullanılırken dosyaların üzerine yazılmasına izin vermek için akış içeriği oluşturuldu - varsayılan olarak devre dışı
            if (false === $target = @fopen($targetFile, 'w', null, stream_context_create(['ftp' => ['overwrite' => true]]))) {
                throw new IOException(
                    'Hedef dosya ("'.$originFile.'") yazmak için açılamadığından, "'.$targetFile.'" klasörüne kopyalanamadı.',
                    0, null, $originFile);
            }

            $bytesCopied = stream_copy_to_stream($source, $target);
            fclose($source);
            fclose($target);
            unset($source, $target);

            if (!is_file($targetFile)) {
                throw new IOException(
                    'Kaynak dosya ("'.$originFile.'"), "'.$targetFile.'" hedefine kopyalanamadı.',
                    0, null, $originFile);
            }

            if ($originIsLocal) {
                // Like `cp`, preserve executable permission bits
                @chmod($targetFile, fileperms($targetFile) | (fileperms($originFile) & 0111));

                if ($bytesCopied !== $bytesOrigin = filesize($originFile)) {
                    throw new IOException(sprintf('Failed to copy the whole content of "%s" to "%s" (%g of %g bytes copied).', $originFile, $targetFile, $bytesCopied, $bytesOrigin), 0, null, $originFile);
                }
            }
        }

    }

    /**
     * @param string $file
     * @param int|null $time
     * @param int|null $atime
     * @throws IOException
     */
    public function touch($file,  $time = null,  $atime = null)
    {
        $res = $time != null ? @touch($file, $time, $atime) : @touch($file);
        if ($res !== true) {
            throw new IOException(
                'touch işlemi başarısız oldu. ("'.$file.'")', 0, null, $file
            );
        }
    }

    /**
     * @param iterable $files
     * @param int|null $time
     * @param int|null $atime
     * @throws IOException
     */
    public function touchAll($files,  $time = null,  $atime = null)
    {
        foreach ($files as $file) {
            $this->touch($file, $time, $atime);
        }
    }

    /**
     * @param string $fileOrDir
     * @throws IOException
     * @throws \Throwable
     */
    public function remove($fileOrDir)
    {

        if (is_link($fileOrDir)) {
            // See https://bugs.php.net/52176
            if (
                !(self::errorHandleInvoke('unlink', $fileOrDir) ||
                    '\\' !== DIRECTORY_SEPARATOR ||
                    self::errorHandleInvoke('rmdir', $fileOrDir)) && file_exists($fileOrDir)
            ) {
                throw new IOException(
                    'symlink ("'.$fileOrDir.'") silinemedi : '. self::$lastErrorMessage,
                    0,
                    null,
                    $fileOrDir
                );
            }
        } elseif (is_dir($fileOrDir)) {
            $this->removeAll(new FilesystemIterator($fileOrDir, FilesystemIterator::CURRENT_AS_PATHNAME | FilesystemIterator::SKIP_DOTS));

            if (!self::errorHandleInvoke('rmdir', $fileOrDir) && file_exists($fileOrDir)) {
                throw new IOException(
                    'Dizin ("'.$fileOrDir.'") silinemedi : '. self::$lastErrorMessage,
                    0,
                    null,
                    $fileOrDir
                );
            }
        } elseif (!self::errorHandleInvoke('unlink', $fileOrDir) && file_exists($fileOrDir)) {
            throw new IOException(
                'Dosya ("'.$fileOrDir.'") silinemedi : '. self::$lastErrorMessage,
                0,
                null,
                $fileOrDir
            );
        }

    }

    /**
     * @param iterable $filesOrDirs
     * @throws IOException
     * @throws \Throwable
     */
    public function removeAll($filesOrDirs)
    {
        foreach ($filesOrDirs as $fileOrDir) {
            $this->remove($fileOrDir);
        }
    }

    /**
     * @param string $file
     * @param int $mode
     * @param int $umask
     * @param bool $recursive
     * @throws IOException
     */
    public function chmod($file, $mode, $umask = 0000, $recursive = false)
    {
        if (chmod($file, $mode & ~$umask) !== true) {
            throw new IOException(
                'chmod işlemi başarısız oldu: "'.$file.'".' , 0, null, $file);
        }
        if ($recursive && is_dir($file) && !is_link($file)) {
            $this->chmodAll(new FilesystemIterator($file), $mode, $umask, true);
        }
    }

    /**
     * @param iterable $files
     * @param int $mode
     * @param int $umask
     * @param bool $recursive
     * @throws IOException
     */
    public function chmodAll($files, $mode, $umask = 0000, $recursive = false)
    {
        foreach ($files as $file) {
            $this->chmod($file, $mode, $umask, $recursive);
        }
    }

    /**
     * @param string $file
     * @param string|int $user
     * @param bool $recursive
     * @throws IOException
     */
    public function chown($file, $user, $recursive = false)
    {
        if ($recursive && is_dir($file) && !is_link($file)) {
            $this->chownAll(new FilesystemIterator($file), $user, true);
        }
        if (is_link($file) && function_exists('lchown')) {
            if (true !== lchown($file, $user)) {
                throw new IOException(
                    'chown işlemi başarısız oldu "'.$file.'".',
                    0, null, $file
                );
            }
        } else {
            if (chown($file, $user) !== true) {
                throw new IOException(
                    'chown işlemi başarısız oldu "'.$file.'".',
                    0, null, $file
                );
            }
        }
    }

    /**
     * @param iterable $files
     * @param string|int $user
     * @param bool $recursive
     * @throws IOException
     */
    public function chownAll($files, $user, $recursive = false)
    {
        foreach ($files as $file) {
            $this->chown($file, $user, $recursive);
        }
    }

    /**
     * @param string $file
     * @param string|int $group
     * @param bool $recursive
     */
    public function chgrp($file, $group, $recursive = false)
    {
        if ($recursive && is_dir($file) && !is_link($file)) {
            $this->chgrp(new FilesystemIterator($file), $group, true);
        }
        if (is_link($file) && function_exists('lchgrp')) {
            if (lchgrp($file, $group) !== true) {
                throw new IOException(
                    'chgrp işlemi başarızıs oldu "'.$file.'".',
                    0, null, $file
                );
            }
        } else {
            if (chgrp($file, $group) !== true) {
                throw new IOException(
                    'chgrp işlemi başarızıs oldu "'.$file.'".',
                    0, null, $file
                );
            }
        }
    }

    /**
     * @param iterable $files
     * @param string|int $group
     * @param bool $recursive
     */
    public function chgrpAll( $files, $group,  $recursive = false)
    {
        foreach ($files as $file) {
            $this->chgrp($file, $group, $recursive);
        }
    }

    /**
     * @param string $file
     * @return bool
     * @throws IOException Windows yolu 258 karakterden uzun olduğunda...
     */
    public function isReadable($file)
    {
        if (strlen($file) > self::MAX_PATH_LEN) {
            throw new IOException(
                'Dosya yolu uzunluğu '.self::MAX_PATH_LEN.' karakteri aştığından dosyanın okunabilir olup olmadığı kontrol edilemedi.',
                0, null, $file);
        }

        return is_readable($file);
    }

    /**
     * @param iterable $files
     * @return bool
     * @throws IOException Windows yolu 258 karakterden uzun olduğunda...
     */
    public function isReadableAll($files)
    {
        foreach ($files as $file) {
            if ($this->isReadable($file) !== true) {
                return false;
            }
        }
        return true;
    }


    /**
     * @param string $origin
     * @param string $target
     * @param bool $overwrite
     * @throws IOException
     * @throws \Throwable
     *
     */
    public function rename($origin, $target, $overwrite = false)
    {
        if (!$overwrite && $this->isReadable($target)) {
            throw new IOException(
                '"'.$target.'" hedefi zaten mevcut olduğundan yeniden adlandırılamıyor.',
                0, null, $target
            );
        }

        if (@rename($origin, $target) !== true) {
            if (is_dir($origin)) {
                // See https://bugs.php.net/54097 & https://php.net/rename#113943
                $option = new MirorOptions();
                $option->override = $overwrite;
                $option->delete = $overwrite;
                $this->mirror($origin, $target, null,$option);
                $this->remove($origin);

                return;
            }
            throw new IOException(
                '"'.$origin.'", "'.$target.'" olarak yeniden adlandırılamıyor.',
                0, null, $target
            );
        }
    }


    /**
     * @param string $originDir
     * @param string $targetDir
     * @param bool $copyOnWindows
     * @throws IOException
     * @throws \Throwable
     */
    public function symlink($originDir, $targetDir, $copyOnWindows = false)
    {
        if (DIRECTORY_SEPARATOR === '\\') {
            $originDir = strtr($originDir, '/', '\\');
            $targetDir = strtr($targetDir, '/', '\\');

            if ($copyOnWindows) {
                $this->mirror($originDir, $targetDir);
                return;
            }
        }

        $this->mkdir(dirname($targetDir));

        if (is_link($targetDir)) {
            if (@readlink($targetDir) === $originDir) {
                return;
            }
            $this->remove($targetDir);
        }

        if (!self::errorHandleInvoke('symlink', $originDir, $targetDir)) {
            $this->linkException($originDir, $targetDir, 'symbolic');
        }
    }


    /**
     * @param string $originFile
     * @param iterable $targetFiles
     * @throws FileNotFoundException
     * @throws IOException
     * @throws \Throwable
     */
    public function hardlink($originFile, $targetFiles)
    {
        if (!$this->exists($originFile)) {
            throw new FileNotFoundException(null, 0, null, $originFile);
        }

        if (!is_file($originFile)) {
            throw new FileNotFoundException('Kaynak "'.$originFile.'" bir dosya değil.');
        }

        foreach ($targetFiles as $targetFile) {
            if (is_file($targetFile)) {
                if (fileinode($originFile) === fileinode($targetFile)) {
                    continue;
                }
                $this->remove($targetFile);
            }

            if (!self::errorHandleInvoke('link', $originFile, $targetFile)) {
                $this->linkException($originFile, $targetFile, 'hard');
            }
        }
    }

    /**
     * @param string $path
     * @param bool $canonicalize
     * @return string|null
     * @throws IOException
     */
    public function readlink($path, $canonicalize = false)
    {
        if (!$canonicalize && !is_link($path)) {
            return null;
        }

        if ($canonicalize) {
            if (!$this->exists($path)) {
                return null;
            }

            if (DIRECTORY_SEPARATOR === '\\') {
                $path = readlink($path);
            }

            return realpath($path);
        }

        if (DIRECTORY_SEPARATOR === '\\') {
            return realpath($path);
        }

        return readlink($path);
    }

    /**
     * @param string $endPath
     * @param string $startPath
     * @return string
     * @throws InvalidArgumentException
     */
    public function makePathRelative( $endPath,  $startPath)
    {
        if (!$this->isAbsolutePath($startPath)) {
            throw new InvalidArgumentException(
                'start path "'.$startPath.'" absolute path değil.',
                0,
                null
            );
        }

        if (!$this->isAbsolutePath($endPath)) {
            throw new InvalidArgumentException(
                'end path "'.$endPath.'" absolute path değil.',
                0,
                null
            );
        }

        //Windows dizin ayırıcılarını normalleştiriliyor
        if (DIRECTORY_SEPARATOR === '\\') {
            $endPath = str_replace('\\', '/', $endPath);
            $startPath = str_replace('\\', '/', $startPath);
        }

        $splitDriveLetter = function ($path) {
            return (strlen($path) > 2 && ':' === $path[1] && '/' === $path[2] && ctype_alpha($path[0]))
                ? [substr($path, 2), strtoupper($path[0])]
                : [$path, null];
        };

        $splitPath = function ($path) {
            $result = [];

            foreach (explode('/', trim($path, '/')) as $segment) {
                if ('..' === $segment) {
                    array_pop($result);
                } elseif ('.' !== $segment && '' !== $segment) {
                    $result[] = $segment;
                }
            }

            return $result;
        };

        list($endPath, $endDriveLetter) = $splitDriveLetter($endPath);
        list($startPath, $startDriveLetter) = $splitDriveLetter($startPath);

        $startPathArr = $splitPath($startPath);
        $endPathArr = $splitPath($endPath);

        if ($endDriveLetter && $startDriveLetter && $endDriveLetter != $startDriveLetter) {
            // Bitiş yolu başka bir sürücüde, bu nedenle göreceli yol yok.Absolute path döndür.
            return $endDriveLetter . ':/' . ($endPathArr ? implode('/', $endPathArr) . '/' : '');
        }

        //Ortak yolun hangi dizinde durduğunu bulma
        $index = 0;
        while (isset($startPathArr[$index]) && isset($endPathArr[$index]) && $startPathArr[$index] === $endPathArr[$index]) {
            ++$index;
        }

        // Başlangıç​yolunun ortak yola göre ne kadar derin olduğunu belirleyin (örn. "dizin/alt_dizinr" = 2 seviye)
        if (count($startPathArr) === 1 && '' === $startPathArr[0]) {
            $depth = 0;
        } else {
            $depth = count($startPathArr) - $index;
        }

        // Her seviye için tekrarlanan "../"
        $traverser = str_repeat('../', $depth);

        $endPathRemainder = implode('/', array_slice($endPathArr, $index));

        $relativePath = $traverser . ($endPathRemainder !== '' ? $endPathRemainder . '/' : '');

        return '' === $relativePath ? './' : $relativePath;
    }

    /**
     * @param string $file
     * @return bool
     */
    public function isAbsolutePath($file)
    {
        return strspn($file, '/\\', 0, 1)
            || (strlen($file) > 3 && ctype_alpha($file[0])
                && $file[1] === ':'
                && strspn($file, '/\\', 2, 1)
            )
            || parse_url($file, PHP_URL_SCHEME) !== null;
    }

    /**
     * @param string $dir
     * @param string $prefix
     * @param string $suffix
     * @return string
     * @throws IOException
     */
    public function tempnam($dir, $prefix, $suffix = '')
    {

        list($scheme, $hierarchy) = $this->getSchemeAndHierarchy($dir);


        //Hiçbir şema yoksa ya da şema "dosya" veya "gs" (Google Cloud) değilse,
        // yerel dosya sisteminde geçici dosya oluştur
        if ((null === $scheme || 'file' === $scheme || 'gs' === $scheme) && '' === $suffix) {
            $tmpFile = @tempnam($hierarchy, $prefix);

            // Tempnam başarısız olursa veya şema yoksa, dosya adını döndürün, aksi takdirde şemanın başına ekle
            if ($tmpFile !== false) {
                if ($scheme !== null && $scheme !== 'gs') {
                    return $scheme . '://' . $tmpFile;
                }

                return $tmpFile;
            }

            throw new IOException('Geçici dosya (temporary file) oluşturulamadı.');
        }

        // Geçerli bir geçici dosya oluşturana veya 10 denemeye ulaşıncaya kadar döngü yap
        for ($i = 0; $i < 10; ++$i) {
            // Benzersiz bir dosya adı oluştur
            $tmpFile = $dir . '/' . $prefix . uniqid(mt_rand(), true) . $suffix;


            //Bazı akışlar stat'i desteklemediğinden file_exists yerine fopen kullan
            // Varlığı atomik olarak kontrol etmek için 'x +' modunu kullanın ve TOCTOU güvenlik açığından kaçınmak için oluştur
            $handle = @fopen($tmpFile, 'x+');

            if (false === $handle) {
                continue;
            }

            //Dosya başarıyla açılmışsa kapat
            @fclose($handle);

            return $tmpFile;
        }

        throw new IOException('Geçici dosya (temporary file) oluşturulamadı.');
    }

    /**
     * @param string $filename
     * @param string|resource $content
     * @throws InvalidArgumentException
     * @throws IOException
     * @throws \Throwable
     */
    public function dumpFile($filename, $content)
    {
        if (!is_string($content) && !is_resource($content)) {
            throw new InvalidArgumentException(
                '$content parametresi geçerli bir string veya resource değil.',
                0,
                null
            );
        }

        $dir = dirname($filename);

        if (!is_dir($dir)) {
            $this->mkdir($dir);
        }

        if (!is_writable($dir)) {
            throw new IOException(
                '"'.$dir.'" dizinine yazılamıyor.',
                0, null, $dir
            );
        }

        // Dosya sistemi chmod'u desteklediğinde 0600 erişim haklarına sahip bir geçici dosya oluşturur.
        $tmpFile = $this->tempnam($dir, basename($filename));

        if (@file_put_contents($tmpFile, $content) === false) {
            throw new IOException(
                '"'.$filename.'" dosyası yazılamadı.',
                0, null, $filename
            );
        }

        @chmod($tmpFile, file_exists($filename) ? fileperms($filename) : 0666 & ~umask());

        $this->rename($tmpFile, $filename, true);
    }

    /**
     * @param string $filename
     * @param string|resource $content
     * @throws InvalidArgumentException
     * @throws IOException
     * @throws \Throwable
     */
    public function appendToFile($filename, $content) {
        if (!is_string($content) && !is_resource($content)) {
            throw new InvalidArgumentException(
                '$content parametresi geçerli bir string veya resource değil.',
                0,
                null
            );
        }

        $dir = dirname($filename);

        if (!is_dir($dir)) {
            $this->mkdir($dir);
        }

        if (!is_writable($dir)) {
            throw new IOException(
                '"'.$dir.'" dizinine yazılamıyor.',
                0, null, $dir
            );
        }

        if (false === @file_put_contents($filename, $content, FILE_APPEND)) {
            throw new IOException(
                '"'.$filename.'" dosyası yazılamadı.',
                0, null, $filename
            );
        }

    }

    /**
     * @param string $originDir
     * @param string $targetDir
     * @param iterable|null $iterator
     * @param MirorOptions|null $options
     * @throws IOException
     * @throws \Throwable
     */
    public function mirror($originDir, $targetDir,  $iterator = null,  $options = null) {

        $targetDir = rtrim($targetDir, '/\\');
        $originDir = rtrim($originDir, '/\\');
        $originDirLen = strlen($originDir);

        if (!$this->exists($originDir)) {
            throw new IOException(
                '"'.$originDir.'" belirtilen kaynak dizin bulunamadı.',
                0, null, $originDir
            );
        }

        if($options === null){
            $options = new MirorOptions();
        }




        if ($this->exists($targetDir) &&  $options->delete) {
            $deleteIterator = $iterator;
            if (null === $deleteIterator) {
                $flags = FilesystemIterator::SKIP_DOTS;
                $deleteIterator = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($targetDir, $flags),
                    RecursiveIteratorIterator::CHILD_FIRST);
            }
            $targetDirLen = strlen($targetDir);
            foreach ($deleteIterator as $file) {
                $origin = $originDir.substr($file->getPathname(), $targetDirLen);
                if (!$this->exists($origin)) {
                    $this->remove($file);
                }
            }
        }

        $copyOnWindows = $options->copyOnWindows !== null ? $options->copyOnWindows : false;

        if ($iterator === null ) {
            $flags = $copyOnWindows ?
                FilesystemIterator::SKIP_DOTS | FilesystemIterator::FOLLOW_SYMLINKS :
                FilesystemIterator::SKIP_DOTS;
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($originDir, $flags),
                RecursiveIteratorIterator::SELF_FIRST
            );
        }

        $this->mkdir($targetDir);
        $filesCreatedWhileMirroring = [];

        foreach ($iterator as $file) {
            if ($file->getPathname() === $targetDir || $file->getRealPath() === $targetDir || isset($filesCreatedWhileMirroring[$file->getRealPath()])) {
                continue;
            }

            $target = $targetDir.substr($file->getPathname(), $originDirLen);
            $filesCreatedWhileMirroring[$target] = true;

            if (!$copyOnWindows && is_link($file)) {
                $this->symlink($file->getLinkTarget(), $target);
            } elseif (is_dir($file)) {
                $this->mkdir($target);
            } elseif (is_file($file)) {
                $this->copy($file, $target,  $options->override);
            } else {
                throw new IOException(
                    '"'.$file.'" dosya türü tahmin edilemiyor.',
                    0, null, $file
                );
            }
        }

    }


    #endregion methods

    #region Utils

    /**
     * @param string $filename
     * @return array
     */
    private function getSchemeAndHierarchy($filename)
    {
        $components = explode('://', $filename, 2);
        return count($components) === 2 ? [$components[0], $components[1]] : [null, $components[0]];
    }

    /**
     * @param string $origin
     * @param string $target
     * @param string $linkType
     * @throws IOException
     */
    private function linkException($origin, $target, $linkType)
    {
        if (self::$lastErrorMessage) {
            if (DIRECTORY_SEPARATOR === '\\' && strpos(self::$lastErrorMessage, 'error code(1314)') !== false) {
                throw new IOException(
                    'Hata kodu 1314 nedeniyle "'.$linkType.'" bağlantısı oluşturulamıyor: "İstemci tarafından gerekli izin alınamadı". Gerekli Yönetici haklarına sahip olmayabilirsiniz.',
                    0, null, $target);
            }
        }
        throw new IOException(
            '"'.$origin.'" ile "'.$target.'" arasında "'.$linkType.'" bağlantısı oluşturulamadı.',
            0, null, $target
        );
    }

    /**
     * Exception atmayıp hatadurumunda false döndürem fonksiyonların hata mesajlarını
     * yakalamak için kullanılır.
     * @param callable $fn
     * @param mixed ...$params
     * @return mixed
     * @throws \Throwable
     */
    private static function errorHandleInvoke($fn, ...$params)
    {
        self::$lastErrorMessage = null;
        set_error_handler(get_called_class() . '::handleError');
        try {
            $res = $fn(...$params);
            restore_error_handler();
            return $res;
        } catch (Exception $exp) {
        }
        restore_error_handler();
        throw $exp;
    }

    /**
     * Geçici hata işleyici.
     * @param $type
     * @param $message
     */
    public static function handleError($type, $message)
    {
        self::$lastErrorMessage = $message;
    }

    #endregion Utils


}