<?php


namespace App\Libs\Filesystem\Exceptions;


use RuntimeException;

class IOException extends  RuntimeException
{

    /** @var string|null  */
    private $path;


    /**
     * Construct the exception. Note: The message is NOT binary safe.
     * @link https://php.net/manual/en/exception.construct.php
     * @param string $message [optional] The Exception message to throw.
     * @param int $code [optional] The Exception code.
     * @param \Throwable|null $previous [optional] The previous throwable used for the exception chaining.
     * @param string|null $path
     * @since 5.1.0
     */
    public function __construct($message = "", $code = 0,  $previous = null,  $path = null)
    {
        $this->path = $path;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string|null
     */
    public function getPath()
    {
        return $this->path;
    }

}