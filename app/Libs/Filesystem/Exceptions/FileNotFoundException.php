<?php


namespace App\Libs\Filesystem\Exceptions;


class FileNotFoundException extends IOException
{

    /**
     * {@inheritDoc}
     */
    public function __construct($message = "", $code = 0,  $previous = null,  $path = null)
    {
        if (empty($message)) {
            if ($path === null) {
                $message = 'Dosya bulunamadı.';
            } else {
                $message = 'Dosya bulunamadı. Dosya yolu: "'.$path.'"';
            }
        }
        parent::__construct($message, $code, $previous, $path);
    }


}