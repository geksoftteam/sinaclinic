<?php


namespace App\Libs\Filesystem;


class MirorOptions
{

    /**
     * MirorOptions constructor.
     * @param bool $override
     * @param bool|null $copyOnWindows
     * @param bool $delete
     */
    public function __construct($override = false, $copyOnWindows = null, $delete = false)
    {
        $this->override = $override;
        $this->copyOnWindows = $copyOnWindows;
        $this->delete = $delete;
    }

    /** @var bool  */
    public $override = false;

    /** @var bool|null  */
    public $copyOnWindows = null;

    /** @var bool  */
    public $delete = false;


}