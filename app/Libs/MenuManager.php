<?php


namespace App\Libs;


use App\Libs\Cache\DataCache;
use App\Libs\Cache\Ttl;
use App\Libs\Filesystem\Filesystem;
use Exception;
use SimpleXMLElement;

class MenuManager
{

    #region fields

    /**
     * @var DataCache
     */
    protected $dataCache;

    #endregion fields

    #region ctor

    public function __construct()
    {
        $this->dataCache = DataCache::instance();
    }

    #endregion ctor

    #region Methods

    /**
     * @param string $xmlPath
     * @return Menu
     */
    public function loadMenu($xmlPath)
    {
        $cacheKey = $this->cacheKey($xmlPath);
        return $this->dataCache->getOrSave(
            $cacheKey,
            [$this, 'loadMenuNoCache'],
            [$xmlPath],
            Ttl::fromWeeks(2)
        );
    }

    /**
     * @param string $xmlPath
     * @return Menu
     * @throws Exception
     */
    public function loadMenuNoCache($xmlPath)
    {
        $fs = new Filesystem();
        if(! $fs->exists($xmlPath)){
            throw new Exception('Menu dosyası bulunamadı. path: ' . $xmlPath );
        }
        $xmlStr = file_get_contents($xmlPath);
        $root = new SimpleXMLElement($xmlStr);
        $menu = new Menu();
        foreach ($root->children() as $menuItem){
            $menu->addMenuItem($this->readMenuItem($menuItem));
        }

        return $menu;
    }

    /**
     * @param Menu $menu
     * @param string $xmlPath
     * @throws \Throwable
     */
    public function saveMenu($menu, $xmlPath){
        $fs = new Filesystem();
        $fs->dumpFile($xmlPath,$menu->toXml());
        $cacheKey = $this->cacheKey($xmlPath);
        $this->dataCache->delete($cacheKey);
    }

    /**
     * @param string $jsonData
     * @return array|MenuItem[]
     * @throws \ReflectionException
     */
    public function parseJson($jsonData)
    {
        $data = json_decode($jsonData);


        $items = array();
        foreach ($data as $obj) {
            $items[] = $this->parseFromObj($obj);
        }
        return $items;
    }

    #endregion Methods

    #region utils

    /**
     * @param object $obj
     * @return MenuItem
     * @throws \ReflectionException
     */
    protected function parseFromObj($obj)
    {

        $mn = new MenuItem();
        $mn->setText($obj->text);
        $mn->setUrl($obj->url);
        $mn->setMenuType($obj->menutype);
        $mn->setIconClass($obj->iconclass);
        $mn->setLoginRequired($obj->loginrequired);
        $mn->setTableId($obj->tableid);
        $mn->setTableName($obj->tablename);

        if(isset($obj->attributes)){
            foreach ($obj->attributes as $k => $v){
                $mn->addAttribute($k,$v);
            }
        }

        if(isset($obj->children)){
            foreach ($obj->children as $chld){
                $mn->addChild($this->parseFromObj($chld));
            }
        }

        return $mn;
    }


    /**
     * @param string $xmlPath
     * @return string
     */
    protected function cacheKey($xmlPath)
    {
        return 'menu_' . md5($xmlPath);
    }

    /**
     * @param SimpleXMLElement $element
     * @return MenuItem
     */
    protected function readMenuItem($element){
        $mi = new MenuItem();

        foreach ($element->attributes() as $key => $val){
            $val = (string)$val;
            switch (strtolower($key)){
                case 'text':
                    $mi->setText($val);
                    break;
                case 'url':
                    $mi->setUrl($val);
                    break;
                case 'menuType':
                    $mi->setMenuType($val);
                    break;
                case 'iconClass':
                    $mi->setIconClass($val);
                    break;
                case 'loginRequired':
                    $mi->setLoginRequired((bool)$val);
                    break;
                case 'tableName':
                    $mi->setTableName($val);
                    break;
                case 'tableId':
                    $mi->setTableId($val);
                    break;
                default:
                    $mi->addAttribute($key,$val);
                    break;
            }
        }

        foreach ($element->children() as $subMenuItem){
            $mi->addChild($this->readMenuItem($subMenuItem));
        }
        return $mi;
    }

    #endregion utils

}