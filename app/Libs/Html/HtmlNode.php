<?php


namespace App\Libs\Html;


abstract class HtmlNode implements \IteratorAggregate
{

    #region fields

    /**
     * @var HtmlNode|null
     */
    protected $parentNode = null;


    /**
     * @var array|HtmlNode[]
     */
    protected $childNodes;

    #endregion fields

    #region ctor

    protected function __construct(){
        $this->childNodes = array();
    }

    #endregion ctor

    #region Properties

    /**
     * @return HtmlNode|null
     */
    public function getParentNode()
    {
        return $this->parentNode;
    }

    /**
     * @param HtmlNode|null $parentNode
     * @return HtmlNode
     */
    public function setParentNode($parentNode)
    {
        $this->parentNode = $parentNode;
        return $this;
    }

    /**
     * @return HtmlNode[]|array
     */
    public function getChildNodes()
    {
        return $this->childNodes;
    }

    /**
     * @param array|HtmlNode[] $childNodes
     * @return HtmlNode
     */
    public function setChildNodes($childNodes)
    {
        $this->childNodes = $childNodes;
        return $this;
    }

    #endregion Properties

    #region methods

    /**
     * @param HtmlNode $node
     * @return static
     */
    public function addChild($node){
        $node->setParentNode($this);
        $this->childNodes[] = $node;
        return $this;
    }

    /**
     * @return string
     */
    public abstract function toHtmlString();

    #endregion methods

    #region IteratorAggregate

    /**
     * Retrieve an external iterator
     * @link https://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return \Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @throws \Exception on failure.
     */
    public function getIterator(){
        return new \RecursiveIteratorIterator(new HtmlNodeIterator($this), \RecursiveIteratorIterator::SELF_FIRST);
    }

    #endregion IteratorAggregate

}