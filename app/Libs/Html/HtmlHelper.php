<?php


namespace App\Libs\Html;


class HtmlHelper
{

    #region fields

    /**
     * @var array|string[]
     */
    protected $selfClosingTags = array(
        'area',
        'base',
        'basefont',
        'br',
        'col',
        'embed',
        'hr',
        'img',
        'input',
        'keygen',
        'link',
        'meta',
        'param',
        'source',
        'spacer',
        'track',
        'wbr'
    );

    /**
     * @var array|string[]
     */
    protected $formTypes = array(
        'input',
        'form',
        'textarea',
        'select',
        'button',
    );

    /**
     * @var array
     */
    protected $allIds = array();

    /**
     * @var array
     */
    protected $formElementIds = array();

    /**
     * @var string|null
     */
    protected  $lastId = null;

    /**
     * @var string|null
     */
    protected $lastFormElementId = null;

    #endregion fields

    #region methods

    /**
     * @param string $id
     * @return bool
     */
    public function isUsedId($id)
    {
        return in_array($id, $this->allIds);
    }

    /**
     * @param string $id
     * @return string
     */
    public function fixId($id)
    {
        return $id . rand(1, 500);
    }

    /**
     * @param string $id
     * @param string $tag
     * @param bool $autoFix
     * @return string
     */
    public function addId($id,  $tag,  $autoFix = true)
    {
        if ($autoFix) {
            if ($this->isUsedId($id)) {
                $id = $this->fixId($id);
            }
        }
        $this->allIds[] = $id;
        $this->lastId = $id;
        if (in_array(strtolower(trim($tag)), $this->formTypes)) {
            $this->formElementIds[] = $id;
            $this->lastFormElementId = $id;
        }
        return $id;
    }

    /**
     * @param string $tagName
     * @param array|HtmlNode|string|HtmlNode[]|string[]|null $content
     * @param array $attributes
     * @return HtmlElement
     */
    public function createTag($tagName, $content = null, array $attributes = array())
    {
        $node = new HtmlElement($tagName);
        if (in_array($tagName, $this->selfClosingTags)) {
            $node->setSelfClosing();
        }
        if (!empty($attributes)) {
            if (isset($attributes['id'])) {
                $attributes['id'] = $this->addId($attributes['id'], $tagName, true);
            }
        }
        $node->setAttributes(!empty($attributes) ? $attributes : array());
        if (!empty($content)) {
            if (is_array($content)) {
                foreach ($content as $c) {
                    $this->addChild($node, $c);
                }
            } else {
                $this->addChild($node, $content);
            }
        }
        return $node;
    }

    /**
     * @param string $text
     * @return TextNode
     */
    public function createTextNode( $text)
    {
        return new TextNode($text);
    }

    /**
     * @param string $text
     * @return CommentNode
     */
    public function createCommentNode($text)
    {
        return new CommentNode($text);
    }

    /**
     * @param string $label
     * @param string|null $for
     * @param array $attributes
     * @return HtmlElement
     */
    public function createLabel($label,  $for = null, $attributes = array())
    {
        if ($for !== null) {
            $attributes['for'] = $for;
        }
        return $this->createTag('label', $label, $attributes);
    }

    /**
     * @param string $name
     * @param string $type
     * @param mixed|null $value
     * @param array $attributes
     * @return HtmlElement
     */
    public function createInput($name, $type, $value = null, $attributes = array())
    {
        $attributes['name'] = $name;
        $attributes['type'] = $type;
        if ($value !== null) {
            $attributes['value'] = $value === false ? '0' : strval($value);
        }
        return $this->createTag('input', null, $attributes);
    }

    /**
     * @param string $name
     * @param mixed|null $value
     * @param array $attributes
     * @return HtmlElement
     */
    public function createTextArea($name, $value = null,  $attributes = array())
    {
        $attributes['name'] = $name;
        return $this->createTag('textarea',$value,$attributes);
    }

    #endregion methods

    #region utils

    /**
     * @param $attributes
     * @return array
     */
    protected function fixAttributesArg($attributes)
    {
        if (is_object($attributes)) {
            $attributes = (array)$attributes;
        } elseif ($attributes === null) {
            $attributes = array();
        }
        return $attributes;
    }

    /**
     * @param HtmlNode $base
     * @param HtmlNode|string $child
     */
    protected function addChild($base, $child)
    {
        if ($child instanceof HtmlNode) {
            if ($child instanceof HtmlElement) {
                if ($child->getId() !== null) {
                    $id = $this->addId($child->getId(), $child->getTagName(), true);
                    $child->setId($id);
                }
            }
            $base->addChild($child);
        } else {
            $base->addChild($this->createTextNode($child));
        }
    }

    #endregion utils
}