<?php


namespace App\Libs\Html;


class TextNode extends HtmlNode
{

    #region fields

    /**
     * @var string
     */
    protected $text;



    #endregion fields

    #region ctor

    /**
     * TextNode constructor.
     * @param string $text
     */
    public function __construct($text = '')
    {
        parent::__construct();
        $this->text = $text;
    }

    #endregion ctor

    #region Properties

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return TextNode
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    #endregion Properties

    #region methods

    /**
     * @return string
     */
    public function toHtmlString()
    {
        return $this->text;
    }

    #endregion methods

}