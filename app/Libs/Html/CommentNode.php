<?php


namespace App\Libs\Html;


class CommentNode extends TextNode
{
    const OPEN = '<!-- ';
    const CLOSE = ' -->';

    #region ctor

    /**
     * CommentNode constructor.
     * @param string $comment
     */
    public function __construct($comment = '')
    {
        parent::__construct($comment);
    }

    #endregion ctor

    #region methods

    /**
     * @return string
     */
    public function toHtmlString(): string
    {
        $res = parent::toHtmlString();
        return static::OPEN . $res . static::CLOSE;
    }


    #endregion methods
}