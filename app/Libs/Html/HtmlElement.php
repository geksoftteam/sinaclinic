<?php


namespace App\Libs\Html;


class HtmlElement extends HtmlNode
{
    #region fields

    /**
     * @var string
     */
    protected $tagName;

    /**
     * @var array<string,string>
     */
    protected $attributes;


    /**
     * @var bool
     */
    protected $selfClosing = false;

    #endregion fields

    #region ctor
    /**
     * HtmlElement constructor.
     * @param string $tagName
     */
    public function __construct($tagName)
    {
        parent::__construct();
        $this->tagName = $tagName;
        $this->attributes = array();
    }

    #endregion ctor

    #region Properties

    /**
     * @return string
     */
    public function getTagName()
    {
        return $this->tagName;
    }

    /**
     * @param string $tagName
     * @return HtmlNode
     */
    public function setTagName($tagName)
    {
        $this->tagName = $tagName;
        return $this;
    }

    /**
     * @return array<string,string>
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array<string,string> $attributes
     * @return HtmlNode
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSelfClosing()
    {
        return $this->selfClosing;
    }

    /**
     * @param bool $selfClosing
     * @return HtmlNode
     */
    public function setSelfClosing($selfClosing = true)
    {
        $this->selfClosing = $selfClosing;
        return $this;
    }

    #endregion Properties

    #region methods


    /**
     * @param string $key
     * @return string|null
     */
    public function getAttr($key)
    {
        $key = strtolower($key);
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }
        return null;
    }

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function setAttr($key, $value)
    {
        $key = strtolower($key);
        $this->attributes[$key] = $value;
        
        return $this;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function removeAttr($key)
    {
        $key = strtolower($key);
        if(array_key_exists($key,$this->attributes)){
            unset($this->attributes[$key]);
            return true;
        }
        return false;
    }

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->getAttr('id');
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setAttr('id', $id);
        return $this;
    }

    /**
     * @param string $class
     * @return bool
     */
    public function hasClass($class)
    {
        $class = trim($class);
        if (empty($class)) {
            return false;
        }

        $classes = $this->getAttr('class');
        if (empty($classes)) {
            return false;
        }
        return in_array($class,explode(' ', trim($classes)));

    }

    /**
     * @param string $class
     * @return $this
     */
    public function addClass($class)
    {
        $classes = $this->getAttr('class');
        if (empty($classes)) {
            $this->setAttr('class', $class);
        } elseif (!$this->hasClass($class)) {
            $classes .= " " . $class;
            $this->setAttr('class', $classes);
        }
        return $this;
    }


    public function toHtmlString()
    {
        $htmlRes = "<" . $this->tagName;

        if (!empty($this->attributes)) {
            $htmlRes .= " ";
            foreach ($this->attributes as $k => $v) {
                $htmlRes .= $k;
                if ($v !== null) {
                    if (((strpos($v, '[') !== false) || (strpos($v, '{') !== false)) && static::isJson($v)) {
                        $v = htmlspecialchars($v);
                    } else {
                        $v = str_replace('"', '\"', $v);
                    }
                    $htmlRes .= '=' . '"' . $v . '"';
                }
                $htmlRes .= ' ';
            }
        }
        if ($this->selfClosing) {
            $htmlRes .= '/>';
            return $htmlRes;
        }
        $htmlRes .= '>';
        if (!empty($this->childNodes)) {
            foreach ($this->childNodes as $chNd) {
                $htmlRes .= $chNd->toHtmlString();
            }
        }
        $htmlRes .= '</' . $this->tagName . '>';
        return $htmlRes;
    }

    #endregion methods

    #region utils

    /**
     * @param string $string
     * @return bool
     */
    protected static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    #endregion utils
}