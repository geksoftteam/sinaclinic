<?php


namespace App\Libs\Html;


use RecursiveIterator;

class HtmlNodeIterator implements \RecursiveIterator
{

    #region fields

    /**
     * @var HtmlNode
     */
    protected $htmlNode;

    /**
     * @var int
     */
    protected $pos = 0;

    #endregion fields

    #region ctor

    /**
     * HtmlNodeIterator constructor.
     * @param HtmlNode $htmlNode
     */
    public function __construct($htmlNode)
    {
        $this->htmlNode = $htmlNode;
    }

    #endregion ctor

    #region RecursiveIterator

    /**
     * @return HtmlNode|mixed
     */
    public function current()
    {
        return $this->htmlNode->getChildNodes()[$this->pos];
    }


    public function next()
    {
        ++$this->pos;
    }

    /**
     * @return bool|float|int|string|null
     */
    public function key()
    {
        return $this->pos;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->pos < count($this->htmlNode->getChildNodes());
    }

    public function rewind()
    {
        $this->pos = 0;
    }

    public function hasChildren()
    {
        $cur = $this->current();
        return !empty($cur->getChildNodes());
    }

    public function getChildren()
    {
        $cur = $this->current();
        return new HtmlNodeIterator($cur);
    }

    #endregion RecursiveIterator
}