<?php


namespace App\Libs;


use App\Libs\Xml\ItoXml;
use App\Libs\Xml\XmlNode;

class Menu implements ItoXml, \JsonSerializable
{

    #region field

    /**
     * @var array|MenuItem[]
     */
    protected $menuItems = array();

    #endregion field

    #region ctor

    /**
     * Menu constructor.
     * @param array $menuItems
     */
    public function __construct($menuItems = array())
    {
        $this->menuItems = $menuItems;
    }

    #endregion ctor

    #region Properties

    /**
     * @return array|MenuItem[]
     */
    public function getMenuItems()
    {
        return $this->menuItems;
    }

    /**
     * @param array|MenuItem[] $menuItems
     * @return Menu
     */
    public function setMenuItems($menuItems)
    {
        $this->menuItems = $menuItems;
        return $this;
    }

    #endregion Properties

    #region Methods

    /**
     * @param MenuItem $menuItem
     */
    public function addMenuItem($menuItem){
        $this->menuItems[] = $menuItem;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toXml();
    }

    #endregion Methods

    #region ItoXml

    /**
     * @return XmlNode
     */
    public function toXmlNode()
    {
        $root = new XmlNode('menu');
        $root->isRoot = true;

        foreach ($this->menuItems as $mi){
            $root->addChild($mi->toXmlNode());
        }
        return $root;
    }

    /**
     * @return string
     */
    public function toXml()
    {
        return $this->toXmlNode()->toXmlString();
    }

    #endregion ItoXml

    #region JsonSerializable

    public function jsonSerialize()
    {
        return (object)array(
            'menuItems' => $this->menuItems,
        );
    }

    #endregion JsonSerializable

}