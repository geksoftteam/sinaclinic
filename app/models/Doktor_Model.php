<?php

class Doktor_Model extends Model {

	

	public function listele(){
		return $this->db->select('doktor')->run();
	}

	public function doktorGetir($doktorUrl)
	{
		return $this->db->select('doktor')->where('doktor_seo_name', $doktorUrl)->run(1);

	}

	public function bolumegoreDoktorlar($doktorId){
		return $this->db->select('doktormap')->where('doktor_bolum_id', $doktorId)
		->join('doktor', '%s.doktor_id  = %s.doktor_id', 'left')
		->run();
	}

	public function doktorlistele()
	{
		return $this->db->select('page')
		->join('nav', '%s.nav_id = %s.page_kat_id', 'left')
		->where('page_tur', 2)
		->orderby('page_id', 'desc')
		->run();
		
	}

}

