<?php

class Bolum_Model extends Model {

	

	public function listele(){
		return $this->db->select('bolum')->run();
	}

	public function bolumGetir($bolumUrl)
	{
		return $this->db->select('bolum')->where('bolum_seo_name', $bolumUrl)->run(1);

	}

	public function bolumegoreDoktorlar($doktorId){
		return $this->db->select('doktormap')->where('doktor_bolum_id', $doktorId)
		->join('doktor', '%s.doktor_id  = %s.doktor_id', 'left')
		->groupby('doktormap.doktor_id')
		->run();
	}

	public function blogListele()
	{
		return $this->db->select('page')
		->join('nav', '%s.nav_id = %s.page_kat_id', 'left')
		->where('page_tur', 2)
		->orderby('page_id', 'desc')
		->run();
		
	}

}

