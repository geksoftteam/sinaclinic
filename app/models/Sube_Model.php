<?php

class Sube_Model extends Model {


    public function listele(){
        return $this->db->select('sube')->run();
    }

    public function subeGetir($subeUrl)
    {
        return $this->db->select('sube')->where('sube_seo_name', $subeUrl)->run(1);

    }

}
