<?php



class Page_Model extends Model {

	public function __construct() {

		parent::__construct();

	}



	public function bilgikat($seoname) {



		$varmi = $this->db->select('nav')->from('count(nav_id) as total')->where('nav_url', $seoname)->total();

		if ($varmi > 0) {

			return $this->db->select('nav')->where('nav_url', $seoname)->run(1);

		} else {

			return false;

		}



	}



	public function bilgipage($seoname) {

		$varmi = $this->db->select('page')->from('count(page_id) as total')->where('page_url', $seoname)->total();
		if ($varmi > 0) {
			return $this->db->select('page')->where('page_url', $seoname)->run(1);
		} else {
			return false;
		}
	}



	public function page_gallery($id = '') {



		return $this->db->select('galeri')->where('galeri_page_id', $id)->orderby('galeri_name', 'asc')->run();



	}

	public function Sidebar_Menu($tur)
	{

		return $this->db->select('page')->where('page_durum', 1)->where('page_tur', $tur)->run();
	}
	



	public function page_gallery_count($id='')

	{

		return $this->db->select('galeri')->from('count(galeri_id) as total')->where('galeri_page_id', $id)->total();

	}



	

	public function ekhizmetler($id='') {



		return $this->db->select('ekhizmet')->where('ekhizmet_ustid',$id)->run();



	}



	public function bilgileri($id='') {



		return $this->db->select('ekhizmet')->where('ekhizmet_id',$id)->run(1);



	}

}

