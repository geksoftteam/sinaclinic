<?php

class Index_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function nav() {

		$main = $this->db->select('nav')->where('nav_main', 0)->where("nav_durum", 1)->orderby('nav_sira', 'asc')->run();
		$dizi = array();
		foreach ($main as $li) {
			$indis = $li['nav_id'];
			$dizi[$indis] = $li;
			$sub_nav = $this->db->select('nav')->where('nav_main', $indis)->where("nav_durum", 1)->orderby('nav_sira', 'asc')->run();
			if (count($sub_nav) > 0) {

				$dizi[$indis]['sub'] = $sub_nav;
				$dizi[$indis]['sub_type'] = 'menu';
			} else {
				$dizi[$indis]['sub'] = $this->db->select('page')
				->join('nav', '%s.nav_id = %s.page_kat_id', 'left')
				->where('page_kat_id', $indis)
				->where("page_durum", 1)
				->where("page_konum", 1)
				->orderby('page_sira', 'asc')
				->run();
				$dizi[$indis]['sub_type'] = 'content';

			}

		}

		return $dizi;

	}
	public function dillist($id = '') {
		return $this->db->select('diller')
		->where("diller_durum", 1)
		->orderby('diller_sira', 'asc')
		->run();
	}

	public function Sube_listele($id = '') {
		return $this->db->select('sube')->run();
	}

	public function one_page($id = '') {
		return $this->db->select('page')
		->join('nav', '%s.nav_id = %s.page_kat_id', 'left')
		->where('page_id', $id)
		->where("page_durum", 1)
		->run(1);
	}

	public function one_cat($id = '') {
		return $this->db->select('nav')
		->where('nav_id', $id)
		->where("nav_durum", 1)
		->run(1);
	}

	public function nav_page($id = '') {
		return $this->db->select('page')
		->join('nav', '%s.nav_id = %s.page_kat_id', 'left')

		->where('page_kat_id', $id)
		->where("page_durum", 1)
		->where("page_konum_side", 1)
		->orderby('page_sira', 'asc')
		->run();
	}


	public function nav_page_anasayfa($id = array(),$limit=100) {
		$idler = implode(',', $id);

		
		$sql_str = 'SELECT * FROM page 
		LEFT JOIN nav   ON ( nav.`nav_id` = `page`.`page_kat_id` )
		
		WHERE  page_durum = 1 AND page_kat_id IN ('.$idler.') AND page_konum_home = 1
		order by page_sira asc	 limit 0, '.$limit;

		return $this->db->query($sql_str)->fetchAll(PDO::FETCH_ASSOC);	

	}

	public function nav_page_anasayfa_hizmet($id = array(),$limit=100) {
		$idler = implode(',', $id);

		
		$sql_str = 'SELECT * FROM page 
		LEFT JOIN nav   ON ( nav.`nav_id` = `page`.`page_kat_id` )
		WHERE  page_durum = 1 AND page_kat_id IN ('.$idler.') AND page_konum_home = 1
		order by page_sira asc	 limit 0, '.$limit;

		return $this->db->query($sql_str)->fetchAll(PDO::FETCH_ASSOC);	

	}

	public function nav_page_anasayfa_urun($id = array(),$limit=100) {
		$idler = implode(',', $id);

		
		$sql_str = 'SELECT * FROM page 
		LEFT JOIN nav   ON ( nav.`nav_id` = `page`.`page_kat_id` )
		WHERE  page_durum = 1 AND page_kat_id IN ('.$idler.') AND page_konum_home = 1
		order by page_sira asc	 limit 0, '.$limit;

		return $this->db->query($sql_str)->fetchAll(PDO::FETCH_ASSOC);	

	}

	public function nav_cat($id = '') {
		return $this->db->select('nav')
		->where('nav_main', $id)
		->where("nav_durum", 1)
		->orderby('nav_sira', 'asc')
		->run();
	}

	public function nav_page_one($id = '') {
		return $this->db->select('page')
		->join('nav', '%s.nav_id = %s.page_kat_id', 'left')

		->where('page_kat_id', $id)
		->where("page_durum", 1)
		->run(1);
	}



	public function slider()
	{
		return   $this->db->select('slider2')->orderby('slider2_sira','asc')->run();
		

		
	}


	public function page_gallery($id = '') {

		return $this->db->select('galeri')->where('galeri_page_id', $id)->orderby('galeri_name', 'asc')->limit(0,2)->run();

	}




	public function rgaleri($limit = 20 )
	{
		
		return $this->db->select('page')
		->join('nav', '%s.nav_id = %s.page_kat_id', 'left')
		->where("page_tur", 7)
		->where("page_durum", 1)
		->where("page_konum_home", 1)
		->limit(0,$limit)
		->run();



	}


	public function vgaleri($limit = 20 )
	{
		
		
		return $this->db->select('page')
		->join('nav', '%s.nav_id = %s.page_kat_id', 'left')
		->where('page_kat_id', 23)
		->where("page_konum_home", 1)		
		->where("page_durum", 1)
		->run();


	}


	

}
