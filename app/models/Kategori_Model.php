<?php



class Kategori_Model extends Model {

	public function __construct() {

		parent::__construct();

	}

	public function cat_pages($id = '') {
		$cat = $this->db->select('nav')->where('nav_id', $id)->run(1);
		if ($cat['nav_main'] == 0) {
			$sub = $this->db->select('nav')->where('nav_main', $id)->run();
			if ($sub) {
				# code...
				$idler = array();
				foreach ($sub as $sub_row) {
					$idler[] = $sub_row['nav_id'];
				}
				$in_id = implode(',', $idler);
				$qrt = 'select * from page
				LEFT JOIN nav ON (page.page_kat_id=nav.nav_id)
				where page_kat_id in(' . $in_id . ') order by page_id desc';
			} else {
				$qrt = 'select * from page
				LEFT JOIN nav ON (page.page_kat_id=nav.nav_id)
				where page_kat_id = ' . $id . ' order by page_id desc';
			}
			$retrun = $this->db->query($qrt)->fetchAll(PDO::FETCH_ASSOC);
			return $retrun;
		} else {
			return $this->db->select('page')->join('nav', '%s.nav_id = %s.page_kat_id', 'left')
			->where('page_kat_id', $id)->run();
		}
	}
	public function get_content($id = '',$count = '') {


		if ($count == '') {
			return $this->db->select('page')->where('page_kat_id', $id)->run();

		}else{


			$per = 5;
			if ($count>1) {
				$count = $count * $per-5;
			}else{
				$count = 0;
			}

			return $this->db->select('page')->where('page_kat_id', $id)->limit($count,$per)->run();


		}



	}

	public function KategoriTurBul($nav_id)
	{
		return $this->db->select('page')->where('page_kat_id', $nav_id)->run(1);

	}

	public function main_name($id='')

	{

		return $this->db->select('nav')->where('nav_id', $id)->run(1);

	}
	
	public function bilgikat($seoname) {



		$varmi = $this->db->select('nav')->from('count(nav_id) as total')->where('nav_url', $seoname)->total();

		if ($varmi > 0) {

			return $this->db->select('nav')->where('nav_url', $seoname)->run(1);

		} else {



			return false;

		}



	}


	

	public function page_gallery($id = '') {



		return $this->db->select('galeri')->where('galeri_page_id', $id)->orderby('galeri_name', 'asc')->run();



	}
}
