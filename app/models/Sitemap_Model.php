<?php

class Sitemap_Model extends Model{
  public function __construct() {
    parent::__construct();
  }
  

  public function all_data()
  {
    return $this->db->select('page')
    ->join('categories', '%s.Cat_id = %s.pages_cat_id', 'left')
    ->where('pages_durum','1')
    ->orderby('pages_sira','asc')                         
    ->run();

  }

  public function ic_data()
  {
    return $this->db->select('content')                          
    ->where('content_durum','1')
    ->orderby('content_sira','asc')                         
    ->run();

  }


  public function ara($ara='')
  {
    $data['sayfacount'] = $this->db->select('page')
    ->from('count(pages_id) as total')
    ->where('pages_name',$ara,'like')
    ->where('pages_durum',1)
    ->total();
    if ($data['sayfacount']>0) {
      $data['sayfa'] = $this->db->select('page')                          
      ->join('categories', '%s.Cat_id = %s.pages_cat_id', 'left')
      ->where('pages_name',$ara,'like')
      ->where('pages_durum',1)                      
      ->run();                    
    } else {
     $data['sayfa'] =   array();                        
   }


   $data['iccount'] = $this->db->select('content')
   ->from('count(content_id) as total')
   ->where('content_name',$ara,'like')
   ->where('content_durum',1)
   ->total();
   if ($data['iccount']>0) {
    $data['ic'] = $this->db->select('content')
    ->where('content_name',$ara,'like')
    ->where('content_durum',1)                      
    ->run();                    
  } else {
   $data['ic'] =   array();                        
 }
 
 return $data;
}

}
