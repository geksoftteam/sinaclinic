<?php //model 
class Bolum_Model extends Model{
	public function __construct() {
		parent::__construct();
	}

	public function Bolum_listele(){

		$result = $this->db->select("bolum")->run();
		$map = $this->db->select("subebolum")->run();

		foreach ($result as &$item) {
			$bolums = array_filter($map, function($i) use ($item){
				return $i['bolum_id'] == $item['bolum_id'];
			});
			$item['sube_ids'] = array();
			foreach ($bolums as $blm) {
				$item['sube_ids'][] = $blm['sube_id']; 
			}
		}
		return $result;
	}
	
	public function sube_listele(){

		return $this->db->select("sube")->run();
		
	}

	public function subebolum(){

		return $this->db->select("sube")->run();

	}
	
	public function Bolum_Guncelle_Form($id){

		return $this->db->select("bolum")->where("bolum_id",$id)->run(1);	 

	}
	
	
	public function Bolum_insert($array){

		$sub_category = $this->db->insert("bolum")->set($array);

		if($sub_category){
			return  true;
		}else{
			return  false;
		}

	}


	public function subeBolumInsert($dataArray)
	{
		
		$Insert = $this->db->insert("subebolum")->set($dataArray);

		if($Insert){
			return  true;
		}else{
			return  false;
		}
	}
	public function subeBolumDelete($subeID, $bolumId){
		$query = $this->db->delete("subebolum")->where("sube_id",$subeID)->where("bolum_id",$bolumId)->done(); 
		if ( $query ){
			return true;
		}else{
			return false; 
		}
	}

	public function Bolum_update($array,$id){

		$sub_category = $this->db->update("bolum")->where("bolum_id",$id)->set($array);

		if($sub_category){
			return  true;
		}else{
			return  false;
		}

	}



	public function Bolum_delete($id){
		$query = $this->db->delete("bolum")->where("bolum_id",$id)->done(); 
		if ( $query ){
			return true;
		}else{
			return false; 
		}
	}

	public function Bolum_kopyala($id){

		$data =  $this->db->select("bolum")->where("bolum_id",$id)->run(1);
		unset($data['bolum_id']);
		$sub_category = $this->db->insert("bolum")->set($data);

		if($sub_category){
			return  true;
		}else{
			return  false;
		}
	}
	
	
	
}
