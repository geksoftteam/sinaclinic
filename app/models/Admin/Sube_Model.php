<?php //model 
class Sube_Model extends Model{
	public function __construct() {
		parent::__construct();
	}
	
	public function Sube_listele(){
		
		return $this->db->select("sube")->run();
		
		
	}
	
	
	
	public function Sube_Guncelle_Form($id){
		
		return $this->db->select("sube")->where("sube_id",$id)->run(1);	 
		
	}
	
	
	public function Sube_insert($array){
		
		$sub_category = $this->db->insert("sube")->set($array);
		
		if($sub_category){
			return  true;
		}else{
			return  false;
		}
		
	}
	
	
	public function Sube_update($array,$id){
		
		$sub_category = $this->db->update("sube")->where("sube_id",$id)->set($array);
		
		if($sub_category){
			return  true;
		}else{
			return  false;
		}
		
	}
	
	
	
	public function Sube_delete($id){
		$query = $this->db->delete("sube")->where("sube_id",$id)->done(); 
		if ( $query ){
			return true;
		}else{
			return false; 
		}
	}
	
	public function Sube_kopyala($id){
		
		$data =  $this->db->select("sube")->where("sube_id",$id)->run(1);
		unset($data['sube_id']);
		$sub_category = $this->db->insert("sube")->set($data);
		
		if($sub_category){
			return  true;
		}else{
			return  false;
		}
	}
	
	
	

	public function seoname($value = '', $id = 0) {



		if ($id == 0) {

			return $this->db->select('sube')->from('count(sube_id) as total')->where('sube_seo_name', $value)->total();



		} else {

			return $this->db->select('sube')

			->from('count(sube_id) as total')

			->where('sube_seo_name', $value)

			->where('sube_id', $id, '!=')

			->total();



		}



	}
	
}
