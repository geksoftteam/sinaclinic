<?php //model 
class Doktor_Model extends Model{
	public function __construct() {
		parent::__construct();
	}
	
	public function Doktor_listele(){
		
		return $this->db->select("doktor")->run();
		
		
	}
	
	public function subeBolumGetir(){
		$result =  $this->db->select("subebolum")
		->join('bolum', '%s.bolum_id = %s.bolum_id', 'left')
		->join('sube', '%s.sube_id = %s.sube_id', 'left')
		->run();
		return $result;
	}

	public function doktorBolumGetir($id){
		$result =  $this->db->select("doktormap")
		->join('bolum', '%s.bolum_id = %s.doktor_bolum_id', 'left')
		->join('sube', '%s.sube_id = %s.doktor_sube_id', 'left')
		->where('doktor_id', $id)
		->run();
		return $result;
	}

	public function Doktor_Guncelle_Form($id){
		
		return $this->db->select("doktor")->where("doktor_id",$id)->run(1);	 
		
	}
	
	
	public function Doktor_insert($array){
		
		$sub_category = $this->db->insert("doktor")->set($array);
		
		if($sub_category){
			return $this->db->lastId();
		}else{
			return  false;
		}
		
	}
	
	public function subebolum_insert($array){
		
		$sub_category = $this->db->insert("doktormap")->set($array);
		
		if($sub_category){
			return  true;
		}else{
			return  false;
		}
		
	}


	
	public function Doktor_update($array,$id){
		
		$sub_category = $this->db->update("doktor")->where("doktor_id",$id)->set($array);
		
		if($sub_category){
			return  true;
		}else{
			return  false;
		}
		
	}
	
	
	
	public function doktorMapSil($id){

		
		$query = $this->db->delete("doktormap")->where("doktor_id",$id)->done(); 
		if ( $query ){
			return true;
		}else{
			return false; 
		}
	}

	public function Doktor_delete($id){
		$query = $this->db->delete("doktor")->where("doktor_id",$id)->done(); 
		if ( $query ){
			return true;
		}else{
			return false; 
		}
	}
	
	public function Doktor_kopyala($id){
		
		$data =  $this->db->select("doktor")->where("doktor_id",$id)->run(1);
		unset($data['doktor_id']);
		$sub_category = $this->db->insert("doktor")->set($data);
		
		if($sub_category){
			return  true;
		}else{
			return  false;
		}
	}
	
	
	
	public function seoname($value = '', $id = 0) {



		if ($id == 0) {

			return $this->db->select('page')->from('count(page_id) as total')->where('page_url', $value)->total();



		} else {

			return $this->db->select('page')

			->from('count(page_id) as total')

			->where('page_url', $value)

			->where('page_id', $id, '!=')

			->total();



		}



	}

}
