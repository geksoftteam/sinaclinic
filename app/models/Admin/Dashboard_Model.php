<?php
class Dashboard_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function gapi() {

		return $this->db->select("general_settings")->where("General_Set_id", 1)->run(1);

	}

	public function pagescount() {
		return $this->db->select("page")->from('count(page_id) as total')->total();
	}

	public function catcount() {
		return $this->db->select("nav")->from('count(nav_id) as total')->total();
	}

}