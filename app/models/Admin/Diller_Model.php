<?php //model 
class Diller_Model extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function Diller_listele(){
	 
	return $this->db->select("diller")->run();
	 
        
    }
	
	
	
	    public function Diller_Guncelle_Form($id){
 
	return $this->db->select("diller")->where("diller_id",$id)->run(1);	 
        
    }

      
	
	 	  public function Diller_insert($array){
		  
		$sub_category = $this->db->insert("diller")->set($array);
		  
	    if($sub_category){
	   return  true;
		  }else{
			   return  false;
			  }
		  
		  }
		  
		  
		    public function Diller_update($array,$id){
		  
		$sub_category = $this->db->update("diller")->where("diller_id",$id)->set($array);
		  
	    if($sub_category){
	   return  true;
		  }else{
			   return  false;
			  }
		  
		  }
		  
		  
		  
		  	 public function Diller_delete($id){
		$query = $this->db->delete("diller")->where("diller_id",$id)->done(); 
		 if ( $query ){
        return true;
         }else{
			return false; 
			 }
		 }
	 
	public function Diller_kopyala($id){
 
	$data =  $this->db->select("diller")->where("diller_id",$id)->run(1);
		 unset($data['diller_id']);
        $sub_category = $this->db->insert("diller")->set($data);
		  
	    if($sub_category){
	   return  true;
		  }else{
			   return  false;
			  }
    }
	
	
	
}
