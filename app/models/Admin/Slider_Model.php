<?php //model 
class Slider_Model extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function Slider_listele(){
	 
	return $this->db->select("slider")->run();
	 
        
    }
	
	    public function Sliderx_listele($id){
 
	return $this->db->select("sliderx")->where("sliderx_main",$id)->run();	 
        
    }
	
	    public function Slider_Guncelle_Form($id){
 
	return $this->db->select("slider")->where("slider_id",$id)->run(1);	 
        
    }
	
	
	 	  public function Slider_insert($array){
		  
		$sub_category = $this->db->insert("slider")->set($array);
		  
	    if($sub_category){
	   return  $this->db->lastId();
		  }else{
			   return  false;
			  }
		  
		  }
		  
		  
		    public function Slider_update($array,$id){
		  
		$sub_category = $this->db->update("slider")->where("slider_id",$id)->set($array);
		  
	    if($sub_category){
	   return  true;
		  }else{
			   return  false;
			  }
		  
		  }
		  
		  
		  
		  	 public function Slider_delete($id){
		$query = $this->db->delete("slider")->where("slider_id",$id)->done(); 
		 if ( $query ){
        return true;
         }else{
			return false; 
			 }
		 }
	 
	public function Slider_kopyala($id){
 
	$data =  $this->db->select("slider")->where("slider_id",$id)->run(1);
		 unset($data['slider_id']);
        $sub_category = $this->db->insert("slider")->set($data);
		  
	    if($sub_category){
	   return  true;
		  }else{
			   return  false;
			  }
    }
	
	
	
}
