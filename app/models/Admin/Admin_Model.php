<?php //model 
class Admin_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listele()
    {

        return $this->db->select("admin")->run();


    }


    public function Guncelle_Form($id)
    {

        return $this->db->select("admin")->where("Admin_id", $id)->run(1);

    }


    public function insert($array)
    {

        $sub_category = $this->db->insert("admin")->set($array);

        if ($sub_category) {
            return true;
        } else {
            return false;
        }

    }


    public function update($array, $id)
    {

        $sub_category = $this->db->update("admin")->where("Admin_id", $id)->set($array);

        if ($sub_category) {
            return true;
        } else {
            return false;
        }

    }


    public function delete($id)
    {
        $query = $this->db->delete("admin")->where("Admin_id", $id)->done();
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function getKategorilerForMenu( $searchTerm = null, $pageIndex = 0, $pageSize = 3)
    {

        $this->db->select("nav")
        ->where('nav_durum',1);
        if(!empty($searchTerm)){
            $this->db->where_group_start()
            ->where('nav_name', $searchTerm,"like","")
            ->or_where('nav_title',$searchTerm,"like")
            ->or_where("nav_url",$searchTerm,"like")
            ->where_group_end();
        }
        $count = $this->db->countAll(false);

        $this->db->limit($pageIndex,$pageSize);

        $data = $this->db->run();

        return (object)array(
            'ExtraData' => null,
            'Data' => $data,
            'Errors' => null,
            'Total' => $count
        );




    }

    public function getPagesForMenu( $searchTerm = null, $pageIndex = 0, $pageSize = 3)
    {

        $this->db->select("page")
        ->join('nav', '%s.nav_id = %s.page_kat_id', 'left')
        ->where('page_durum',1);
        if(!empty($searchTerm)){
            $this->db->where_group_start()
            ->where('page_name', $searchTerm,"like","")
            ->or_where('page_jenerik',$searchTerm,"like")
            ->or_where("page_url",$searchTerm,"like")
            ->where_group_end();
        }
        $count = $this->db->countAll(false);

        $this->db->limit($pageIndex,$pageSize);

        $data = $this->db->run();

        return (object)array(
            'ExtraData' => null,
            'Data' => $data,
            'Errors' => null,
            'Total' => $count
        );




    }

    public function getSubelerForMenu( $searchTerm = null, $pageIndex = 0, $pageSize = 3)
    {

        $this->db->select("sube");
        if(!empty($searchTerm)){
            $this->db->where_group_start('')
            ->where('sube_name', $searchTerm,"like","")
            ->or_where('sube_seo_name',$searchTerm,"like")
            ->where_group_end();
        }
        $count = $this->db->countAll(false);

        $this->db->limit($pageIndex,$pageSize);

        $data = $this->db->run();

        return (object)array(
            'ExtraData' => null,
            'Data' => $data,
            'Errors' => null,
            'Total' => $count
        );




    }

    public function getBolumlerForMenu( $searchTerm = null, $pageIndex = 0, $pageSize = 3)
    {

        $this->db->select("bolum");
        if(!empty($searchTerm)){
            $this->db->where_group_start('')
            ->where('bolum_name', $searchTerm,"like","")
            ->or_where('bolum_seo_name',$searchTerm,"like")
            ->where_group_end();
        }
        $count = $this->db->countAll(false);

        $this->db->limit($pageIndex,$pageSize);

        $data = $this->db->run();

        return (object)array(
            'ExtraData' => null,
            'Data' => $data,
            'Errors' => null,
            'Total' => $count
        );




    }

    public function getDoktorForMenu( $searchTerm = null, $pageIndex = 0, $pageSize = 3)
    {

        $this->db->select("doktor");
        if(!empty($searchTerm)){
            $this->db->where_group_start('')
            ->where('doktor_name', $searchTerm,"like","")
            ->or_where('doktor_seo_name',$searchTerm,"like")
            ->where_group_end();
        }
        $count = $this->db->countAll(false);

        $this->db->limit($pageIndex,$pageSize);

        $data = $this->db->run();

        return (object)array(
            'ExtraData' => null,
            'Data' => $data,
            'Errors' => null,
            'Total' => $count
        );




    }


}
