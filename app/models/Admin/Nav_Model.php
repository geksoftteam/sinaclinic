<?php //model
class Nav_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function Nav_listele() {

		$gt = "SELECT `nav_id`, `nav_name`,`nav_main`,`nav_url`,`nav_durum`,`nav_sira`, (SELECT `nav_name` FROM nav WHERE `nav_id` = a.`nav_main`) parent_name FROM nav a order by a.nav_sira asc";

		return $this->db->query($gt)->fetchAll();

	}


	public function menuListele($value='')
	{

		$result = $this->db->select("nav")->run();
		return $result;

	}

	public function Nav_Guncelle_Form($id) {

		return $this->db->select("nav")->where("nav_id", $id)->run(1);

	}

	public function Nav_insert($array) {

		$sub_category = $this->db->insert("nav")->set($array);


		
		if ($sub_category) {
			return true;
		} else {
			return false;
		}

	}

	public function Nav_update($array, $id) {

		$sub_category = $this->db->update("nav")->where("nav_id", $id)->set($array);

		if ($sub_category) {
			return true;
		} else {
			return false;
		}

	}

	public function Nav_delete($id) {
		$query = $this->db->delete("nav")->where("nav_id", $id)->done();
		if ($query) {
			return true;
		} else {
			return false;
		}
	}

	public function Nav_kopyala($id) {

		$data = $this->db->select("nav")->where("nav_id", $id)->run(1);
		unset($data['nav_id']);
		$sub_category = $this->db->insert("nav")->set($data);

		if ($sub_category) {
			return true;
		} else {
			return false;
		}
	}

	//ozel sorgu
	public function Nav_select($select = 0) {

		$mainler = $this->db->select("nav")->where('nav_main', 0)->run();
		$selected = ($select == 0) ? "selected" : "";
		$opt = '<option ' . $selected . ' value="0">Ana Kategori</option>';
		foreach ($mainler as $nav) {

			$selected = ($select == $nav["nav_id"]) ? "selected" : "";

			$opt = $opt . '<option ' . $selected . ' value="' . $nav["nav_id"] . '">¦&nbsp;' . $this->db->is_js($nav["nav_name"]) . '</option>';
			$sub = $this->db->select("nav")->where('nav_main', $nav["nav_id"])->run();
			if ($sub) {

				foreach ($sub as $sb) {
					$selected = ($select == $sb["nav_id"]) ? "selected" : "";

					$opt = $opt . '<option ' . $selected . '  value="' . $sb["nav_id"] . '">¦&nbsp;&nbsp;&nbsp;&nbsp;¦&nbsp;' . $this->db->is_js($sb["nav_name"]) . '</option>';

					$altsub = $this->db->select("nav")->where('nav_main', $sb["nav_id"])->run();
					if ($altsub) {
						foreach ($altsub as $asb) {
							$selected = ($select == $asb["nav_id"]) ? "selected" : "";

							$opt = $opt . '<option ' . $selected . '  value="' . $asb["nav_id"] . '">¦&nbsp;&nbsp;&nbsp;&nbsp;¦&nbsp;&nbsp;&nbsp;¦&nbsp;' . $this->db->is_js($asb["nav_name"]) . '</option>';
						}
					}

				}

			}

		}

		return $opt;

	}
	//ozel sorgu end

	public function seoname($value = '') {
		return $this->db->select('nav')->from('count(nav_id) as total')->where('nav_url', $value)->total();
	}

}
