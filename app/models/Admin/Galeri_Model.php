<?php //model
class Galeri_Model extends Model {
	public function __construct() {
		parent::__construct();
	}

	public function Galeri_listele($page_id = 0) {

		if ($page_id == 0) {
			return $this->db->select("galeri")->run();
		} else {
			return $this->db->select("galeri")->where('galeri_page_id', $page_id)->run();

		}

	}

	public function Galeri_Guncelle_Form($id) {

		return $this->db->select("galeri")->where("galeri_id", $id)->run(1);

	}

	public function page_info($id) {

		return $this->db->select("page")->where("page_id", $id)->run(1);

	}

	public function pages_info() {

		return $this->db->select("page")
		->where("page_tur", 3, '<')
		->run();

	}

	public function Galeri_insert($array) {

		$sub_category = $this->db->insert("galeri")->set($array);

		if ($sub_category) {
			return true;
		} else {
			return false;
		}

	}

	public function Galeri_update($array, $id) {

		$sub_category = $this->db->update("galeri")->where("galeri_id", $id)->set($array);

		if ($sub_category) {
			return true;
		} else {
			return false;
		}

	}

	public function Galeri_delete($id) {
		$query = $this->db->delete("galeri")->where("galeri_id", $id)->done();
		if ($query) {
			return true;
		} else {
			return false;
		}
	}

	public function Galeri_kopyala($id) {

		$data = $this->db->select("galeri")->where("galeri_id", $id)->run(1);
		unset($data['galeri_id']);
		$sub_category = $this->db->insert("galeri")->set($data);

		if ($sub_category) {
			return true;
		} else {
			return false;
		}
	}

}
