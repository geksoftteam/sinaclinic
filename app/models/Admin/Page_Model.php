<?php //model

class Page_Model extends Model {

	public function __construct() {

		parent::__construct();

	}



	public function Page_listele($tur) {



		return $this->db->select("page")

		->join('nav', '%s.nav_id = %s.page_kat_id', 'left')

		->where("page_tur", $tur)
		->orderby('page_sira', 'asc')
		->run();



	}





	public function Page_listelem() {



		$data =  $this->db->select("page")->run();



		$ret =  array();

		foreach ($data as   $row) {



			$ret[] = $row['page_image']; 

			

		}

		return $ret;

	}





	public function Page_Guncelle_Form($id) {



		return $this->db->select("page")->where("page_id", $id)->run(1);



	}





	public function Page_insert($array) {



		$sub_category = $this->db->insert("page")->set($array);



		if ($sub_category) {

			return $this->db->lastId();

		} else {

			return false;

		}



	}




	public function Page_update($array, $id) {



		$sub_category = $this->db->update("page")->where("page_id", $id)->set($array);



		if ($sub_category) {

			return true;

		} else {

			return false;

		}



	}



	public function Page_delete($id) {

		$query = $this->db->delete("page")->where("page_id", $id)->done();

		if ($query) {

			return true;

		} else {

			return false;

		}

	}



	public function Page_kopyala($id) {



		$data = $this->db->select("page")->where("page_id", $id)->run(1);

		unset($data['page_id']);

		$sub_category = $this->db->insert("page")->set($data);



		if ($sub_category) {

			return true;

		} else {

			return false;

		}

	}



	//ozel sorgu

	public function Nav_select($select = 0) {



		$mainler = $this->db->select("nav")->where('nav_main', 0)->run();

		$selected = ($select == 0) ? "selected" : "";

		$opt = '<option ' . $selected . ' value="0">Ana Kategori</option>';

		foreach ($mainler as $nav) {



			$selected = ($select == $nav["nav_id"]) ? "selected" : "";



			$opt = $opt . '<option ' . $selected . ' value="' . $nav["nav_id"] . '">¦&nbsp;' . $this->db->is_js($nav["nav_name"]) . '</option>';

			$sub = $this->db->select("nav")->where('nav_main', $nav["nav_id"])->run();

			if ($sub) {



				foreach ($sub as $sb) {

					$selected = ($select == $sb["nav_id"]) ? "selected" : "";



					$opt = $opt . '<option ' . $selected . '  value="' . $sb["nav_id"] . '">¦&nbsp;&nbsp;&nbsp;&nbsp;¦&nbsp;' . $this->db->is_js($sb["nav_name"]) . '</option>';



					$altsub = $this->db->select("nav")->where('nav_main', $sb["nav_id"])->run();

					if ($altsub) {

						foreach ($altsub as $asb) {

							$selected = ($select == $asb["nav_id"]) ? "selected" : "";



							$opt = $opt . '<option ' . $selected . '  value="' . $asb["nav_id"] . '">¦&nbsp;&nbsp;&nbsp;&nbsp;¦&nbsp;&nbsp;&nbsp;¦&nbsp;' . $this->db->is_js($asb["nav_name"]) . '</option>';

						}

					}



				}



			}



		}



		return $opt;



	}

	//ozel sorgu end



	public function seoname($value = '', $id = 0) {



		if ($id == 0) {

			return $this->db->select('page')->from('count(page_id) as total')->where('page_url', $value)->total();



		} else {

			return $this->db->select('page')

			->from('count(page_id) as total')

			->where('page_url', $value)

			->where('page_id', $id, '!=')

			->total();



		}



	}





	public function urunkat()

	{

		return $this->db->select("nav")->where('nav_main', 16)->run();

	}



	public function resimsor($res)

	{

		$resci = $this->db->select('page')->from('count(page_id) as total')->where('page_image', $res)->total();

		$rescis = $this->db->select('galeri')->from('count(galeri_id) as total')->where('galeri_resim', $res)->total();



		return $resci+$rescis;

	}







	public function Galeri_listele() {



		

		$data =  $this->db->select("galeri")->run();



		$ret =  array();

		foreach ($data as   $row) {



			$ret[] = $row['galeri_resim']; 

			

		}

		return $ret;



		

	}





	public function Galeri_listele_one($id) {



		

		return  $this->db->select("galeri")->where('galeri_page_id',$id)->run();

		

		

	}





}

