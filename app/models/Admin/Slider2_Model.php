<?php //model 
class Slider2_Model extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function Slider2_listele(){
	 
	return $this->db->select("slider2")->run();
	 
        
    }
	
	
	
	    public function Slider2_Guncelle_Form($id){
 
	return $this->db->select("slider2")->where("slider2_id",$id)->run(1);	 
        
    }
	
	
	 	  public function Slider2_insert($array){
		  
		$sub_category = $this->db->insert("slider2")->set($array);
		  
	    if($sub_category){
	   return  true;
		  }else{
			   return  false;
			  }
		  
		  }
		  
		  
		    public function Slider2_update($array,$id){
		  
		$sub_category = $this->db->update("slider2")->where("slider2_id",$id)->set($array);
		  
	    if($sub_category){
	   return  true;
		  }else{
			   return  false;
			  }
		  
		  }
		  
		  
		  
		  	 public function Slider2_delete($id){
		$query = $this->db->delete("slider2")->where("slider2_id",$id)->done(); 
		 if ( $query ){
        return true;
         }else{
			return false; 
			 }
		 }
	 
	public function Slider2_kopyala($id){
 
	$data =  $this->db->select("slider2")->where("slider2_id",$id)->run(1);
		 unset($data['slider2_id']);
        $sub_category = $this->db->insert("slider2")->set($data);
		  
	    if($sub_category){
	   return  true;
		  }else{
			   return  false;
			  }
    }
	
	
	
}
