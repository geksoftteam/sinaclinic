-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost:3306
-- Üretim Zamanı: 05 Tem 2021, 15:21:52
-- Sunucu sürümü: 5.7.33
-- PHP Sürümü: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `hujf1med_sina`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `admin`
--

CREATE TABLE `admin` (
  `Admin_id` int(11) NOT NULL,
  `Admin_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Admin_Mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Admin_User_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Admin_Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Admin_Login_Status` int(11) DEFAULT NULL,
  `Admin_Last_Login` timestamp NULL DEFAULT NULL,
  `Admin_Power` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `admin`
--

INSERT INTO `admin` (`Admin_id`, `Admin_Name`, `Admin_Mail`, `Admin_User_Name`, `Admin_Password`, `Admin_Login_Status`, `Admin_Last_Login`, `Admin_Power`) VALUES
(1, 'Admin', 'info@ysbilisim.com', 'q', '7694f4a66316e53c8cdd9d9954bd611d', 1, '2021-06-30 10:37:23', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `alerts`
--

CREATE TABLE `alerts` (
  `alerts_id` int(11) NOT NULL,
  `alerts_name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `alerts_type` int(11) NOT NULL,
  `alerts_durum` int(11) NOT NULL,
  `alerts_link` varchar(1000) COLLATE utf8_turkish_ci NOT NULL,
  `alerts_not` varchar(1000) COLLATE utf8_turkish_ci DEFAULT NULL,
  `alerts_insert_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `bolum`
--

CREATE TABLE `bolum` (
  `bolum_id` int(11) NOT NULL,
  `bolum_name` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `bolum_seo_name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `bolum_content` text COLLATE utf8_turkish_ci NOT NULL,
  `bolum_insert_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `bolum`
--

INSERT INTO `bolum` (`bolum_id`, `bolum_name`, `bolum_seo_name`, `bolum_content`, `bolum_insert_tarih`) VALUES
(1, '{\"tr\":\"Psikiyatri\"}', 'psikiyatri.html', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2021-06-03 09:18:50'),
(2, '{\"tr\":\"Beslenme ve Diyet\"}', 'beslenme-ve-diyet.html', '', '2021-06-03 09:19:18'),
(3, '{\"tr\":\"Cildiye\"}', 'cildiye.html', '', '2021-06-03 09:19:22');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `diller`
--

CREATE TABLE `diller` (
  `diller_id` int(11) NOT NULL,
  `diller_name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `diller_flag` varchar(321) COLLATE utf8_turkish_ci DEFAULT NULL,
  `diller_durum` tinyint(1) DEFAULT NULL,
  `diller_current` tinyint(1) DEFAULT NULL,
  `diller_sira` int(11) DEFAULT NULL,
  `diller_kod` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `dil_string` text COLLATE utf8_turkish_ci,
  `diller_insert_tarih` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `diller`
--

INSERT INTO `diller` (`diller_id`, `diller_name`, `diller_flag`, `diller_durum`, `diller_current`, `diller_sira`, `diller_kod`, `dil_string`, `diller_insert_tarih`) VALUES
(1, 'Türkçe', 'tr.png', 1, 1, 1, 'tr', '[\"Toplantıya bağlanabilmek için aydınlatma metinlerinin açılıp, okunduğundan emin olunuz.\",\"Daha Fazla\",\"E Bültene Kaydol!\",\"Gönüllü Ol\",\"Dünyadan Raporlar\",\"Dünyadan Etkinlikler \",\"Öne Çıkanlar\",\"Bize Ulaşın\",\"Detay\",\"Kurumsal\",\"Çalışma Alanlarımız\",\"Site Haritası\",\"Tüm Hakları Saklıdır\",\"İLETİŞİM\",\"Bizi Arayın\",\"ADRESİMİZ\",\"E-Posta\",\"Tüm Hakları Saklıdır\",\"E-Bültene Kaydol\",\"Bülten Kayıt Formu\",\"Mail Adresinizi Giriniz\",\"Bültene Kaydol\",\"Gönüllü Ol!\",\"Tam Adınızı Giriniz\",\"Mail Adresinizi Giriniz\",\"Telefon Numaranızı Giriniz\",\"Başlangıç Tarihi \",\"Etkinlik Yeri \",\"Etkinlik Adı \",\"Dünyadan Etkinlikler\",\"Detay İçin Tıklayınız\",\"Turgay Soysal Tenis Akademisi\",\"Alanya merkez de yer alan tesisimizde Uluslar arası tenis federasyonu, avrupa tenis birliği ve Türkiye Tenis Federasyonu standart, ölçü ve kalitesinde 4 adet toprak kort bulunuyor. Merkez kortun yanında yaklaşık 300 kişilik çim bahçe ve seyir alanı, her kortun yanında izleme bahçe si vardır. Modern standartlara uygun erkek ve kadın soyunma odaları, WC’ler, yaklaşık 100 kişilik kapalı resturant ,kafeterya, sıcak ve soğuk servislerle sizleri aramızda görmekten mutluluk duyarız.\"]', '2015-12-30 00:45:26');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `doktor`
--

CREATE TABLE `doktor` (
  `doktor_id` int(11) NOT NULL,
  `doktor_name` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `doktor_seo_name` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `doktor_desc` varchar(1500) COLLATE utf8_turkish_ci NOT NULL,
  `doktor_image` varchar(321) COLLATE utf8_turkish_ci NOT NULL,
  `doktor_content` text COLLATE utf8_turkish_ci NOT NULL,
  `doktor_durum` tinyint(1) NOT NULL,
  `doktor_insert_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `doktor`
--

INSERT INTO `doktor` (`doktor_id`, `doktor_name`, `doktor_seo_name`, `doktor_desc`, `doktor_image`, `doktor_content`, `doktor_durum`, `doktor_insert_tarih`) VALUES
(20, '{\"tr\":\"Uzm. Dr. Osman Çetin \"}', 'uzm-dr-osman-cetin.html', '{\"tr\":\"Ağız ve Diş Sağlığı\\r\\n\"}', 'truzm-dr-osman-cetin_37.jpg', '{\"tr\":\"\"}', 1, '2021-07-03 12:35:00'),
(21, '{\"tr\":\"Op. Dr. Ali Çetin\"}', 'op-dr-ali-cetin.html', '{\"tr\":\"Genel Cerrahi\"}', 'trop-dr-ali-cetin_52.png', '{\"tr\":\"\"}', 1, '2021-07-03 12:41:11');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `doktormap`
--

CREATE TABLE `doktormap` (
  `doktormap_id` int(11) NOT NULL,
  `doktor_id` int(11) NOT NULL,
  `doktor_bolum_id` int(11) NOT NULL,
  `doktor_sube_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `doktormap`
--

INSERT INTO `doktormap` (`doktormap_id`, `doktor_id`, `doktor_bolum_id`, `doktor_sube_id`) VALUES
(30, 20, 1, 3),
(31, 20, 1, 4),
(32, 20, 1, 5),
(33, 20, 2, 3),
(34, 20, 2, 5),
(35, 20, 3, 3),
(36, 21, 1, 3),
(37, 21, 1, 4),
(38, 21, 1, 5),
(39, 21, 2, 3),
(40, 21, 2, 5),
(41, 21, 3, 3);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `galeri`
--

CREATE TABLE `galeri` (
  `galeri_id` int(11) NOT NULL,
  `galeri_name` varchar(1010) COLLATE utf8_turkish_ci DEFAULT NULL,
  `galeri_content` varchar(1800) COLLATE utf8_turkish_ci DEFAULT NULL,
  `galeri_page_id` int(11) DEFAULT NULL,
  `galeri_resim` varchar(321) COLLATE utf8_turkish_ci DEFAULT NULL,
  `galeri_video` varchar(1010) COLLATE utf8_turkish_ci DEFAULT NULL,
  `galeri_insert_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `general_settings`
--

CREATE TABLE `general_settings` (
  `General_Set_id` int(11) NOT NULL,
  `General_Site_Name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `General_desc` varchar(500) COLLATE utf8_turkish_ci DEFAULT NULL,
  `General_title` varchar(260) COLLATE utf8_turkish_ci DEFAULT NULL,
  `googleanalitics` text COLLATE utf8_turkish_ci NOT NULL,
  `analiticsmail` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `analiticspass` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `analiticsprofilid` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `adres` varchar(500) COLLATE utf8_turkish_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `gsm` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `mail` varchar(244) COLLATE utf8_turkish_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `maps` varchar(1000) COLLATE utf8_turkish_ci DEFAULT NULL,
  `social` varchar(1000) COLLATE utf8_turkish_ci DEFAULT NULL,
  `site_logo` varchar(255) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `general_settings`
--

INSERT INTO `general_settings` (`General_Set_id`, `General_Site_Name`, `General_desc`, `General_title`, `googleanalitics`, `analiticsmail`, `analiticspass`, `analiticsprofilid`, `adres`, `tel`, `gsm`, `mail`, `fax`, `maps`, `social`, `site_logo`) VALUES
(1, 'Sina Clinic', 'Türk tenis tarihinde önemli değer taşıyan ve oldukça da geniş olan birkaç tenis ailesi var. Bunlsdf', 'Sina Clinic', 'Test', 'sfsanaliticsler@sfsanaliticsler.iam.gserviceaccount.com', 'sfsanaliticsler-00fcdae151b8.p12', '124078926', 'Karadolap Mh, Neşeli Sk Yağmur Ap. No:24A Eyüp/İstanbul', '0212 627 79 79', '0549 627 79 79', 'info@sinaclinic.com', '0549 627 79 79', 'mampKodu', '{\"facebook\":\"Test\",\"twitter\":\"Test\",\"linkedin\":\"#\",\"instagram\":\"#\",\"youtube\":\"Test\"}', 'logo_38.png');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `nav`
--

CREATE TABLE `nav` (
  `nav_id` int(11) NOT NULL,
  `nav_name` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `nav_title` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `nav_desc` varchar(500) COLLATE utf8_turkish_ci DEFAULT NULL,
  `nav_keyw` varchar(500) COLLATE utf8_turkish_ci DEFAULT NULL,
  `nav_main` int(11) NOT NULL,
  `nav_url` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `nav_durum` tinyint(1) DEFAULT NULL,
  `nav_sira` int(9) DEFAULT NULL,
  `nav_content` text COLLATE utf8_turkish_ci,
  `nav_insert_tarih` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `nav`
--

INSERT INTO `nav` (`nav_id`, `nav_name`, `nav_title`, `nav_desc`, `nav_keyw`, `nav_main`, `nav_url`, `nav_durum`, `nav_sira`, `nav_content`, `nav_insert_tarih`) VALUES
(1, '{\"tr\":\"Ana Sayfa\"}', '{\"tr\":\"Ana Sayfa\"}', '{\"tr\":\"Ana Sayfa\"}', '{\"tr\":\"Ana Sayfa\"}', 0, '', 1, 0, '{\"tr\":\"\"}', '2017-10-23 11:18:27'),
(65, '{\"tr\":\"İletişim\"}', '{\"tr\":\"İletişim\"}', '{\"tr\":\"İletişim\"}', '{\"tr\":\"İletişim\"}', 0, 'Iletisim', 1, 100, '{\"tr\":\"\"}', '2019-12-21 09:36:48'),
(76, '{\"tr\":\"Kurumsal\"}', '{\"tr\":\"Kurumsal\"}', '{\"tr\":\"Kurumsal\"}', '{\"tr\":\"Kurumsal\"}', 0, 'kurumsal', 1, 10, '{\"tr\":\"\"}', '2021-05-27 11:01:26'),
(77, '{\"tr\":\"Bölümlerimiz\"}', '{\"tr\":\"Bölümlerimiz\"}', '{\"tr\":\"Bölümlerimiz\"}', '{\"tr\":\"Bölümlerimiz\"}', 0, 'bolumler', 1, 20, '{\"tr\":\"\"}', '2021-05-28 07:05:42'),
(78, '{\"tr\":\"Şubelerimiz\"}', '{\"tr\":\"Şubelerimiz\"}', '{\"tr\":\"Şubelerimiz\"}', '{\"tr\":\"Şubelerimiz\"}', 0, 'subelerimiz', 1, 25, '{\"tr\":\"\"}', '2021-05-28 10:35:02'),
(81, '{\"tr\":\"Doktorlarımız\"}', '{\"tr\":\"Doktorlarımız\"}', '{\"tr\":\"Doktorlarımız\"}', '{\"tr\":\"Doktorlarımız\"}', 0, 'doktorlarimiz', 1, 30, '{\"tr\":\"\"}', '2021-06-24 11:43:35'),
(82, '{\"tr\":\"Blog\"}', '{\"tr\":\"Blog\"}', '{\"tr\":\"Blog\"}', '{\"tr\":\"Blog\"}', 0, 'blog', 1, 40, '{\"tr\":\"\"}', '2021-06-24 12:20:43');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_name` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `page_url` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `page_kat_id` int(11) NOT NULL,
  `page_desc` varchar(1500) COLLATE utf8_turkish_ci DEFAULT NULL,
  `page_jenerik` varchar(1800) COLLATE utf8_turkish_ci DEFAULT NULL,
  `page_image` varchar(321) COLLATE utf8_turkish_ci DEFAULT NULL,
  `page_content` text COLLATE utf8_turkish_ci,
  `page_video` text COLLATE utf8_turkish_ci,
  `page_durum` tinyint(1) DEFAULT NULL,
  `page_tur` int(7) DEFAULT NULL,
  `page_konum` int(7) DEFAULT NULL,
  `page_konum_home` int(9) DEFAULT NULL,
  `page_konum_side` int(9) DEFAULT NULL,
  `page_konum_footer` int(9) DEFAULT NULL,
  `page_sira` int(9) DEFAULT NULL,
  `page_insert_tarih` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `page`
--

INSERT INTO `page` (`page_id`, `page_name`, `page_url`, `page_kat_id`, `page_desc`, `page_jenerik`, `page_image`, `page_content`, `page_video`, `page_durum`, `page_tur`, `page_konum`, `page_konum_home`, `page_konum_side`, `page_konum_footer`, `page_sira`, `page_insert_tarih`) VALUES
(1, '{\"tr\":\"Hakkımızda\"}', 'hakkimizda.html', 76, '{\"tr\":\"Hakkımızda\"}', '{\"tr\":\"Özel Alibey Hospital hastanesi 1989 yılında Çetinler Polikliniği olarak sağlık hizmetine başlamış olup,2007 yılında Çetinler Cerrahi Tıp Merkezi, 2011 yılında ise Alibey Hospital hastanesine dönüşerek gelişimini sürdürmüştür..\"}', 'trhakkimizda_52.jpg', '{\"tr\":\"<p>&Ouml;zel Alibey Hospital hastanesi 1989 yılında &Ccedil;etinler Polikliniği olarak sağlık hizmetine başlamış olup,2007 yılında &Ccedil;etinler Cerrahi Tıp Merkezi, 2011 yılında ise Alibey Hospital hastanesine d&ouml;n&uuml;şerek gelişimini s&uuml;rd&uuml;rm&uuml;şt&uuml;r..<\\/p>\\r\\n<p>İstanbul Avrupa Yakasında , uluslararası standart ve kalitede hizmet veren Alibey Hospital hastanesi olarak; 18 uzman doktor, 20 sağlık personeli, 3 Kadın Doğum Uzmanı, 3 Diş Hekimi, 2 Genel Cerrahi Uzmanı, 1 Dahiliye ,1 Ortopedi ve travmatoloji,1 cildiye,1 &Ccedil;ocuk,1 N&ouml;roloji,1 &Uuml;roloji,1 G&ouml;z, 1 KBB, Anestezi ve Reanimasyon Uzmanı1 Estetisyen, 1 Diyetisyen , 3 Genel Yoğun Bakım, 5 Yeni Doğan Bakımı olmak &uuml;zere 31 yatak kapasitesi ile geniş bir teknoloji imkanına sahiptir.<\\/p>\\r\\n<h4><strong>VİZYON<\\/strong><\\/h4>\\r\\n<p>&Ouml;zel Alibey Hospital Hastanesi, &uuml;lkemizin en g&uuml;venilir sağlık kuruluşlarından biri olmayı hedeflemektedir.<\\/p>\\r\\n<h4><strong>MİSYON<\\/strong><\\/h4>\\r\\n<p>&Ouml;zel Alibey Hospital Hastanesi t&uuml;m &ccedil;alışanlarıyla,uluslararası standartlarda verdiği hizmetle zirveyi hedefler, hasta mahremiyetini korur, g&uuml;venliğini sağlar,memnuniyetini kazanır,insana saygı duyar ve ahl&acirc;kı esas alır.<\\/p>\"}', '', 1, 0, 0, NULL, NULL, NULL, NULL, '2021-05-27 11:02:53'),
(3, '{\"tr\":\"İnsan Kaynakları\"}', 'insan-kaynaklari.html', 76, '{\"tr\":\"İnsan Kaynakları\"}', '{\"tr\":\"İnsan Kaynakları\"}', '', '{\"tr\":\"<p>İnsan Kaynakları<\\/p>\"}', '', 1, 0, 0, NULL, NULL, NULL, NULL, '2021-05-28 08:19:13'),
(4, '{\"tr\":\"Hasta Hakları\"}', 'hasta-haklari.html', 76, '{\"tr\":\"Hasta Hakları\"}', '{\"tr\":\"Hasta Hakları\"}', '', '{\"tr\":\"<p>Hasta Hakları<\\/p>\"}', '', 1, 0, 0, NULL, NULL, NULL, NULL, '2021-05-28 08:19:20'),
(5, '{\"tr\":\"Refakat Kuralları\"}', 'refakat-kurallari.html', 76, '{\"tr\":\"Refakat Kuralları\"}', '{\"tr\":\"Refakat Kuralları\"}', '', '{\"tr\":\"<p>Refakat Kuralları<\\/p>\"}', '', 1, 0, 0, NULL, NULL, NULL, NULL, '2021-05-28 08:19:27'),
(6, '{\"tr\":\"Covid-19 Aşısı Hakkında Merak Edilenler\"}', 'covid-19-asisi-hakkinda-merak-edilenler.html', 82, '{\"tr\":\"Covid-19 Aşısı Hakkında Merak Edilenler\"}', '{\"tr\":\"\"}', 'trcovid-19-asisi-hakkinda-merak-edilenler_67.jpg', '{\"tr\":\"<p>Grip vir&uuml;s&uuml;, SARS ve MERS; koronavir&uuml;s olarak tanımlanan ailede yer alan viral etkenler arasındadır. 2019 yılında &Ccedil;in&rsquo;de bu vir&uuml;s ailesinin yeni bir &uuml;yesi tanımlandı ve bu koronavir&uuml;s kısa bir s&uuml;rede t&uuml;m gezegeni etkisi altına alarak salgın haline geldi. SARS-CoV-2 olarak isimlendirilen bu vir&uuml;s ve meydana getirdiği covid-19 hastalığı D&uuml;nya Sağlık &Ouml;rg&uuml;t&uuml; tarafından 2020 yılının mart ayında pandemi olarak tanımlandı.<\\/p>\\r\\n<p>Covid-19 hastalığı ile ilgil semptomları olan bireylerin veya bu hastalığın etkeni olan vir&uuml;se maruz kaldığından ş&uuml;phelenen herkesin sağlık kuruluşlarından ve hekimlerden destek alması &ouml;nerilir. Şu an i&ccedil;in bu rahatsızlıktan hayatını kaybeden insan sayısı 2 milyona yaklaşmaktadır. Zamana karşı m&uuml;cadele i&ccedil;erisinde geliştirilen covid-19 aşıları, bir&ccedil;ok &uuml;lkede temel koruyucu y&ouml;ntemler arasında uygulanmaya devam edilmektedir.<\\/p>\\r\\n<h2>Covid-19 Aşıları Merak Edilenler<\\/h2>\\r\\n<p>Şu an i&ccedil;in T&uuml;rkiye&rsquo;de uygulanmakta olan aşılar, &Ccedil;in aşısı olarak bilinen Sinovac firmasının aşısı Coronovac ile Pfizer-BioNTech ortaklığının &uuml;r&uuml;n&uuml; olan covid-19 aşılarıdır. Genel olarak aşılar herhangi bir &uuml;lkede kullanılmadan &ouml;nce &ccedil;eşitli aşamalardan ge&ccedil;erek onay alabilir. Aşı &ccedil;alışmaları olduk&ccedil;a kalabalık hasta grupları ile ger&ccedil;ekleştirilen araştırmalardır. Son aşama olan faz 3 &ccedil;alışmaları onbinlerce katılımcı ile ger&ccedil;ekleştirilir.<\\/p>\\r\\n<p>Her ne kadar kapsamlı &ccedil;alışmalar yapılsa da, herhangi bir tedavi girişiminin ya da aşı &ccedil;alışmalarının uzun vadedeki etkileri hakkında tam olarak emin olabilmek imkansızdır. Bu noktadaki temel yaklaşım aşı olmak ile olmamak arasındaki kar zarar farkına g&ouml;re belirlenir. Aşılar, bireyi ve toplumu &ouml;l&uuml;mc&uuml;l seyredebilen ve tam olarak anlaşılmamış bir hastalığa karşı koruyorsa uygun olan bu aşı &ccedil;alışmasına toplum olarak hep birlikte destek olunmasıdır. Kısa vadede covid-19 aşıları ile oluşabilecek yan etkiler ise şu şekilde &ouml;zetlenebilir:<\\/p>\\r\\n<ul>\\r\\n<li>Enjeksiyon b&ouml;lgesinde ağrı, şişlik<\\/li>\\r\\n<li>Halsizlik<\\/li>\\r\\n<li>Baş ağrısı, kas ağrıları<\\/li>\\r\\n<li>Ateş<\\/li>\\r\\n<\\/ul>\\r\\n<p>V&uuml;cudun daha kapsamlı bir yanıt vermesi nedeniyle aşı sonrası oluşan bu şikayetler ikinci dozdan sonra daha şiddetli olarak ortaya &ccedil;ıkabilir. Daha &ouml;nceki zamanlarda herhangi bir ila&ccedil;, aşı ya da maddeye karşı alerjik reaksiyon geliştiren bireylerin bu durumları hekimleri ile aşı &ouml;ncesinde paylaşması olduk&ccedil;a &ouml;nemlidir.<\\/p>\\r\\n<h2>Covid-19 Aşılarının &Ouml;zellikleri Nelerdir?<\\/h2>\\r\\n<p>Aşılar genel olarak &uuml;retilme şekillerine g&ouml;re 4 ayrı grupta incelenebilir. Tam vir&uuml;s aşıları, rekombinant protein alt &uuml;nite aşıları, &ccedil;oğalması yavaşlatılmış vekt&ouml;r aşıları ve n&uuml;kleik asit bazlı (mRNA) aşıları, covid-19 aşılarının 4 grubunu oluşturur. İnaktive ya da zayıflatılmış aşılar olarak da bilinen tam vir&uuml;s aşıları i&ccedil;erisinde hastalık etkeni ajanı direkt olarak ihtiva eden aşılardır. Ancak bu ajanların deaktive ya da &ouml;l&uuml; olması nedeniyle enjeksiyon sonrası bireyde hastalık tablosu meydana gelmez. Sinovac firmasının covid-19 aşısı bu mantık ile &ccedil;alışan aşılar arasında yer alır. Rekombinant protein alt &uuml;nitesi aşısı, nanopartik&uuml;l teknolojisi ile &uuml;retilen ve vir&uuml;s&uuml;n kilit noktalarını barındırması nedeniyle olduk&ccedil;a g&uuml;&ccedil;l&uuml; bir bağışıklık yanıtını harekete ge&ccedil;iren aşılardır. Koronavir&uuml;slerin adı, korona (kral tacı) kelimesinden k&ouml;ken alır. Bu ismin onlara yakıştığının d&uuml;ş&uuml;n&uuml;lmesinde ise vir&uuml;slerin dış y&uuml;zeyinde sahip olduğu protein &ccedil;ıkıntıların sağladığı g&ouml;r&uuml;n&uuml;m ile ilgilidir. Rekombinant aşılarda vir&uuml;slerin başka y&uuml;zeylere tutunmasını sağlayan bu yapıların proteinleri hedef alınır.<\\/p>\\r\\n<p>&Ccedil;oğalma &ouml;zelliği olmayan vekt&ouml;r aşılarında zararsız ve zayıflatılmış adenovirus vekt&ouml;r (aracı) aşıları kullanılır. Ebola salgınında kullanılan onaylanmış aşılardan biri bu şekilde &uuml;retilmiş olup zaman i&ccedil;erisinde bu teknik covid-19 aşılarında da kullanılabilir olması nedeniyle &ouml;nem arz eder. Bir diğer aşı t&uuml;r&uuml; olan n&uuml;kleik asit aşıları, konak h&uuml;crelere ajan mRNA&rsquo;sı enjekte edilmesine dayanan &ouml;nemli bir aşı t&uuml;r&uuml;d&uuml;r. Bu aşı t&uuml;r&uuml; herhangi bir rahatsızlığa &ouml;zel olarak dizayn edilir ve bağışıklık sistemi h&uuml;crelerine vir&uuml;slerin &ccedil;eşitli protein yapılarının gizli kodlarını sağlayarak v&uuml;cudun savunmasını g&uuml;&ccedil;lendirici etki g&ouml;sterir. BioNTech aşısı, bu teknik kullanılarak koronavir&uuml;s&uuml;n dikensi &ccedil;ıkıntılarına dair protein kodları ile &uuml;retilen covid-19 aşısıdır.<\\/p>\\r\\n<h2>Covid-19 Aşılarının Yan Etkileri Nelerdir?<\\/h2>\\r\\n<p>Covid-19 aşıları ile kısa vadede ortaya &ccedil;ıkabilecek yan etkiler uygulanan aşı tipinden bağımsız olarak olduk&ccedil;a benzer şikayetlerdir. Enjeksiyon b&ouml;lgesinde ağrı, kızarıklık ve şişme, halsizlik, ateş, &uuml;ş&uuml;me titreme, baş ağrısı, v&uuml;cudun &ccedil;eşitli noktalarında ağrı oluşması, bulantı ve lenf bezi b&uuml;y&uuml;mesi gibi durumlar aşıların kısa vadede oluşturabileceği ortak yan etkiler arasında yer alır. Aşu sonrası bu tarz hafif d&uuml;zeyde yan etkilerin oluşması normal kabul edilen bir durumdur. Ancak şikayetler bazen olduk&ccedil;a can sıkıcı hale gelse de v&uuml;cudun bağışıklık yanıt oluşturmaya başladığına işaret etmeleri a&ccedil;ısından aslında olumlu bir gelişme olarak değerlendirilebilir.<\\/p>\\r\\n<p>Ancak oluşabilecek yan etkiler bu tarz hafif d&uuml;zeydeki şikayetler ile sınırlı kalmayabilir. Nadir de olsa bazı kişilerde covid-19 aşısı sonrasında bazen yaşamı tehdit edici anafilaksiye kadar ilerleyen olduk&ccedil;a ciddi seyirli alerjik reaksiyonlar meydana gelebilir. Erken başlangı&ccedil;lı aşıya bağlı alerjik reaksiyon olguları tipik olarak uygulamayı takiben 4 saat i&ccedil;erisinde başlar ve ciltte d&ouml;k&uuml;nt&uuml;, nefes darlığı ve v&uuml;cudun &ccedil;eşitli b&ouml;lgelerinde &ouml;dem ile kendini g&ouml;sterir. Anafilaktik reaksiyonların başlangıcı ise aşı uygulamasını takiben olduk&ccedil;a kısa bir s&uuml;re i&ccedil;erisinde ortaya &ccedil;ıkar. Alerjik reaksiyon belirtileri dışında ishal, karın ağrısı, hipotansiyon ve k&ouml;t&uuml; bir şey olacakmış hissi gibi diğer belirtiler de aşıya bağlı anaflaksi tablosunda yer alabilecek diğer şikayetler arasında yer alır. Bu nedenle genel olarak covid-19 aşısı sonrasında bireylerin en az 15 dakika s&uuml;re ile g&ouml;zlem altında tutulması &ouml;nerilir.<\\/p>\\r\\n<p>Covid-19 aşılarının uzun vadede oluşturabileceği yan etkiler hakkında konuşmak i&ccedil;in şu an olduk&ccedil;a erken bir d&ouml;nemdir. Aşıların hen&uuml;z yeni olması ve insanlar &uuml;zerindeki etkilerini g&ouml;zlemleyebilecek &ccedil;alışmalar i&ccedil;in yeterince zaman ge&ccedil;memiş olması nedeniyle bu konular hakkında herhangi bir şey s&ouml;yleyebilmek i&ccedil;in bilim d&uuml;nyasının biraz daha zamana ihtiya&ccedil; duyduğu aşikardır.<\\/p>\\r\\n<p>Aşılar hakkında merak edilenler arasında yer alan aşı uygulaması sonrası bireyin covid-19 hastalığına yakalanabileceği d&uuml;ş&uuml;ncesi olduk&ccedil;a yanlış bir mantığın sonucudur. Ne BioNTech ne de Sinovac aşıları i&ccedil;erisinde canlı vir&uuml;s i&ccedil;ermemesi nedeniyle uygulama sonrasında kişiyi hasta etmeleri imkansızdır.<\\/p>\\r\\n<h2>Covid-19 Aşıları Etkili mi?<\\/h2>\\r\\n<p>Covid-19 aşılarının etkinliği uygulanan aşı t&uuml;r&uuml;ne bağlı değişiklik g&ouml;sterir. Şu an i&ccedil;in yapılan &ccedil;alışmalar mRNA aşılarının olduk&ccedil;a etkili olduğunu g&ouml;steren sonu&ccedil;lara sahiptir. Genel olarak bu aşıların uygulanması sonrasında tek dozu takiben 14 g&uuml;n i&ccedil;erisinde covid-19&rsquo;a karşı koruyuculuk oranı %80 olup bu d&uuml;zey ikinci doz sonrasında %90&rsquo;a ulaşır. Dolayısıyla aşılamalar sonrasında toplumda hem hastalığın g&ouml;r&uuml;lme sıklığında hem de hastaneye yatış gereksinimi duyacak hasta sayısında bir azalma meydana gelmesi beklenir.<\\/p>\\r\\n<p>Dikkat edilmesi gereken bir diğer grup da daha &ouml;nce hastalığı ge&ccedil;irmiş kişilerdir. Covid-19 hastalığını ge&ccedil;iren ve bağışıklık sisteminin antikor &uuml;retimine başladığı kişiler de aşı uygulaması i&ccedil;erisine dahil edilmelidir. Bu kişilerde bağışıklık sisteminin sadece kısa bir s&uuml;reliğine antikor &uuml;retebilmesi nedeniyle bireylerin aşılanması ile savunma sisteminin bu rahatsızlığa dair hafızasının g&uuml;&ccedil;lendirilmesi sağlanabilir. Aynı zamanda aşılanma ile &uuml;retilen antikorlar ve v&uuml;cudun hastalığı atlatması ile &uuml;rettiği antikorların birbirinden farklı olması nedeniyle bu kişilerde daha g&uuml;&ccedil;l&uuml; bir bağışıklık yanıtına ulaşılabilir.<\\/p>\\r\\n<p>Covid-19 aşıları, t&uuml;m toplumun bir arada &ccedil;alışmasını gerektiren &ouml;nemli bir koruyucu uygulamadır. Aşılama planı i&ccedil;erisine dahil olan herkesin bu sağlık probleminin kontrol altına alınmasında &uuml;st&uuml;ne d&uuml;şeni yapması olduk&ccedil;a &ouml;nemlidir.<\\/p>\\r\\n<p>&nbsp;<\\/p>\\r\\n<p id=\\\"hg-content-end\\\" data-gtm-vis-recent-on-screen-6387505_86=\\\"72095\\\" data-gtm-vis-first-on-screen-6387505_86=\\\"72095\\\" data-gtm-vis-total-visible-time-6387505_86=\\\"100\\\" data-gtm-vis-has-fired-6387505_86=\\\"1\\\">&nbsp;<\\/p>\\r\\n<p>&nbsp;<\\/p>\"}', '', 1, 2, NULL, NULL, NULL, NULL, NULL, '2021-06-24 12:24:49'),
(7, '{\"tr\":\"Uterus Nakli Hakkında Merak Edilenler\"}', 'uterus-nakli-hakkinda-merak-edilenler.html', 82, '{\"tr\":\"Uterus Nakli Hakkında Merak Edilenler\"}', '{\"tr\":\"\"}', 'truterus-nakli-hakkinda-merak-edilenler_58.jpg', '{\"tr\":\"<p>Kadınlarda doğumsal olarak veya sonradan gelişen herhangi bir nedenle uterusun (rahim) bulunmaması nedeniyle s&ouml;z konusu olan kısırlık, uterin fakt&ouml;r infertilitesi olarak adlandırılır. Yumurtalıklarda &uuml;retilen yumurta h&uuml;cresinin sperm ile bir araya gelerek d&ouml;llenmesi sonucunda oluşan embriyo rahim duvarına tutunarak doğuma kadar olan gelişimini rahim i&ccedil;erisinde tamamlar. Bu nedenle rahmi olmayan veya herhangi bir sağlık sorunu nedeniyle operasyonla rahmi alınmış kadınlarda gebeliğin ger&ccedil;ekleşmesi m&uuml;mk&uuml;n değildir. Bu durumda tek &ccedil;&ouml;z&uuml;m yolu uterus nakli olsa da bu durum uzun yıllardır tartışılan, sıklıkla bilimsel &ccedil;alışmalara ve etik anlamda tartışmalara konu olan bir alandır. D&uuml;nya genelinde ger&ccedil;ekleştirilmiş sınırlı sayıda rahim nakli operasyonu bulunmakla birlikte bunların az bir kısmında sağlıklı bir gebelik elde edilebilmiştir. &Uuml;lkemizin de &ouml;nc&uuml;l&uuml;ğ&uuml;n&uuml; yaptığı rahim nakli alanında ilk başarılı nakil 2011 yılında ger&ccedil;ekleştirilmiş, gebelik i&ccedil;in yapılan &ccedil;alışmalar sonucunda 2020 yılında hasta ilk bebeğini kucağına almıştır.<\\/p>\\r\\n<h2>Uterus Nakli Nedir ve İlk Kez Ne Zaman Yapılmıştır?<\\/h2>\\r\\n<p>Rahim (uterus), kadınlarda yumurtalıklardan uzanan fallop t&uuml;pleri ile vajina arasında kalan, gebelik durumunda bebeğin tutunarak doğuma kadar gelişimini s&uuml;rd&uuml;rd&uuml;ğ&uuml; organdır. Uterus nakli, herhangi bir nedenle uterusu bulunmayan ya da işlevini yerine getiremeyen bir kadına canlı vericiden veya kadavradan alınan uterusun nakledilmesi şeklinde ger&ccedil;ekleştirilen bir t&uuml;r organ naklidir. Ge&ccedil;miş yıllarda infertilite (kısırlık) tedavisi alanında atılan hızlı adımlara karşı en b&uuml;y&uuml;k ve &ccedil;&ouml;z&uuml;ms&uuml;z kısırlık nedenlerinden bir tanesini uterin fakt&ouml;r infertilitesi oluşturmaktaydı. Bu sorunu yaşayan kadınlar i&ccedil;in tek &ccedil;&ouml;z&uuml;m yolu taşıyıcı annelik iken &ouml;zellikle de sosyal ve k&uuml;lt&uuml;rel yapı nedeniyle bu tedavi &uuml;lkemiz de dahil olmak &uuml;zere bir&ccedil;ok &uuml;lkede benimsenmemiştir. İlerleyen tıbbi teknolojiler ve organ nakli alanında atılan &ouml;nemli adımlar ile birlikte uterus nakli fikri ortaya &ccedil;ıkmış ve 2000&rsquo;li yıllara doğru bu alanda somut &ccedil;alışmalar başlatılmıştır. İlk uterus nakli 2000 yılında Suudi Arabistan&rsquo;da, doğum sonrasında oluşan kanama nedeniyle uterusu alınan gen&ccedil; bir kadına canlı bir verici olan alıcıya menopoz &ouml;ncesi d&ouml;nemdeki 46 yaşındaki bir kadın don&ouml;rden alınan uterusun nakli ile ger&ccedil;ekleştirilmiştir. Organ nakli operasyonu başarılı ge&ccedil;miş olması nedeniyle operasyondan birka&ccedil; ay sonrasında oluşan pıhtı nedeniyle nakledilen uterus &ccedil;ıkarılmak durumunda kalmış, b&ouml;ylelikle ilk nakil başarısızlıkla sonu&ccedil;lanmıştır. Bundan sonraki ilk uterus nakli ise 2011 yılında &uuml;lkemizde ger&ccedil;ekleştirilmiştir. 22 yaşındaki kadavradan alınan rahim MRKH (Mayer Rokitansky Kuster Hauser) Sendromu nedeniyle doğuştan uterusu bulunmayan 21 yaşındaki bir kadına nakledilmiştir. Nakil başarılı bir şekilde sonu&ccedil;lanmış, birka&ccedil; kez gebelik elde edilmiş fakat gebelik kesesinin b&uuml;y&uuml;memesi nedeniyle bu gebelikler zorunlu olarak sonlandırılmıştır. Yıllar boyunca s&uuml;ren araştırmalar ve takip neticesinde 2020 yılında yeniden gebe kalan hasta sağlıklı bir şekilde doğum yapmıştır. Bu s&uuml;re&ccedil;te 2013 yılında İsvi&ccedil;re&rsquo;de 9 kadına &ccedil;oğunluğu alıcı ile akrabalık ilişkisine sahip olan canlı don&ouml;rlerden uterus nakli ger&ccedil;ekleştirilmiş, bunlardan 2&rsquo;sinde pıhtı atma dolayısıyla histeroskopi yapılarak nakledilen uterus yeniden &ccedil;ıkarılmak durumunda kalınmıştır. Diğer hastalardan 5 tanesinde sağlıklı bir şekilde ilerleyen gebelik elde edilmiş ve sonucunda doğum ger&ccedil;ekleşmiştir.<\\/p>\\r\\n<h2>Uterus Nakli Nasıl Yapılır?<\\/h2>\\r\\n<p>Uterus nakli; tıpkı b&ouml;brek, karaciğer gibi organ nakillerinde olduğu gibi iki farklı yolla ger&ccedil;ekleştirilebilir. Bunlardan ilki canlı don&ouml;rden alınan uterusun alıcıya nakledilmesi şeklinde iken ikincisi ise organ bağış&ccedil;ısı olan kadavradan alınan uterusun alıcıya nakli şeklinde ger&ccedil;ekleştirilir. Bu iki y&ouml;ntemin yanı sıra gelecekte yapılması muhtemel olan uterus nakli uygulamalarından bir tanesi de k&ouml;k h&uuml;creler kullanılarak laboratuvar ortamında oluşturulan uterusun hastaya naklidir. Biyom&uuml;hendislik alanında yoğun şekilde araştırılan ve &ccedil;ok sayıda &ccedil;alışmaya konu olan organ geliştirme metotlarının yardımıyla ilerleyen s&uuml;re&ccedil;te bir&ccedil;ok organ nakli ile birlikte rahim naklinin de ger&ccedil;ekleştirilebileceği &ouml;ng&ouml;r&uuml;lmektedir. &Uuml;lkemizde ger&ccedil;ekleştirilen uterus nakli, kadavradan yapılmıştır. İsvi&ccedil;re\'de yapılan organ nakillerinde ise canlı don&ouml;rden alınan uterus kullanılmıştır. Nakil operasyonu sırasında temel olarak uterus, kadavradan veya canlı vericiden farklı tekniklerle alındıktan sonra alıcı kişiye damarların birleştirilmesi (anastomoz) ve komşu organların damar yapılarının korunmasına ilişkin bir takım cerrahi girişimler neticesinde transfer edilir. Operasyonun başarılı şekilde tamamlanması ve organa yeterli kan akımının sağlanmasının ardından v&uuml;cudun organı reddetmemesi i&ccedil;in diğer t&uuml;m organ nakillerinde de olduğu gibi yoğun bir imm&uuml;nsupresif (bağışıklık sistemi baskılayıcı) tedavi s&uuml;reci başlatılır. Nakledilen organ alıcı kişinin v&uuml;cudunda kaldığı s&uuml;rece imm&uuml;nsupresif tedavi genellikle devam etmek zorundadır. Başarıyla tamamlanmış olan nakillerden bir s&uuml;re sonra, herhangi farklı bir hormonal ya da jinekolojik sorunu bulunmayan kişilerde adet d&ouml;ng&uuml;s&uuml; başlar. Bununla birlikte embriyo transferi yoluyla gebelik elde etmeye y&ouml;nelik &ccedil;alışmalar başlatılır. D&uuml;nya genelinde ger&ccedil;ekleştirilmiş uterus nakli olgularının sayısı hen&uuml;z olduk&ccedil;a azdır. Başarılı ge&ccedil;en nakiller sonrasında gebeliğin oluşması veya doğal bir şekilde devam etmesine ilişkin sorunlar g&ouml;r&uuml;lme olasılığı y&uuml;ksektir. Bu alanda &ccedil;alışmalar halen yoğun bir şekilde s&uuml;rd&uuml;r&uuml;lmekle birlikte uterus nakli sonrasında gebeliği etkileyen fakt&ouml;rler hen&uuml;z tam olarak bilinmemektedir.<\\/p>\\r\\n<h2>Uterus Nakli Riskleri Nelerdir?<\\/h2>\\r\\n<p>Uterus nakli riskleri, gelecekte bu operasyona başvurmayı planlayan kişilerin en &ccedil;ok merak ettiği konulardan bir tanesidir. T&uuml;m organ nakillerinde olduğu gibi rahim naklinde de birtakım risklerden s&ouml;z edilebilir. Bu riskler alıcı a&ccedil;ısından oluşabilecek riskler ve canlı vericiden nakil yapılması halinde don&ouml;r kişi i&ccedil;in s&ouml;z konusu olan riskler olarak iki farklı alanda incelenebilir. Alıcı a&ccedil;ısından g&ouml;r&uuml;len riskler arasında,<\\/p>\\r\\n<ul>\\r\\n<li>Nakledilen organın v&uuml;cut tarafından reddedilmesi ve yeniden operasyonla alınmak durumunda kalınması,<\\/li>\\r\\n<li>Cerrahi operasyona bağlı kanama ve enfeksiyon gibi komplikasyonlar,<\\/li>\\r\\n<li>İmm&uuml;nsupresif tedaviye bağlı sorunlar,<\\/li>\\r\\n<li>Uterus nakline rağmen gebelik elde edilememe olasılığı,<\\/li>\\r\\n<li>Nakil sonrası gebe kalan kadınlarda imm&uuml;nsupresif tedavi nedeniyle bebekte oluşabilecek komplikasyonlar gibi bazı muhtemel risklerden s&ouml;z edilebilir.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Alıcı a&ccedil;ısından s&ouml;z konusu olabilecek bu risklere karşılık uterus nakli, canlı vericiler a&ccedil;ısından da belirli riskler taşır. Diğer organ nakillerine oranla uterus nakli, don&ouml;r kişide karın i&ccedil;erisinde daha fazla cerrahi m&uuml;dahale gerektirir. Bu bakımdan ameliyatın riskleri don&ouml;r a&ccedil;ısından, alıcıya oranla daha fazladır. Dolayısıyla canlı vericiden ger&ccedil;ekleştirilecek uterus nakillerinde cerrahın başarısı ve tecr&uuml;besi b&uuml;y&uuml;k bir &ouml;neme sahiptir. Kadavradan ger&ccedil;ekleştirilen nakiller don&ouml;r a&ccedil;ısından bu gibi riskler oluşturmaması nedeniyle daha &ouml;ncelikli bir tercih olarak g&ouml;r&uuml;nse de bu operasyonlarda da başarı oranı canlı don&ouml;rlere oranlara daha d&uuml;ş&uuml;kt&uuml;r. Uterus naklinin alıcı a&ccedil;ısından faydaları arasında ise psikolojik refahın ve yaşam kalitesinin artması sayılabilir. Hen&uuml;z yeni bir tedavi uygulaması olan ve d&uuml;nya genelinde &ouml;rnekleri olduk&ccedil;a sınırlı olan uterus naklinin yaygınlaşabilmesi i&ccedil;in tıbbi alanda &ccedil;eşitli d&uuml;zenlemelere ihtiya&ccedil; vardır. &Ouml;zellikle de uterus bir organ olarak yalnızca doğurganlık i&ccedil;in gerekli olduğundan yokluğunda herhangi bir hayati tehdit oluşmayacağından rahim nakli etik anlamda d&uuml;nya genelinde pek &ccedil;ok tartışmaya yol a&ccedil;mıştır. Organ nakli yasaları da kadavradan kalp, pankreas, karaciğer, akciğer, b&ouml;brekler ve ince bağırsaklarını kapsadığından kadavra nakilleri i&ccedil;in yeni yasal d&uuml;zenlemelere ihtiya&ccedil; vardır.<\\/p>\\r\\n<p>Eğer siz de &ccedil;ocuk sahibi olmak istiyor fakat doğal yollarla gebelik elde edemiyorsanız sağlık kuruluşlarına başvurarak jinekolojik ve &uuml;rolojik muayenelerinizi yaptırabilirsiniz. Hekiminizin &ouml;nerileri doğrultusunda infertilite tedavisi y&ouml;ntemleri hakkında bilgi alabilir, uygun şartların oluşması halinde tedavi s&uuml;recinize başlayabilirsiniz.<\\/p>\"}', '', 1, 2, NULL, NULL, NULL, NULL, NULL, '2021-06-24 12:24:49'),
(8, '{\"tr\":\"Mutasyon Nedir?\"}', 'mutasyon-nedir.html', 82, '{\"tr\":\"Mutasyon Nedir?\"}', '{\"tr\":\"\"}', 'trmutasyon-nedir_7.jpg', '{\"tr\":\"<p>Vir&uuml;sler, genetik yapılarını oluşturan RNA&rsquo;larını canlı h&uuml;cre i&ccedil;erisine aktararak burada kopyalar ve bu şekilde &ccedil;oğalır. RNA&rsquo;nın kopyalanması esnasında meydana gelen herhangi bir sorun, RNA &uuml;zerindeki genetik dizilimi değiştirdiğinde bu durum mutasyon olarak adlandırılır. Mutasyon ge&ccedil;iren vir&uuml;sler yeni vir&uuml;s varyantlarının oluşumuna neden olurken, oluşan bu varyantlar &ouml;nceki genetik yapılarına g&ouml;re daha farklı etkiler g&ouml;sterebilir. İnsanlar da dahil olmak &uuml;zere t&uuml;m canlı t&uuml;rlerinde mutasyonlar s&ouml;z konusu olabilse de tek h&uuml;creli canlılarda ve vir&uuml;slerde, mutasyon ge&ccedil;iren bireylerden sonra gelecek nesillerin genetik yapısının tamamen farklılaşması nedeniyle mutasyonun etkileri &ccedil;ok daha belirgindir. Koronavir&uuml;s pandemisi ile m&uuml;cadele s&uuml;recinde farklı &uuml;lkelerde Covid-19 vir&uuml;s&uuml;n&uuml;n mutasyon ge&ccedil;irmiş yeni varyantları tespit edilmiş, bu genetik değişimlerin vir&uuml;s&uuml;n bulaşma hızı, semptomları ve insan v&uuml;cudunda etkilediği b&ouml;lgeler gibi pek &ccedil;ok farklı y&ouml;nden vir&uuml;ste değişikliğe yol a&ccedil;tığı saptanmıştır. Peki, koronavir&uuml;ste mutasyon ne gibi olumsuzluklara yol a&ccedil;ar? İşte Covid-19 mutasyonu konusunda bilinmesi gerekenler&hellip;<\\/p>\\r\\n<h2>Koronavir&uuml;ste Mutasyon Nedir?<\\/h2>\\r\\n<p>2019 yılında &Ccedil;in&rsquo;de ortaya &ccedil;ıkarak hızla yayılan Sars-Cov-2 vir&uuml;s&uuml;, s&uuml;re&ccedil; i&ccedil;erisinde &ccedil;eşitli b&ouml;lgelerde mutasyon ge&ccedil;irmiş yeni varyantları ortaya &ccedil;ıkarmıştır. Bir&ccedil;ok hastalık etkeninde olduğu gibi koronavir&uuml;ste de ger&ccedil;ekleşen mutasyonlar sonucunda dolaşımdaki vir&uuml;s pop&uuml;lasyonunda genetik varyasyonlar meydana gelerek hastalığın semptomları, seyri ve bulaşıcılığı y&ouml;n&uuml;nden &ccedil;eşitli değişiklikler g&ouml;r&uuml;lmeye başlanmıştır. &Ouml;zellikle pandeminin etkili bir şekilde y&ouml;netilebilmesi i&ccedil;in olduk&ccedil;a &ouml;nemli olan mutasyonlar, zamanında tespit edilerek tanımlanmadığı takdirde mevcut tarama testlerinde tespit edilemez. Bir diğer deyişle yanlış negatif sonu&ccedil;lar alınabilir. 2019 yılının sonundan bu yana binlerce kez mutasyon ge&ccedil;irmiş olabileceği d&uuml;ş&uuml;n&uuml;len koronavir&uuml;sler i&ccedil;in bilim insanları tarafından bu varyantların b&uuml;y&uuml;k bir kısmının herhangi bir etkiye yol a&ccedil;mayacağı &ouml;ng&ouml;r&uuml;lm&uuml;ş ve bunların yolcu olarak nitelendirilebileceği &ouml;ne s&uuml;r&uuml;lm&uuml;şt&uuml;r. Bunun yanı sıra bazı durumlarda vir&uuml;s&uuml;n hayatta kalması ve &uuml;remesi i&ccedil;in olumlu etkiler yaratan mutasyonlar ger&ccedil;ekleştiğinde, doğal se&ccedil;ilim yolu ile yeni varyasyonların sıklığı artabilir. Bu durum, koronavir&uuml;s pandemisi i&ccedil;in yanlış test sonu&ccedil;ları alınması konusunda etki g&ouml;sterebilecek en &ouml;nemli etkendir. Dolayısıyla etkili tedavi ve aşılama uygulamaları ile doğru teşhis i&ccedil;in vir&uuml;se ilişkin genetik varyasyonlar s&uuml;rekli olarak araştırılmalı ve kayıt altına alınmalıdır. B&ouml;lgesel olarak tespit edilen yeni varyantlara hızlı şekilde m&uuml;dahale edilmesi, pandemi s&uuml;recinin kontrol&uuml; a&ccedil;ısından b&uuml;y&uuml;k bir etkiye sahiptir. Yeni varyantın d&uuml;nya &ccedil;apında yayılmasını &ouml;nleme ve aşıların etkinliğini koruma a&ccedil;ısından yeni varyantların tespit edildiği b&ouml;lge veya &uuml;lkelerde daha katı karantina uygulamalarına başvurulması gerekir.<\\/p>\\r\\n<h2>Covid-19 Vir&uuml;s&uuml;n&uuml;n Mutasyon Ge&ccedil;irmesi Nelere Sebep Olur?<\\/h2>\\r\\n<p>Vir&uuml;ste oluşan bir mutasyon, genetik yapıyı doğrudan doğruya değiştirmez. &Ccedil;oğu mutasyon t&uuml;r&uuml; belirli bir b&ouml;lgeyi etkiler ve bu b&ouml;lgeye bağlı olarak vir&uuml;s&uuml;n etki bi&ccedil;iminde farklılıklar yaratır. Yine mutasyonların &ccedil;ok b&uuml;y&uuml;k bir kısmı baskınlığını artıracak şekilde başarılı olamaz. Fakat vir&uuml;s&uuml;n varlığını s&uuml;rd&uuml;rmesi ve &uuml;remesi i&ccedil;in avantaj sağlayabilecek etkiler yaratan mutasyonlar, baskın varyantların oluşumuna neden olabilir. Vir&uuml;slerin mutasyona uğraması, ilk olarak hastalığın teşhisini zorlaştırması nedeniyle &ouml;zellikle de pandemi koşulları g&ouml;zetildiğinde &ouml;nemli bir risk oluşturur. Yukarıda da belirtildiği gibi vir&uuml;s&uuml;n &uuml;remesi ve hayatta kalması a&ccedil;ısından olumlu etki yaratan mutasyonlar doğal se&ccedil;ilimin etkisiyle &ccedil;oğalarak yeni yaygın varyantları oluşturabilir. Bu durum, yeni varyantın tanımlanarak viral testlerin kapsamı i&ccedil;erisine alınmasına dek yanlış negatif sonu&ccedil;lara yol a&ccedil;ar. Test sonu&ccedil;larının hatalı olarak negatif sonu&ccedil;lanması halinde hem tedavide aksama hem de tespit edilmemiş vakalardan kaynaklı bulaşın artması gibi olumsuzluklar ortaya &ccedil;ıkar. Bu durum normal viral hastalıklar i&ccedil;in geniş &ccedil;aplı bir etki yaratmasa da pandemi durumunda hızlı bir hastalık yayılımına yol a&ccedil;abilir. Bununla birlikte vir&uuml;ste g&ouml;r&uuml;len mutasyon, koronavir&uuml;s&uuml;n insanda oluşturduğu enfeksiyonun seyrini değiştirebilir. Bazı varyantların yol a&ccedil;tığı enfeksiyonlar daha hafif seyirli olurken bazı varyantlar daha şiddetli bir enfeksiyona yol a&ccedil;arak şiddetli komplikasyonları beraberinde getirebilir. Farklı varyantlar arasında hastalık semptomları ve bulaşma hızına ilişkin farklılıklar da rapor edilmektedir.<\\/p>\\r\\n<h2>Koronavir&uuml;s Mutasyonları Nerelerde G&ouml;r&uuml;lm&uuml;şt&uuml;r?<\\/h2>\\r\\n<p>Koronavir&uuml;ste mutasyon ge&ccedil;irmiş yeni varyantların ilki İngiltere&rsquo;de tespit edilmiştir. Pandemi ile m&uuml;cadele s&uuml;recinde t&uuml;m d&uuml;nyada korkuya yol a&ccedil;an vir&uuml;s&uuml;n mutasyona uğradığına ilişkin a&ccedil;ıklamaların ardından yeni varyantlara ilişkin araştırma ve &ccedil;alışmalar hızlandırılmıştır. D&uuml;nya Sağlık &Ouml;rg&uuml;t&uuml; (WHO) tarafından paylaşılan verilere g&ouml;re d&uuml;nya genelinde 68 farklı &uuml;lkede yapılan on binden fazla koronavir&uuml;s&uuml;n genomunu inceleyen bir araştırmada beş binin &uuml;zerinde farklı SARS-Cov-2 varyantı saptanmıştır. Bunlar arasında D614G adı verilen varyantın ise en baskın genetik varyasyon t&uuml;r&uuml;n&uuml; oluşturduğu tespit edilerek aşı &ccedil;alışmaları i&ccedil;in en &ouml;nemli tehdidi bu varyantın oluşturduğu paylaşılmıştır. 2020 yılında İngiltere&rsquo;de tespit edilen ve İngiltere varyantı olarak bilinen genoma sahip vir&uuml;ste 17 farklı mutasyon g&ouml;zlenmiştir. S&ouml;z konusu varyantın bulaşma hızının daha y&uuml;ksek olması, daha şiddetli klinik bulgularla birlikte seyreden bir enfeksiyon tablosuna yol a&ccedil;ması gibi etkileri tespit edilmiş olsa da hen&uuml;z bu gibi verilerin doğrulanabilmesi i&ccedil;in &ccedil;ok daha fazla vaka &ccedil;alışmasına ihtiya&ccedil; vardır. Mevcut baskın varyantlar arasında şunlar yer alır:<\\/p>\\r\\n<ul>\\r\\n<li>&Ccedil;in&rsquo;in Wuhan şehrinde ilk olarak tespit edilen varyant<\\/li>\\r\\n<li>Avrupa kıtasında ilk dalgaya yol a&ccedil;an varyant<\\/li>\\r\\n<li>İngiltere&rsquo;de yaygın olan Alfa varyantı<\\/li>\\r\\n<li>G&uuml;ney Afrika&rsquo;da saptanan Beta varyantı<\\/li>\\r\\n<li>Brezilya&rsquo;da tespit edilen Gamma varyantı<\\/li>\\r\\n<li>Hindistan&rsquo;da tespit edilen Delta varyantı<\\/li>\\r\\n<\\/ul>\\r\\n<p>Tespit edilen bu gibi varyantlara uygun şekilde Covid-19 test kitlerinde de g&uuml;ncellemeler yapılmaktadır. Bu sayede mutasyon ge&ccedil;irmiş vir&uuml;slerle enfekte olan hastalarda koronavir&uuml;s test sonucunda yanlış negatif sonu&ccedil;ların &ouml;n&uuml;ne ge&ccedil;ilebilir. Genel olarak bulaşma hızını g&ouml;steren &ccedil;alışmalara bakıldığında Alfa varyantından Delta varyantına doğru ilerledik&ccedil;e bulaşma hızının arttığı g&ouml;r&uuml;lmektedir. Fakat kesin yargıların ortaya konulabilmesi i&ccedil;in &ccedil;ok daha fazla &ccedil;alışmaya ihtiya&ccedil; vardır. &Ouml;te yandan vir&uuml;s&uuml;n bulaşma hızının artması, vir&uuml;s&uuml;n &ouml;ld&uuml;r&uuml;c&uuml;l&uuml;ğ&uuml;n&uuml;n veya etkisinin artması anlamına gelmez. Dolayısıyla yeni oluşan varyantlar i&ccedil;in etki ve bulaşıcılık y&ouml;n&uuml;nden incelenmesi, pandemi s&uuml;recinin y&ouml;netimi a&ccedil;ısından da olduk&ccedil;a fayda sağlayacaktır.<\\/p>\\r\\n<h2>Mutasyonların Aşı &Uuml;zerindeki Etkisi Nedir?<\\/h2>\\r\\n<p>Mutasyondan kaynaklı g&ouml;r&uuml;lebilecek bir diğer &ouml;nemli sorun ise mevcut aşı uygulamalarının etkisinin azalmasıdır. Mevcut varyantlara y&ouml;nelik olarak geliştirilen aşılar bir s&uuml;re sonra ortaya &ccedil;ıkan yeni varyantlar i&ccedil;in yeterince etkili olmayabilir. Bu nedenle koronavir&uuml;s aşılarının tıpkı grip aşıları gibi belirli periyotlar ile yenilenerek g&uuml;ncel varyantlara uyumlu hale getirilmesi gerekir. Mevcut aşı se&ccedil;eneklerinden &ouml;nde gelenler, vir&uuml;slerdeki diken proteinlerini hedef alarak etki g&ouml;sterir. Şu ana dek tespit edilmiş olan varyantlarda diken proteinlerinde &ccedil;eşitli mutasyonlar saptanmış olsa da bu durum aşının etkisinin tamamen ortadan kalktığı anlamına gelmez. Bunun nedeni aşıların diken proteinler &uuml;zerindeki bir&ccedil;ok farklı b&ouml;lgeye karşı antikor &uuml;retimine yol a&ccedil;masıdır. Tek bir b&ouml;lgeye ilişkin mutasyon aşının başarı oranlarında belirli miktarda değişiklik yapabilse de, b&ouml;yle bir mutasyonun aşıyı etkisiz hale getirmesi şimdilik bilim insanları tarafından m&uuml;mk&uuml;n g&ouml;r&uuml;lmemektedir. Fakat zaman i&ccedil;erisinde oluşan yeni mutasyonlarla birlikte mevcut aşıların etkisiz hale gelme olasılığı s&ouml;z konusudur. Dolayısıyla genetik varyasyonlar &ccedil;ok sıkı şekilde takip edilmeli ve aşılama prosed&uuml;rleri buna g&ouml;re şekillendirilmelidir.<\\/p>\\r\\n<p>Eğer siz de koronavir&uuml;s belirtileri yaşıyorsanız veya mevcut Covid-19 aşıları hakkında bilgi almak istiyorsanız sağlık kuruluşlarına başvurarak hekiminizden &ouml;neri alabilirsiniz. Maske, mesafe ve hijyen kurallarına azami şekilde dikkat etmenin yanı sıra sıranız geldiğinde aşınızı yaptırarak koronavir&uuml;sten korunabilirsiniz.<\\/p>\"}', '', 1, 2, NULL, NULL, NULL, NULL, NULL, '2021-07-03 10:42:49'),
(9, '{\"tr\":\"Kalp Nakli Nedir, Nasıl Yapılır?\"}', 'kalp-nakli-nedir-nasil-yapilir.html', 82, '{\"tr\":\"Kalp Nakli Nedir, Nasıl Yapılır?\"}', '{\"tr\":\"\"}', 'trkalp-nakli-nedir-nasil-yapilir_16.jpg', '{\"tr\":\"<p>Kalp nakli, en ciddi kalp hastalığı vakalarında &ccedil;&ouml;z&uuml;me ulaşmak i&ccedil;in yapılan cerrahi bir uygulamadır. Genellikle kalp yetmezliğinin son aşamalarında, ila&ccedil;ların, yaşam tarzı değişikliklerinin ve diğer tedavi y&ouml;ntemlerinin yetersiz kaldığı durumlarda, uygun bir tedavi se&ccedil;eneği olarak bilinir. Prosed&uuml;re uygun aday olarak kabul edilmeniz i&ccedil;in belirli kriterleri karşılamalısınız.<\\/p>\\r\\n<h2>Kimler Kalp Nakli Adayı Olabilir?<\\/h2>\\r\\n<p>Kalp naklinin sizin i&ccedil;in uygun olup olmadığını belirlemek i&ccedil;in bazı temel sorulara verdiğiniz cevaplar dikkate alınır.<a href=\\\"https:\\/\\/www.medicalpark.com.tr\\/bypass-nedir\\/hg-1545\\\">&nbsp;<\\/a>Kalp hastalığınızın tedavisi i&ccedil;in diğer t&uuml;m tedavilerin denenip denenmediği, nakil ger&ccedil;ekleşmediği takdirde &ouml;l&uuml;m ihtimalinin olup olmadığı ve genel sağlık durumunuzun iyi ya da k&ouml;t&uuml; olması gibi soruların cevabı &ouml;nem taşır. Nakilden sonra karmaşık ila&ccedil; tedavileri ve sık muayeneler de dahil olmak &uuml;zere yaşam tarzınızda bazı değişiklikler ger&ccedil;ekleşir. Bu değişikliklere bağlı kalabilmeniz de tedavinin başarısını etkiler.<\\/p>\\r\\n<p>Ayrıca, diğer ciddi hastalıklara, aktif enfeksiyonlara sahipseniz veya morbid obezite ile m&uuml;cadele ediyorsanız b&uuml;y&uuml;k olasılıkla nakil i&ccedil;in aday olarak kabul edilmezsiniz.<\\/p>\\r\\n<h2>Kalp Nakli Nasıl Yapılır?<\\/h2>\\r\\n<p>Kalp nakli olmak i&ccedil;in, kapsamlı bir tarama s&uuml;recinden ge&ccedil;erek nakil listesine alınırsınız. Uzman nakil ekibi, tıbbi ge&ccedil;mişinizi, tanı koymak &uuml;zere yapılan testlerin sonu&ccedil;larını ve sosyal ve psikolojik durumunuzu kontrol eder. Bunun yanında &ouml;m&uuml;r boyu &ouml;zbakım i&ccedil;in uygun olup olmadığınıza karar verir.<\\/p>\\r\\n<p>Onay aldıktan sonra bir bağış&ccedil;ıdan, size uygun bir kalbi bekleme s&uuml;reciniz başlar. Bu d&ouml;nem sizin ve sevdikleriniz i&ccedil;in uzun ve sancılı olabilir. Ancak sağlık ekibiniz, don&ouml;r kalp bulunana kadar, kalp yetmezliğiniz i&ccedil;in size destek olur ve durumunuzu yakından takip eder. Size uygun bir kalp bulunduğunda hemen sizinle iletişim kurulur ve işlemler başlar.<\\/p>\\r\\n<h3>ORGAN BAĞIŞ&Ccedil;ILARI NASIL BULUNUR?<\\/h3>\\r\\n<p>Kalp nakli i&ccedil;in bağış&ccedil;ılar, genellikle yakın zamanda hayatını kaybetmiş veya beyin &ouml;l&uuml;m&uuml; ger&ccedil;ekleşmiş kişiler olur. B&ouml;yle durumlarda, don&ouml;rlerin beyin &ouml;l&uuml;m&uuml; ger&ccedil;ekleşmiş olup v&uuml;cutları makineler tarafından canlı tutulmaktadır. Don&ouml;r olan kişiler genelde bir araba kazası, ciddi bir kafa travması veya silah yaralanması sonucunda vefat etmiş ve &ouml;lmeden &ouml;nce organ bağışı i&ccedil;in g&ouml;n&uuml;ll&uuml; olmuş kişilerdir. Ne yazık ki, nakil bekleyen kişiler i&ccedil;in yeterli kalp bulunmamaktadır. Bir kişi nakil i&ccedil;in aylarca bekleyebilir ve bekleyenlerin bir kısmı yeni bir kalp alacak kadar uzun yaşayamayabilir.<\\/p>\\r\\n<p>Organ nakli beklerken karışık duygular i&ccedil;inde olabilirsiniz. &Ccedil;&uuml;nk&uuml; organ naklinin ger&ccedil;ekleşmesinden &ouml;nce birinin &ouml;lmesi gerektiğinin farkındasınızdır. Bağış yapan bir&ccedil;ok aile, sevdiklerinin &ouml;l&uuml;m&uuml;n&uuml;n ardından, hasta birine iyilik yaptıklarını bilerek bir huzur duygusu hisseder ve bunu bilmek sizin durumunuzu da kolaylaştırabilir.<\\/p>\\r\\n<h3>KALP NAKLİ NASIL YAPILIR?<\\/h3>\\r\\n<p>Nakil i&ccedil;in uygun bir kalp bulununca, bu kalp &ouml;zel bir &ccedil;&ouml;zeltiye alınır ve soğutularak alıcıya getirilir. Nakil operasyonu başlamadan &ouml;nce, gerekli bir dizi kontrol yapılarak alınan bu kalbin iyi durumda olduğundan emin olunur. Şartlar uygun hale geldiğinde, en kısa zamanda ameliyat ger&ccedil;ekleştirilir. Kalbi alacak kişi, ameliyat esnasında kalp-akciğer makinesine bağlı kalır ve bu makine sayesinde, v&uuml;cut, kandan temel ihtiya&ccedil;ları olan oksijeni ve besini alır. Daha sonra, kalbin &uuml;st odacıkları olan kulak&ccedil;ıkların arka duvarları dışında hastanın kalbi &ccedil;ıkarılır. Yeni kalbin kulak&ccedil;ıklarının arkaları a&ccedil;ılır ve kalp yerine yerleştirilir.<\\/p>\\r\\n<p>Sonrasında kan damarları birbirine bağlanır ve kanın kalp ve akciğerlerden akmasına izin verilir. Kalp ısındık&ccedil;a atmaya başlar. Hasta kalp-akciğer makinesinden &ccedil;ıkarılmadan &ouml;nce t&uuml;m kan damarları ve kalp odacıkları sızıntılara karşı kontrol edilir. Kalp nakli ameliyatı, 4 saatten 10 saate kadar s&uuml;rebilir. &Ccedil;oğu hasta ameliyattan birka&ccedil; g&uuml;n sonra ayağa kalkar ve v&uuml;cudun organı hemen reddettiğine dair herhangi bir belirti g&ouml;stermezse, yaklaşık olarak 1-4 haftalık bir s&uuml;re zarfı i&ccedil;erisinde evine d&ouml;nebilir.<\\/p>\\r\\n<h2>Kalp Nakli Riskleri Nelerdir?<\\/h2>\\r\\n<p>Kalp nakli sonrası ger&ccedil;ekleşen &ouml;l&uuml;mlerin nedenleri arasında enfeksiyon ve reddedilme, en sık g&ouml;r&uuml;lenlerdir. Yeni kalbin reddini engellemek i&ccedil;in ila&ccedil; kullanan hastalarda b&ouml;breklerde hasar,<a href=\\\"https:\\/\\/www.medicalpark.com.tr\\/tansiyonu-ne-dusurur\\/hg-2346\\\">&nbsp;<\\/a>y&uuml;ksek tansiyon, osteoporoz (kemiklerin ciddi şekilde incelmesi ve kırılması) ve lenfoma (bağışıklık h&uuml;crelerini etkileyen bir kanser t&uuml;r&uuml;) g&ouml;r&uuml;lebilir. Bu risklere ek olarak, kalp arterlerinin aterosklerozu veya koroner arter hastalığı, nakil yapılan hastaların neredeyse yarısında gelişir.<\\/p>\\r\\n<h3>ORGAN REDDİ NEDİR?<\\/h3>\\r\\n<p>Normalde bağışıklık sistemi, v&uuml;cudunuzu enfeksiyonlardan korumakla g&ouml;revlidir. Organ reddi, v&uuml;cudun bağışıklık h&uuml;crelerinin, nakledilen organı, v&uuml;cudun geri kalanından farklı olarak tanıması ve onu yok etmeye &ccedil;alışması olarak tanımlanır. Kendi haline bırakılırsa, bağışıklık sistemi bu yeni kalbin h&uuml;crelerine zarar verir ve sonunda onu yok eder.<\\/p>\\r\\n<p>Reddedilmeyi &ouml;nlemek i&ccedil;in hastalara imm&uuml;nosupresanlar adı verilen ila&ccedil;lar verilir. Bu ila&ccedil;lar bağışıklık sistemini baskılayarak yeni kalbin zarar g&ouml;rmesini engeller. Reddedilmeyi engellemek amacıyla kalp nakli alıcıları, imm&uuml;nos&uuml;presan ila&ccedil;ların talimatlarına kesinlikle uymalıdır. Araştırmacılar s&uuml;rekli olarak daha g&uuml;venli, daha etkili ve iyi tolere edilen imm&uuml;nos&uuml;presif ila&ccedil;lar &uuml;zerinde &ccedil;alışmaktadır.<\\/p>\\r\\n<p>Kalp nakli alıcıları, reddetme belirtileri a&ccedil;ısından dikkatle takip edilir. Doktorlar mikroskop altında incelemek &uuml;zere, sıklıkla nakledilen kalpten k&uuml;&ccedil;&uuml;k &ouml;rnekler alırlar. Biyopsi adı verilen bu prosed&uuml;rle, kateter adı verilen ince bir t&uuml;p bir damar yoluyla kalbe ilerletilir. Kateterin ucunda bir doku par&ccedil;asını kesmeye yarayan bir bioptome bulunur. Biyopsi sonucu hasarlı h&uuml;creler g&ouml;steriyorsa, imm&uuml;nsupresif ilacın dozu ve t&uuml;r&uuml; değiştirilir. Kalp kasından biyopsiler, genellikle ameliyattan sonraki d&ouml;nemlerde doktorunuzun belirlediği şekilde tekrarlanır.<\\/p>\\r\\n<p>Olası bir reddedilme ve enfeksiyon belirtilerinin farkına varmanız hayati &ouml;nem taşır. Farkına varır varmaz, bunları doktorlarınıza bildirebilir ve hemen tedaviye alınabilirsiniz.<\\/p>\\r\\n<p>Nakledilen organın reddedildiğine dair belirtiler şu şekilde sıralanır:<\\/p>\\r\\n<ul>\\r\\n<li>Ateş (38&deg;C &uuml;zeri)<\\/li>\\r\\n<li>Titreme, baş ağrısı veya d&ouml;nmesi, mide bulantısı ve kusma<\\/li>\\r\\n<li>Nefeste darlık<\\/li>\\r\\n<li>G&ouml;ğ&uuml;ste ağrı veya hassasiyet<span style=\\\"text-decoration: underline;\\\">&nbsp;oluşması<\\/span><\\/li>\\r\\n<li>Kendini k&ouml;t&uuml; hissetme ve bitkinlik<\\/li>\\r\\n<li>Y&uuml;kselen kan basıncı<\\/li>\\r\\n<\\/ul>\\r\\n<p>Bağışıklığın fazla bastırılması sebebiyle, bağışıklık sistemi yavaşlar ve bu hasta kolayca ciddi enfeksiyonlar yaşayabilir. Bu nedenle enfeksiyonlarla savaşmak i&ccedil;in, enfeksiyon ila&ccedil;ları da verilebilir.<\\/p>\\r\\n<p>Enfeksiyon işaretleri ise şu şekildedir:<\\/p>\\r\\n<ul>\\r\\n<li>Ateş (38&deg;C &uuml;zerinde)<\\/li>\\r\\n<li>Terleme ve titreme<\\/li>\\r\\n<li>Deride d&ouml;k&uuml;nt&uuml;ler olması<\\/li>\\r\\n<li>Ağrı, kızarma şişme ve hassaslık<\\/li>\\r\\n<li>Yara veya kesiklerin iyileşmesinde g&uuml;&ccedil;l&uuml;k<\\/li>\\r\\n<li>Boğaz ağrısı ve tahrişi veya yutarken acı hissi<\\/li>\\r\\n<li>Burun tıkanıklığı, sin&uuml;s drenajı ve baş ağrısı<\\/li>\\r\\n<li>İki g&uuml;nden fazla devam eden &ouml;ks&uuml;r&uuml;k<\\/li>\\r\\n<li>Ağızda veya dilde beyaz lekeler oluşması<\\/li>\\r\\n<li>Bulantı, kusma veya ishal gibi bazı sindirim problemleri<\\/li>\\r\\n<li>Bitkinlik hissi<\\/li>\\r\\n<li>İdrara &ccedil;ıkma esnasında ağrı veya yanma ve sık sık idrara &ccedil;ıkma<\\/li>\\r\\n<li>K&ouml;t&uuml; kokan, bulanık veya kanla karışık idrar<\\/li>\\r\\n<\\/ul>\\r\\n<p>Organ reddi veya enfeksiyon belirtileri g&ouml;sterirseniz hemen doktorunuza bildiriniz.<\\/p>\\r\\n<h2>Kalp nakli sonrası yaşam<\\/h2>\\r\\n<p>Kalp nakli sonrasındaki hayatınız, yaşınıza, genel sağlık durumunuza ve nakile verdiğiniz yanıta g&ouml;re değişiklik g&ouml;sterir. Nakil sonrasında doktorunuz, genel bedensel ve zihinsel sağlığınız i&ccedil;in fiziksel aktiviteyi d&uuml;zenli bir şekilde yapmayı &ouml;nerebilir. D&uuml;zenli fiziksel aktivite yapmanız, kan basıncını kontrolde tutar, stresi azaltır, ideal kiloyu korumaya yardımcı olur ve kemikleri g&uuml;&ccedil;lendirir. Nakli ger&ccedil;ekleştiren ekip, bireysel ihtiya&ccedil;larınız ve hedefleriniz doğrultusunda size &ouml;zel tasarlanmış bir egzersiz programı oluşturur. Bu esnada nakil ekibinizle sizin i&ccedil;in en uygun olan aktiviteleri belirleyebilir ve iyileşme s&uuml;recini başarılı ge&ccedil;irebilirsiniz. V&uuml;cudunuzda merkezi rol oynayan kalbinizin sağlığını ihmal etmeyin ve kalp nakli hakkında daha detaylı bilgi almak i&ccedil;in doktorunuza başvurun.<\\/p>\"}', '', 1, 2, NULL, NULL, NULL, NULL, NULL, '2021-07-03 10:45:08');
INSERT INTO `page` (`page_id`, `page_name`, `page_url`, `page_kat_id`, `page_desc`, `page_jenerik`, `page_image`, `page_content`, `page_video`, `page_durum`, `page_tur`, `page_konum`, `page_konum_home`, `page_konum_side`, `page_konum_footer`, `page_sira`, `page_insert_tarih`) VALUES
(10, '{\"tr\":\"Pankreas Nakli Hakkında Merak Edilenler\"}', 'pankreas-nakli-hakkinda-merak-edilenler.html', 82, '{\"tr\":\"Pankreas Nakli Hakkında Merak Edilenler\"}', '{\"tr\":\"\"}', 'trpankreas-nakli-hakkinda-merak-edilenler_57.jpg', '{\"tr\":\"<p>Pankreas nakli; sağlıklı bir kişinin pankreasının cerrahi işlemle alınarak, pankreası işlevi bozuk olan bir kişiye nakledilmesidir. Pankreas nakli hakkında merak edilenler yazının devamında a&ccedil;ıklanmıştır.<\\/p>\\r\\n<h2>Pankreas Nedir?<\\/h2>\\r\\n<p>Pankreas, midenin alt kısmının arkasında yer alan bir organdır. Pankreasın v&uuml;cuttaki temel g&ouml;revi kan şekerini d&uuml;zenlemektir. Pankreastan salgılanan ins&uuml;lin hormonu, kandaki şekerin h&uuml;creye alınmasını ve b&ouml;ylece kan şekeri d&uuml;zeyinin d&uuml;şmesini sağlar. A&ccedil;lık durumunda ise pankreastan salgılanan glukagon hormonu, şekerin v&uuml;cuttaki depolardan kana karışmasını sağlar ve b&ouml;ylelikle kan şekerini y&uuml;kseltir.<\\/p>\\r\\n<p>Tip 1 diyabet, pankreasın yeterince ins&uuml;lin &uuml;retemediği bir hastalıktır. Pankreas nakli de genellikle bu hasta grubuna yapılır. Ancak pankreas naklinin &ouml;nemli yan etkileri olabileceğinden nakil işlemi diyabete bağlı ciddi komplikasyonları olan hastalara uygulanır. Komplikasyon oluşmayan hastalar, ins&uuml;lin tedavisi ile yaşamına devam eder.<\\/p>\\r\\n<p>Bazı durumlarda Tip 2 diyabeti tedavi etmek i&ccedil;in de pankreas nakline başvurulabilir. Yine pankreas kanseri, safra yolu kanseri gibi kanser t&uuml;rlerinin tedavisinde de nakil işlemi tercih edilebilir.<\\/p>\\r\\n<p>Pankreas nakli, &ouml;zellikle diyabete bağlı ciddi b&ouml;brek hasarı olan hastalarda b&ouml;brek nakli ile birlikte yapılabilir.<\\/p>\\r\\n<h2>Pankreas Nakli Neden Yapılır?<\\/h2>\\r\\n<p>Pankreas nakli, diyabet hastalarında normal seviyede ins&uuml;lin &uuml;retilmesi sağlamak ve kan şekerinin kontrol&uuml;n&uuml; iyileştirmek i&ccedil;in yapılır. Ancak pankreas nakli diyabet hastalarında standart tedavi değildir.<\\/p>\\r\\n<p>Pankreas nakli<\\/p>\\r\\n<ul>\\r\\n<li>Standart tedavi ile kontrol altına alınamayan Tip 1 diyabeti olan<\\/li>\\r\\n<li>Sık sık ins&uuml;line bağlı hipoglisemi (kan şekeri d&uuml;ş&uuml;kl&uuml;ğ&uuml;) atağı yaşayan<\\/li>\\r\\n<li>Kan şekeri kontrol&uuml; zayıf olan<\\/li>\\r\\n<li>B&ouml;breklerinde ciddi hasar bulunan<\\/li>\\r\\n<li>Tip 2 diyabeti olup İns&uuml;lin direnci d&uuml;ş&uuml;k ve ins&uuml;lin &uuml;retimi az olan hastalarda pankreas nakli bir tedavi se&ccedil;eneği olarak g&ouml;z &ouml;n&uuml;nde bulundurulabilir.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Tip 2 diyabette hastalardaki asıl sorun, v&uuml;cut h&uuml;crelerinin ins&uuml;line verdiği yanıtta azalma olmasıdır. Genellikle ins&uuml;lin &uuml;retim miktarında bir azalma s&ouml;z konusu değildir. Dolayısıyla bu hastalara sağlıklı bir pankreas nakledilse de hastalık ortadan kalkmaz. Tip 2 diyabet hastalığı tedavisinde pankreas naklinin tedavi olarak d&uuml;ş&uuml;n&uuml;lebilmesi i&ccedil;in hastada hem ins&uuml;lin direncinin hem de ins&uuml;lin &uuml;retiminin d&uuml;ş&uuml;k olması gerekir. Pankreas nakillerinin yaklaşık %10&rsquo;u bu şartları sağlayan Tip 2 diyabet hastalarına yapılır.<\\/p>\\r\\n<p>Pankreas naklinin birka&ccedil; farklı t&uuml;r&uuml; bulunur. Bu t&uuml;rler:<\\/p>\\r\\n<ul>\\r\\n<li><strong>Sadece Pankreas Nakli:&nbsp;<\\/strong>Diyabeti olan ancak b&ouml;brek hastalığı olmayan veya b&ouml;brek hastalığının erken evresinde olan hastalara sadece pankreas nakli yapmak yeterli olabilir.<\\/li>\\r\\n<li><strong>Kombine B&ouml;brek-Pankreas Nakli:&nbsp;<\\/strong>Diyabeti olan ve b&ouml;brek yetmezliği riski taşıyan hastalara aynı anda hem pankreas hem de b&ouml;brek nakli uygulanabilir. Bu işlemin uygulandığı hastalarda gelecekte diyabete bağlı ciddi b&ouml;brek hastalıkların g&ouml;r&uuml;lme riski de azaltılmış olur.<\\/li>\\r\\n<li><strong>&Ouml;nce B&ouml;brek Sonra Pankreas Nakli:&nbsp;<\\/strong>B&ouml;brek ve pankreas vericileri i&ccedil;in uzun s&uuml;re beklemesi gereken hastalarda eğer uygun verici (don&ouml;r) bulunursa &ouml;ncelikle b&ouml;brek nakli yapılabilir. Daha sonra uygun pankreas da bulunduğunda hastaya bu nakil de yapılır.<\\/li>\\r\\n<li><strong>Pankreas Adacık H&uuml;cre Nakli:&nbsp;<\\/strong>Pankreasta ins&uuml;lin h&uuml;cre &uuml;retiminden sorumlu h&uuml;crelere adacık h&uuml;cre adı verilir. Bu nakil t&uuml;r&uuml;nde sağlıklı pankreastan alınan adacık h&uuml;creleri, hastanın karaciğerdeki toplar damarına enjekte edilir. Enjeksiyon işleminin birden fazla kez tekrarlanması gerekebilir.<\\/li>\\r\\n<\\/ul>\\r\\n<h2>Pankreas Nakli Nasıl Yapılır?<\\/h2>\\r\\n<p>Pankreas nakli i&ccedil;in sıra bekleyen hastalar tetikte olmalıdır. &Ccedil;&uuml;nk&uuml; uygun verici bulunduktan sonra 18-24 saat i&ccedil;erisinde naklin ger&ccedil;ekleşmesi gerekir. Dolayısıyla nakil bekleyen hastaların, doktorları kendilerine ulaştığında kısa s&uuml;rede hastaneye gidebilecek durumunda olmaları &ouml;nemlidir.<\\/p>\\r\\n<p>Nakil ameliyatı &ouml;ncesi hastaya genel anestezi uygulanır. Dolayısıyla hasta ameliyat sırasında hi&ccedil;bir şey hissetmez ve etrafının farkında olmaz. Hasta ameliyat sırasında ent&uuml;be haldedir yani solunumu makineye bağlı olarak devam eder.<\\/p>\\r\\n<p>Hasta anestezi etkisi altına girdikten sonra;<\\/p>\\r\\n<ul>\\r\\n<li>Karnın ortasından aşağıya doğru bir kesi yapılır.<\\/li>\\r\\n<li>Vericiden alınan pankreas ve k&uuml;&ccedil;&uuml;k bir bağırsak kısmı, hastanın karnın alt b&ouml;lgesine yerleştirilir.<\\/li>\\r\\n<li>Vericiden alınan k&uuml;&ccedil;&uuml;k bağırsak par&ccedil;ası hastanın mesanesine veya bağırsağına dikilir.<\\/li>\\r\\n<li>Vericiden alınan pankreas ise hastanın bacaklarına giden kan damarlarına bağlanarak beslenmesi sağlanır.<\\/li>\\r\\n<li>Hastanın kendi pankreası, sindirime yardım etmesi amacıyla yerinde bırakılır.<\\/li>\\r\\n<li>Pankreas nakli ile birlikte b&ouml;brek nakli de ger&ccedil;ekleştiriliyorsa, b&ouml;brekler de alt karın b&ouml;lgesinde damarlara bağlanarak beslenmesi sağlanır.<\\/li>\\r\\n<li>Nakledilen b&ouml;breğe ait &uuml;reter (b&ouml;brekle mesaneyi birleştiren ince t&uuml;p şeklinde yapı) mesaneye bağlanır.<\\/li>\\r\\n<li>Hastanın kendi b&ouml;breklerinde enfeksiyon benzeri bir sorun yoksa eski b&ouml;brekler yerinde bırakılır.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Hastanın solunumu, tansiyonu, nabzı; ameliyat boyunca takip edilir ve bu yaşamsal g&ouml;stergelerde bir sorun meydana gelirse hemen m&uuml;dahale edilir.<\\/p>\\r\\n<p>Pankreas nakli 3 ila 6 saat arasında s&uuml;rebilir. Hastaya pankreasa ek olarak b&ouml;brek nakli de olacaksa bu s&uuml;re uzayabilir.<\\/p>\\r\\n<p>Ameliyattan sonra hastanın yapması gerekenler:<\\/p>\\r\\n<ul>\\r\\n<li><strong>Birka&ccedil; g&uuml;n boyunca yoğun bakım &uuml;nitesinde kalmak:&nbsp;<\\/strong>İşlem sonrası hasta yoğun bakımda sağlık &ccedil;alışanları tarafından sıkı g&ouml;zetim altında tutulur. Bu s&uuml;re boyunca hastada ameliyata bağlı komplikasyon gelişip gelişmediği, hastaya nakledilen organların &ccedil;alışmaya başlayıp başlamadığı gibi konular takip edilir.<\\/li>\\r\\n<li><strong>Bir hafta kadar hastanede kalmak:&nbsp;<\\/strong>Hasta stabil hale gelip yoğun bakımdan &ccedil;ıktıktan sonra servise yatırılır. Burada hem hasta takip edilir hem de hastanın ameliyat yaralarının iyileşmesi beklenir. İyileşme s&uuml;recinde hastanın ayağa kalkıp y&uuml;r&uuml;mesi, kendi başına tuvalete gidebilmesi gibi becerileri kazanması da beklenen durumlardandır.<\\/li>\\r\\n<li><strong>Sık sık kontrole gitmek:&nbsp;<\\/strong>Hasta taburcu edildikten sonraki 3-4 hafta boyunca yakından izlenmeye devam eder. Bu s&uuml;re zarfında hastayla ilgilenen doktor hastayı sık sık kontrole &ccedil;ağırabilir. Bu nedenle hastanın bir s&uuml;re boyunca naklin yapıldığı hastaneye yakın yerde ikamet etmesi gerekebilir.<\\/li>\\r\\n<li><strong>&Ouml;m&uuml;r boyu ila&ccedil; kullanmak:&nbsp;<\\/strong>Pankreas nakli sonrasında hastanın &ouml;mr&uuml; boyunca ila&ccedil; kullanması gerekir. Kullanılan bu ila&ccedil;lar bağışıklık baskılayıcı ila&ccedil;lardır ve hastanın bağışıklık sisteminin yeni pankreasa saldırarak hasar vermesini &ouml;nler.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Pankreas naklinin başarı bir şekilde ger&ccedil;ekleşmesinin ardından tip 1 diyabet hastasının ins&uuml;lin tedavisine devam etmesine gerek kalmaz. Ancak hasta ve verici arasında y&uuml;ksek uyum yakalansa bile organ reddi adı verilen bir durum ger&ccedil;ekleşebilir. Organ nakli, hastanın bağışıklık sisteminin yeni takılan pankreası yabancı olarak algılayıp saldırması sonucu oluşur. Hastada pankreas reddine bağlı oluşabilecek belirtiler:<\\/p>\\r\\n<ul>\\r\\n<li>Karın ağrısı<\\/li>\\r\\n<li>Ateş<\\/li>\\r\\n<li>Nakil b&ouml;lgesinde aşırı hassasiyet<\\/li>\\r\\n<li>Kan şekerinde y&uuml;kselme<\\/li>\\r\\n<li>Kusma<\\/li>\\r\\n<li>İdrar &uuml;retiminde azalma olarak sıralanabilir.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Organ reddi durumunun ger&ccedil;ekleşmesini &ouml;nlemek i&ccedil;in hastanın imm&uuml;nsupresif yani bağışıklık baskılayıcı ila&ccedil;lar kullanması gerekir.<\\/p>\\r\\n<h2>Pankreas Nakli Riskleri Nelerdir?<\\/h2>\\r\\n<p>Pankreas naklinde ameliyata bağlı bazı riskler vardır. Bu riskler:<\\/p>\\r\\n<ul>\\r\\n<li>Kan pıhtılaşması<\\/li>\\r\\n<li>Kanama<\\/li>\\r\\n<li>Enfeksiyon<\\/li>\\r\\n<li>Kan şekerinde y&uuml;kselme (hiperglisemi) veya diğer metabolik problemleri<\\/li>\\r\\n<li>İdrar yolu enfeksiyonu gibi &ccedil;eşitli idrar yolu sorunları<\\/li>\\r\\n<li>Takılan pankreasın yeterince işlev g&ouml;stermemesi<\\/li>\\r\\n<li>Organ reddi olarak sıralanabilir.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Hastanın kullandığı bağışıklık baskılayıcı ila&ccedil;ların da bazı yan etkileri vardır. Bu yan etkiler:<\\/p>\\r\\n<ul>\\r\\n<li>Kemik erimesi (osteoporoz)<\\/li>\\r\\n<li>Y&uuml;ksek kolesterol<\\/li>\\r\\n<li>Y&uuml;ksek tansiyon<\\/li>\\r\\n<li>Mide bulantısı, kusma, ishal<\\/li>\\r\\n<li>G&uuml;neş ışığına karşı hassasiyet<\\/li>\\r\\n<li>&Ouml;dem<\\/li>\\r\\n<li>Kilo alımı<\\/li>\\r\\n<li>Diş etlerinde şişme<\\/li>\\r\\n<li>Akne<\\/li>\\r\\n<li>Kıllanmada artış<\\/li>\\r\\n<li>Sa&ccedil; d&ouml;k&uuml;lmesi olarak sayılabilir.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Pankreas nakli Tip 1 diyabet hastalığı i&ccedil;in standart bir tedavi olmasa da &ouml;zellikle diyabete bağlı ciddi sağlık sorunlarıyla boğuşan hastalarda &ouml;nemli bir tedavi se&ccedil;eneğidir. Hastaların nakil i&ccedil;in uygun bir aday olup olmadığı &ouml;ğrenmek, olası tedavi se&ccedil;eneklerini tartışmak i&ccedil;in donanımlı bir merkeze başvurması faydalı olur.<\\/p>\"}', '', 1, 2, NULL, NULL, NULL, NULL, NULL, '2021-07-03 10:46:15'),
(11, '{\"tr\":\"Bağırsak Nakli Hakkında Merak Edilenler\"}', 'bagirsak-nakli-hakkinda-merak-edilenler.html', 82, '{\"tr\":\"Bağırsak Nakli Hakkında Merak Edilenler\"}', '{\"tr\":\"\"}', 'trbagirsak-nakli-hakkinda-merak-edilenler_99.jpg', '{\"tr\":\"<p>İnce bağırsak nakli, hastalıklı ya da kısalmış bağırsağın vericiden alınan sağlıklı bağırsakla değiştirilmesi operasyonudur. Olduk&ccedil;a kompleks ve zorlu olan bağırsak nakli bazı hastalar i&ccedil;in tek tedavi alternatifi olabilir. Hastalar başarılı bir bağırsak nakli sayesinde sağlıklı bir yaşam s&uuml;rebilir ancak hastaların yaşamlarının geri kalanında devamlı ila&ccedil; kullanması ve d&uuml;zenli olarak doktor kontrol&uuml;ne gitmesi gerekir. Bağırsak nakli hakkında merak ettiğiniz bilgilere buradan ulaşabilirsiniz.<\\/p>\\r\\n<h2>Bağırsak Nakli Neden Yapılır?<\\/h2>\\r\\n<p>Bağırsak nakli, bağırsaklarında ciddi sorun olan kişilere uygulanır. Bağırsaklar yenilen besinlerin sindirilerek emildiği organlardır. Bu organda meydana gelen bir sorun besinlerin emilememesine neden olur ve yaşamı tehlikeye sokar. Bağırsak ile ilgili ciddi rahatsızlığı olan kişiler, total parenteral beslenme (TPN) adı verilen bir y&ouml;ntemle beslenir. Bu y&ouml;ntemde hastanın ihtiya&ccedil; duyduğu protein, karbonhidrat, yağ, vitamin ve diğer n&uuml;trientler; damar yolundan verilir. B&ouml;ylece hastanın beslenmesi devam ettirilmiş olur. Ancak uzun s&uuml;reli parenteral beslenme hastada komplikasyonlara neden olabilir. Bazı hastalara ise parenteral beslenme verilemeyebilir. İşte bu hasta gruplarına da bağırsak nakli uygulanabilir.<\\/p>\\r\\n<p>Bağırsak yetmezliği olarak adlandırılan durum, besinlerin bağırsak tarafında yeterince emilememesine bağlı olarak oluşur. Bu durum bağırsağın kısalığından kaynaklanabileceği gibi bağırsak fonksiyonlarındaki bozukluklar nedeniyle de oluşabilir.<\\/p>\\r\\n<p>Kısa bağırsak sendromu, ince bağırsağın b&uuml;y&uuml;k bir kısmının olmadığı veya hasarlı olduğu bir rahatsızlıktır. Kısa bağırsak sendromu;<\\/p>\\r\\n<ul>\\r\\n<li>bağırsak d&ouml;nmesi (volvulus)<\\/li>\\r\\n<li>bebeğin bağırsaklarının v&uuml;cut dışında olduğu doğuştan gelen bir kusur (gastroşizis)<\\/li>\\r\\n<li>bağırsak dokusunun bir kısmının &ouml;lmesi (nekrotizan enterokolit)<\\/li>\\r\\n<li>Crohn hastalığı, bağırsak kanseri gibi rahatsızlıklarda tedavi amacıyla bağırsağın bir kısmının cerrahi olarak &ccedil;ıkarılması gibi nedenlerle oluşabilir.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Kısa bağırsak sendromu olan &ccedil;ok sayıda hasta evde parenteral beslenme ile sorunsuz bir şekilde yaşamına devam edebilir. Ancak bazı hastalarda parenteral (damardan) beslenmeye bağlı yaşamı tehdit edecek &ouml;l&ccedil;&uuml;de ciddi komplikasyonlar oluşabilir. Bu komplikasyonlar:<\\/p>\\r\\n<ul>\\r\\n<li>Kateteri takacak uygun damar kalmaması<\\/li>\\r\\n<li>Damar yolunun a&ccedil;ıldığı b&ouml;lge enfeksiyon oluşup kana karışması ve sepsise yol a&ccedil;ması<\\/li>\\r\\n<li>Karaciğer hastalıkları olarak sıralanabilir.<\\/li>\\r\\n<\\/ul>\\r\\n<h2>Bağırsak Nakli Kimlere Yapılır?<\\/h2>\\r\\n<p>Hastanın bağırsak nakline uygun olup olmadığını g&ouml;rmek i&ccedil;in bir dizi test yapılır. Bu testlerin tamamlanması birka&ccedil; haftayı bulabilir ve bu s&uuml;re&ccedil;te hastanın sık sık hastaneye gidip gelmesi gerekebilir. Hastanın nakle uygunluğu i&ccedil;in yapılan tetkikler:<\\/p>\\r\\n<ul>\\r\\n<li>Kan tahlili ile hastanın karaciğer ve b&ouml;brek fonksiyonlarına, HIV ya da hepatit gibi ciddi enfeksiyonları ge&ccedil;irip ge&ccedil;irmediğine bakılır.<\\/li>\\r\\n<li>Ultrason ve BT gibi g&ouml;r&uuml;nt&uuml;leme y&ouml;ntemleri ile hastanın organları ayrıntılı bir bi&ccedil;imde incelenir.<\\/li>\\r\\n<li>Kolonoskopi yani an&uuml;sten i&ccedil;eri doğru ucunda kamera bulunan ince bir t&uuml;p&uuml;n ilerlitildiği işlem ile de hastanın bağırsaklarının i&ccedil; b&ouml;l&uuml;mleri değerlendirilir.<\\/li>\\r\\n<li>Akciğer fonksiyon testleri ile de hastanın b&uuml;y&uuml;k bir ameliyatı kaldırıp kaldırmayacağına bakılır.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Hastada;<\\/p>\\r\\n<ul>\\r\\n<li>V&uuml;cutta birka&ccedil; yere sı&ccedil;ramış kanser varsa,<\\/li>\\r\\n<li>Yaşam beklentisini d&uuml;ş&uuml;recek ciddi bir rahatsızlık bulunuyorsa,<\\/li>\\r\\n<li>Solunum mekanik ventilat&ouml;r yardımıyla devam ediyorsa,<\\/li>\\r\\n<li>Yaş 60&rsquo;tan b&uuml;y&uuml;kse,<\\/li>\\r\\n<li>Ameliyat &ouml;ncesi ve sonrası sağlık ekibinin tavsiyelerine uymama davranışı varsa (&ouml;rneğin sigarayı bırakmak) hasta nakil i&ccedil;in uygun g&ouml;r&uuml;lmeyebilir.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Bağırsak nakline uygun bulunan kişi bekleme listesine alınır. Hasta ile eşleşen bir verici (don&ouml;r) bulunduğunda hastaya haber verilir ve s&uuml;re&ccedil; başlar. Hastaya uygun organ bulunduktan sonra operasyonun kısa bir s&uuml;re i&ccedil;inde tamamlanması gerektiğinden bekleme listesindeki hasta tetikte olmalıdır. Hastanın her an ulaşılabilir olması ve kısa s&uuml;rede hastaneye gelmesi nakil işleminin ger&ccedil;ekleşmesi i&ccedil;in olduk&ccedil;a &ouml;nemlidir.<\\/p>\\r\\n<h2>Bağırsak Nakli Nasıl Yapılır?<\\/h2>\\r\\n<p>Bağırsak nakli farklı şekillerde yapılabilir. Bağırsak naklinin 3 temel t&uuml;r&uuml; şu şekildedir:<\\/p>\\r\\n<ul>\\r\\n<li><strong>Sadece İnce Bağırsak Nakli:&nbsp;<\\/strong>Bağırsak yetmezliği olan ancak karaciğerinde sorun bulunmayan hastalara uygulanır.<\\/li>\\r\\n<li><strong>Kombine Karaciğer ve İnce Bağırsak Nakli:&nbsp;<\\/strong>Bağırsak yetmezliğine ek olarak ileri evre karaciğer hastalığı olan kişilere &ouml;nerilir.<\\/li>\\r\\n<li><strong>&Ccedil;oklu Organ (Multivisceral) Nakli:&nbsp;<\\/strong>&Ccedil;ok sık yapılmamakla birlikte &ccedil;oklu organ yetmezliği olan kişilere uygulanabilir. Bu nakilde mide, pankreas, bağırsak ve karaciğer organları yer alır.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Bağırsak nakli genel anestezi altında yapılır. Yani hastanın bilinci kapalıdır ve herhangi bir ağrı, acı hissetmez. Ayrıca genel anestezide hastanın solunumu makineye bağlı olarak devam eder.<\\/p>\\r\\n<p>Ameliyatta hastanın karnına bir kesi yapılır. Bu kesiden karın i&ccedil;erisindeki hastalıklı bağırsak par&ccedil;ası &ccedil;ıkarılır. &Ccedil;ıkarılan bu b&ouml;lgeye vericiden (don&ouml;rden) alınan bağırsak par&ccedil;ası yerleştirilir. Yeni koyulan bağırsak kan damarlarına bağlanır ve b&ouml;ylece bağırsağın beslenmesi sağlanır. Aynı zamanda bağırsağın iki ucu, &ccedil;ıkarılan par&ccedil;adan ayrılan b&ouml;lgelerle birleştirilir. Cerrah, karından dışarı doğru ileostomi olarak adlandırılan bir delik a&ccedil;ar. Ameliyat sonrası ileostomiden bağırsaktaki atıklar uzaklaşır. Ayrıca ileostomi sağlık ekibinin nakledilen bağırsağın durumunu kontrol etmesini sağlar. Bazı hastalarda ileostomi iyileşme s&uuml;recinden sonra kapatılsa da bazılarında a&ccedil;ık kalması gerekir. A&ccedil;ık kalan hastalar ileostominin dışına bir torba bağlar ve torba dolduk&ccedil;a yenisiyle değiştirir.<\\/p>\\r\\n<p>Bağırsak nakli 8-10 saat s&uuml;ren bir ameliyattır. Bazı hastalarda bu s&uuml;re daha da uzayabilir.<\\/p>\\r\\n<p>Bağırsak naklinden sonra hasta yoğun bakım &uuml;nitesine alınır ve burada yakından takip edilir. Nakledilen bağırsağın durumunu g&ouml;rmek i&ccedil;in d&uuml;zenli olarak ileostomi a&ccedil;ıklığından biyopsi alınabilir veya hastaya endoskopi yapılabilir. Bu sayede nakledilen bağırsağın v&uuml;cuda uyum sağlayıp sağlayamadığı kontrol edilir, organ reddi durumu değerlendirilir.<\\/p>\\r\\n<p>Nakil sonrası hasta iyileştik&ccedil;e damardan beslenmeyi bırakarak ağızdan (oral) beslenmeye başlayabilir.<\\/p>\\r\\n<p>Hastalar genellikle nakil sonrası 4-6 hafta i&ccedil;inde taburcu olur. Taburcu olduktan sonra da hastanın sık sık doktor kontrol&uuml;ne gelmesi gerekir. Bu nedenle hastaneden uzakta yaşayan kişilerin, ameliyat sonrası birka&ccedil; aylığına nakil merkezine yakın bir b&ouml;lgeye yerleşmesi faydalı olur.<\\/p>\\r\\n<p>Hastanın bağışıklık sistemi, yeni nakledilen organı yabancı olarak algılayıp zarar verebilir. Bu durum da nakledilen organın reddine yol a&ccedil;ar. Organ reddinin &ouml;nlenmesi i&ccedil;in hastanın nakil sonrası yaşam boyu bağışıklık baskılayıcı ila&ccedil; kullanması gerekir.<\\/p>\\r\\n<p>Organ reddi, nakledilen bağırsağın d&uuml;zg&uuml;n bir şekilde &ccedil;alışması engeller. Bazı durumlarda organ reddi, bağırsağın i&ccedil;erisindeki bakterilerin kana karışmasına neden olarak v&uuml;cut geneline yayılmış bir enfeksiyon tablosu oluşturur.<\\/p>\\r\\n<h2>Bağırsak Nakli Riskleri Nelerdir?<\\/h2>\\r\\n<p>T&uuml;m ameliyatlarda olduğu gibi bağırsak nakli operasyonunda da risk s&ouml;z konusudur.<\\/p>\\r\\n<p>Bağışıklık sistemi hakkındaki bilgilerin artması ve daha iyi bağışıklık baskılayıcı ila&ccedil;ların geliştirilmesiyle organ naklindeki başarı artmıştır. Buna rağmen yine nakil sonrası bazı komplikasyonlarla karşılaşılabilir. Bunlar:<\\/p>\\r\\n<ul>\\r\\n<li>Kalp ile ilgili sorunlar<\\/li>\\r\\n<li>Solunum problemleri<\\/li>\\r\\n<li>Kan pıhtılaşması (tromboz)<\\/li>\\r\\n<li>Nakil sonrası lenfoproliferatif hastalık<\\/li>\\r\\n<li>Organ reddi olarak sıralanabilir.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Uzun s&uuml;reli bağışıklık baskılayıcı ila&ccedil; kullanımı da enfeksiyona yatkınlıktan diyabete, kanserden y&uuml;ksek tansiyona kadar ciddi problemlere yol a&ccedil;abilir. Kemik erimesi, sa&ccedil; d&ouml;k&uuml;lmesi, y&uuml;zde kıllanmada artış da yine bu ila&ccedil;ların yan etkileri arasındadır.<\\/p>\\r\\n<p>Bağırsak nakli &ccedil;ok sayıda riski beraberinde getirse de bazı hasta grupları i&ccedil;in en doğru tedavi se&ccedil;eneğidir. Diğer y&ouml;ntemlerle başarılı bir şekilde tedavi edilemeyen, beslenmesi ciddi &ouml;l&ccedil;&uuml;de bozulan hastalarda bağırsak nakli makul bir se&ccedil;enek olarak değerlendirilebilir. Hasta doktoruna bağırsak nakli i&ccedil;in uygun bir aday olup olmadığını, uygun organ i&ccedil;in ne kadar beklemek gerektiğini, ameliyata bağlı komplikasyonların neler olabileceğini sormalıdır. Bu sorulara verilen cevaplar ışığında hasta nakil kararını vermelidir. Nakil isteğini belirten hasta hem nakil &ouml;ncesi hem de nakil sonrası doktorun uyarılarını dikkate almalı ve tavsiyelerini uygulamalıdır.<\\/p>\"}', '', 1, 2, NULL, NULL, NULL, NULL, NULL, '2021-07-03 10:47:29'),
(12, '{\"tr\":\"Akciğer Nakli Hakkında Merak Edilenler\"}', 'akciger-nakli-hakkinda-merak-edilenler.html', 82, '{\"tr\":\"Akciğer Nakli Hakkında Merak Edilenler\"}', '{\"tr\":\"\"}', 'trakciger-nakli-hakkinda-merak-edilenler_77.jpg', '{\"tr\":\"<p>Akciğer nakli, sağlıklı bir akciğerin alınarak akciğeri işlev g&ouml;rmeyen bir kişiye nakledilmesidir. Akciğer nakli, diğer tedavi y&ouml;ntemleri ile sağlığına kavuşamayan kişilere yapılır.<\\/p>\\r\\n<p>Akciğer naklinde, hastadaki rahatsızlığa bağlı olarak tek akciğer kullanılabileceği gibi her iki akciğerin de değişmesi gerekebilir. Bazı durumlarda ise vericiden (don&ouml;rden) akciğerler ile birlikte kalp de alınarak hastaya nakledilebilir.<\\/p>\\r\\n<p>Akciğer nakli olduk&ccedil;a zorlu ve b&uuml;y&uuml;k bir operasyon olmakla birlikte bazı hastalar i&ccedil;in tek se&ccedil;enek olabilir. Hastalar, ameliyatın t&uuml;m risklerine ve komplikasyonlarına rağmen nakil işlemini denemek zorunda kalabilir. Nakille birlikte hastanın genel sağlık durumu ve yaşam kalitesinde artış g&ouml;r&uuml;lebilir. Akciğer nakli ile ilgili merak edilenler, yazının devamında a&ccedil;ıklanmıştır.<\\/p>\\r\\n<h2>Akciğer Nakli Neden Yapılır?<\\/h2>\\r\\n<p>Akciğerde meydana gelen hasarlar, v&uuml;cudun yaşamak i&ccedil;in temel ihtiya&ccedil;larından olan oksijenin alınmasında soruna neden olur. Farklı hastalıklar akciğeri etkileyebilir ve akciğer fonksiyonlarını bozabilir. Akciğerde ve dolayısıyla solunumda soruna yol a&ccedil;an rahatsızlıklardan sık g&ouml;r&uuml;lenler şu şekilde sıralanabilir:<\\/p>\\r\\n<ul>\\r\\n<li>Kronik Obstr&uuml;ktif Akciğer Hastalığı (KOAH)<\\/li>\\r\\n<li>Akciğerde skarlaşma (Pulmoner fibrozis)<\\/li>\\r\\n<li>Akciğerlerde kan basıncında artış olması (Pulmoner hipertansiyon)<\\/li>\\r\\n<li>Kistik fibrozis<\\/li>\\r\\n<\\/ul>\\r\\n<p>Akciğerdeki hasarın neden oldupu solunum sıkıntıları, ila&ccedil; tedavisi ve oksijen desteği ile tedavi edilebilir. Ancak bazı durumlarda bu tedavi y&ouml;ntemleri hastanın yeterli solunum yapması i&ccedil;in yeterli olmaz ve hastaya akciğer nakledilmesi gerekir. Akciğer hastalığına ek olarak ciddi kalp rahatsızlığı bulunan kişilere ise akciğer naklinin yanı sıra kalp naklinin yapılması da g&uuml;ndeme gelebilir.<\\/p>\\r\\n<p>Bir hastaya akciğer nakli yapılıp yapılmayacağı pek &ccedil;ok fakt&ouml;re bağlı olarak değişiklik g&ouml;sterir. Her hasta i&ccedil;in nakil uygun bir se&ccedil;enek olmayabilir. &Ouml;rneğin;<\\/p>\\r\\n<ul>\\r\\n<li>Enfeksiyon ge&ccedil;irmekte olan<\\/li>\\r\\n<li>Kanser hastası olan<\\/li>\\r\\n<li>B&ouml;brek, kalp, karaciğer hastalıkları gibi ciddi bir rahatsızlığı bulunan<\\/li>\\r\\n<li>Nakil sonrası sağlıklı bir yaşam i&ccedil;in gerekli olan sigara kullanmama, alkol t&uuml;ketmeme gibi yaşam tarzı değişikliklerini uygulamak istemeyen hastalar; akciğer nakli i&ccedil;in uygun g&ouml;r&uuml;lmeyebilir.<\\/li>\\r\\n<\\/ul>\\r\\n<h2>Akciğer Nakli Nasıl Yapılır?<\\/h2>\\r\\n<p>Akciğer nakli i&ccedil;in uygun bulunan hasta bekleme listesine alınır. Hastayla eşleşen bir verici bulunana dek bekleme s&uuml;reci devam eder. Bu s&uuml;re&ccedil;te hastanın akciğerle ilgili problemleri ila&ccedil; tedavisi ve oksijen terapileri ile iyileştirilmeye &ccedil;alışılır. Hastanın rehabilitasyon programlarına katılarak doğru tekniklerle etkili bir bi&ccedil;imde solunum yapmayı &ouml;ğrenmesi de bekleme s&uuml;recinin daha sağlıklı ilerlemesine katkıda bulunur.<\\/p>\\r\\n<p>Nakil hastası eşleşen bir verici bulmak uzun s&uuml;rebilir. Maalesef bazen hastalar bekleme s&uuml;recinde hayatını kaybedebilir. Toplumda organ bağışı farkındalığının artması, nakil sırasında bekleyen hasta ve hasta yakınlarının gelecek hakkında daha umutlu olmasını sağlar.<\\/p>\\r\\n<p>Nakil sırasında bekleyen hastalar tetikte olmalıdır. Hastaya uyan bir akciğer bulunduğu andan itibaren m&uuml;mk&uuml;n olan en kısa s&uuml;rede nakil işlemleri başlamalıdır. Bu nedenle hastanın her an ulaşılabilir olması ve kısa s&uuml;rede naklin ger&ccedil;ekleşeceği hastaneye gitmesi olması faydalı olur.<\\/p>\\r\\n<p>Akciğer nakli ameliyatı genel anestezi altında yapılır. Dolayısıyla operasyon sırasında hastanın bilinci a&ccedil;ık değildir, herhangi bir ağrı veya acı hissetmez. Ameliyat sırasında hasta ent&uuml;be edilir yani hastanın solunumu ağızdan soluk borusuna iletilen bir t&uuml;p aracılığıyla makineye bağlı olarak devam eder.<\\/p>\\r\\n<p>Ameliyatta cerrah g&ouml;ğ&uuml;se bir kesi yapar. Daha sonra bu kesiden dışarı doğru hasar g&ouml;ren akciğer &ccedil;ıkartılır. &Ccedil;ıkarılan akciğere bağlı bronşlar ve kan damarları vericiden alınan akciğere bağlanır. Bu sayede nakledilen akciğer, &ccedil;ıkarılan akciğerin yerini almış olur.<\\/p>\\r\\n<p>Bazı akciğer nakli ameliyatlarında hasta kalp-akciğer bypass cihazına bağlanabilir. Bu cihaz operasyon boyunca kanın v&uuml;cutta dolaşımını sağlar.<\\/p>\\r\\n<h2>Akciğer Nakli Sonrası Ne Olur?<\\/h2>\\r\\n<p>Ameliyat sonrası hastanın birka&ccedil; g&uuml;n boyunca yoğun bakım &uuml;nitesinde kalması gerekir. Bu s&uuml;re&ccedil;te hastanın solunumu mekanik ventilat&ouml;r aracılığıyla devam eder. Hastanın g&ouml;ğs&uuml;ne bağlı bir t&uuml;p de akciğer ve kalpten gelen sıvıların dışarı atılmasını sağlar. Nakil sonrası hastada ağrı kontrol&uuml;n&uuml; sağlamak ve organ reddini &ouml;nlemek amacıyla hastaya damar yolundan &ccedil;eşitli ila&ccedil;lar verilebilir. Hastanın durumu iyileştik&ccedil;e mekanik ventilat&ouml;r ihtiyacı ortadan kalkar ve hasta kendi kendine solunum yapabilir hale gelir. Bu aşamaya gelindiğinde hasta yoğun bakımdan normal servise nakledilir. Akciğer nakli sonrası hastaların genellikle 1-3 hafta hastanede kalması gerekir. Elbette bu s&uuml;re hastadan hastaya farklılık g&ouml;sterebilir.<\\/p>\\r\\n<p>Nakil işlemi sonrasında yaklaşık 3 ay boyunca hastanın, nakle bağlı komplikasyonlar ve akciğer fonksiyonları bakımından yakından takip edilmesi gerekir. Bu s&uuml;re&ccedil; boyunca hastanın nakil merkezine yakın bir yerde ikamet etmesi faydalı olur. Daha sonraki zaman diliminde kontroller giderek seyrekleşir. Bu nedenle de hastanın ulaşım sorunu &ccedil;ok b&uuml;y&uuml;k bir probleme yol a&ccedil;maz.<\\/p>\\r\\n<p>Nakil sonrası kontrollerde hastaya; laboratuvar testleri, g&ouml;ğ&uuml;s filmleri, elektrokardiyogram (EKG), akciğer fonksiyon testleri, akciğer biyopsisi gibi tetkikler yapılabilir. Bu tetkikler ile hastada ameliyata bağlı enfeksiyon gibi sorunlar olup olmadığına bakılır.<\\/p>\\r\\n<p>Diğer doku ve organ nakillerinde olduğu gibi akciğer naklinde de organ reddi riski vardır. Hastalar nakilden sonra organ reddi a&ccedil;ısından sıkı bir bi&ccedil;imde takip edilir. Organ reddi, hastanın bağışıklık sisteminin v&uuml;cuda yeni nakledilen organı yabancı olarak algılayarak saldırması sonucu ger&ccedil;ekleşir. Bu durum ger&ccedil;ekleştiğinde nakledilen organ hasar g&ouml;r&uuml;r ve hastanın sağlığı bozulur.<\\/p>\\r\\n<p>Nakil sonrası hastaların uzun s&uuml;reli bazı uygulamaları yapması gerekir. Bu uygulamalar:<\\/p>\\r\\n<ul>\\r\\n<li><strong>Bağışıklık baskılayıcı ila&ccedil; kullanmak:&nbsp;<\\/strong>Organ reddi durumunun &ouml;nlenmesi i&ccedil;in hastanın &ouml;m&uuml;r boyu bağışıklık baskılayıcı ila&ccedil; kullanması şarttır.<\\/li>\\r\\n<li><strong>Tedavi planına uymak:&nbsp;<\\/strong>Nakilden sonra doktor hastaya ila&ccedil;, terapi ve g&uuml;nl&uuml;k &ouml;nerilerden oluşan tavsiyelerde bulunur. Hastanın bu tavsiyelere uyması ve ila&ccedil;larını d&uuml;zg&uuml;n bir şekilde alması sağlık a&ccedil;ısından olduk&ccedil;a &ouml;nemlidir.<\\/li>\\r\\n<li><strong>Sağlıklı bir yaşam bi&ccedil;imi benimsemek:&nbsp;<\\/strong>Nakledilen akciğerin sağlıklı kalabilmesi i&ccedil;in hastanın yaşamından sigara, alkol gibi maddeleri &ccedil;ıkarması gerekir. Ayrıca hastanın iyileşme s&uuml;reci tamamlandıktan sonra kendisine uygun bir egzersiz programına başlaması da sağlık i&ccedil;in iyidir.<\\/li>\\r\\n<\\/ul>\\r\\n<h2>Akciğer Nakli Riskleri Nelerdir?<\\/h2>\\r\\n<p>Akciğer naklinde ameliyata bağlı olarak kanama, kanın pıhtılaşması, enfeksiyon, hava yollarının kapanması gibi riskler vardır. Bu riskler anestezi altında yapılan ameliyatlarda g&ouml;r&uuml;len ortak risklerdir.<\\/p>\\r\\n<p>Akciğer naklindeki en b&uuml;y&uuml;k risk organ reddidir. Organ reddi, akciğer hastaya nakledildikten sonra hemen ortaya &ccedil;ıkabileceği gibi belirli bir zaman sonra da oluşabilir. Nefes darlığı, kuru &ouml;ks&uuml;r&uuml;k, d&ouml;k&uuml;nt&uuml;, aşırı yorgunluk; organ reddi belirtileri arasında yer alır.<\\/p>\\r\\n<p>Organ reddini &ouml;nlemek i&ccedil;in kullanılan bağışıklık baskılayıcı ila&ccedil;ların da pek &ccedil;ok yan etkisi vardır. Bu yan etkiler şu şekildedir:<\\/p>\\r\\n<ul>\\r\\n<li>Kilo alma<\\/li>\\r\\n<li>Y&uuml;zde kıllanma artışı<\\/li>\\r\\n<li>Mide sorunları<\\/li>\\r\\n<\\/ul>\\r\\n<p>Bağışıklık baskılayıcı ila&ccedil;ların uzun s&uuml;reli kullanımı hastalarda<\\/p>\\r\\n<ul>\\r\\n<li>Diyabet<\\/li>\\r\\n<li>B&ouml;brek hasarı<\\/li>\\r\\n<li>Kemik erimesi<\\/li>\\r\\n<li>Kanser<\\/li>\\r\\n<li>Y&uuml;ksek tansiyon gibi hastalıkların ortaya &ccedil;ıkmasına neden olabilir.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Bağışıklık baskılayıcı ila&ccedil;lar organ reddini &ouml;nlese de bağışıklığı zayıflattığından hastayı enfeksiyona a&ccedil;ık hale getirir. Bu nedenle nakil hastalarının hijyen kurallarına dikkat ederek enfeksiyondan olabildiğince ka&ccedil;ınması faydalı olur.<\\/p>\\r\\n<p>Akciğer nakli, solunumla ilgili ciddi sıkıntısı olan ve diğer tedavi y&ouml;ntemleri ile sağlığına kavuşamayan hastalar i&ccedil;in alternatif bir se&ccedil;enektir. Doktor ve hasta; akciğer naklinin hasta i&ccedil;in doğru bir se&ccedil;enek olup olmadığı, nakil i&ccedil;in bekleme s&uuml;resi, ameliyatın risklerini ve komplikasyonlarını birlikte tartışarak sonuca varmalıdır. Akciğer nakli ile ilgili ayrıntılı bilgi almak i&ccedil;in donanımlı bir sağlık merkezine başvurabilirsiniz.<\\/p>\"}', '', 1, 2, NULL, NULL, NULL, NULL, NULL, '2021-07-03 10:48:39'),
(13, '{\"tr\":\"Asperger Sendromu Nedir? Tanı ve Tedavi Yöntemleri Nelerdir?\"}', 'asperger-sendromu-nedir-tani-ve-tedavi-yontemleri-nelerdir.html', 82, '{\"tr\":\"Asperger Sendromu Nedir? Tanı ve Tedavi Yöntemleri Nelerdir?\"}', '{\"tr\":\"\"}', 'trasperger-sendromu-nedir-tani-ve-tedavi-yontemleri-nelerdir_34.jpg', '{\"tr\":\"<p><strong>Asperger sendromu<\\/strong>; bir grup n&ouml;rolojik bozukluk olan otizm spektrum bozuklukları grubunun bir alt t&uuml;r&uuml;n&uuml; oluşturan sağlık sorunudur. İsmini 1944 yılında hastalığı ilk kez tanımlayan Dr. Hans Asperger&rsquo;den alan bu sendrom, temel olarak insanlarla iletişim kurmada g&uuml;&ccedil;l&uuml;k oluşturmasıyla karakterizedir. Bununla birlikte takıntılı davranış ve d&uuml;ş&uuml;nceler de s&ouml;z konusudur. G&uuml;&ccedil;l&uuml; entelekt&uuml;el yetenekleri ve s&ouml;zl&uuml; dil becerilerinin daha iyi olması ile diğer otizm spektrum bozukluklarından ayrılan Asperger sendromunda bireyler genellikle normal veya &uuml;st&uuml;n zekalıdır.<\\/p>\\r\\n<p>Asperger sendromu tanısı alan &ccedil;ocuklarda eğitim ve gelişim s&uuml;recinin iyi y&ouml;netilmesi ile birlikte &ouml;zel bir eğitim ihtiyacı oluşmaksızın normal sınıflarda eğitim almak, yetişkinlik d&ouml;neminde normal bir sosyal yaşam ve iş hayatı edinmek m&uuml;mk&uuml;nd&uuml;r.<\\/p>\\r\\n<h2>Asperger Nedir?<\\/h2>\\r\\n<p>Asperger sendromu, &ouml;nceleri tek başına bir sendrom olarak tanımlanmış olsa da g&uuml;n&uuml;m&uuml;zde otizm spektrum bozuklukları grubuna dahil edilmiş bir gelişimsel bozukluktur. Fakat halen &ccedil;oğu hekim tarafından Asperger sendromu teşhis olarak kullanılmaktadır, Diğer otizm spektrum bozukluğu t&uuml;rlerinden en &ouml;nemli farkı ise Asperger&rsquo;de entelekt&uuml;el veya dil becerilerine ilişkin bir bozukluğun bulunmamasıdır.<\\/p>\\r\\n<p>Asperger sendromlu &ccedil;ocuk ve gen&ccedil;lerde g&ouml;r&uuml;len temel sorunlara &ouml;rnek olarak sosyal anlamda başkaları ile ilişki ve iletişim kurmakta zorlanma, tekrarlayıcı katı davranış ve d&uuml;ş&uuml;nce kalıpları oluşturma gibi durumlar verilebilir. Bu kişilerde okul ve iş performansı olduk&ccedil;a iyi olabilir, bu ortamlarda kişiler ile sorunsuz bir bi&ccedil;imde konuşabilir. Fakat mimikler, beden dili, mizah ve imalar gibi incelikli iletişim yollarını anlamakta zorlanır. Bir ilgi alanı veya belirli bir konu &uuml;zerinde saatlerce d&uuml;ş&uuml;nmelerine ve konuşmalarına rağmen somut olarak &ccedil;ok k&uuml;&ccedil;&uuml;k bir dizi etkinlik ger&ccedil;ekleştirebilirler. &Ouml;yleki bu ilgi alanları onlar i&ccedil;in bir hobi veya zamanı verimli ge&ccedil;irme yolu olmak yerine takıntılı bir d&uuml;ş&uuml;nce veya g&uuml;nl&uuml;k yaşamı zorlaştıran bir saplantı haline d&ouml;n&uuml;şebilir.<\\/p>\\r\\n<p>Vakaların &ccedil;ok b&uuml;y&uuml;k bir kısmında ilk belirtiler 5-9 yaşlar arasında ortaya &ccedil;ıkar ve teşhis bu aşamada konulur. Erkeklerde kızlara oranla daha yaygın g&ouml;r&uuml;len Asperger sendromu veya genel olarak otizm spektrum bozukluğunda kesin bir tedavi y&ouml;ntemi yoktur. Fakat erken teşhis ve m&uuml;dahale, &ccedil;ocukta sosyal bağlantılar kurma potansiyelinin artması ve sağlıklı, &uuml;retken bir yaşam s&uuml;rmesi olasılığını &ouml;nemli &ouml;l&ccedil;&uuml;de artırır.<\\/p>\\r\\n<h2>Asperger Belirtileri Nelerdir?<\\/h2>\\r\\n<p>Asperger sendromunun belirtileri kişiden kişiye farklılık g&ouml;sterir. Ancak diğer otizmli bireylere g&ouml;re belirli bir ilgi alanına odaklanma sorunu yaygınlık g&ouml;sterir. Bu odaklanma &ccedil;oğu zaman takıntılı bir hal alır. Takıntılı olunacak durum &ccedil;ok geniş bir tanıma sahip olabilir. Bir otob&uuml;s&uuml;n geliş saati, herhangi bir &ccedil;izgi film kahramanı, komşunun &ccedil;ocuğu, sevdiği bir arkadaşı veya ailenin bir bireyi takıntı durumu haline gelebilir. Bu ilgi durumu iletişim esnasında tek taraflı bir konuşma konusu olur. Aspergerli birey iletişim halinde &ccedil;ok farklı bir konudan bahsedilirken ilgi duyduğu konuyu karşısındaki anlatır. Anlatım esnasında konuşulan konu &ccedil;oğu zaman umrunda değildir. B&ouml;yle zamanlarda karşıdaki kişinin konuyu değiştirme &ccedil;abaları &ccedil;oğunlukla başarısız olur. Zaten yapılan araştırmalar asperger sendromlu &ccedil;ocukların iletişimde ve sosyalleşmede sıkıntı &ccedil;ekmelerinin ana nedeninin bu durum olduğunu kanıtlamıştır.<\\/p>\\r\\n<p>Aspergerli bireyler karşılarındaki insanların y&uuml;z ifadelerini ve duygularını anlamakta g&uuml;&ccedil;l&uuml;k &ccedil;ekerler. Bu nedenler karşılarındaki insanın g&ouml;zlerinin i&ccedil;ine bakmaktan ka&ccedil;ınırlar. Ayrıca konuşmaları &ccedil;ok monoton ve duygusuzdur. Buna sınırlı mimik de &ccedil;oğu zaman eşlik eder. Anlattıkları şeyler ile y&uuml;z ifadeleri arasında farklılık vardır. &Ccedil;oğu zaman seslerini ne zaman y&uuml;kseltip ne zaman al&ccedil;altacaklarını ayarlamakta g&uuml;&ccedil;l&uuml;k &ccedil;ekerler. Ayrıca y&uuml;r&uuml;me veya koşma gibi temel motor becerilerde birtakım aksaklıklar olabilir. Bunun sebebi aspergerli &ccedil;ocuklarda koordinasyon sorunu olmasıdır. Koşma ve y&uuml;r&uuml;menin yanı sıra bisiklete binme veya tırmanma gibi basit aktiviteleri yapamayabilirler. Her &ccedil;ocukta b&uuml;t&uuml;n belirtiler olmayacağı gibi bazı &ccedil;ocuklarda belirtilerin bir&ccedil;oğu da g&ouml;r&uuml;lebilir.<\\/p>\\r\\n<h2>Asperger Tanısı Nasıl Konulur?<\\/h2>\\r\\n<p>Asperger tanısı basit bir test ile belirlenebilecek durumlardan değildir. Bir &ccedil;ocuğun asperger sendromuna sahip olup olmadığının anlaşılabilmesi i&ccedil;in birtakım testlerin bir arada yapılması gerekir. Aileler &ccedil;ocuklarında bir sorun olduğunu &ccedil;oğu zaman akranları ile kıyaslama sonucunda anlarlar. &Ccedil;&uuml;nk&uuml; gelişimsel s&uuml;re&ccedil;te gecikmeler mevcuttur ve bunlar yaş ilerledik&ccedil;e bariz bir şekilde ortaya &ccedil;ıkar.<\\/p>\\r\\n<p>Eğer okul &ccedil;ağına kadar bu durum saptanamamış ise &ccedil;ocuğun &ouml;ğretmeni gelişimsel sorunlara dikkat &ccedil;ekebilir ve aileyi bilgilendirebilir. Bu durumların hepsi doktora bildirilmelidir. Gelişimsel sorunların başlıcaları,<\\/p>\\r\\n<ul>\\r\\n<li>Dil gelişimi,<\\/li>\\r\\n<li>Sosyal etkileşim sorunu,<\\/li>\\r\\n<li>Konuşurken y&uuml;z ifadesinde farklılıklar,<\\/li>\\r\\n<li>Başkalarıyla olan etkileşimde sıkıntılar ve zorluklar,<\\/li>\\r\\n<li>Değişime karşı olumsuz ya da farklı tutumlar,<\\/li>\\r\\n<li>Motor kas becerilerinde sorunlar,<\\/li>\\r\\n<li>Koordinasyon problemleri olarak &ouml;ne &ccedil;ıkar.<\\/li>\\r\\n<\\/ul>\\r\\n<p>Bu semptomların bir&ccedil;oğu aslında kimi zaman yanlış değerlendirilir. Asperger sendromuna sahip bir&ccedil;ok &ccedil;ocuğa &ccedil;oğu zaman hiperaktivite veya dikkat eksikliği tanısı konur. Ancak semptomların bir arada değerlendirilmesi ve doğru tanı konulmasında muayene eden doktorun yetkinliği &ccedil;ok &ouml;nemlidir. Aksi takdirde yanlış teşhis konulması yaygın g&ouml;r&uuml;len bir durumdur. T&uuml;m semptomlar doğru şekilde değerlendirildiği takdirde asperger tanısı koyulması m&uuml;mk&uuml;n olur. B&ouml;ylece bu duruma uygun şekilde tedavi s&uuml;reci başlatılır.<\\/p>\\r\\n<h2>Asperger Tedavi Y&ouml;ntemleri<\\/h2>\\r\\n<p>Asperger tedavi y&ouml;ntemleri maalesef olduk&ccedil;a kısıtlıdır hatta belirli bir tedavi y&ouml;ntemi yoktur denilebilir. &Ccedil;ocuğun potansiyeline ulaşmasına yardımcı olabilecek ve semptomları azaltacak birtakım y&ouml;ntemler mevcuttur. Bu y&ouml;ntemler semptomlardan yola &ccedil;ıkılarak uygulanır. Asperger sendromlu &ccedil;ocuğun geliştirilmesi gereken becerilerine odaklanılarak o y&ouml;nlerinin tedavi edilmesi ama&ccedil;lanır.<\\/p>\\r\\n<p>Tabii ki tedavi amacıyla kullanılan birtakım ila&ccedil;lar da mevcuttur. Aspergerli bireylerde sinirli olma durumu yaygın g&ouml;r&uuml;l&uuml;r. Bunun tedavisi amacıyla siniri azaltmak i&ccedil;in birtakım ila&ccedil;lar re&ccedil;ete edilir. Hiperaktivite de aspergerli &ccedil;ocuklarda g&ouml;r&uuml;lebilen semptomlar arasındadır. Bunun da &ouml;n&uuml;ne ge&ccedil;ilebilmesi amacıyla guanfasin veya olanzapin etken maddeli ila&ccedil;lara başvurulabilir. Yine uyku sorununun &ouml;n&uuml;ne ge&ccedil;ilebilmesi amacıyla uyku ila&ccedil;ları da re&ccedil;ete edilebilir. T&uuml;m ila&ccedil;lar semptomlara g&ouml;re re&ccedil;ete edileceği i&ccedil;in kesinlikle re&ccedil;ete edilmeyen ila&ccedil;ların kullanılmaması gerekir.<\\/p>\\r\\n<p>Esas tedavi y&ouml;ntemleri sosyal, iletişim ve duygusal beceriler &uuml;zerinde uygulanır. Bu becerilerin tedavisi hususunda ila&ccedil; tedavisi m&uuml;mk&uuml;n değildir. Ancak sosyal becerilerin geliştirilebilmesi amacıyla sosyal beceri eğitimleri uygulanır. Konuşma zorluklarının aşılabilmesi i&ccedil;in konuşma ve dil terapileri gerekir. Koordinasyon eksiklikleri ile motor kas sisteminin geliştirilmesi amacıyla fizik tedavi uygulanması gerekebilir. Asperger sendromlu &ccedil;ocuğun zihinsel gelişimi a&ccedil;ısından bilişsel davranış&ccedil;ı terapilere ihtiya&ccedil; vardır. Bu eğitimlerin tamamının uzmanlar tarafından sağlanması m&uuml;mk&uuml;n olmayabilir. Bu nedenle ailelerin de birtakım terapilerle birlikte &ccedil;ocuklarına nasıl davranacaklarını &ouml;ğrenmeleri gerekir. &Ccedil;&uuml;nk&uuml; en etkili eğitim ve tedavi aile ile birlikte y&uuml;r&uuml;t&uuml;ld&uuml;ğ&uuml;nde ger&ccedil;ekleşir. Ayrıca aileler de bu s&uuml;re&ccedil;te &ccedil;ok fazla yıpranırlar ve psikolojik olarak zorlu bir durumda kalırlar. Bunun &ouml;n&uuml;ne ge&ccedil;ilebilmesi amacıyla ailelerin de psikolojik destek almaları gerekebilir.<\\/p>\\r\\n<p>Teşhisin erken yaşlarda yapılması sağlıklı bir birey yetiştirmek i&ccedil;in olduk&ccedil;a &ouml;nemlidir. &Ccedil;&uuml;nk&uuml; ailesinden ve uzmanlardan gerekli tedaviyi alan aspergerli birey normal bir şekilde hayatını s&uuml;rd&uuml;rmeyi &ouml;ğrenebilir. B&ouml;ylece ilerleyen yaşlarında bağımsız olarak yaşayabilir. Bunun i&ccedil;in erken teşhis ve etkili tedavi olduk&ccedil;a &ouml;nemlidir.<\\/p>\\r\\n<p>Siz de &ccedil;ocuğunuzda Asperger sendromu veya otizm spektrum bozukluklarına ilişkin belirtiler g&ouml;zlemliyorsanız bir hekime danışarak &ccedil;ocuğunuzun muayeneden ge&ccedil;mesini sağlayabilirsiniz. Herhangi bir psikiyatrik veya n&ouml;rolojik bozukluk teşhisi alması halinde erken teşhis ve tedavi s&uuml;reci ile &ccedil;ocuğunuzun sağlıklı bir yetişkinlik d&ouml;nemine adım atmasını sağlayabilirsiniz.<\\/p>\"}', '', 1, 2, NULL, NULL, NULL, NULL, NULL, '2021-07-03 12:22:41');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `slider2`
--

CREATE TABLE `slider2` (
  `slider2_id` int(11) NOT NULL,
  `slider2_name` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `slider2_t1` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `slider2_t2` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `slider2_link` varchar(240) COLLATE utf8_turkish_ci NOT NULL,
  `slider2_resim` varchar(321) COLLATE utf8_turkish_ci NOT NULL,
  `slider2_sira` int(9) DEFAULT NULL,
  `slider2_insert_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `slider2`
--

INSERT INTO `slider2` (`slider2_id`, `slider2_name`, `slider2_t1`, `slider2_t2`, `slider2_link`, `slider2_resim`, `slider2_sira`, `slider2_insert_tarih`) VALUES
(10, '{\"tr\":\"Sina Klinik\"}', '{\"tr\":\"Açılış Yazısı\"}', '{\"tr\":\"One Minute Dostum\"}', 'link', 'trsina-klinik_47.png', NULL, '2020-11-11 16:55:06'),
(13, '{\"tr\":\"Sina Klinik\"}', '{\"tr\":\"Açılış Yazısı\"}', '{\"tr\":\"One Minute Dostum\"}', 'link', 'trsina-klinik_47.png', NULL, '2020-11-11 16:55:06');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sube`
--

CREATE TABLE `sube` (
  `sube_id` int(11) NOT NULL,
  `sube_name` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `sube_seo_name` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `sube_telefon` varchar(240) COLLATE utf8_turkish_ci NOT NULL,
  `sube_fax` varchar(240) COLLATE utf8_turkish_ci NOT NULL,
  `sube_image` varchar(321) COLLATE utf8_turkish_ci NOT NULL,
  `sube_adres` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `sube_maps` varchar(1010) COLLATE utf8_turkish_ci NOT NULL,
  `sube_content` text COLLATE utf8_turkish_ci NOT NULL,
  `sube_insert_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `sube`
--

INSERT INTO `sube` (`sube_id`, `sube_name`, `sube_seo_name`, `sube_telefon`, `sube_fax`, `sube_image`, `sube_adres`, `sube_maps`, `sube_content`, `sube_insert_tarih`) VALUES
(3, '{\"tr\":\"Alibey Hospital\"}', 'alibey-hospital.html', '0212 627 79 79', '0212 627 79 79', 'tralibey-hospital_92.png', '{\"tr\":\"Karadolap mahallesi neşeli sokak numara:22 Eyüp İstanbul \"}', '{\"tr\":\"        <iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m14!1m8!1m3!1d12030.175332169238!2d28.9273154!3d41.0789629!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x63d3ff54300d7a5f!2sAlibey%20Hospital!5e0!3m2!1str!2str!4v1624342445019!5m2!1str!2str\\\" width=\\\"450\\\" height=\\\"300\\\" style=\\\"border:0;\\\" allowfullscreen=\\\"\\\" loading=\\\"lazy\\\"><\\/iframe>                            \"}', '{\"tr\":\"<p class=\\\"jenerik\\\">&Ouml;zel Alibey Hospital hastanesi 1989 yılında &Ccedil;etinler Polikliniği olarak sağlık hizmetine başlamış olup,2007 yılında &Ccedil;etinler Cerrahi Tıp Merkezi, 2011 yılında ise Alibey Hospital hastanesine d&ouml;n&uuml;şerek gelişimini s&uuml;rd&uuml;rm&uuml;şt&uuml;r.<\\/p>\\r\\n<p>İstanbul Avrupa Yakasında , uluslararası &nbsp;standart &nbsp;ve kalitede&nbsp; hizmet veren &nbsp;&nbsp;Alibey &nbsp;Hospital hastanesi olarak; 18 uzman doktor ,20 sağlık personeli,3 Kadın Doğum Uzmanı,3 Diş Hekimi,2 Genel Cerrahi Uzmanı,1 Dahiliye ,1 Ortopedi ve travmatoloji,1 cildiye,1 &Ccedil;ocuk,1 N&ouml;roloji,1 &Uuml;roloji,1 G&ouml;z, 1 KBB,Anestezi ve Reanimasyon Uzmanı1 Estetisyen,1 Diyetisyen ,3 Genel Yoğun Bakım,5 Yeni Doğan Bakımı olmak &uuml;zere&nbsp; 31&nbsp; yatak kapasitesi ile geniş&nbsp; bir teknoloji &nbsp;imkanına sahiptir.<\\/p>\\r\\n<p><strong>VİZYON<\\/strong><\\/p>\\r\\n<p>&Ouml;zel &nbsp;Alibey &nbsp;Hospital Hastanesi, &uuml;lkemizin en g&uuml;venilir sağlık kuruluşlarından biri olmayı hedeflemektedir. &nbsp;<\\/p>\\r\\n<p><strong>MİSYON<\\/strong><\\/p>\\r\\n<p>&Ouml;zel Alibey &nbsp;Hospital Hastanesi t&uuml;m &ccedil;alışanlarıyla,uluslararası standartlarda verdiği hizmetle zirveyi hedefler, hasta mahremiyetini korur, g&uuml;venliğini &nbsp;sağlar,memnuniyetini kazanır,insana saygı duyar ve ahl&acirc;kı esas alır.<\\/p>\"}', '2021-06-04 09:59:25'),
(4, '{\"tr\":\"Pendik Şube\"}', 'sina-clinic.html', '5425334805', '5425334805', 'trpendik-sube_64.jpg', '{\"tr\":\"adres\"}', '{\"tr\":\"                maps        \"}', '{\"tr\":\"<p>i&ccedil;erik<\\/p>\"}', '2021-06-04 10:09:55'),
(5, '{\"tr\":\"Avrasya Şube\"}', 'avrasya-sube.html', 'Telefon', 'Faks', 'travrasya-sube_15.jpg', '{\"tr\":\"Adres\"}', '{\"tr\":\"                Maps        \"}', '{\"tr\":\"<p>İ&ccedil;erik<\\/p>\"}', '2021-06-06 11:18:04');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `subebolum`
--

CREATE TABLE `subebolum` (
  `subebolum_id` int(11) NOT NULL,
  `sube_id` int(11) NOT NULL,
  `bolum_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `subebolum`
--

INSERT INTO `subebolum` (`subebolum_id`, `sube_id`, `bolum_id`) VALUES
(89, 3, 1),
(91, 4, 1),
(92, 5, 1),
(94, 3, 2),
(95, 3, 3),
(96, 5, 2);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `theme`
--

CREATE TABLE `theme` (
  `theme_id` int(11) NOT NULL,
  `theme_name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `theme_image` varchar(321) COLLATE utf8_turkish_ci NOT NULL,
  `theme_folder` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `theme_json` varchar(500) COLLATE utf8_turkish_ci NOT NULL,
  `theme_durum` bigint(11) NOT NULL,
  `theme_insert_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `theme`
--

INSERT INTO `theme` (`theme_id`, `theme_name`, `theme_image`, `theme_folder`, `theme_json`, `theme_durum`, `theme_insert_tarih`) VALUES
(1, 'Sina Klinik', 'basic_74.jpg', 'sinaweb', 'e', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) COLLATE utf8_turkish_ci DEFAULT NULL,
  `user_surname` varchar(50) COLLATE utf8_turkish_ci DEFAULT NULL,
  `user_phone` varchar(50) COLLATE utf8_turkish_ci DEFAULT NULL,
  `user_mail` varchar(50) COLLATE utf8_turkish_ci DEFAULT NULL,
  `user_insert_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_surname`, `user_phone`, `user_mail`, `user_insert_date`) VALUES
(1, 'Süleyman', 'Efe', '5425334805', 'suleyman.efe@live.com', NULL),
(2, 'Süleyman2', 'Efe', '5425334805', 'suleyman.efe@live.com', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `video`
--

CREATE TABLE `video` (
  `video_id` int(11) NOT NULL,
  `video_name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `video_desc` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `video_tags` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `video_page_id` int(11) NOT NULL,
  `video_file` varchar(321) COLLATE utf8_turkish_ci DEFAULT NULL,
  `video_kod` varchar(600) COLLATE utf8_turkish_ci DEFAULT NULL,
  `video_content` text COLLATE utf8_turkish_ci NOT NULL,
  `video_durum` bigint(11) NOT NULL,
  `video_insert_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`Admin_id`);

--
-- Tablo için indeksler `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`alerts_id`);

--
-- Tablo için indeksler `bolum`
--
ALTER TABLE `bolum`
  ADD PRIMARY KEY (`bolum_id`);

--
-- Tablo için indeksler `diller`
--
ALTER TABLE `diller`
  ADD PRIMARY KEY (`diller_id`);

--
-- Tablo için indeksler `doktor`
--
ALTER TABLE `doktor`
  ADD PRIMARY KEY (`doktor_id`);

--
-- Tablo için indeksler `doktormap`
--
ALTER TABLE `doktormap`
  ADD PRIMARY KEY (`doktormap_id`);

--
-- Tablo için indeksler `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`galeri_id`);

--
-- Tablo için indeksler `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`General_Set_id`);

--
-- Tablo için indeksler `nav`
--
ALTER TABLE `nav`
  ADD PRIMARY KEY (`nav_id`);

--
-- Tablo için indeksler `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Tablo için indeksler `slider2`
--
ALTER TABLE `slider2`
  ADD PRIMARY KEY (`slider2_id`);

--
-- Tablo için indeksler `sube`
--
ALTER TABLE `sube`
  ADD PRIMARY KEY (`sube_id`);

--
-- Tablo için indeksler `subebolum`
--
ALTER TABLE `subebolum`
  ADD PRIMARY KEY (`subebolum_id`);

--
-- Tablo için indeksler `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Tablo için indeksler `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Tablo için indeksler `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`video_id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `admin`
--
ALTER TABLE `admin`
  MODIFY `Admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `alerts`
--
ALTER TABLE `alerts`
  MODIFY `alerts_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `bolum`
--
ALTER TABLE `bolum`
  MODIFY `bolum_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `diller`
--
ALTER TABLE `diller`
  MODIFY `diller_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `doktor`
--
ALTER TABLE `doktor`
  MODIFY `doktor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Tablo için AUTO_INCREMENT değeri `doktormap`
--
ALTER TABLE `doktormap`
  MODIFY `doktormap_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Tablo için AUTO_INCREMENT değeri `galeri`
--
ALTER TABLE `galeri`
  MODIFY `galeri_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Tablo için AUTO_INCREMENT değeri `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `General_Set_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `nav`
--
ALTER TABLE `nav`
  MODIFY `nav_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- Tablo için AUTO_INCREMENT değeri `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Tablo için AUTO_INCREMENT değeri `slider2`
--
ALTER TABLE `slider2`
  MODIFY `slider2_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Tablo için AUTO_INCREMENT değeri `sube`
--
ALTER TABLE `sube`
  MODIFY `sube_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Tablo için AUTO_INCREMENT değeri `subebolum`
--
ALTER TABLE `subebolum`
  MODIFY `subebolum_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- Tablo için AUTO_INCREMENT değeri `theme`
--
ALTER TABLE `theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `video`
--
ALTER TABLE `video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
