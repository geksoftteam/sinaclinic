<?php
ob_start();
ini_set('display_errors','on');
define("ROOT", __DIR__);
include_once 'app/config/config.php';
include_once 'inc/libs/Helper.php';
include_once 'inc/libs/Mobile_Detect.php';

$link = $_SERVER['REQUEST_URI'];
$link = trim($_SERVER['REQUEST_URI'],'/');


$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);

if (!isset($_SESSION)) {
	session_start();
}

if(!isset($_SESSION['dil'])){

	if($lang == 'tr'){
		$_SESSION['dil'] = 'tr';
	}else{
		$_SESSION['dil'] = 'tr';
	}
}

/*

$protocol = (@$_SERVER["HTTPS"] == "on") ? "https://" : "https://";

if (substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.') {
    header('Location: '.$protocol.'www.'.$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']);
    exit;
}
 */




spl_autoload_register(function ($class) {

    // project-specific namespace prefix
    $prefix = 'App\\';

    // base directory for the namespace prefix
    $base_dir = __DIR__ . '/app/';

    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class, $len);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});

spl_autoload_register(function ($class) {
    include_once 'inc/libs/' . $class . '.php';
});


$boot = new Bootstrap();
