

/*-------------------nav arama baslıyor------------------*/
$('#listarama').keyup(function() {
	if($(this).val().length>2){
		//$('.search-list ul').slideDown();
		filter(this); }else{
			$('.search-list').show();
			$(".search-list ul li").show();
			$('.search-list ul').show();
			$('.left_nav_active').slideDown();

		}
	});

function filter(element) {
	var value = $(element).val().toLowerCase();
	var $li = $(".search-list");
	var $liul = $(".search-list ul li");

	$li.hide();
	$liul.hide();
	$li.filter(function() {
		return $(this).text().toLowerCase().indexOf(value) > -1;
	}).show();
	$liul.filter(function() {
		return $(this).text().toLowerCase().indexOf(value) > -1;
	}).show();
	
	
}


$('.m_list a').click(function() {


	$.cookie("menu_cur", "Dashboard");
	$.cookie("menu_a", "Admin/Dashboard");

	$.cookie("parent_cur_id", "0");
	$.cookie("parent_cur_label", "0");


});



$('.search-list a').click(function() {

	parentval = $(this).data("id");
	parentlabel = $("#"+parentval).children("a").children(".title").html();

	vala = $(this).attr('href');


	valsi = $(this).html();
	vala = $(this).attr('href');
	$.cookie("menu_cur", valsi);
	$.cookie("menu_a", vala);

	$.cookie("parent_cur_id", parentval);
	$.cookie("parent_cur_label", parentlabel);


});

$(document).ready(function() {
	if($.cookie("menu_cur") != null){

		
		

		$(".menu_sub").html($.cookie("menu_cur")); 
		$(".breadcrumb .active").html($.cookie("menu_cur")); 
		valsa = $.cookie("menu_a");
		$("a[href$='"+valsa+"']").css("color","#FD8400");
		if($.cookie("parent_cur_label") != 0){
			$(".Page_Name").html($.cookie("parent_cur_label"));
		}else{
			$(".pgyy").css("display","none")
		}
		var li_id = $.cookie("parent_cur_id");

		$("#"+li_id).addClass("open star");
		
		$("#"+li_id).children("ul").slideDown();


				// alert(valsa);

			}else{

				$(".menu_sub").html(""); 
				$(".breadcrumb .active").html(""); 

			}
		});

/*-------------nav arama end------------------*/





/*-------------Temel Fonksiyonlar------------------*/	


$('.tipsi').tooltip();
$(".tip_top").tooltip({placement : 'top'});
$(".tip_right").tooltip({placement : 'right'});
$(".tip_bottom").tooltip({placement : 'bottom'});
$(".tip_left").tooltip({ placement : 'left'});

function sole(q){
	$.growl({

		message: q
	});
}

function modalyap(h,p,x){

	$('#myModal').modal({
		backdrop: 'static'
	})
	$('#modalkapat').css('display',x);
	$('.mymodalinh4u').html(h);
	$('.mymodalinbodisi').html(p);
}

function modalyapbig(h,p,x){

	$('#bigModal').modal({
		backdrop: 'static'
	})
	$('#modalkapatbig').css('display',x);
	$('.mymodalinh4ubig').html(h);
	$('.mymodalinbodisibig').html(p);
}

function modalkapat(p){
	$('#'+p).modal("hide");
}

///////////////sayma ve gosterme fonksiyonu---------------

$(".say").keyup(function() {
	$(this).css("resize","none");
	sayi = $(this).val().length;
	limit = $(this).data("say");
	helpblock_id = $(this).attr("name")+"_say";
	if(sayi>limit){
		$("#"+helpblock_id).css("color","#F00");
		$("#"+helpblock_id).html("Karakter Sayısı : <b>"+sayi+"</b> Limiti Aştınız");
	}else{
		$("#"+helpblock_id).css("color","#000");
		$("#"+helpblock_id).html("Karakter Sayısı : <b>"+sayi+"</b>");
	}
	
	
});

/////////////////resim on izleme-----------------------

function resim_on_izle(input,id) {

	boyut = input.files[0].size;
	boyut = boyut/1024/1024;

	if(boyut > 4){alert(boyut.toFixed(2)+' MB Web Platformları için büyüktür.');}
	
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#onizle'+id).attr('src', e.target.result);
			$('#onizle'+id).css('display',"block");
		}

		reader.readAsDataURL(input.files[0]);
	}
}





	/// forn kontrol ve submit
	
	
	$(".submit_btn").click(function() {
		
		data_id  = $(this).data('id');
		$site_url = $("#site_url").val();

		$('#'+data_id+' .zor').each(function(index, element) {

			var degert = $(this).val();

			if(degert ==''){

				$(this).css('border-color','red');

				$(this).focus();

				die();
			} else {

				$(this).css('border-color','green');

			}
		});


		$(this).attr("disabled","disabled");
		
		$(this).css("opacity","0.6");

		$(this).html("Bekleyiniz");

		modalyap('Bilgiler Kontrol Ediliyor','<img src="'+$site_url+'public/admin/media/loading42.gif" style="width:168px; height:40px; margin:5px auto;">','none');


		setTimeout(function(){ $('#'+data_id).submit(); }, 1000);

		
	});

	function ucfirst(str){
		var text = str;


		var parts = text.split(' '),
		len = parts.length,
		i, words = [];
		for (i = 0; i < len; i++) {
			var part = parts[i];
			var first = part[0].toUpperCase();
			var rest = part.substring(1, part.length);
			var word = first + rest;
			words.push(word);

		}

		return words.join(' ');
	}

	function alertkapat(id) {

		$site_url = $("#site_url").val();

		$.post( $site_url+"Admin/Alerts/kapat/"+id, { id: id })
		.done(function(data) {

			if(data>=0 ){
				$(".alertblocksu"+id).fadeOut();
				$('.alertcount').html(data);

				if (data==0) {

					$('#notification-list').slideUp();
					$('#alertcounts').hide();

				};



			}else{
				sole("Bir Hata Oluştu Tekrar Deneyiniz");  
			}


		});


	} 