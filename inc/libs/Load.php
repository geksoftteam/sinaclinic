<?php

class Load {
	public function __construct() {

	}

	public function tview($fileName, $data = false) {
		if ($data == true) {
			extract($data);
		}

		include "app/views/Admin/" . $fileName . "_hm.php";

	}

	public function view($fileName, $data = false) {

		if (!isset($_SESSION)) {
			session_start();
		}

		$model = $this->admin_model("Theme_Model");

		$theme_model = $model->current_theme();

		$set = $model->SettingsAll();

		if (isset($_SESSION['dil']) && !empty($_SESSION['dil'])) {
			$dil_kodu = $_SESSION['dil'];
		} else {
			$dil_kodu = 'tr';

		}
		$dil_set = $model->SettingsLanguage($dil_kodu);

		$theme_folder = $theme_model['theme_folder'];

		$theme_patch = SITE_URL . "app/views/Front/" . $theme_folder;

		$data['theme_patch'] = $theme_patch;

		$data['set'] = $set;
		$data['dil_set'] = json_decode($dil_set['dil_string']);
		$data['lang'] = $dil_set['diller_kod'];
		$data['lang_name'] = $dil_set['diller_name'];

		if ($data == true) {

			extract($data);

		}
		
		include "app/views/Front/" . $theme_folder . '/' . $fileName . ".php";
	}

	public function admin_view($fileName, $data = false) {

		$model = $this->admin_model("Alerts_Model");
		$data['uyari'] = $model->listele_active();
		$data["maindiller"] = $model->Diller_listele();
		if ($data == true) {
			extract($data);
		}

		include "app/views/Admin/header.php";
		include "app/views/Admin/" . $fileName . "_hm.php";
		include "app/views/Admin/footer.php";

	}

	public function model($fileName) {
		include "app/models/" . $fileName . ".php";
		return new $fileName();
	}

	public function admin_model($fileName) {
		include "app/models/Admin/" . $fileName . ".php";
		return new $fileName();
	}

	public function helper($fileName) {
		include "app/helper/" . $fileName . ".php";
		return new $fileName();
	}

	public function inc($fileName) {
		include "app/helper/" . $fileName . ".php";

	}

}
