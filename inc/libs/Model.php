<?php

class Model{

    protected $db = array();

    
    public function __construct() {
        try
        {
            $this->db = new Database(DB_HOST, DB_NAME, DB_USER_NAME, DB_PASSWORD);
        }
        catch (PDOException $e)
        {
            die('Veritabanı Bağlantısı Kurulamadı');
        }
    }


    public function nav() {

        $main = $this->db->select('nav')->where('nav_main', 0)->where("nav_durum", 1)->orderby('nav_sira', 'asc')->run();
        $dizi = array();
        foreach ($main as $li) {
            $indis = $li['nav_id'];
            $dizi[$indis] = $li;
            $sub_nav = $this->db->select('nav')->where('nav_main', $indis)->where("nav_durum", 1)->orderby('nav_sira', 'asc')->run();
            if (count($sub_nav) > 0) {
                $dizi[$indis]['sub'] = $sub_nav;
                $dizi[$indis]['sub_type'] = 'menu';
                $sub_nav_sub = $this->db->select('nav')->where('nav_main', $dizi[$indis]['sub'][0]['nav_id'])->where("nav_durum", 1)->orderby('nav_sira', 'asc')->run();
                if (count($sub_nav_sub) > 0) {
                    $dizi[$indis]['sub'][0]['sub_nav'] = $this->db->select('nav')->where('nav_main', $dizi[$indis]['sub'][0]['nav_id'])->where("nav_durum", 1)->orderby('nav_sira', 'asc')->run();
                    $dizi[$indis]['sub'][0]['sub_nav'][0]['sub_type'] = 'menu';
                }
            } else {
                $dizi[$indis]['sub'] = $this->db->select('page')
                ->join('nav', '%s.nav_id = %s.page_kat_id', 'left')
                ->where('page_kat_id', $indis)
                ->where("page_durum", 1)
                ->where("page_konum", 1)
                ->orderby('page_sira', 'asc')
                ->run();
                $dizi[$indis]['sub_type'] = 'content';
            }
        }
        return $dizi;
    }



}
